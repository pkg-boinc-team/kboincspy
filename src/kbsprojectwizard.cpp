/***************************************************************************
 *   Copyright (C) 2006 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlabel.h>
#include <qlayout.h>
#include <qradiobutton.h>
#include <qvalidator.h>
#include <qtextbrowser.h>
#include <qwidgetstack.h>

#include <kapplication.h>
#include <kiconloader.h>
#include <klineedit.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kpassdlg.h>
#include <kpushbutton.h>
#include <kstandarddirs.h>

#include <kbsboincdata.h>
#include <kbsboincmonitor.h>
#include <kbsrpcmonitor.h>

#include "kbsprojectwizard.h"

KBSProjectWizard::KBSProjectWizard(KBSBOINCMonitor *monitor, QWidget *parent, const char *name)
                : KDialog(parent, name), m_config(NULL), m_monitor(monitor)
{
  setupMonitor();
  setupView();
}

KBSProjectWizard::~KBSProjectWizard()
{
  if(NULL != m_config) delete m_config;
}

KBSBOINCMonitor *KBSProjectWizard::monitor() const
{
  return m_monitor;
}

int KBSProjectWizard::previous(int page) const
{
  switch(page) {
    case ProjectURL:
      return Welcome;
    case Account:
    case AccountKey:
      return ProjectURL;
    case ConnectError:
    case NetworkError:
    case AuthError:
    case DuplicateError:
    case GenericError:
      return m_legacy ? AccountKey
           : (NULL == m_config) ? ProjectURL
           : m_config->client_account_creation_disabled ? AccountKey
           : Account;
    default:
      return NoPage;
  }
}

int KBSProjectWizard::next(int page) const
{
  switch(page) {
    case Welcome:
      return ProjectURL;
    case ProjectURL:
      return !isValid(ProjectURL) ? NoPage
           : m_legacy ? AccountKey
           : ConnectHost;
    case Account:
      return isValid(Account) ? ConnectHost : NoPage;
    case AccountKey:
      return isValid(AccountKey) ? ConnectHost : NoPage;
    default:
      return NoPage;
  }
}

bool KBSProjectWizard::isTerminal(int page) const
{
  return page >= Success;
}

void KBSProjectWizard::reject()
{
  if(!isTerminal(m_page))
  {
    const QString warning = i18n("Do you really want to cancel?");
    if(KMessageBox::warningYesNo(this, warning) != KMessageBox::Yes)
      return;
  }
  
  KDialog::reject();
}

void KBSProjectWizard::setupMonitor()
{
  m_legacy = true;
  m_config = NULL;
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL != state) m_legacy = (state->core_client.major_version < 5);
  
  KBSRPCMonitor *rpcMonitor = m_monitor->rpcMonitor();
  if(NULL == rpcMonitor) return;
  
  connect(rpcMonitor, SIGNAL(output(const KBSBOINCProjectConfig &)),
          this, SLOT(input(const KBSBOINCProjectConfig &)));
  connect(rpcMonitor, SIGNAL(output(const QString &, const QString &)),
          this, SLOT(input(const QString &, const QString &)));
  connect(rpcMonitor, SIGNAL(error(const QString &, int, QStringList)),
          this, SLOT(error(const QString &, int, QStringList)));
}

void KBSProjectWizard::setupView()
{
  (new QVBoxLayout(this))->setAutoAdd(true);
  m_view = new KBSProjectWizardContent(this);
  
  setCaption(m_view->caption());
  setMaximumSize(0, 0);
  
  m_view->sidebar->setPixmap(UserIcon("sidebar"));
  m_view->account_key->setValidator(new QRegExpValidator(QRegExp("[0-9a-f]{32}"), this));
  
  m_view->connect_host_movie->setPixmap(UserIcon("connect_host"));
  m_view->connect_network_movie->setPixmap(UserIcon("connect_network"));
  
  connect(m_view->help, SIGNAL(clicked()), this, SLOT(help()));
  connect(m_view->previous, SIGNAL(clicked()), this, SLOT(previous()));
  connect(m_view->next, SIGNAL(clicked()), this, SLOT(next()));
  connect(m_view->finish, SIGNAL(clicked()), this, SLOT(reject()));
  
  connect(m_view->project_url, SIGNAL(textChanged(const QString &)),
          this, SLOT(validateText(const QString &)));
  connect(m_view->username, SIGNAL(textChanged(const QString &)),
          this, SLOT(validateText(const QString &)));
  connect(m_view->password, SIGNAL(textChanged(const QString &)),
          this, SLOT(validateText(const QString &)));
  connect(m_view->confirm_password, SIGNAL(textChanged(const QString &)),
          this, SLOT(validateText(const QString &)));
  connect(m_view->account_key, SIGNAL(textChanged(const QString &)),
          this, SLOT(validateText(const QString &)));
  
  show(Welcome);
}

void KBSProjectWizard::show(int page)
{
  m_page = page;
  if(NoPage == m_page) return;
  
  m_view->previous->setEnabled(previous(m_page) != NoPage);
  m_view->next->setEnabled(next(m_page) != NoPage);
  m_view->finish->setText(isTerminal(m_page) ? i18n("&Finish") : i18n("&Cancel"));
  
  switch(m_page) {
    case ProjectURL:
      if(NULL != m_config) { delete m_config; m_config = NULL; }
      validateText(m_view->project_url->text());
      break;
    case Account:
      validateText(m_view->username->text().stripWhiteSpace());
      break;
    case AccountKey:
      validateText(m_view->account_key->text());
      break;
    default:
      break;
  }
  
  m_view->pages->raiseWidget(m_page);
}

bool KBSProjectWizard::isValid(int page) const
{
  return m_valid.contains(page) ? m_valid[page] : false;
}

QString KBSProjectWizard::project(const QString &projectName) const
{
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return QString::null;
  
  for(QMap<QString,KBSBOINCProject>::const_iterator project = state->project.begin();
      project != state->project.end(); ++project)
    if((*project).project_name == projectName)
      return project.key();
  
  return QString::null;
}

void KBSProjectWizard::input(const KBSBOINCProjectConfig &config)
{
  qDebug("input config");
  
  if(NULL != m_config) { delete m_config; m_config = NULL; }
  
  if(project(config.name).isNull()) {
    m_config = new KBSBOINCProjectConfig(config);
    show(m_config->client_account_creation_disabled ? AccountKey : Account);
  } else
    show(DuplicateError);
  
  if(m_config->account_creation_disabled) {
    m_view->create_account->setEnabled(false);
    m_view->use_account->setChecked(true);
  } else {
    m_view->create_account->setEnabled(true);
    m_view->create_account->setChecked(true);
  }
  if(m_config->uses_username)
    m_view->username_label->setText(i18n("Login:"));
  else
    m_view->username_label->setText(i18n("EMail:"));
}

void KBSProjectWizard::input(const QString &, const QString &account_key)
{
  qDebug("input account key");
  
  KURL url = KURL(m_view->project_url->text());
  m_monitor->rpcMonitor()->attachProject(url, account_key);
}

void KBSProjectWizard::error(const QString &tag, int num, QStringList messages)
{
  // ignore errors on polling commands for clients < 5.x.x
  if(m_legacy && tag.endsWith("_poll")) return;
  
  if(tag == "get_project_config_poll") {
    m_monitor->rpcMonitor()->lookupWebsite("google");
    show(ConnectNetwork);
  } else if(tag == "lookup_website_poll")
    show(0 == num ? ConnectError : NetworkError);
  else if(tag == "lookup_account_poll")
    switch(num) {
      case -161:
      case -205:
      case -206:
      case -207:
        show(AuthError);
        break;
      default:
        m_view->error_messages->setText(messages.join("\n"));
        show(GenericError);
        break;
    }
  else if(tag == "project_attach")
  {
    if(num < 0) {
      m_view->error_messages->setText(messages.join("\n"));
      show(GenericError);
    } else if(m_legacy)
      show(Success);
  }
  else if(tag == "project_attach_poll")
    switch(num) {
      case 0:
        m_monitor->checkFiles();
        show(Success);
        break;
      case -212:
        show(AuthError);
        break;
      default:
        m_view->error_messages->setText(messages.join("\n"));
        show(GenericError);
        break;
    }
  
  qDebug("RPC call %s returned error %d", tag.latin1(), num);
}

void KBSProjectWizard::help()
{
  kapp->invokeHelp("attach_project");
}

void KBSProjectWizard::previous()
{
  show(previous(m_page));
}

void KBSProjectWizard::next()
{
  switch(m_page) {
    case ProjectURL:
      if(!m_legacy)
      {
        const KURL project_url(m_view->project_url->text());
        
        m_monitor->rpcMonitor()->getProjectConfig(project_url);
      }
      break;
    case Account:
      {
        const KURL project_url(m_view->project_url->text());
        bool createAccount = m_view->create_account->isChecked();
        const QString email(m_config->uses_username ? "" : m_view->username->text().stripWhiteSpace()),
                      username(m_config->uses_username ? m_view->username->text().stripWhiteSpace() : ""),
                      password(m_view->password->password());
        
        if(createAccount)
          m_monitor->rpcMonitor()->createAccount(project_url, email, username, password);
        else
          m_monitor->rpcMonitor()->lookupAccount(project_url, email, username, password);
      }
      break;
    case AccountKey:
      {
        const KURL url(m_view->project_url->text());
        const QString account_key(m_view->account_key->text());
        
        m_monitor->rpcMonitor()->attachProject(url, account_key);
      }
      break;
    default:
      break;
  }
  
  show(next(m_page));
}

void KBSProjectWizard::validateText(const QString &value)
{
  switch(m_page) {
    case ProjectURL:
      {
        const KURL url(value);
        
        m_valid[m_page] = url.isValid() && !url.host().isEmpty();
      }
      break;
    case Account:
      {
        bool createAccount = m_view->create_account->isChecked(),
             validPasswords = false;
        const QString username = m_view->username->text(),
                      password = m_view->password->password(),
                      confirmPassword = m_view->confirm_password->password();
        
        if(password.isEmpty())
          m_view->remark->clear();
        else if(password.length() < m_config->min_passwd_length)
          m_view->remark->setText(i18n("Password is too short."));
        else if(createAccount && (password != confirmPassword))
          m_view->remark->setText(i18n("Passwords don't match."));
        else {
          m_view->remark->clear();
          validPasswords = true;
        }
        
        m_valid[m_page] = !username.isEmpty() && validPasswords;
      }
      break;
    case AccountKey:
      m_valid[m_page] = m_view->account_key->hasAcceptableInput();
      break;
    default:
      break;
  }
  
  m_view->next->setEnabled(m_valid[m_page]);
}

#include "kbsprojectwizard.moc"
