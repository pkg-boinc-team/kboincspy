#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsprogresscontent.ui'
**
** Created: Mon Feb 6 18:29:02 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsprogresscontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qpushbutton.h>
#include <qprogressbar.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSProgressContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSProgressContent::KBSProgressContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSProgressContent" );
    KBSProgressContentLayout = new QVBoxLayout( this, 0, 6, "KBSProgressContentLayout"); 

    app = new KBSPanelField( this, "app" );
    KBSProgressContentLayout->addWidget( app );

    layout_group = new QHBoxLayout( 0, 0, 6, "layout_group"); 

    layout_left = new QVBoxLayout( 0, 0, 6, "layout_left"); 

    status = new KBSPanelField( this, "status" );
    layout_left->addWidget( status );

    cpu_time = new KBSPanelField( this, "cpu_time" );
    layout_left->addWidget( cpu_time );

    rate = new KBSPanelField( this, "rate" );
    layout_left->addWidget( rate );

    credit = new KBSPanelField( this, "credit" );
    layout_left->addWidget( credit );
    layout_group->addLayout( layout_left );
    spacer_middle = new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout_group->addItem( spacer_middle );

    layout_right = new QVBoxLayout( 0, 0, 6, "layout_right"); 

    completed = new KBSPanelField( this, "completed" );
    layout_right->addWidget( completed );

    total_time = new KBSPanelField( this, "total_time" );
    layout_right->addWidget( total_time );

    remaining_time = new KBSPanelField( this, "remaining_time" );
    layout_right->addWidget( remaining_time );

    total_credit = new KBSPanelField( this, "total_credit" );
    layout_right->addWidget( total_credit );
    layout_group->addLayout( layout_right );
    KBSProgressContentLayout->addLayout( layout_group );
    spacer_top = new QSpacerItem( 40, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSProgressContentLayout->addItem( spacer_top );

    progress = new QProgressBar( this, "progress" );
    progress->setProgress( 0 );
    KBSProgressContentLayout->addWidget( progress );
    spacer_bottom = new QSpacerItem( 40, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSProgressContentLayout->addItem( spacer_bottom );

    report_deadline = new KBSPanelField( this, "report_deadline" );
    KBSProgressContentLayout->addWidget( report_deadline );
    languageChange();
    resize( QSize(124, 86).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSProgressContent::~KBSProgressContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSProgressContent::languageChange()
{
    setCaption( tr2i18n( "Processor" ) );
    setIconText( tr2i18n( "exec" ) );
}

#include "kbsprogresscontent.moc"
