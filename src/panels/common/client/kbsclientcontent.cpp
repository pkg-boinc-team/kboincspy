#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsclientcontent.ui'
**
** Created: Mon Feb 6 18:29:00 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsclientcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSClientContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSClientContent::KBSClientContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSClientContent" );
    KBSClientContentLayout = new QVBoxLayout( this, 0, 6, "KBSClientContentLayout"); 

    client_version = new KBSPanelField( this, "client_version" );
    KBSClientContentLayout->addWidget( client_version );

    url = new KBSPanelField( this, "url" );
    KBSClientContentLayout->addWidget( url );

    os = new KBSPanelField( this, "os" );
    KBSClientContentLayout->addWidget( os );

    memory = new KBSPanelField( this, "memory" );
    KBSClientContentLayout->addWidget( memory );

    swap = new KBSPanelField( this, "swap" );
    KBSClientContentLayout->addWidget( swap );

    disk = new KBSPanelField( this, "disk" );
    KBSClientContentLayout->addWidget( disk );

    network = new KBSPanelField( this, "network" );
    KBSClientContentLayout->addWidget( network );
    spacer = new QSpacerItem( 41, 70, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSClientContentLayout->addItem( spacer );
    languageChange();
    resize( QSize(124, 52).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSClientContent::~KBSClientContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSClientContent::languageChange()
{
    setCaption( tr2i18n( "KBSClient" ) );
    setIconText( QString::null );
}

#include "kbsclientcontent.moc"
