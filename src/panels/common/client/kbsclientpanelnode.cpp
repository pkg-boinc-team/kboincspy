/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbsclientcontent.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbsboincdata.h>

#include "kbsclientpanelnode.h"

using namespace KBSBOINC;

class KBSClientPanelFactory : KGenericFactory<KBSClientPanelNode,KBSTreeNode>
{
  public:
    KBSClientPanelFactory() : KGenericFactory<KBSClientPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsclientpanel, KBSClientPanelFactory());

KBSClientPanelNode::KBSClientPanelNode(KBSTreeNode *parent, const char *name, const QStringList &)
                     : KBSPanelNode(parent, name)
{
  if(NULL != monitor())
    connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
}

QString KBSClientPanelNode::name() const
{
  return i18n("Client");
}
    
QStringList KBSClientPanelNode::icons() const
{
  return QStringList("client");
}

KBSPanel *KBSClientPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSClientContent *content = new KBSClientContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSClientPanelNode::setupContent(KBSClientContent *content)
{
  content->client_version->setName(i18n("BOINC client version:"));
  content->url->setName(i18n("URL:"));
  content->os->setName(i18n("Operating system:"));
  content->memory->setName(i18n("Memory:"));
  content->swap->setName(i18n("Swap:"));
  content->disk->setName(i18n("Disk:"));
  content->network->setName(i18n("Average bandwidth:"));  
  
  updateContent();
}

void KBSClientPanelNode::updateContent()
{

  QString url = monitor()->url().prettyURL(-1);
  QString client_version, os, memory, swap, disk, network;
  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  
  if(NULL != state)
  {
    KLocale *locale = KGlobal::locale();
    
    client_version = QString("%0.%1").arg(state->core_client.major_version)
                                     .arg(state->core_client.minor_version);
    if(state->core_client.release > 0)
      client_version.append(QString(".%0").arg(state->core_client.release));
    
    os = i18n("%1 %2").arg(state->host_info.os.name).arg(state->host_info.os.version);
    memory = formatBytes(unsigned(state->host_info.m.nbytes));
    swap = formatBytes(unsigned(state->host_info.m.swap));
    
    const double percent = state->host_info.d.free / state->host_info.d.total * 1e2;
    disk = i18n("%1 (%2% free)").arg(formatBytes(state->host_info.d.total))
                                .arg(locale->formatNumber(percent, 2));
    network = i18n("%1 KB/s up, %2 KB/s down").arg(locale->formatNumber(state->net_stats.bwup / 1024, 2))
                                              .arg(locale->formatNumber(state->net_stats.bwdown / 1024, 2));
  }
  else
    client_version = os = memory = swap = disk = network = i18n("unknown");
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSClientContent *content = static_cast<KBSClientContent*>(it.current()->content());
      
      content->client_version->setText(client_version);
      content->url->setSqueezedText(url);
      content->os->setSqueezedText(os);
      content->memory->setText(memory);
      content->swap->setText(swap);
      content->disk->setText(disk);
      content->network->setText(network);
    }
}

#include "kbsclientpanelnode.moc"
