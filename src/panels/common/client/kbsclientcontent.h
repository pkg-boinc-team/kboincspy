/****************************************************************************
** Form interface generated from reading ui file './kbsclientcontent.ui'
**
** Created: Mon Feb 6 18:29:00 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSCLIENTCONTENT_H
#define KBSCLIENTCONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;

class KBSClientContent : public QWidget
{
    Q_OBJECT

public:
    KBSClientContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSClientContent();

    KBSPanelField* client_version;
    KBSPanelField* url;
    KBSPanelField* os;
    KBSPanelField* memory;
    KBSPanelField* swap;
    KBSPanelField* disk;
    KBSPanelField* network;

protected:
    QVBoxLayout* KBSClientContentLayout;
    QSpacerItem* spacer;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSCLIENTCONTENT_H
