/****************************************************************************
** Form interface generated from reading ui file './kbsprocessorcontent.ui'
**
** Created: Mon Feb 6 18:29:03 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSPROCESSORCONTENT_H
#define KBSPROCESSORCONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;

class KBSProcessorContent : public QWidget
{
    Q_OBJECT

public:
    KBSProcessorContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSProcessorContent();

    KBSPanelField* cpu_type;
    KBSPanelField* cache;
    KBSPanelField* iops;
    KBSPanelField* fpops;

protected:
    QVBoxLayout* KBSProcessorContentLayout;
    QSpacerItem* spacer;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSPROCESSORCONTENT_H
