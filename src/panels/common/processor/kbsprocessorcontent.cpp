#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsprocessorcontent.ui'
**
** Created: Mon Feb 6 18:29:03 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsprocessorcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSProcessorContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSProcessorContent::KBSProcessorContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSProcessorContent" );
    KBSProcessorContentLayout = new QVBoxLayout( this, 0, 6, "KBSProcessorContentLayout"); 

    cpu_type = new KBSPanelField( this, "cpu_type" );
    KBSProcessorContentLayout->addWidget( cpu_type );

    cache = new KBSPanelField( this, "cache" );
    KBSProcessorContentLayout->addWidget( cache );

    iops = new KBSPanelField( this, "iops" );
    KBSProcessorContentLayout->addWidget( iops );

    fpops = new KBSPanelField( this, "fpops" );
    KBSProcessorContentLayout->addWidget( fpops );
    spacer = new QSpacerItem( 41, 100, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSProcessorContentLayout->addItem( spacer );
    languageChange();
    resize( QSize(124, 31).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSProcessorContent::~KBSProcessorContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSProcessorContent::languageChange()
{
    setCaption( tr2i18n( "KBSProcessorContent" ) );
}

#include "kbsprocessorcontent.moc"
