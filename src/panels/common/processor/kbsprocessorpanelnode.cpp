/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>
#include <kbsprocessorcontent.h>

#include <kbsboincdata.h>

#include "kbsprocessorpanelnode.h"

class KBSProcessorPanelFactory : KGenericFactory<KBSProcessorPanelNode,KBSTreeNode>
{
  public:
    KBSProcessorPanelFactory() : KGenericFactory<KBSProcessorPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsprocessorpanel, KBSProcessorPanelFactory());

KBSProcessorPanelNode::KBSProcessorPanelNode(KBSTreeNode *parent, const char *name, const QStringList &)
                     : KBSPanelNode(parent, name)
{
  if(NULL != monitor())
    connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
}

QString KBSProcessorPanelNode::name() const
{
  return i18n("Processor");
}

QStringList KBSProcessorPanelNode::icons() const
{
  return QStringList("processor");
}

KBSPanel *KBSProcessorPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSProcessorContent *content = new KBSProcessorContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSProcessorPanelNode::setupContent(KBSProcessorContent *content)
{
  content->cpu_type->setName(i18n("CPU:"));
  content->cache->setName(i18n("Cache:"));
  content->iops->setName(i18n("Measured integer speed (Dhrystone):"));
  content->fpops->setName(i18n("Measured floating point speed (Whetstone):"));
  
  updateContent();
}

void KBSProcessorPanelNode::updateContent()
{
  QString cpu_type, cache, iops, fpops;
  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  
  if(NULL != state)
  {
    KLocale *locale = KGlobal::locale();

    cpu_type = state->host_info.p.model;
    const unsigned ncpus = state->host_info.p.ncpus;
    if(ncpus > 1)
      cpu_type = i18n("%1 x %2").arg(locale->formatNumber(ncpus, 0)).arg(cpu_type);
    
    cache = i18n("%1 on CPU or CPU module").arg(KBSBOINC::formatBytes(state->host_info.m.cache));
    iops = i18n("%1 MIPS").arg(locale->formatNumber(state->host_info.p.iops * 1e-6, 2));
    fpops = i18n("%1 MFLOPS").arg(locale->formatNumber(state->host_info.p.fpops * 1e-6, 2));
  }
  else
    cpu_type = cache = iops = fpops = i18n("unknown");
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSProcessorContent *content = static_cast<KBSProcessorContent*>(it.current()->content());
      
      content->cpu_type->setSqueezedText(cpu_type);
      content->cache->setText(cache);
      content->iops->setText(iops);
      content->fpops->setText(fpops);
    }
}

#include "kbsprocessorpanelnode.moc"
