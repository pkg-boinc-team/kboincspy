/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>
#include <kbsperformancecontent.h>

#include <kbsboincdata.h>

#include "kbsperformancepanelnode.h"

class KBSPerformancePanelFactory : KGenericFactory<KBSPerformancePanelNode,KBSTreeNode>
{
  public:
    KBSPerformancePanelFactory() : KGenericFactory<KBSPerformancePanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsperformancepanel, KBSPerformancePanelFactory());

KBSPerformancePanelNode::KBSPerformancePanelNode(KBSTreeNode *parent, const char *name,
                                                         const QStringList &args)
                       : KBSPanelNode(parent, name),
                         m_prog(0.0), m_deltaProg(0.0), m_cpu(0.0), m_deltaCPU(0.0),
                         m_task(args[0].toUInt(0, 10))                             
{
  setupMonitor();
}

QString KBSPerformancePanelNode::name() const
{
  return i18n("Performance");
}

QStringList KBSPerformancePanelNode::icons() const
{
  return QStringList("performance");
}

KBSPanel *KBSPerformancePanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSPerformanceContent *content = new KBSPerformanceContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

unsigned KBSPerformancePanelNode::task() const
{
  return m_task;
}

QString KBSPerformancePanelNode::workunit() const
{
  return m_workunit;
}

QString KBSPerformancePanelNode::result() const
{
  return m_result;
}

QString KBSPerformancePanelNode::project() const
{
  return m_project;
}

void KBSPerformancePanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->active_task_set.active_task[m_task]);
  m_result = state->active_task_set.active_task[m_task].result_name;
  m_workunit = state->result[m_result].wu_name;
  
  m_prog = state->active_task_set.active_task[m_task].fraction_done;
  m_cpu = state->active_task_set.active_task[m_task].current_cpu_time;
}

void KBSPerformancePanelNode::setupContent(KBSPerformanceContent *content)
{
  content->result->setName(i18n("Result name:"));
  content->result->setSqueezedText(m_result);
  content->rate_avg->setName(i18n("Average processing rate:"));
  content->speed_avg->setName(i18n("Average processing speed:"));
  content->rate_instant->setName(i18n("Instantaneous processing rate:"));
  content->speed_instant->setName(i18n("Instanteous processing speed:"));
  content->work->setName(i18n("Floating point operations to process:"));
    
  updateContent();
}

void KBSPerformancePanelNode::updateContent()
{
  QString result_tip, rate_avg, rate_avg_tip, speed_avg,
          rate_instant, rate_instant_tip, speed_instant, work;
  
  KLocale *locale = KGlobal::locale();

  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  if(NULL != state)
  {
    result_tip = state->project[m_project].project_name;
    
    const double prog = state->active_task_set.active_task[m_task].fraction_done;
    const double cpu = state->active_task_set.active_task[m_task].current_cpu_time;
    
    double deltaProg = m_deltaProg, deltaCPU = m_deltaCPU;
    if(prog > m_prog && cpu > m_cpu) {
      deltaProg = m_deltaProg = (prog - m_prog);
      deltaCPU = m_deltaCPU = (cpu - m_cpu);
    }
    m_prog = prog; m_cpu = cpu;
    
    const double fpops_est = state->workunit[m_workunit].rsc.fpops_est;
    
    if(prog > 0.0 && cpu > 0.0)
    {
      rate_avg = i18n("%1 hours / work unit")
                   .arg(locale->formatNumber(cpu / (prog * 3600), 2));
      rate_avg_tip = i18n("%1% / hour")
                       .arg(locale->formatNumber((prog * 1e2) / (cpu / 3600), 2));
      
      if(fpops_est > 0.0)
        speed_avg = i18n("%1 MegaFLOPS").arg(locale->formatNumber((fpops_est / 1e6) / (cpu / prog), 2));
      else
        speed_avg = i18n("unknown");
    }
    else
      rate_avg = speed_avg = i18n("unknown");
    
    if(deltaProg > 0.0 && deltaCPU > 0.0)
    {
      rate_instant = i18n("%1 hours / work unit")
                       .arg(locale->formatNumber(deltaCPU / (deltaProg * 3600), 2));
      rate_instant_tip = i18n("%1% / hour")
                           .arg(locale->formatNumber((deltaProg * 1e2) / (deltaCPU / 3600), 2));
      
      if(fpops_est > 0.0)
        speed_instant = i18n("%1 MegaFLOPS")
                          .arg(locale->formatNumber((fpops_est / 1e6) / (deltaCPU / deltaProg), 2));
      else
        speed_instant = i18n("unknown");
    }
    else
      rate_instant = speed_instant = i18n("unknown");
    
    work = (fpops_est > 0.0) ? i18n("%1 TeraFLOPs").arg(locale->formatNumber(fpops_est / 1e12, 2))
                             : i18n("unknown");
  }
  else 
  {
    rate_avg = rate_instant = speed_avg = speed_instant = work = i18n("unknown");
    result_tip = rate_avg_tip = rate_instant_tip = QString::null;
  }
    
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSPerformanceContent *content = static_cast<KBSPerformanceContent*>(it.current()->content());
      
      content->result->setTooltip(result_tip);
      content->rate_avg->setText(rate_avg);
      content->rate_avg->setTooltip(rate_avg_tip);
      content->rate_instant->setText(rate_instant);
      content->rate_instant->setTooltip(rate_instant_tip);
      content->speed_avg->setText(speed_avg);
      content->speed_instant->setText(speed_instant);
      content->work->setText(work);
    }
}

#include "kbsperformancepanelnode.moc"
