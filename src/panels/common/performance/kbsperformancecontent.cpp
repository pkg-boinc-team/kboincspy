#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsperformancecontent.ui'
**
** Created: Mon Feb 6 18:29:01 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsperformancecontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSPerformanceContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSPerformanceContent::KBSPerformanceContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSPerformanceContent" );
    KBSPerformanceContentLayout = new QVBoxLayout( this, 0, 6, "KBSPerformanceContentLayout"); 

    result = new KBSPanelField( this, "result" );
    KBSPerformanceContentLayout->addWidget( result );

    rate_avg = new KBSPanelField( this, "rate_avg" );
    KBSPerformanceContentLayout->addWidget( rate_avg );

    speed_avg = new KBSPanelField( this, "speed_avg" );
    KBSPerformanceContentLayout->addWidget( speed_avg );

    rate_instant = new KBSPanelField( this, "rate_instant" );
    KBSPerformanceContentLayout->addWidget( rate_instant );

    speed_instant = new KBSPanelField( this, "speed_instant" );
    KBSPerformanceContentLayout->addWidget( speed_instant );

    work = new KBSPanelField( this, "work" );
    KBSPerformanceContentLayout->addWidget( work );
    spacer = new QSpacerItem( 31, 30, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSPerformanceContentLayout->addItem( spacer );
    languageChange();
    resize( QSize(124, 45).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSPerformanceContent::~KBSPerformanceContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSPerformanceContent::languageChange()
{
    setCaption( tr2i18n( "KBSPerformanceContent" ) );
}

#include "kbsperformancecontent.moc"
