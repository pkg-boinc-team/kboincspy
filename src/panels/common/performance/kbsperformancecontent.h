/****************************************************************************
** Form interface generated from reading ui file './kbsperformancecontent.ui'
**
** Created: Mon Feb 6 18:29:01 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSPERFORMANCECONTENT_H
#define KBSPERFORMANCECONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;

class KBSPerformanceContent : public QWidget
{
    Q_OBJECT

public:
    KBSPerformanceContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSPerformanceContent();

    KBSPanelField* result;
    KBSPanelField* rate_avg;
    KBSPanelField* speed_avg;
    KBSPanelField* rate_instant;
    KBSPanelField* speed_instant;
    KBSPanelField* work;

protected:
    QVBoxLayout* KBSPerformanceContentLayout;
    QSpacerItem* spacer;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSPERFORMANCECONTENT_H
