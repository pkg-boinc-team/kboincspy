/****************************************************************************
** Form interface generated from reading ui file './kbshostcontent.ui'
**
** Created: Mon Feb 6 18:29:00 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSHOSTCONTENT_H
#define KBSHOSTCONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;
class KPushButton;

class KBSHostContent : public QWidget
{
    Q_OBJECT

public:
    KBSHostContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSHostContent();

    KBSPanelField* project_name;
    KBSPanelField* domain_name;
    KBSPanelField* total_credit;
    KBSPanelField* create_time;
    KBSPanelField* user_name;
    KBSPanelField* avg_credit;
    KBSPanelField* venue;
    KBSPanelField* resource_share;
    KPushButton* chart_button;

protected:
    QVBoxLayout* KBSHostContentLayout;
    QSpacerItem* spacer;
    QHBoxLayout* layout_group;
    QSpacerItem* spacer_middle;
    QVBoxLayout* layout_left;
    QVBoxLayout* layout_right;
    QHBoxLayout* layout_buttons;
    QSpacerItem* spacer_buttons;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSHOSTCONTENT_H
