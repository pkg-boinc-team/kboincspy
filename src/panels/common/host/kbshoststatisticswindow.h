/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSHOSTSTATISTICSWINDOW_H
#define KBSHOSTSTATISTICSWINDOW_H

#include <qmap.h>
#include <qptrdict.h>

#include <kbsstandardwindow.h>

class KBSBOINCMonitor;
class KBSStatisticsChart;

class KBSHostStatisticsWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSHostStatisticsWindow* window(KBSBOINCMonitor *monitor, const QString &project);
    virtual ~KBSHostStatisticsWindow();
    
    virtual KBSBOINCMonitor *monitor();
    virtual QString project();
    
  protected:
    virtual QPixmap pixmap();
    
  private slots:
    virtual void updateStatistics(const QString &project);
  
  private:
    void setupView();
    
  protected:
    KBSHostStatisticsWindow(KBSBOINCMonitor *monitor, const QString &project);
  
  protected:
    KBSStatisticsChart *m_chart;
    
  private:
    KBSBOINCMonitor *m_monitor;
    QString m_project;
    
    static QMap< KBSBOINCMonitor*,QDict<KBSHostStatisticsWindow> > s_windows;
};

#endif
