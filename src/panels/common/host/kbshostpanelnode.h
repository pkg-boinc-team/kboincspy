/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSHOSTPANELNODE_H
#define KBSHOSTPANELNODE_H

#include <kbspanelnode.h>

class KBSHostContent;
class KBSHostStatisticsWindow;

class KBSHostPanelNode : public KBSPanelNode
{
  Q_OBJECT
  public:
    KBSHostPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args);
    virtual ~KBSHostPanelNode();
    
    virtual QString name() const;
    virtual QStringList icons() const;

    virtual QString project() const;
    
    virtual KBSPanel *createPanel(QWidget *parent=0);
    
  private:
    void setupContent(KBSHostContent *panel);
    
    QString formatVenue(const QString &venue);
  
  private slots:
    void updateContent();
    
    void showStatistics();
  
  protected:
    QString m_project;
    QString m_domainLink, m_userLink, m_venueLink;
  
  private:
    KBSHostStatisticsWindow *m_chart;
};

#endif
