/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbsstatisticschart.h>

#include "kbshoststatisticswindow.h"

QMap< KBSBOINCMonitor*,QDict<KBSHostStatisticsWindow> > KBSHostStatisticsWindow::s_windows =
  QMap< KBSBOINCMonitor*,QDict<KBSHostStatisticsWindow> >();

KBSHostStatisticsWindow *KBSHostStatisticsWindow::window(KBSBOINCMonitor *monitor, const QString &project)
{
  if(NULL == monitor) return NULL;
  
  QDict<KBSHostStatisticsWindow> dict = s_windows[monitor];
  
  KBSHostStatisticsWindow *out = dict.find(project);
  if(NULL == out) {
    out = new KBSHostStatisticsWindow(monitor, project);
    dict.insert(project, out);
  }
  
  return out;
}

KBSHostStatisticsWindow::KBSHostStatisticsWindow(KBSBOINCMonitor *monitor, const QString &project)
                       : KBSStandardWindow(0, project), m_monitor(monitor), m_project(project)
{
  setupActions();
  
  setupView();
  
  updateStatistics(project);
  
  connect(monitor, SIGNAL(statisticsUpdated(const QString &)),
          this, SLOT(updateStatistics(const QString &)));
}

KBSHostStatisticsWindow::~KBSHostStatisticsWindow()
{
  QDict<KBSHostStatisticsWindow> dict = s_windows[m_monitor];
  dict.remove(m_project);
  if(dict.isEmpty()) s_windows.remove(m_monitor);
}

KBSBOINCMonitor *KBSHostStatisticsWindow::monitor()
{
  return m_monitor;
}

QString KBSHostStatisticsWindow::project()
{
  return m_project;
}

QPixmap KBSHostStatisticsWindow::pixmap()
{
  return m_chart->pixmap();
}

void KBSHostStatisticsWindow::setupView()
{
  m_chart = new KBSStatisticsChart(KBSStatisticsChart::Host, this);
  setCentralWidget(m_chart);
  
  const QString host = m_monitor->location().host;
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  const QString projectName = state->project[m_project].project_name;

  setCaption(i18n("%1 Host Statistics (%2)").arg(projectName).arg(host));
  
  setAutoSaveGeometry(QString("%1 Host Statistics (%2)").arg(projectName).arg(host));
}

void KBSHostStatisticsWindow::updateStatistics(const QString &project)
{
  if(project != m_project) return;
  
  const KBSBOINCProjectStatistics *statistics = m_monitor->statistics(project);
  if(NULL == statistics) return;
  
  if(statistics->daily_statistics.isEmpty()) return;
  
  if(statistics->daily_statistics.last().day > m_chart->end())
    m_chart->setData(statistics->daily_statistics);
}

#include "kbshoststatisticswindow.moc"
