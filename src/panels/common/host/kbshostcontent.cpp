#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbshostcontent.ui'
**
** Created: Mon Feb 6 18:29:01 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbshostcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qpushbutton.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSHostContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSHostContent::KBSHostContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSHostContent" );
    KBSHostContentLayout = new QVBoxLayout( this, 0, 6, "KBSHostContentLayout"); 

    project_name = new KBSPanelField( this, "project_name" );
    KBSHostContentLayout->addWidget( project_name );

    layout_group = new QHBoxLayout( 0, 0, 6, "layout_group"); 

    layout_left = new QVBoxLayout( 0, 0, 6, "layout_left"); 

    domain_name = new KBSPanelField( this, "domain_name" );
    layout_left->addWidget( domain_name );

    total_credit = new KBSPanelField( this, "total_credit" );
    layout_left->addWidget( total_credit );

    create_time = new KBSPanelField( this, "create_time" );
    layout_left->addWidget( create_time );
    layout_group->addLayout( layout_left );
    spacer_middle = new QSpacerItem( 20, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout_group->addItem( spacer_middle );

    layout_right = new QVBoxLayout( 0, 0, 6, "layout_right"); 

    user_name = new KBSPanelField( this, "user_name" );
    layout_right->addWidget( user_name );

    avg_credit = new KBSPanelField( this, "avg_credit" );
    layout_right->addWidget( avg_credit );

    venue = new KBSPanelField( this, "venue" );
    layout_right->addWidget( venue );
    layout_group->addLayout( layout_right );
    KBSHostContentLayout->addLayout( layout_group );

    resource_share = new KBSPanelField( this, "resource_share" );
    KBSHostContentLayout->addWidget( resource_share );
    spacer = new QSpacerItem( 41, 110, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSHostContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_buttons = new QSpacerItem( 91, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_buttons );

    chart_button = new KPushButton( this, "chart_button" );
    layout_buttons->addWidget( chart_button );
    KBSHostContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(124, 75).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSHostContent::~KBSHostContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSHostContent::languageChange()
{
    setCaption( tr2i18n( "KBSHostContent" ) );
    chart_button->setText( tr2i18n( "Chart" ) );
}

#include "kbshostcontent.moc"
