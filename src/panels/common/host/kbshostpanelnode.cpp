/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>
#include <kpushbutton.h>

#include <kbsboincdata.h>
#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>
#include <kbshostcontent.h>

#include <kbshoststatisticswindow.h>

#include "kbshostpanelnode.h"

const QString DefaultDomainPath = "show_host_detail.php?hostid=%1";
const QString DefaultUserPath= "home.php";

class KBSHostPanelFactory : KGenericFactory<KBSHostPanelNode,KBSTreeNode>
{
  public:
    KBSHostPanelFactory() : KGenericFactory<KBSHostPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbshostpanel, KBSHostPanelFactory());


KBSHostPanelNode::KBSHostPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args)
                : KBSPanelNode(parent, name), m_project(args[0]),
                  m_chart(NULL)
{
  m_domainLink = (args.count() >= 2) ? args[1] : QString::null;
  m_userLink = (args.count() >= 3) ? args[2] : QString::null;
  m_venueLink = (args.count() >= 4) ? args[3] : QString::null;
  
  if(NULL != monitor())
    connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
}

KBSHostPanelNode::~KBSHostPanelNode()
{
  if(NULL != m_chart) delete m_chart;
}

QString KBSHostPanelNode::name() const
{
  return i18n("Host Statistics");
}
    
QStringList KBSHostPanelNode::icons() const
{
  return QStringList("host");
}

QString KBSHostPanelNode::project() const
{
  return m_project;
}

KBSPanel *KBSHostPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSHostContent *content = new KBSHostContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSHostPanelNode::setupContent(KBSHostContent *content)
{
  content->project_name->setName(i18n("Project:"));
  content->domain_name->setName(i18n("Host:"));
  content->user_name->setName(i18n("Owner:"));
  content->create_time->setName(i18n("Registered:"));
  content->total_credit->setName(i18n("Total credits:"));
  content->avg_credit->setName(i18n("Average daily credits:"));
  content->venue->setName(i18n("Environment:"));
  content->resource_share->setName(i18n("Resource share:"));
  connect(content->chart_button, SIGNAL(clicked()), this, SLOT(showStatistics()));
  
  updateContent();
}

QString KBSHostPanelNode::formatVenue(const QString &venue)
{
  if(venue.startsWith("home", false)) return i18n("home");
  if(venue.startsWith("work", false)) return i18n("work");
  if(venue.startsWith("school", false)) return i18n("school");
  return i18n("unknown");
}
  
void KBSHostPanelNode::updateContent()
{
  KURL project_url, domain_url, user_url, venue_url;
  QString project_name, domain_name, user_name, create_time, total_credit, avg_credit,
          venue_name, resource_share;
  
  KLocale *locale = KGlobal::locale();

  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL; 
  if(NULL != state && !m_project.isEmpty())
  {
    project_name = state->project[m_project].project_name;
    project_url = KURL(state->project[m_project].master_url);
    domain_name = state->host_info.domain_name;
    
    const unsigned hostid = state->project[m_project].hostid;
    
    if(!m_domainLink.isEmpty())
      if(m_domainLink.contains("%1"))
        domain_url = KURL(m_domainLink.arg(hostid));
      else
        domain_url = KURL(m_domainLink);
    else if(project_url.isValid())
      domain_url = KURL(project_url, DefaultDomainPath.arg(hostid));
    
    user_name = state->project[m_project].user_name;
    
    if(!m_userLink.isEmpty())
      if(m_userLink.contains("%1"))
        user_url = KURL(m_userLink.arg(KURL::encode_string(user_name)));
      else
        user_url = KURL(m_userLink);
    else if(project_url.isValid())
      user_url = KURL(project_url, DefaultUserPath);

    
    create_time = locale->formatDate(state->project[m_project].host.create_time.date(), true);
    total_credit = locale->formatNumber(state->project[m_project].host.total_credit, 2);
    avg_credit = locale->formatNumber(state->project[m_project].host.expavg_credit, 2);
    venue_name = formatVenue(state->host_venue);
    
    if(!m_venueLink.isEmpty())
      if(m_venueLink.contains("%1"))
        venue_url = KURL(m_venueLink.arg(KURL::encode_string(state->host_venue)));
      else
        venue_url = KURL(m_venueLink);
  }
  else
    project_name = domain_name = user_name = create_time = total_credit = avg_credit
                 = venue_name = i18n("unknown");
  
  const KBSBOINCAccount *account = (NULL != monitor()) ? monitor()->account(m_project) : NULL;
  if(NULL != account)
  {
    const unsigned project_share = account->project_preferences.resource_share;
    double ratio;
    
    if(NULL == state)
      ratio = -1;
    else if(state->project[m_project].suspended_via_gui)
      ratio = 0;
    else
    {
      double total_share = 0;
      const QStringList projects = state->project.keys();
      for(QStringList::const_iterator project = projects.constBegin();
          project != projects.constEnd(); ++project)
        if(NULL != monitor()->account(*project)) {
          if(!state->project[*project].suspended_via_gui)
            total_share += monitor()->account(*project)->project_preferences.resource_share;
        } else {
          total_share = 0;
          break;
        }
      
      ratio = (total_share > 0) ? project_share / total_share : -1;
    }
    
    if(ratio >= 0)
      resource_share = i18n("%1 (%2%)").arg(locale->formatNumber(project_share, 0))
                                       .arg(locale->formatNumber(ratio * 1e2, 2));
    else
      resource_share = locale->formatNumber(project_share, 0);
  }
  else
    resource_share = i18n("unknown");
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSHostContent *content = static_cast<KBSHostContent*>(it.current()->content());
      
      if(project_url.isValid())
        content->project_name->setURL(project_url.prettyURL(), project_name,
                                      i18n("Click to visit the project main page"));
      else
        content->project_name->setSqueezedText(project_name);
      
      if(domain_url.isValid())
        content->domain_name->setURL(domain_url.prettyURL(), domain_name,
                                     i18n("Click to visit your host account page"));
      else
        content->domain_name->setSqueezedText(domain_name);
      
      if(user_url.isValid())
        content->user_name->setURL(user_url.prettyURL(), user_name,
                                   i18n("Click to visit your user account page"));
      else
        content->user_name->setSqueezedText(user_name);
      
      content->create_time->setText(create_time);
      content->total_credit->setText(total_credit);
      content->avg_credit->setText(avg_credit);
      
      if(venue_url.isValid())
        content->venue->setURL(venue_url.prettyURL(), venue_name,
                               i18n("Click to see the top participants for this environment"));
      else
        content->venue->setText(venue_name);
      
      content->resource_share->setText(resource_share);
    }
}

void KBSHostPanelNode::showStatistics()
{
  if(NULL == m_chart)
    m_chart = KBSHostStatisticsWindow::window(monitor(), m_project);
  if(NULL == m_chart) return;
  
  if(!m_chart->isVisible()) m_chart->show();
}

#include "kbshostpanelnode.moc"
