/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSUSERSTATISTICSWINDOW_H
#define KBSUSERSTATISTICSWINDOW_H

#include <qdict.h>

#include <kbsstandardwindow.h>

class KBSBOINCMonitor;
class KBSStatisticsChart;

class KBSUserStatisticsWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSUserStatisticsWindow* window(KBSBOINCMonitor *monitor, const QString &project);
    
    virtual void attachMonitor(KBSBOINCMonitor *monitor, const QString &project);
    virtual void detachMonitor(KBSBOINCMonitor *monitor);
    
  protected:
    virtual void updateStatistics(KBSBOINCMonitor *monitor);
    
    virtual QPixmap pixmap();
  
  private:
    void setupView();
  
  private slots:
    void updateStatistics(const QString &project);
  
  protected:
    KBSUserStatisticsWindow(const QString &projectName);
  
  protected:
    KBSStatisticsChart *m_chart;
  
  private:
    QString m_projectName;
    QMap<KBSBOINCMonitor*,QString> m_projects;
    static QDict<KBSUserStatisticsWindow> s_windows;
};

#endif
