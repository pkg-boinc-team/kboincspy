/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qbuttongroup.h>
#include <qpopupmenu.h>

#include <kiconloader.h>
#include <klocale.h>
#include <knuminput.h>
#include <kpushbutton.h>

#include <kbsboincmonitor.h>
#include <kbsprojectmonitor.h>

#include <kbscreditcalendar.h>
#include <kbscreditcalendarcontent.h>

#include "kbscreditcalendarwindow.h"

QDict<KBSCreditCalendarWindow> KBSCreditCalendarWindow::s_windows = QDict<KBSCreditCalendarWindow>();

KBSCreditCalendarWindow *KBSCreditCalendarWindow::window(KBSBOINCMonitor *monitor, const QString &project)
{
  if(NULL == monitor) return NULL;
  
  const KBSBOINCClientState *state = monitor->state();
  if(NULL == state) return NULL;
  
  const QString projectName = state->project[project].project_name;
  if(projectName.isEmpty()) return NULL;
  
  KBSCreditCalendarWindow *out = s_windows.find(projectName);
  if(NULL == out) {
    out = new KBSCreditCalendarWindow(projectName);
    out->attachMonitor(monitor, project);
    s_windows.insert(projectName, out);
  }
  
  return out;
}

KBSCreditCalendarWindow::KBSCreditCalendarWindow(const QString &projectName)
                       : KBSStandardWindow(0, projectName), m_projectName(projectName)
{
  setupActions();
  setupView(); 
}

void KBSCreditCalendarWindow::attachMonitor(KBSBOINCMonitor *monitor, const QString &project)
{
  if(NULL == monitor || m_projects.contains(monitor)) return;
  
  m_projects.insert(monitor, project);
  
  updateState(monitor);
  
  connect(monitor, SIGNAL(stateUpdated()), this, SLOT(updateState()));
}

void KBSCreditCalendarWindow::detachMonitor(KBSBOINCMonitor *monitor)
{
  if(NULL == monitor) return;
  
  disconnect(monitor, SIGNAL(stateUpdated()), this, SLOT(updateState()));
  
  m_projects.remove(monitor);
  if(!m_projects.isEmpty()) return;
  
  s_windows.remove(m_projectName);
  
  close();
  destroy();
}

void KBSCreditCalendarWindow::updateState(KBSBOINCMonitor *monitor)
{
  if(!m_projects.contains(monitor)) return;
  const QString project = m_projects[monitor];
  
  const KBSBOINCClientState *state = (NULL != monitor) ? monitor->state() : NULL;
  if(NULL == state) return;
  
  const double total_credit = state->project[project].user.total_credit;
  const double expavg_credit = state->project[project].user.expavg_credit;
  
  if(total_credit > m_calendar->calendar->initial()) {
    m_calendar->calendar->setInitial(total_credit);
    m_calendar->increment->setValue(expavg_credit);
  }
}

QPixmap KBSCreditCalendarWindow::pixmap()
{
  return m_calendar->calendar->pixmap();
}

void KBSCreditCalendarWindow::setupView()
{
  setCaption(i18n("%1 Credits Calendar").arg(m_projectName));
  
  m_calendar = new KBSCreditCalendarContent(this);
  setCentralWidget(m_calendar);
  
  m_calendar->calendar->setProjectName(m_projectName);
  
  QButtonGroup *group = new QButtonGroup(this);
  group->hide();
  
  m_calendar->prev2->setIconSet(SmallIconSet("2leftarrow"));
  group->insert(m_calendar->prev2, 0);
  
  m_calendar->prev->setIconSet(SmallIconSet("1leftarrow"));
  group->insert(m_calendar->prev, 5);
  
  m_calendar->next->setIconSet(SmallIconSet("1rightarrow"));
  group->insert(m_calendar->next, 7);
  
  m_calendar->next2->setIconSet(SmallIconSet("2rightarrow"));
  group->insert(m_calendar->next2, 12);
  
  connect(group, SIGNAL(clicked(int)), this, SLOT(handleButtons(int)));
  
  setAutoSaveGeometry(QString("%1 Credits Calendar").arg(m_projectName));
}

void KBSCreditCalendarWindow::updateState()
{
  KBSBOINCMonitor *monitor = (KBSBOINCMonitor *) sender();
  if(NULL == monitor) return;
  
  updateState(monitor);
}

void KBSCreditCalendarWindow::handleButtons(int index)
{
  m_calendar->calendar->addMonths(index - 6);
}

#include "kbscreditcalendarwindow.moc"
