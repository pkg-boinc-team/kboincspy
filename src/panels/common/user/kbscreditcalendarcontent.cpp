#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbscreditcalendarcontent.ui'
**
** Created: Mon Feb 6 18:29:04 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbscreditcalendarcontent.h"

#include <qvariant.h>
#include <kbscreditcalendar.h>
#include <kpushbutton.h>
#include <qlabel.h>
#include <knuminput.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSCreditCalendarContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSCreditCalendarContent::KBSCreditCalendarContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSCreditCalendarContent" );
    KBSCreditCalendarContentLayout = new QVBoxLayout( this, 11, 6, "KBSCreditCalendarContentLayout"); 

    calendar = new KBSCreditCalendar( this, "calendar" );
    calendar->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 1, calendar->sizePolicy().hasHeightForWidth() ) );
    KBSCreditCalendarContentLayout->addWidget( calendar );

    layout13 = new QHBoxLayout( 0, 0, 6, "layout13"); 

    prev2 = new KPushButton( this, "prev2" );
    prev2->setFlat( TRUE );
    layout13->addWidget( prev2 );

    prev = new KPushButton( this, "prev" );
    prev->setFlat( TRUE );
    layout13->addWidget( prev );
    spacer_left = new QSpacerItem( 40, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout13->addItem( spacer_left );

    label = new QLabel( this, "label" );
    layout13->addWidget( label );

    increment = new KDoubleSpinBox( this, "increment" );
    increment->setMaxValue( 10000 );
    increment->setMinValue( 0 );
    increment->setValue( 0 );
    increment->setPrecision( 2 );
    layout13->addWidget( increment );
    spacer_right = new QSpacerItem( 30, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout13->addItem( spacer_right );

    next = new KPushButton( this, "next" );
    next->setFlat( TRUE );
    layout13->addWidget( next );

    next2 = new KPushButton( this, "next2" );
    next2->setFlat( TRUE );
    layout13->addWidget( next2 );
    KBSCreditCalendarContentLayout->addLayout( layout13 );
    languageChange();
    resize( QSize(498, 56).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( increment, SIGNAL( valueChanged(double) ), calendar, SLOT( setIncrement(double) ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSCreditCalendarContent::~KBSCreditCalendarContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSCreditCalendarContent::languageChange()
{
    setCaption( tr2i18n( "KBSCreditCalendarContent" ) );
    prev2->setText( QString::null );
    prev->setText( QString::null );
    label->setText( tr2i18n( "Credits per day:" ) );
    next->setText( QString::null );
    next2->setText( QString::null );
}

#include "kbscreditcalendarcontent.moc"
