/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSCREDITCALENDAR_H
#define KBSCREDITCALENDAR_H

#include <qdatetime.h>
#include <qpixmap.h>
#include <qwidget.h>

class KBSCreditCalendar : public QWidget
{
  Q_OBJECT
  public:
    KBSCreditCalendar(QWidget *parent=0, const char *name=0);
  
    virtual QString projectName() const;
    virtual void setProjectName(const QString &projectName);
    
    virtual double initial() const;
    virtual double increment() const;
    
    virtual QDate currentDate() const;
    
    virtual QPixmap pixmap();
    
  public slots:
    virtual void addMonths(int months);
    
    virtual void setInitial(double start);
    virtual void setIncrement(double increment);
    
  protected:
    virtual void paintEvent(QPaintEvent *event);
  
  private slots:
    void updateLog();
    
  private:
    void setupCache();
    
    QDate firstOfMonth(const QDate& date);
    
  private:
    QString m_projectName;
    double m_initial, m_increment;
    QDate m_start, m_current;
    double m_cache[31];
    
    static const unsigned s_border;  
};

#endif
