/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbsstatisticschart.h>

#include "kbsuserstatisticswindow.h"

QDict<KBSUserStatisticsWindow> KBSUserStatisticsWindow::s_windows = QDict<KBSUserStatisticsWindow>();

KBSUserStatisticsWindow *KBSUserStatisticsWindow::window(KBSBOINCMonitor *monitor, const QString &project)
{
  if(NULL == monitor) return NULL;
  
  const KBSBOINCClientState *state = monitor->state();
  if(NULL == state) return NULL;
  
  const QString projectName = state->project[project].project_name;
  if(projectName.isEmpty()) return NULL;
  
  KBSUserStatisticsWindow *out = s_windows.find(projectName);
  if(NULL == out) {
    out = new KBSUserStatisticsWindow(projectName);
    out->attachMonitor(monitor, project);
    s_windows.insert(projectName, out);
  }
  
  return out;
}

KBSUserStatisticsWindow::KBSUserStatisticsWindow(const QString &projectName)
                       : KBSStandardWindow(0, projectName), m_projectName(projectName)
{
  setupActions();
  setupView(); 
}

void KBSUserStatisticsWindow::attachMonitor(KBSBOINCMonitor *monitor, const QString &project)
{
  if(NULL == monitor || m_projects.contains(monitor)) return;
  
  m_projects.insert(monitor, project);
  
  updateStatistics(monitor);
  
  connect(monitor, SIGNAL(statisticsUpdated(const QString &)),
          this, SLOT(updateStatistics(const QString &)));
}

void KBSUserStatisticsWindow::detachMonitor(KBSBOINCMonitor *monitor)
{
  if(NULL == monitor) return;
  
  disconnect(monitor, SIGNAL(statisticsUpdated(const QString &)),
             this, SLOT(updateStatistics(const QString &)));
  
  m_projects.remove(monitor);
  if(!m_projects.isEmpty()) return;
  
  s_windows.remove(m_projectName);
  
  close();
  destroy();
}

void KBSUserStatisticsWindow::updateStatistics(KBSBOINCMonitor *monitor)
{
  if(!m_projects.contains(monitor)) return;
  const QString project = m_projects[monitor];
  
  const KBSBOINCProjectStatistics *statistics = monitor->statistics(project);
  if(NULL == statistics) return;
  
  if(statistics->daily_statistics.isEmpty()) return;
  
  if(statistics->daily_statistics.last().day > m_chart->end())
    m_chart->setData(statistics->daily_statistics);
}

QPixmap KBSUserStatisticsWindow::pixmap()
{
  return m_chart->pixmap();
}

void KBSUserStatisticsWindow::setupView()
{
  setCaption(i18n("%1 User Statistics").arg(m_projectName));
  
  m_chart = new KBSStatisticsChart(KBSStatisticsChart::User, this);
  setCentralWidget(m_chart);
  
  setAutoSaveGeometry(QString("%1 User Statistics").arg(m_projectName));
}

void KBSUserStatisticsWindow::updateStatistics(const QString &project)
{
  KBSBOINCMonitor *monitor = (KBSBOINCMonitor*) sender();
  if(NULL == monitor) return;
  
  if(!m_projects.contains(monitor) || project != m_projects[monitor])
    return;
  
  updateStatistics(monitor);
}

#include "kbsuserstatisticswindow.moc"
