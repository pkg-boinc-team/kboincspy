#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsusercontent.ui'
**
** Created: Mon Feb 6 18:29:04 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsusercontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qpushbutton.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSUserContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSUserContent::KBSUserContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSUserContent" );
    KBSUserContentLayout = new QVBoxLayout( this, 0, 6, "KBSUserContentLayout"); 

    project_name = new KBSPanelField( this, "project_name" );
    KBSUserContentLayout->addWidget( project_name );

    layout_group = new QHBoxLayout( 0, 0, 6, "layout_group"); 

    layout_left = new QVBoxLayout( 0, 0, 6, "layout_left"); 

    user_name = new KBSPanelField( this, "user_name" );
    layout_left->addWidget( user_name );

    total_credit = new KBSPanelField( this, "total_credit" );
    layout_left->addWidget( total_credit );
    layout_group->addLayout( layout_left );
    spacer_middle = new QSpacerItem( 20, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout_group->addItem( spacer_middle );

    layout_right = new QVBoxLayout( 0, 0, 6, "layout_right"); 

    team_name = new KBSPanelField( this, "team_name" );
    layout_right->addWidget( team_name );

    avg_credit = new KBSPanelField( this, "avg_credit" );
    layout_right->addWidget( avg_credit );
    layout_group->addLayout( layout_right );
    KBSUserContentLayout->addLayout( layout_group );

    create_time = new KBSPanelField( this, "create_time" );
    KBSUserContentLayout->addWidget( create_time );
    spacer = new QSpacerItem( 41, 170, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSUserContentLayout->addItem( spacer );

    layout_button = new QHBoxLayout( 0, 0, 6, "layout_button"); 
    spacer_buttons = new QSpacerItem( 310, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_button->addItem( spacer_buttons );

    calendar_button = new KPushButton( this, "calendar_button" );
    layout_button->addWidget( calendar_button );

    chart_button = new KPushButton( this, "chart_button" );
    layout_button->addWidget( chart_button );
    KBSUserContentLayout->addLayout( layout_button );
    languageChange();
    resize( QSize(183, 74).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSUserContent::~KBSUserContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSUserContent::languageChange()
{
    setCaption( tr2i18n( "KBSUserContent" ) );
    setIconText( QString::null );
    calendar_button->setText( tr2i18n( "Calendar" ) );
    chart_button->setText( tr2i18n( "Chart" ) );
}

#include "kbsusercontent.moc"
