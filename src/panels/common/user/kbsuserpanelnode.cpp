/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>
#include <kpushbutton.h>

#include <kbsboincdata.h>
#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>
#include <kbsusercontent.h>

#include <kbscreditcalendarwindow.h>
#include <kbsuserstatisticswindow.h>

#include "kbsuserpanelnode.h"

const QString DefaultUserPath= "home.php";
const QString DefaultTeamPath= "home.php";

class KBSUserPanelFactory : KGenericFactory<KBSUserPanelNode,KBSTreeNode>
{
  public:
    KBSUserPanelFactory() : KGenericFactory<KBSUserPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsuserpanel, KBSUserPanelFactory());


KBSUserPanelNode::KBSUserPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args)
                : KBSPanelNode(parent, name), m_project(args[0]),
                  m_calendar(NULL), m_chart(NULL)
{
  m_userLink = (args.count() >= 2) ? args[1] : QString::null;
  m_teamLink = (args.count() >= 3) ? args[2] : QString::null;
  
  if(NULL != monitor())
    connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
}

KBSUserPanelNode::~KBSUserPanelNode()
{
  if(NULL != m_calendar) m_calendar->detachMonitor(monitor());
  if(NULL != m_chart) m_chart->detachMonitor(monitor());
}

QString KBSUserPanelNode::name() const
{
  return i18n("User Statistics");
}

QStringList KBSUserPanelNode::icons() const
{
  return QStringList("user");
}

QString KBSUserPanelNode::project() const
{
  return m_project;
}

KBSPanel *KBSUserPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSUserContent *content = new KBSUserContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSUserPanelNode::setupContent(KBSUserContent *content)
{
  content->project_name->setName(i18n("Project:"));
  content->user_name->setName(i18n("User:"));
  content->team_name->setName(i18n("Team:"));
  content->create_time->setName(i18n("Registered:"));
  content->total_credit->setName(i18n("Total credits:"));
  content->avg_credit->setName(i18n("Average daily credits:"));
  connect(content->calendar_button, SIGNAL(clicked()), this, SLOT(showCalendar()));
  connect(content->chart_button, SIGNAL(clicked()), this, SLOT(showStatistics()));
  
  updateContent();
}

void KBSUserPanelNode::updateContent()
{
  KURL project_url, user_url, team_url;
  QString project_name, user_name, team_name, create_time, total_credit, avg_credit;
  
  KLocale *locale = KGlobal::locale();

  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL; 
  if(NULL != state && !m_project.isEmpty())
  {
    project_name = state->project[m_project].project_name;
    project_url = KURL(state->project[m_project].master_url);
    user_name = state->project[m_project].user_name;
    
    if(!m_userLink.isEmpty())
      if(m_userLink.contains("%1"))
        user_url = KURL(m_userLink.arg(KURL::encode_string(user_name)));
      else
        user_url = KURL(m_userLink);
    else if(project_url.isValid())
      user_url = KURL(project_url, DefaultUserPath);
    
    team_name = state->project[m_project].team_name;
    
    if(!team_name.isEmpty()) {
      if(!m_teamLink.isEmpty())
        if(m_teamLink.contains("%1"))
          team_url = KURL(m_teamLink.arg(KURL::encode_string(team_name)));
        else
          team_url = KURL(m_teamLink);
    } else
      team_name = i18n("none");
    
    create_time = locale->formatDate(state->project[m_project].user.create_time.date(), true);
    total_credit = locale->formatNumber(state->project[m_project].user.total_credit, 2);
    avg_credit = locale->formatNumber(state->project[m_project].user.expavg_credit, 2);
  }
  else
  {
    user_name = create_time = total_credit = avg_credit = i18n("unknown");
    team_name = QString::null;
  }
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSUserContent *content = static_cast<KBSUserContent*>(it.current()->content());
      
      if(project_url.isValid())
        content->project_name->setURL(project_url.prettyURL(), project_name,
                                      i18n("Click to visit the project main page"));
      else
        content->project_name->setText(project_name);
      
      if(user_url.isValid())
        content->user_name->setURL(user_url.prettyURL(), user_name,
                                   i18n("Click to visit your user account page"));
      else
        content->user_name->setText(user_name);
      
      if(team_url.isValid())
        content->team_name->setURL(team_url.prettyURL(), team_name,
                                   i18n("Click to visit your team statistics page"));
      else
        content->team_name->setText(team_name);
      
      content->create_time->setText(create_time);
      content->total_credit->setText(total_credit);
      content->avg_credit->setText(avg_credit);
    }
}

void KBSUserPanelNode::showCalendar()
{
  if(NULL == m_calendar)
    m_calendar = KBSCreditCalendarWindow::window(monitor(), m_project);
  if(NULL == m_calendar) return;
  
  if(!m_calendar->isVisible()) m_calendar->show();
}

void KBSUserPanelNode::showStatistics()
{
  if(NULL == m_chart)
    m_chart = KBSUserStatisticsWindow::window(monitor(), m_project);
  if(NULL == m_chart) return;
  
  if(!m_chart->isVisible()) m_chart->show();
}

#include "kbsuserpanelnode.moc"
