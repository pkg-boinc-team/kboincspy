/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <kcalendarsystem.h>
#include <kgenericfactory.h>
#include <klocale.h>
#include <kpushbutton.h>
#include <kurl.h>

#include <kbsboincdata.h>
#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbscpdndata.h>
#include <kbscpdnmonitor.h>

#include <kbscpdntricklecontent.h>

#include "kbscpdntricklepanelnode.h"

using namespace KBSBOINC;

class KBSCPDNTricklePanelFactory : KGenericFactory<KBSCPDNTricklePanelNode,KBSTreeNode>
{
  public:
    KBSCPDNTricklePanelFactory() : KGenericFactory<KBSCPDNTricklePanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbscpdntricklepanel, KBSCPDNTricklePanelFactory());

KBSCPDNTricklePanelNode::KBSCPDNTricklePanelNode(KBSTreeNode *parent, const char *name, const QStringList &args)
                   : KBSPanelNode(parent, name), m_workunit(args[0])
{
  setupMonitor();
  if(NULL != monitor())
    connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
}

QString KBSCPDNTricklePanelNode::name() const
{
  return i18n("Trickle");
}

QStringList KBSCPDNTricklePanelNode::icons() const
{
  return QStringList("trickle");
}

QString KBSCPDNTricklePanelNode::project() const
{
  return m_project;
}

KBSCPDNMonitor *KBSCPDNTricklePanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

KBSPanel *KBSCPDNTricklePanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSCPDNTrickleContent *content = new KBSCPDNTrickleContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSCPDNTricklePanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  
  m_projectMonitor = static_cast<KBSCPDNMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSCPDNTricklePanelNode::setupContent(KBSCPDNTrickleContent *content)
{
  content->trickle->setName(i18n("Trickle:"));
  content->model_date->setName(i18n("Model date:"));
  content->phase->setName(i18n("Phase:"));
  content->timesteps->setName(i18n("TimeStep:"));
  content->completed->setName(i18n("Completed:"));
  content->total_time->setName(i18n("Total time:"));
  content->speed->setName(i18n("Speed:"));
  content->remaining_time->setName(i18n("Remaining time:"));
  
  if(NULL != m_projectMonitor && m_projectMonitor->canShowGraphics())
    connect(content->show_graphics, SIGNAL(clicked()), this, SLOT(showGraphics()));
  else
    content->show_graphics->hide();
  
  updateContent();
}

void KBSCPDNTricklePanelNode::updateContent()
{
  QString trickle, model_date, phase, timesteps, completed, speed, total_time, remaining_time;
  
  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  
  double secs, prog;
  if(NULL != state)
  {
    QString result = state->workunit[m_workunit].result_name;
    int task = result.isEmpty() ? -1 : state->active_task_set.index(result);
    
    if(task >= 0) {
      secs = state->active_task_set.active_task[task].current_cpu_time;
      prog = state->active_task_set.active_task[task].fraction_done;
    } else if(!result.isEmpty())
      switch(state->result[result].state) {
        case 0:
          secs = prog = 0.0;
          break;
        case 1:
          secs = prog = 0.0;
          break;
        case 2:
          secs = prog = 0.0;
          break;
        case 3:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          break;
        case 4:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          break;
        case 5:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          break;
        default:
          secs = prog = 0.0;
          break;
      }
    else
      secs = prog = 0.0;  
  }
  else
    secs = prog = 0.0;
  
  const KBSCPDNUMID *cpdnResult =
    (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit) : NULL;
  
  if(NULL != cpdnResult)
  {
    KLocale *locale = KGlobal::locale();
    
    phase = QString::number(cpdnResult->ph);
    
    unsigned ts = cpdnResult->ts;
    if(0 == ts) ts = 1;
    
    trickle = i18n("%1 of %2").arg((ts - 1) / KBSCPDNTrickleTS + 1)
                              .arg(KBSCPDNPhaseTrickles);
    
    timesteps = i18n("%1 of %2")
                  .arg(locale->formatNumber(ts, 0))
                  .arg(locale->formatNumber(KBSCPDNPhaseTS, 0));
    
    const unsigned trickle_ts = (ts - 1) % KBSCPDNTrickleTS + 1;
    const double trickle_prog = double(trickle_ts) / KBSCPDNTrickleTS;
    
    completed = i18n("%1%").arg(locale->formatNumber(trickle_prog * 1e2, 2));
    
    const unsigned total_ts = KBSCPDNPhaseTS * (cpdnResult->ph - 1) + ts;
    const double speed_num = secs / total_ts;
    
    if(speed_num > 0.0)
      speed = i18n("%1 secs / TS").arg(locale->formatNumber(speed_num, 2));
    else
      speed = i18n("unknown");
      
    if(speed_num > 0.0) {
      total_time = formatTime(speed_num * KBSCPDNTrickleTS);
      remaining_time = formatTime(speed_num * (KBSCPDNTrickleTS - trickle_ts));
    } else
      total_time = remaining_time = i18n("unknown");
  
    model_date = i18n("%1 %2 %3 %4")
                   .arg(cpdnResult->day)
                   .arg(locale->calendar()->monthName(QDate(cpdnResult->yr, cpdnResult->mth, 1), false))
                   .arg(cpdnResult->yr)
                   .arg(locale->formatTime(QTime(cpdnResult->hr, cpdnResult->min, cpdnResult->sec)));
  }
  else
    trickle = model_date = phase = timesteps = completed = speed = total_time = remaining_time
            = i18n("unknown");
  
  bool show_graphics = (NULL != m_projectMonitor) ? m_projectMonitor->canShowGraphics(m_workunit) : false;
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSCPDNTrickleContent *content = static_cast<KBSCPDNTrickleContent*>(it.current()->content());
      
      content->trickle->setText(trickle);
      content->model_date->setText(model_date);
      content->phase->setText(phase);
      content->timesteps->setText(timesteps);
      content->completed->setText(completed);
      content->total_time->setText(total_time);
      content->speed->setText(speed);
      content->remaining_time->setText(remaining_time);
      content->show_graphics->setEnabled(show_graphics);
    }
}

void KBSCPDNTricklePanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit)
    updateContent();
}

void KBSCPDNTricklePanelNode::showGraphics()
{
  m_projectMonitor->showGraphics(m_workunit);
}

#include "kbscpdntricklepanelnode.moc"
