#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbscpdntricklecontent.ui'
**
** Created: Mon Feb 6 18:29:05 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbscpdntricklecontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qpushbutton.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSCPDNTrickleContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSCPDNTrickleContent::KBSCPDNTrickleContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSCPDNTrickleContent" );
    KBSCPDNTrickleContentLayout = new QVBoxLayout( this, 0, 6, "KBSCPDNTrickleContentLayout"); 

    phase = new KBSPanelField( this, "phase" );
    KBSCPDNTrickleContentLayout->addWidget( phase );

    layout_group = new QHBoxLayout( 0, 0, 6, "layout_group"); 

    layout_left = new QVBoxLayout( 0, 0, 6, "layout_left"); 

    trickle = new KBSPanelField( this, "trickle" );
    layout_left->addWidget( trickle );

    completed = new KBSPanelField( this, "completed" );
    layout_left->addWidget( completed );

    speed = new KBSPanelField( this, "speed" );
    layout_left->addWidget( speed );
    layout_group->addLayout( layout_left );
    spacer_middle = new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout_group->addItem( spacer_middle );

    layout_right = new QVBoxLayout( 0, 0, 6, "layout_right"); 

    timesteps = new KBSPanelField( this, "timesteps" );
    layout_right->addWidget( timesteps );

    total_time = new KBSPanelField( this, "total_time" );
    layout_right->addWidget( total_time );

    remaining_time = new KBSPanelField( this, "remaining_time" );
    layout_right->addWidget( remaining_time );
    layout_group->addLayout( layout_right );
    KBSCPDNTrickleContentLayout->addLayout( layout_group );

    model_date = new KBSPanelField( this, "model_date" );
    KBSCPDNTrickleContentLayout->addWidget( model_date );
    spacer = new QSpacerItem( 41, 70, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSCPDNTrickleContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_buttons = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_buttons );

    show_graphics = new KPushButton( this, "show_graphics" );
    layout_buttons->addWidget( show_graphics );
    KBSCPDNTrickleContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(129, 75).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSCPDNTrickleContent::~KBSCPDNTrickleContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSCPDNTrickleContent::languageChange()
{
    setCaption( tr2i18n( "KBSTricklesContent" ) );
    setIconText( QString::null );
    show_graphics->setText( tr2i18n( "Show &Graphics" ) );
    show_graphics->setAccel( QKeySequence( tr2i18n( "Alt+G" ) ) );
}

#include "kbscpdntricklecontent.moc"
