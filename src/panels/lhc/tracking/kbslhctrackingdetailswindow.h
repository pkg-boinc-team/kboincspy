/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCTRACKINGDETAILSWINDOW_H
#define KBSLHCTRACKINGDETAILSWINDOW_H

#include <qptrdict.h>
#include <qslider.h>

#include <kactionclasses.h>
#include <kmainwindow.h>

class KBSLHCParticleView;
class KBSLHCProjectMonitor;
class KBSLHCTaskMonitor;

class KBSLHCTrackingDetailsWindow : public KMainWindow
{
  Q_OBJECT
  public:
    static KBSLHCTrackingDetailsWindow *window(KBSLHCTaskMonitor *taskMonitor);
    
    virtual KBSLHCProjectMonitor *projectMonitor() const;
    virtual KBSLHCTaskMonitor *taskMonitor() const;
    
  public slots:
    virtual void play();
    virtual void pause();
    virtual void stop();
    
    virtual void rewind();
    virtual void forward();
    virtual void setProgress(int turn);
  
  protected:
    KBSLHCTrackingDetailsWindow(KBSLHCTaskMonitor *taskMonitor,
                                QWidget *parent=0, const char *name=0);
    
    virtual void timerEvent(QTimerEvent *e);
    virtual void closeEvent(QCloseEvent *e);
    virtual bool queryClose();
  
  protected slots:
    virtual void detach();
  
  private:
    virtual void setupWidgets();
    virtual void setupActions();

  private slots:
    void orientWidgets(Orientation o);
    
    void activateHeader();
    void activateCrossSectionView();
    void activateProgress();
    
    void updateProgress(const QString &workunit);
    void updateProgress(unsigned set);
  
  protected:
    KBSLHCParticleView *m_view;
    QSlider *m_progress;
  
  private:
    KBSLHCTaskMonitor *m_taskMonitor;
    KToggleAction *show_header;
    KToggleAction *cross_section_view;
    KToggleAction *player_pause;
    int m_timer, m_step, m_count;
    
    static QPtrDict<KBSLHCTrackingDetailsWindow> s_windows;
};

#endif
