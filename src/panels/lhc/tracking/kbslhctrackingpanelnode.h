/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCTRACKINGPANELNODE_H
#define KBSLHCTRACKINGPANELNODE_H

#include <kbspanelnode.h>

class KBSLHCProjectMonitor;
class KBSLHCTaskMonitor;
class KBSLHCTrackingContent;
class KBSLHCTrackingDetailsWindow;

class KBSLHCTrackingPanelNode : public KBSPanelNode
{
  Q_OBJECT
  public:
    KBSLHCTrackingPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args);
    
    virtual QString name() const;
    virtual QStringList icons() const;

    virtual KBSPanel *createPanel(QWidget *parent=0);
    
    virtual int task() const;
    virtual QString result() const;
    virtual QString workunit() const;
    virtual QString project() const;
    
  protected:
    virtual KBSLHCProjectMonitor *projectMonitor() const;
    virtual KBSLHCTaskMonitor *taskMonitor() const;
    
  private:
    void setupMonitor();
    void setupContent(KBSLHCTrackingContent *panel);
    
    void attachTaskMonitor(unsigned task);
  
  private slots:
    void detachProjectMonitor();
    void detachTaskMonitor();

    void updateContent();
    void updateContent(unsigned task, const QString &result, bool activated);
    void updateContent(const QString &workunit);
    void updateContent(unsigned set);
    
    void showDetails();
  
  protected:
    QString m_project, m_workunit, m_result;
    KBSLHCProjectMonitor *m_projectMonitor;
    KBSLHCTaskMonitor *m_taskMonitor;
    KBSLHCTrackingDetailsWindow *m_details;
};

#endif
