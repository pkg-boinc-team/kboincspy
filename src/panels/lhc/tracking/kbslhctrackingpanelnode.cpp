/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qgl.h>

#include <kgenericfactory.h>
#include <klocale.h>
#include <kpushbutton.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbslhcprojectmonitor.h>
#include <kbslhctaskmonitor.h>

#include <kbsboincdata.h>
#include <kbslhcdata.h>

#include <kbslhctrackingcontent.h>
#include <kbslhctrackingdetailswindow.h>

#include "kbslhctrackingpanelnode.h"

class KBSLHCTrackingPanelFactory : KGenericFactory<KBSLHCTrackingPanelNode,KBSTreeNode>
{
  public:
    KBSLHCTrackingPanelFactory() : KGenericFactory<KBSLHCTrackingPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbslhctrackingpanel, KBSLHCTrackingPanelFactory());

KBSLHCTrackingPanelNode::KBSLHCTrackingPanelNode(KBSTreeNode *parent, const char *name,
                                                         const QStringList &args)
                       : KBSPanelNode(parent, name), m_workunit(args[0]),
                         m_projectMonitor(NULL), m_taskMonitor(NULL), m_details(NULL)
{
  setupMonitor();
}

QString KBSLHCTrackingPanelNode::name() const
{
  return i18n("Tracking");
}
    
QStringList KBSLHCTrackingPanelNode::icons() const
{
  return QStringList("tracking");
}

KBSPanel *KBSLHCTrackingPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSLHCTrackingContent *content = new KBSLHCTrackingContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

int KBSLHCTrackingPanelNode::task() const
{
  return (NULL != m_taskMonitor) ? int(m_taskMonitor->task()) : -1;
}

QString KBSLHCTrackingPanelNode::workunit() const
{
  return m_workunit;
}

QString KBSLHCTrackingPanelNode::result() const
{
  return m_result;
}

QString KBSLHCTrackingPanelNode::project() const
{
  return m_project;
}

KBSLHCProjectMonitor *KBSLHCTrackingPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

KBSLHCTaskMonitor *KBSLHCTrackingPanelNode::taskMonitor() const
{
  return m_taskMonitor;
}

void KBSLHCTrackingPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  connect(monitor(), SIGNAL(resultActivated(unsigned, const QString &, bool)),
          this, SLOT(updateContent(unsigned, const QString &, bool)));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  
  m_projectMonitor = static_cast<KBSLHCProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL != m_projectMonitor) {
    connect(m_projectMonitor, SIGNAL(destroy()), this, SLOT(detachProjectMonitor()));
    connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
            this, SLOT(updateContent(const QString &)));
  }
  
  m_result = state->workunit[m_workunit].result_name;
  
  const int task = state->active_task_set.index(m_result);
  if(task >= 0) attachTaskMonitor(task);
}

void KBSLHCTrackingPanelNode::setupContent(KBSLHCTrackingContent *content)
{
  content->mode->setName(i18n("Tracking mode:"));
  content->turns->setName(i18n("Turn:"));
  content->amplitudes->setName(i18n("Amplitude increments:"));
  content->amplitude_range->setName(i18n("Horizontal amplitude range:"));
  content->avg_energy->setName(i18n("Average initial energy:"));
  content->mass->setName(i18n("Rest mass:"));
  
  if(QGLFormat::hasOpenGL())
    connect(content->details_button, SIGNAL(clicked()), this, SLOT(showDetails()));
  else
    content->details_button->hide();
  
  updateContent();
}

void KBSLHCTrackingPanelNode::attachTaskMonitor(unsigned task)
{
  m_taskMonitor = static_cast<KBSLHCTaskMonitor*>(monitor()->taskMonitor(task));
  if(NULL == m_taskMonitor) return;
  
  connect(m_taskMonitor, SIGNAL(updatedSet(unsigned)), this, SLOT(updateContent(unsigned)));
  connect(m_taskMonitor, SIGNAL(destroyed()), this, SLOT(detachTaskMonitor()));
}

void KBSLHCTrackingPanelNode::detachProjectMonitor()
{
  m_projectMonitor = NULL;
}

void KBSLHCTrackingPanelNode::detachTaskMonitor()
{
  m_taskMonitor = NULL;
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
      static_cast<KBSLHCTrackingContent*>(it.current()
        ->content())->details_button->setEnabled(false);

  m_details = NULL;
}

void KBSLHCTrackingPanelNode::updateContent()
{
  QString mode, turns, amplitudes, amplitude_range, mass, avg_energy;
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL != state && (m_result.isEmpty() || NULL == m_taskMonitor))
  {
    m_result = state->workunit[m_workunit].result_name;
    
    const int task = state->active_task_set.index(m_result);
    if(task >= 0) attachTaskMonitor(task);
  }
  
  const KBSLHCResult *lhcResult = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit)
                                                             : NULL;
  
  const KBSLHCState *lhcState = (NULL != m_taskMonitor) ? m_taskMonitor->state() : NULL;
  
  if(NULL != lhcResult)
  {
    KLocale *locale = KGlobal::locale();
  
    const KBSLHCTrac &trac = lhcResult->unit3.trac;
    
    if(0 == trac.ird)
      amplitudes = i18n("%1 of equal size").arg(trac.napx);
    else
      amplitudes = i18n("%1 exponentially decreasing").arg(trac.napx);
    
    amplitude_range = i18n("%1 to %2")
                        .arg(locale->formatNumber(trac.amp[0], 6))
                        .arg(locale->formatNumber(trac.amp[1], 6));
    
    unsigned current_turns;
    const unsigned total_turns = trac.numl + trac.numlr;
    
    QValueList<unsigned> sets;
    if(NULL != lhcState) sets = lhcState->output.keys();
    if(!sets.isEmpty())
    {  
      const KBSLHCOutput &output = lhcState->output[sets.first()];
      current_turns = unsigned(output.data[0].count() - 1) * trac.nwr[2];
    }
    else if(!m_result.isEmpty())
    {
      const KBSBOINCResult &result = state->result[m_result];
      current_turns = (result.state >= 3) ? total_turns : 0;
    }
    else
      current_turns = 0;
    
    const KBSLHCInit &init = lhcResult->unit3.init;
    
    mode = (1 == init.itra) ? i18n("singleton particle") : i18n("twin particles");
    
    const double avg_egy = (init.coord[0].egy + init.coord[1].egy) / 2;
    avg_energy = QString("%1 MeV").arg(locale->formatNumber(avg_egy, 0));
    
    const KBSLHCSync &sync = lhcResult->unit3.sync;
    
    mass = QString("%1 MeV/c\xb2").arg(locale->formatNumber(sync.pma, 3));
    
    turns = i18n("%1 of %2 (%3 Km)")
              .arg(locale->formatNumber(current_turns, 0))
              .arg(locale->formatNumber(total_turns, 0))
              .arg(locale->formatNumber(current_turns * sync.tlen / 1e3, 0));
  }
  else
    mode = turns = amplitudes = amplitude_range = mass = avg_energy = i18n("unknown");
    
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSLHCTrackingContent *content = static_cast<KBSLHCTrackingContent*>(it.current()->content());
      
      content->mode->setText(mode);
      content->turns->setText(turns);
      content->amplitudes->setText(amplitudes);
      content->amplitude_range->setText(amplitude_range);
      content->avg_energy->setText(avg_energy);
      content->mass->setText(mass);
      content->details_button->setEnabled(m_taskMonitor != NULL);
    }
}

void KBSLHCTrackingPanelNode::updateContent(unsigned, const QString &result, bool)
{
  if(result == m_result)
    updateContent();
}

void KBSLHCTrackingPanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit)
    updateContent();
}

void KBSLHCTrackingPanelNode::updateContent(unsigned set)
{
  if(0 == set)
    updateContent();
}

void KBSLHCTrackingPanelNode::showDetails()
{
  if(NULL == m_taskMonitor) return;
  
  if(NULL == m_details)
    m_details = KBSLHCTrackingDetailsWindow::window(m_taskMonitor);
  
  if(!m_details->isVisible()) m_details->show();
}

#include "kbslhctrackingpanelnode.moc"
