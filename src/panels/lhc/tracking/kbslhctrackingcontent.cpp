#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbslhctrackingcontent.ui'
**
** Created: Mon Feb 6 18:29:07 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbslhctrackingcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSLHCTrackingContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSLHCTrackingContent::KBSLHCTrackingContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSLHCTrackingContent" );
    KBSLHCTrackingContentLayout = new QVBoxLayout( this, 0, 6, "KBSLHCTrackingContentLayout"); 

    mode = new KBSPanelField( this, "mode" );
    KBSLHCTrackingContentLayout->addWidget( mode );

    turns = new KBSPanelField( this, "turns" );
    KBSLHCTrackingContentLayout->addWidget( turns );

    amplitude_range = new KBSPanelField( this, "amplitude_range" );
    KBSLHCTrackingContentLayout->addWidget( amplitude_range );

    amplitudes = new KBSPanelField( this, "amplitudes" );
    KBSLHCTrackingContentLayout->addWidget( amplitudes );

    avg_energy = new KBSPanelField( this, "avg_energy" );
    KBSLHCTrackingContentLayout->addWidget( avg_energy );

    mass = new KBSPanelField( this, "mass" );
    KBSLHCTrackingContentLayout->addWidget( mass );
    spacer = new QSpacerItem( 20, 70, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSLHCTrackingContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_details = new QSpacerItem( 160, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_details );

    details_button = new KPushButton( this, "details_button" );
    layout_buttons->addWidget( details_button );
    KBSLHCTrackingContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(124, 78).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSLHCTrackingContent::~KBSLHCTrackingContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSLHCTrackingContent::languageChange()
{
    setCaption( tr2i18n( "KBSLHCTrackingContent" ) );
    details_button->setText( tr2i18n( "&Details" ) );
    details_button->setAccel( QKeySequence( tr2i18n( "Alt+D" ) ) );
}

#include "kbslhctrackingcontent.moc"
