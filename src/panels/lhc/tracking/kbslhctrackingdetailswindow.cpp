/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qclipboard.h>

#include <kapplication.h>
#include <kfiledialog.h>
#include <kio/netaccess.h>
#include <klocale.h>
#include <kmenubar.h>
#include <kmessagebox.h>
#include <ktempfile.h>
#include <ktoolbar.h>

#include <kbsboincmonitor.h>

#include <kbslhcprojectmonitor.h>
#include <kbslhctaskmonitor.h>

#include <kbsboincdata.h>
#include <kbslhcdata.h>

#include <kbslhcparticleview.h>

#include "kbslhctrackingdetailswindow.h"

QPtrDict<KBSLHCTrackingDetailsWindow> KBSLHCTrackingDetailsWindow::s_windows
  = QPtrDict<KBSLHCTrackingDetailsWindow>();

KBSLHCTrackingDetailsWindow *KBSLHCTrackingDetailsWindow::window(KBSLHCTaskMonitor *taskMonitor)
{
  KBSLHCTrackingDetailsWindow *out = s_windows.find(taskMonitor);
  
  if(NULL == out) {
    out = new KBSLHCTrackingDetailsWindow(taskMonitor);
    s_windows.insert(taskMonitor, out);
  }
  
  return out;
}

const unsigned KBSLHCTrackingTimerInterval = 100;

KBSLHCTrackingDetailsWindow::KBSLHCTrackingDetailsWindow(KBSLHCTaskMonitor *taskMonitor,
                                                         QWidget *parent, const char *name)
                           : KMainWindow(parent, name),
                             m_view(new KBSLHCParticleView(this)),
                             m_progress(new QSlider(Horizontal, 0)),
                             m_taskMonitor(taskMonitor), m_timer(0), m_step(0), m_count(0)
{
  setCaption(i18n("Tracking Details - %1").arg(taskMonitor->workunit()));
  setCentralWidget(m_view);
  
  setMinimumSize(275, 275);
  
  connect(taskMonitor, SIGNAL(destroyed()), this, SLOT(detach()));
  
  setupWidgets();
  setupActions();
}

KBSLHCProjectMonitor *KBSLHCTrackingDetailsWindow::projectMonitor() const
{
  KBSBOINCMonitor *boincMonitor = m_taskMonitor->boincMonitor();
  const QString project = m_taskMonitor->project();
  
  return static_cast<KBSLHCProjectMonitor*>(boincMonitor->projectMonitor(project));
}

KBSLHCTaskMonitor *KBSLHCTrackingDetailsWindow::taskMonitor() const
{
  return m_taskMonitor;
}

void KBSLHCTrackingDetailsWindow::play()
{
  if(0 != m_timer) return;
  
  m_timer = startTimer(KBSLHCTrackingTimerInterval);
  
  setProgress(0);
  
  stateChanged("playing");
}

void KBSLHCTrackingDetailsWindow::pause()
{
  if(player_pause->isChecked())
  {
    if(0 == m_timer) return;
    killTimer(m_timer);
    m_timer = 0;
  }
  else
  {
    if(0 != m_timer) return;
    m_timer = startTimer(KBSLHCTrackingTimerInterval);
  }
}

void KBSLHCTrackingDetailsWindow::stop()
{
  if(0 == m_timer) return;
  
  killTimer(m_timer);
  m_timer = 0;
  
  player_pause->setChecked(false);
  stateChanged("playing", StateReverse);
}

void KBSLHCTrackingDetailsWindow::rewind()
{
  setProgress(m_view->turn() - m_progress->lineStep());
}

void KBSLHCTrackingDetailsWindow::forward()
{
  setProgress(m_view->turn() + m_progress->lineStep());
}

void KBSLHCTrackingDetailsWindow::setProgress(int turn)
{
  if(turn >= 0 && turn <= (m_step * m_count)) {
    m_progress->setValue(turn);
    m_view->setTurn(turn);
  } else {
    killTimer(m_timer);
    m_timer = 0;
    
    player_pause->setChecked(false);
    stateChanged("playing", StateReverse);
  }
}

void KBSLHCTrackingDetailsWindow::timerEvent(QTimerEvent *e)
{
  if(e->timerId() != m_timer) return;
  
  m_view->addTurn();
  
  const int turn = m_view->turn();
  if(turn >= (m_step * m_count))
    stop();
  else if(turn % (10 * m_step) == 0)
    m_progress->addStep();
}

void KBSLHCTrackingDetailsWindow::closeEvent(QCloseEvent *e)
{
  stop();
  KMainWindow::closeEvent(e);
}

bool KBSLHCTrackingDetailsWindow::queryClose()
{
  if(!kapp->sessionSaving()) {
    hide();
    return false;
  }
  else
    return true;
}

void KBSLHCTrackingDetailsWindow::detach()
{
  s_windows.remove(m_taskMonitor);
  
  close();
  destroy();
}

void KBSLHCTrackingDetailsWindow::setupWidgets()
{
  connect(m_progress, SIGNAL(valueChanged(int)), this, SLOT(setProgress(int)));
  
  updateProgress(m_taskMonitor->workunit());
  connect(projectMonitor(), SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateProgress(const QString &)));
    
  updateProgress(0);
  connect(m_taskMonitor, SIGNAL(updatedSet(unsigned)),
          this, SLOT(updateProgress(unsigned)));
}

void KBSLHCTrackingDetailsWindow::setupActions()
{
  show_header = new KToggleAction(i18n("Show &Header"), Key_H,
                                  this, SLOT(activateHeader()),
                                  actionCollection(), "show_header");
  show_header->setChecked(m_view->isHeaderVisible());
  
  cross_section_view = new KRadioAction(i18n("&Cross Section"), 0,
                                        this, SLOT(activateCrossSectionView()),
                                        actionCollection(), "cross_section_view");
  cross_section_view->setExclusiveGroup("particleview");
                                       
  (new KAction(i18n("&Add Particle"), Key_Plus, m_view, SLOT(addParticle()),
               actionCollection(), "particle_add"))
    ->setEnabled(m_view->particles() < m_view->maxParticles());
  
  (new KAction(i18n("&Remove Particle"), Key_Minus, m_view, SLOT(removeParticle()),
               actionCollection(), "particle_remove"))
    ->setEnabled(m_view->particles() > 0);
  
  KStdAction::close(this, SLOT(close()), actionCollection())->setText(i18n("Close &Window"));

  new KAction(i18n("Play"), 0, this, SLOT(play()), actionCollection(), "player_play");
  
  player_pause = new KToggleAction(i18n("Pause"), 0, this, SLOT(pause()),
                                  actionCollection(), "player_pause");
  
  new KAction(i18n("Stop"), 0, this, SLOT(stop()), actionCollection(), "player_stop");
  
  new KAction(i18n("Rewind"), 0, this, SLOT(rewind()), actionCollection(), "player_rew");
  
  new KAction(i18n("Fast Forward"), 0, this, SLOT(forward()), actionCollection(), "player_fwd");
    
  (new KWidgetAction(m_progress, i18n("Progress"), 0, this, SLOT(activateProgress()),
                     actionCollection(), "player_progress"))
    ->setAutoSized(true);
  
  createGUI("kbslhctrackingdetailsui.rc", false);
  
  connect(toolBar(), SIGNAL(orientationChanged(Orientation)),
          this, SLOT(orientWidgets(Orientation)));
  
  player_pause->setChecked(false);
  stateChanged("playing", StateReverse);
  
  delete menuBar();
}

void KBSLHCTrackingDetailsWindow::orientWidgets(Orientation o)
{
  m_progress->setOrientation(o);
}

void KBSLHCTrackingDetailsWindow::activateHeader()
{
  m_view->setHeaderVisible(show_header->isChecked());
}

void KBSLHCTrackingDetailsWindow::activateCrossSectionView()
{
  m_view->setType(KBSLHCParticleView::CrossSection);
}

void KBSLHCTrackingDetailsWindow::activateProgress()
{
}

void KBSLHCTrackingDetailsWindow::updateProgress(const QString &workunit)
{
  if(0 != m_step || workunit != m_taskMonitor->workunit()) return;
  
  const KBSLHCResult *lhcResult = projectMonitor()->result(workunit);
  if(NULL == lhcResult) return;
  
  const KBSLHCTrac &trac = lhcResult->unit3.trac;
  
  const unsigned num = trac.numl + trac.numlr;
  const unsigned nwr = trac.nwr[2];
  const unsigned napx = trac.napx;
  
  const KBSLHCInit &init = lhcResult->unit3.init;
  
  const unsigned itra = init.itra;
  
  m_view->setMaxTurn(num);
  m_view->setMaxSets(itra, napx);
  m_view->setInitialEnergy(init.coord[0].egy, init.coord[1].egy);
  
  m_progress->setMinValue(0);
  m_progress->setMaxValue(num);
  m_progress->setSteps(nwr, 10 * nwr);
  
  m_step = nwr;
}

void KBSLHCTrackingDetailsWindow::updateProgress(unsigned set)
{
  if(0 != set) return;
  
  const KBSLHCState *lhcState = m_taskMonitor->state();
  if(NULL == lhcState) return;
  
  const KBSLHCOutput &output = lhcState->output[set];
  m_count = int(output.data[0].count() - 1);
}

#include "kbslhctrackingdetailswindow.moc"
