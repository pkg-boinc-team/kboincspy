/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCPARTICLEVIEW_H
#define KBSLHCPARTICLEVIEW_H

#include <qfont.h>
#include <qgl.h>
#include <qimage.h>
#include <qpixmap.h>

class KBSLHCTrackingDetailsWindow;

class KBSLHCParticleView : public QGLWidget
{
  Q_OBJECT
  public:
    KBSLHCParticleView(KBSLHCTrackingDetailsWindow *parent, const char *name=0);
    virtual ~KBSLHCParticleView();
    
    enum Type { CrossSection };
    virtual Type type() const;
    virtual void setType(Type type);
    
    virtual bool isHeaderVisible() const;
    virtual void setHeaderVisible(bool visible);
        
    virtual unsigned turn() const;
    virtual void addTurn();
    virtual void setTurn(unsigned turn);
    
    virtual unsigned maxTurn() const;
    virtual void setMaxTurn(unsigned turn);
    
    virtual unsigned particles() const;
    virtual void setParticles(unsigned particles);
    
    virtual unsigned trackingMode() const;
    virtual unsigned maxSets() const;
    virtual unsigned maxParticles() const;    
    virtual void setMaxSets(unsigned mode, unsigned sets);
    
    virtual double initialEnergy(unsigned position) const;
    virtual void setInitialEnergy(double energy1, double energy2);
  
  public slots:
    virtual void addParticle();
    virtual void removeParticle();
 
  protected:
    virtual void initializeGL();
    virtual void initializeGL(int type, bool reverse=false);
    virtual void resizeGL(int width, int height);
    virtual void paintGL();
    
    virtual void contextMenuEvent(QContextMenuEvent *e);
  
    virtual void drawHeader();
    virtual void drawString(int x, int y, const QString &string);
    virtual void drawParticle2D(int x, int y, double alpha);
    
  private:
    void initTextures();
    void initFont();
    void initShapes();
  
  protected:
    unsigned m_turn, m_maxTurn,
             m_particles, m_mode, m_maxSets;
    double m_energy[2];
  
  private:
    Type m_type;
    bool m_headerVisible;
    
    enum { FontTexture, ParticleTexture, Textures };
    GLuint *m_texture;
    
    GLUquadricObj *m_quadric;
    enum { Particle2D, Floor, Ring, Magnet, Tube, Scene, Particle3D, Shapes };
    GLuint m_font, m_base;
    
    static const QImage s_texture[Textures];
};

#endif
