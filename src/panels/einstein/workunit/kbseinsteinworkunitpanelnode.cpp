/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbseinsteinprojectmonitor.h>
#include <kbseinsteinworkunitcontent.h>

#include <kbsboincdata.h>
#include <kbseinsteindata.h>

#include "kbseinsteinworkunitpanelnode.h"

using namespace KBSEinstein;

class KBSEinsteinWorkunitPanelFactory : KGenericFactory<KBSEinsteinWorkunitPanelNode,KBSTreeNode>
{
  public:
    KBSEinsteinWorkunitPanelFactory() : KGenericFactory<KBSEinsteinWorkunitPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbseinsteinworkunitpanel, KBSEinsteinWorkunitPanelFactory());

KBSEinsteinWorkunitPanelNode::KBSEinsteinWorkunitPanelNode(KBSTreeNode *parent, const char *name,
                                                           const QStringList &args)
                            : KBSPanelNode(parent, name),
                              m_workunit(args[0]), m_projectMonitor(NULL)
{
  setupMonitor();
}

QString KBSEinsteinWorkunitPanelNode::name() const
{
  return i18n("Work Unit");
}

QStringList KBSEinsteinWorkunitPanelNode::icons() const
{
  return QStringList("workunit");
}

KBSPanel *KBSEinsteinWorkunitPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSEinsteinWorkunitContent *content = new KBSEinsteinWorkunitContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

QString KBSEinsteinWorkunitPanelNode::project() const
{
  return m_project;
}

QString KBSEinsteinWorkunitPanelNode::workunit() const
{
  return m_workunit;
}

KBSEinsteinProjectMonitor *KBSEinsteinWorkunitPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

void KBSEinsteinWorkunitPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  if(m_project.isEmpty()) return;
  
  m_projectMonitor = static_cast<KBSEinsteinProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSEinsteinWorkunitPanelNode::setupContent(KBSEinsteinWorkunitContent *content)
{
  content->wu_name->setName(i18n("Work unit name:"));
  content->wu_name->setSqueezedText(m_workunit);
  content->time_0->setName(i18n("Scan A time:"));
  content->time_1->setName(i18n("Scan B time:"));
  content->freq_range->setName(i18n("Frequency range:"));
  content->ra_range->setName(i18n("RA range:"));
  content->dec_range->setName(i18n("Dec range:"));
  
  updateContent();
}

void KBSEinsteinWorkunitPanelNode::updateContent()
{
  QString time[2], freq_range, freq_range_tip, ra_range, ra_range_tip, dec_range, dec_range_tip;
  
  const KBSEinsteinResult *result = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit) : NULL;
  
  if(NULL != result)
  {
    KLocale *locale = KGlobal::locale();
    
    for(unsigned i = 0; i < 2; ++i)
      if(result->args.h[i].contains("startTime") && result->args.h[i].contains("endTime"))
      {
        QDateTime startTime = parseGPSDate(result->args.h[i]["startTime"]),
                  endTime = parseGPSDate(result->args.h[i]["endTime"]);
        double hours = double(startTime.secsTo(endTime)) / 3600;
        
        time[i] = i18n("%1 to %2 (%3 hrs)")
                    .arg(locale->formatDateTime(startTime))
                    .arg(locale->formatDateTime(endTime))
                    .arg(locale->formatNumber(hours, 1));
      }
      else
        time[i] = i18n("unknown");    
    
    if(result->args.h[0].contains("f"))
    {
      const double freq_start = result->args.h[0]["f"].toDouble();
      const double freq_end = freq_start + result->conf.FreqBand;
      const double freq_step = result->conf.dFreq;
      
      freq_range = i18n("%1 to %2 %3")
                     .arg(locale->formatNumber(freq_start, 3))
                     .arg(locale->formatNumber(freq_end, 3))
                     .arg("Hz");
      freq_range_tip = i18n("in %1 %2 increments")
                         .arg(locale->formatNumber(freq_step * 1e6, 3))
                         .arg("\xb5Hz");
    }
    else
    {
      freq_range = i18n("unknown");
      freq_range_tip = QString::null;
    }
    
    const double ra_start = 12.0 * result->conf.Alpha / M_PI;
    const double ra_end = ra_start + 12.0 * result->conf.AlphaBand / M_PI;
    const double ra_step = 12.0 * result->conf.dAlpha / M_PI;
    
    ra_range = i18n("%1 to %2 (%3)")
                 .arg(formatRA(ra_start))
                 .arg(formatRA(ra_end))
                 .arg(formatRA(ra_end - ra_start));
    ra_range_tip = i18n("in steps of %1").arg(formatRA(ra_step));
    
    const double dec_start = 180.0 * result->conf.Delta / M_PI;
    const double dec_end = dec_start + 180.0 * result->conf.DeltaBand / M_PI;
    const double dec_step = 180.0 * result->conf.dDelta / M_PI;
    
    dec_range = i18n("%1 to %2 (%3)")
                 .arg(formatDec(dec_start))
                 .arg(formatDec(dec_end))
                 .arg(formatDec(dec_end - dec_start, false));
    dec_range_tip = i18n("in steps of %1").arg(formatDec(dec_step, false));    
  }
  else {
    time[0] = time[1] = freq_range = ra_range = dec_range = i18n("unknown");
    freq_range_tip = ra_range_tip = dec_range_tip = QString::null;
  }

  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSEinsteinWorkunitContent *content = static_cast<KBSEinsteinWorkunitContent*>(it.current()->content());
      
      content->time_0->setText(time[0]);
      content->time_1->setText(time[1]);
      content->freq_range->setText(freq_range);
      content->freq_range->setTooltip(freq_range_tip);
      content->ra_range->setText(ra_range);
      content->ra_range->setTooltip(ra_range_tip);
      content->dec_range->setText(dec_range);
      content->dec_range->setTooltip(dec_range_tip);
    }
}

void KBSEinsteinWorkunitPanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit) updateContent();
}

#include "kbseinsteinworkunitpanelnode.moc"
