#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbseinsteinworkunitcontent.ui'
**
** Created: Mon Feb 6 18:29:06 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbseinsteinworkunitcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSEinsteinWorkunitContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSEinsteinWorkunitContent::KBSEinsteinWorkunitContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSEinsteinWorkunitContent" );
    KBSEinsteinWorkunitContentLayout = new QVBoxLayout( this, 0, 6, "KBSEinsteinWorkunitContentLayout"); 

    wu_name = new KBSPanelField( this, "wu_name" );
    KBSEinsteinWorkunitContentLayout->addWidget( wu_name );

    time_0 = new KBSPanelField( this, "time_0" );
    KBSEinsteinWorkunitContentLayout->addWidget( time_0 );

    time_1 = new KBSPanelField( this, "time_1" );
    KBSEinsteinWorkunitContentLayout->addWidget( time_1 );

    freq_range = new KBSPanelField( this, "freq_range" );
    KBSEinsteinWorkunitContentLayout->addWidget( freq_range );

    ra_range = new KBSPanelField( this, "ra_range" );
    KBSEinsteinWorkunitContentLayout->addWidget( ra_range );

    dec_range = new KBSPanelField( this, "dec_range" );
    KBSEinsteinWorkunitContentLayout->addWidget( dec_range );
    spacer = new QSpacerItem( 16, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSEinsteinWorkunitContentLayout->addItem( spacer );
    languageChange();
    resize( QSize(124, 45).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSEinsteinWorkunitContent::~KBSEinsteinWorkunitContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSEinsteinWorkunitContent::languageChange()
{
    setCaption( tr2i18n( "KBSEinsteinWorkunitContent" ) );
    setIconText( QString::null );
}

#include "kbseinsteinworkunitcontent.moc"
