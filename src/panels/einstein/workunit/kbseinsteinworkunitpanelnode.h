/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSEINSTEINWORKUNITPANELNODE_H
#define KBSEINSTEINWORKUNITPANELNODE_H

#include <kbspanelnode.h>

class KBSEinsteinProjectMonitor;
class KBSEinsteinWorkunitContent;

class KBSEinsteinWorkunitPanelNode : public KBSPanelNode
{
  Q_OBJECT
  public:
    KBSEinsteinWorkunitPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args);
    
    virtual QString name() const;
    virtual QStringList icons() const;

    virtual KBSPanel *createPanel(QWidget *parent=0);
  
  protected:
    virtual QString project() const;
    virtual QString workunit() const;
    
    virtual KBSEinsteinProjectMonitor *projectMonitor() const;
    
  private:
    void setupMonitor();
    void setupContent(KBSEinsteinWorkunitContent *panel);
    
  private slots:
    void updateContent();
    void updateContent(const QString &workunit);
    
  private:
    QString m_project, m_workunit;
    KBSEinsteinProjectMonitor *m_projectMonitor;
};

#endif
