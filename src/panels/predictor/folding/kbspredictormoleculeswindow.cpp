/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qimage.h>
#include <qpixmap.h>
#include <qpopupmenu.h>

#include <kactionclasses.h>
#include <kglobal.h>
#include <klocale.h>

#include <kbsboincmonitor.h>

#include <kbspredictordata.h>

#include <kbspredictorprojectmonitor.h>
#include <kbspredictormoleculeview.h>

#include "kbspredictormoleculeswindow.h"

QDict<KBSPredictorMoleculesWindow> KBSPredictorMoleculesWindow::s_windows
  = QDict<KBSPredictorMoleculesWindow>();

KBSPredictorMoleculesWindow *KBSPredictorMoleculesWindow::window(const QString &workunit)
{
  KBSPredictorMoleculesWindow *out = s_windows.find(workunit);
  
  if(NULL == out) {
    out = new KBSPredictorMoleculesWindow(workunit);
    s_windows.insert(workunit, out);
  }
  
  return out;
}

KBSPredictorMoleculesWindow::KBSPredictorMoleculesWindow(const QString &workunit,
                                                         QWidget *parent, const char *name)
                           : KBSStandardWindow(parent, name),
                             m_view(new KBSPredictorMoleculeView(this)), m_workunit(workunit)
{
  setCaption(i18n("Molecules - %1").arg(workunit));
  
  setCentralWidget(m_view);
  
  setupActions();
}

QString KBSPredictorMoleculesWindow::workunit() const
{
  return m_workunit;
}

void KBSPredictorMoleculesWindow::attachProjectMonitor(KBSPredictorProjectMonitor *projectMonitor)
{
  if(m_projectMonitors.containsRef(projectMonitor)) return;
  
  m_projectMonitors.append(projectMonitor);
  
  if(m_projectMonitors.count() == 1) {
    connectProjectMonitor(projectMonitor);
    update();
  }
}

void KBSPredictorMoleculesWindow::detachProjectMonitor()
{
  KBSPredictorProjectMonitor *projectMonitor = m_projectMonitors.first();
  
  if(NULL != projectMonitor) {
    disconnectProjectMonitor();
    m_projectMonitors.removeRef(projectMonitor);
  }
  
  projectMonitor = m_projectMonitors.first();
  
  if(NULL != projectMonitor) {
    connectProjectMonitor(projectMonitor);
    update();
  } else {
    close();
    destroy();
    
    s_windows.remove(m_workunit);
  }
}

QPixmap KBSPredictorMoleculesWindow::pixmap()
{
  return m_view->pixmap();
}

void KBSPredictorMoleculesWindow::activateStyle(int style)
{
  m_view->model()->setStyle(KBSPredictorMoleculeModel::Style(style));
  updateActions();
}

void KBSPredictorMoleculesWindow::activateColoring(int coloring)
{
  m_view->model()->setColoring(KBSPredictorMoleculeModel::Coloring(coloring));
  updateActions();
}

void KBSPredictorMoleculesWindow::update()
{
  KBSPredictorProjectMonitor *projectMonitor = m_projectMonitors.first();
  if(NULL == projectMonitor) return;
  
  const KBSPredictorResult *predictorResult = projectMonitor->result(m_workunit);
  if(NULL == predictorResult) return;
  
  const bool input = input_molecule->isChecked();
  KBSPredictorMoleculeModel *model = m_view->model();
  
  if(predictorResult->app_type == MFOLD)
    if(input) {
      model->setChain(predictorResult->mfold.monsster.init_chain);
      model->setSeq(predictorResult->mfold.monsster.seq);
    } else if(predictorResult->mfold.monsster.final.pdb.atom.count() > 0)
      model->setPDB(predictorResult->mfold.monsster.final.pdb);
    else {
      model->setChain(predictorResult->mfold.monsster.final.chain);
      model->setSeq(predictorResult->mfold.monsster.seq);
    }
  else
    model->setPDB(input ? predictorResult->charmm.protein.pdb
                        : predictorResult->charmm.protein.final_pdb);

  updateActions();
}

void KBSPredictorMoleculesWindow::update(const QString &workunit)
{
  if(workunit == m_workunit) update();
}

void KBSPredictorMoleculesWindow::setupActions()
{
  input_molecule = new KRadioAction(i18n("View &Workunit"), 0,
                                    this, SLOT(update()), actionCollection());
  input_molecule->setExclusiveGroup("dataset");

  output_molecule = new KRadioAction(i18n("View &Result"), 0,
                                     this, SLOT(update()), actionCollection());
  output_molecule->setExclusiveGroup("dataset");
  
  input_molecule->setChecked(true);

  KActionMenu *styleMenu = new KActionMenu(i18n("S&tyle"), this);
  
  m_style = new QSignalMapper(this);
  connect(m_style, SIGNAL(mapped(int)), this, SLOT(activateStyle(int)));
  
  {
    const QString name[] = {
                             I18N_NOOP("&Backbone"),
                             I18N_NOOP("S&pline"),
                             I18N_NOOP("&Wireframe"),
                             I18N_NOOP("&Sticks"),
                             I18N_NOOP("S&pacefill"),
                             I18N_NOOP("B&all && Stick")
                           };
    
    for(unsigned i = KBSPredictorMoleculeModel::Backbone;
        i <= KBSPredictorMoleculeModel::BallAndStick; ++i)
    {
      style[i] = new KRadioAction(i18n(name[i]), 0,
                                  m_style, SLOT(map()), actionCollection());
      style[i]->setExclusiveGroup("style");
      styleMenu->insert(style[i], i);
      m_style->setMapping(style[i], i);
    }
  }
  
  KActionMenu *coloringMenu = new KActionMenu(i18n("C&oloring"), this);
  
  m_coloring = new QSignalMapper(this);
  connect(m_coloring, SIGNAL(mapped(int)), this, SLOT(activateColoring(int)));
  
  {
    const QString name[] = {
                             I18N_NOOP("&Monochrome"),
                             I18N_NOOP("&Group"),
                             I18N_NOOP("&Shapely"),
                             I18N_NOOP("&CPK")
                           };
    
    for(unsigned i = KBSPredictorMoleculeModel::Monochrome;
        i <= KBSPredictorMoleculeModel::CPK; ++i)
    {
      coloring[i] = new KRadioAction(i18n(name[i]), 0,
                                     m_coloring, SLOT(map()), actionCollection());
      coloring[i]->setExclusiveGroup("coloring");
      coloringMenu->insert(coloring[i], i);
      m_coloring->setMapping(coloring[i], i);
    }
  }
  
  updateActions();
  
  KBSStandardWindow::setupActions();
  
  QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
  
  context->insertSeparator(0);
  coloringMenu->plug(context, 0);
  styleMenu->plug(context, 0);
  
  context->insertSeparator(0);
  output_molecule->plug(context, 0);
  input_molecule->plug(context, 0);
}

void KBSPredictorMoleculesWindow::updateActions()
{
  KBSPredictorMoleculeModel *model = m_view->model();
  
  for(unsigned i = model->Backbone; i <= model->BallAndStick; ++i)
    style[i]->setEnabled(model->isSupportedStyle(KBSPredictorMoleculeModel::Style(i)));
  if(!style[model->style()]->isChecked())
    style[model->style()]->setChecked(true);
  
  for(unsigned i = model->Monochrome; i <= model->CPK; ++i)
    coloring[i]->setEnabled(model->isSupportedColoring(KBSPredictorMoleculeModel::Coloring(i)));
  if(!coloring[model->coloring()]->isChecked())
    coloring[model->coloring()]->setChecked(true);
}

void KBSPredictorMoleculesWindow::connectProjectMonitor(KBSPredictorProjectMonitor *projectMonitor)
{
  connect(projectMonitor, SIGNAL(updatedResult(const QString &)), this, SLOT(update(const QString &)));
  connect(projectMonitor, SIGNAL(destroyed()), this, SLOT(detachProjectMonitor()));
  
  KBSBOINCMonitor *boincMonitor = projectMonitor->boincMonitor();
  
  connect(boincMonitor, SIGNAL(stateUpdated()), this, SLOT(update()));
}

void KBSPredictorMoleculesWindow::disconnectProjectMonitor()
{
  disconnect(this, SLOT(update(const QString &)));
  disconnect(this, SLOT(detachProjectMonitor()));
  disconnect(this, SLOT(update()));
}

#include "kbspredictormoleculeswindow.moc"
