#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbspredictormfoldcontent.ui'
**
** Created: Mon Feb 6 18:29:08 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbspredictormfoldcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSPredictorMFoldContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSPredictorMFoldContent::KBSPredictorMFoldContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSPredictorMFoldContent" );
    KBSPredictorMFoldContentLayout = new QVBoxLayout( this, 0, 6, "KBSPredictorMFoldContentLayout"); 

    layout_grid = new QGridLayout( 0, 1, 1, 0, 6, "layout_grid"); 

    restraints = new KBSPanelField( this, "restraints" );

    layout_grid->addMultiCellWidget( restraints, 5, 5, 0, 1 );

    icycle = new KBSPanelField( this, "icycle" );

    layout_grid->addWidget( icycle, 4, 1 );

    casp = new KBSPanelField( this, "casp" );

    layout_grid->addMultiCellWidget( casp, 0, 0, 0, 1 );

    atoms = new KBSPanelField( this, "atoms" );

    layout_grid->addWidget( atoms, 1, 1 );

    ncycle = new KBSPanelField( this, "ncycle" );

    layout_grid->addWidget( ncycle, 4, 0 );

    phase = new KBSPanelField( this, "phase" );

    layout_grid->addMultiCellWidget( phase, 2, 2, 0, 1 );

    groups = new KBSPanelField( this, "groups" );

    layout_grid->addWidget( groups, 1, 0 );

    tsteps = new KBSPanelField( this, "tsteps" );

    layout_grid->addWidget( tsteps, 3, 1 );

    random = new KBSPanelField( this, "random" );

    layout_grid->addWidget( random, 3, 0 );
    KBSPredictorMFoldContentLayout->addLayout( layout_grid );
    spacer = new QSpacerItem( 1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSPredictorMFoldContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    buttons_spacer = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( buttons_spacer );

    molecules_button = new KPushButton( this, "molecules_button" );
    layout_buttons->addWidget( molecules_button );
    KBSPredictorMFoldContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(124, 80).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSPredictorMFoldContent::~KBSPredictorMFoldContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSPredictorMFoldContent::languageChange()
{
    setCaption( tr2i18n( "KBSPredictorMFoldContent" ) );
    molecules_button->setText( tr2i18n( "&Molecules" ) );
    molecules_button->setAccel( QKeySequence( tr2i18n( "Alt+M" ) ) );
}

#include "kbspredictormfoldcontent.moc"
