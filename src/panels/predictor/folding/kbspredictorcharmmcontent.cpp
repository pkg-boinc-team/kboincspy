#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbspredictorcharmmcontent.ui'
**
** Created: Mon Feb 6 18:29:08 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbspredictorcharmmcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSPredictorCharmmContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSPredictorCharmmContent::KBSPredictorCharmmContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSPredictorCharmmContent" );
    KBSPredictorCharmmContentLayout = new QVBoxLayout( this, 0, 6, "KBSPredictorCharmmContentLayout"); 

    layout_grid = new QGridLayout( 0, 1, 1, 0, 6, "layout_grid"); 

    nsteps = new KBSPanelField( this, "nsteps" );

    layout_grid->addWidget( nsteps, 4, 0 );

    restraints = new KBSPanelField( this, "restraints" );

    layout_grid->addWidget( restraints, 4, 1 );

    phase = new KBSPanelField( this, "phase" );

    layout_grid->addMultiCellWidget( phase, 2, 2, 0, 1 );

    ntemps = new KBSPanelField( this, "ntemps" );

    layout_grid->addWidget( ntemps, 3, 1 );

    atoms = new KBSPanelField( this, "atoms" );

    layout_grid->addWidget( atoms, 1, 1 );

    groups = new KBSPanelField( this, "groups" );

    layout_grid->addWidget( groups, 1, 0 );

    casp = new KBSPanelField( this, "casp" );

    layout_grid->addMultiCellWidget( casp, 0, 0, 0, 1 );

    trange = new KBSPanelField( this, "trange" );

    layout_grid->addMultiCellWidget( trange, 5, 5, 0, 1 );

    random = new KBSPanelField( this, "random" );

    layout_grid->addWidget( random, 3, 0 );
    KBSPredictorCharmmContentLayout->addLayout( layout_grid );
    spacer = new QSpacerItem( 1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSPredictorCharmmContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    buttons_spacer = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( buttons_spacer );

    molecules_button = new KPushButton( this, "molecules_button" );
    layout_buttons->addWidget( molecules_button );
    KBSPredictorCharmmContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(124, 80).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSPredictorCharmmContent::~KBSPredictorCharmmContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSPredictorCharmmContent::languageChange()
{
    setCaption( tr2i18n( "KBSPredictorCharmmContent" ) );
    molecules_button->setText( tr2i18n( "&Molecules" ) );
    molecules_button->setAccel( QKeySequence( tr2i18n( "Alt+M" ) ) );
}

#include "kbspredictorcharmmcontent.moc"
