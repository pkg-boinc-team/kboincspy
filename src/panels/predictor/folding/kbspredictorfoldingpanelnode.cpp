/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qregexp.h>

#include <kgenericfactory.h>
#include <kglobal.h>
#include <klocale.h>
#include <kpushbutton.h>
#include <kurl.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbspredictorprojectmonitor.h>
#include <kbspredictortaskmonitor.h>

#include <kbspredictorcharmmcontent.h>
#include <kbspredictormfoldcontent.h>
#include <kbspredictormoleculeswindow.h>

#include "kbspredictorfoldingpanelnode.h"

class KBSPredictorFoldingPanelFactory : KGenericFactory<KBSPredictorFoldingPanelNode,KBSTreeNode>
{
  public:
    KBSPredictorFoldingPanelFactory() : KGenericFactory<KBSPredictorFoldingPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbspredictorfoldingpanel, KBSPredictorFoldingPanelFactory());


const QString PredictorCASPTargetURL  =
  I18N_NOOP("http://predictioncenter.llnl.gov/casp%1/targets/templates/t%2.doc.html");

KBSPredictorFoldingPanelNode::KBSPredictorFoldingPanelNode(KBSTreeNode *parent, const char *name,
                                                           const QStringList &args)
                            : KBSPanelNode(parent, name), m_appType(MFOLD), m_workunit(args[0]),
                              m_projectMonitor(NULL), m_molecules(NULL)
{
  setupMonitor();
}

QString KBSPredictorFoldingPanelNode::name() const
{
  return i18n("Folding");
}

QStringList KBSPredictorFoldingPanelNode::icons() const
{
  return QStringList("folding");
}

KBSPredictorAppType KBSPredictorFoldingPanelNode::appType() const
{
  return m_appType;
}

KBSPredictorProjectMonitor *KBSPredictorFoldingPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

KBSPanel *KBSPredictorFoldingPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  
  if(MFOLD == m_appType) {
    KBSPredictorMFoldContent *content = new KBSPredictorMFoldContent(out);
    out->setContent(content);
    setupContent(content);
  } else {
    KBSPredictorCharmmContent *content = new KBSPredictorCharmmContent(out);
    out->setContent(content);
    setupContent(content);
  }
  
  return out;
}

KURL KBSPredictorFoldingPanelNode::targetURL(unsigned casp, unsigned target) const
{
  if(casp <= 2 || target <= 0) return KURL();
  
  return KURL(i18n(PredictorCASPTargetURL).arg(casp).arg(QString().sprintf("%04u", target)));
}

unsigned KBSPredictorFoldingPanelNode::parseCASP(unsigned target)
{
  if(target == 0) return 0;
  else if(target <= 42) return 2;
  else if(target <= 85) return 3;
  else if(target <= 128) return 4;
  else if(target <= 195) return 5;
  else if(target <= 282) return 6;
  else return 7;
}

unsigned KBSPredictorFoldingPanelNode::parseTarget(const QString &workunit)
{
  int i;

  i = workunit.find(QRegExp("[tT]\\d+"));
  if(i < 0) return 0;
  
  const unsigned start = i + 1;
  
  i = workunit.find(QRegExp("\\D"), start);
  if(i < 0) i = workunit.length();
  
  const unsigned len = i - start;
  
  return workunit.mid(start, len).toUInt(0, 10);
}

void KBSPredictorFoldingPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_appType = state->workunit[m_workunit].app_name.contains("mfold") ? MFOLD : CHARMM;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  
  m_projectMonitor = static_cast<KBSPredictorProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSPredictorFoldingPanelNode::setupContent(KBSPredictorMFoldContent *content)
{
  const unsigned target = parseTarget(m_workunit);
  const unsigned casp = parseCASP(target);
  
  const QString caspName = (casp > 0) ? QString("CASP%1").arg(casp) : "CASP";
  
  content->casp->setName(i18n("%1 target:").arg(caspName));
  
  if(target > 0)
  {
    const QString targetName = QString().sprintf("T%04u", target);
    const KURL targetURL = this->targetURL(casp, target);

    if(targetURL.isValid())
      content->casp->setURL(targetURL.prettyURL(),
                            targetName, i18n("Click to visit the web page"));
    else
      content->casp->setText(targetName);
  }
  else
    content->casp->setText(i18n("unknown"));
  
  content->phase->setName(i18n("Phase:"));
  content->phase->setText(i18n("protein structure assembly (MONSSTER)"));
  content->random->setName(i18n("Random seed:"));
  content->tsteps->setName(i18n("Temperature steps:"));
  content->ncycle->setName(i18n("Cycles/tstep:"));
  content->icycle->setName(i18n("Moves/cycle:"));
  content->groups->setName(i18n("Residues:"));
  content->atoms->setName(i18n("Atoms:"));
  content->restraints->setName(i18n("Restraints:"));
  
  connect(content->molecules_button, SIGNAL(clicked()), this, SLOT(showMolecules()));
  
  updateContent();
}

void KBSPredictorFoldingPanelNode::setupContent(KBSPredictorCharmmContent *content)
{
  const unsigned target = parseTarget(m_workunit);
  const unsigned casp = parseCASP(target);
  
  const QString caspName = (casp > 0) ? QString("CASP%1").arg(casp) : "CASP";
  
  content->casp->setName(i18n("%1 target:").arg(caspName));
  
  if(target > 0)
  {
    const QString targetName = QString().sprintf("T%04u", target);
    const KURL targetURL = this->targetURL(casp, target);

    if(targetURL.isValid())
      content->casp->setURL(targetURL.prettyURL(),
                            targetName, i18n("Click to visit the web page"));
    else
      content->casp->setText(targetName);
  }
  else
    content->casp->setText(i18n("unknown"));
  
  content->phase->setName(i18n("Phase:"));
  content->phase->setText(i18n("protein refinement (CHARMM)"));
  content->random->setName(i18n("Random seed:"));
  content->ntemps->setName(i18n("Temperature steps:"));
  content->nsteps->setName(i18n("Cycles/tstep:"));
  content->groups->setName(i18n("Residues:"));
  content->atoms->setName(i18n("Atoms:"));
  content->restraints->setName(i18n("NOE Restraints:"));
  content->trange->setName(i18n("Temperature range:"));
  
  connect(content->molecules_button, SIGNAL(clicked()), this, SLOT(showMolecules()));
  
  updateContent();
}

void KBSPredictorFoldingPanelNode::updateContent()
{
  QString random, tsteps, icycle, ncycle, groups, groups_tooltip, atoms, restraints, trange;
  
  const KBSPredictorResult *predictorResult =
    (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit) : NULL;
  
  if(NULL != predictorResult && predictorResult->app_type == m_appType)
    if(MFOLD == m_appType)
    {
      KLocale *locale = KGlobal::locale();
      
      const KBSPredictorMonssterInput &input = predictorResult->mfold.monsster.input;
      
      if(input.tsteps > 0) {
        random = QString::number(input.random);
        tsteps = locale->formatNumber(input.tsteps, 0);
        ncycle = locale->formatNumber(input.ncycle, 0);
        icycle = locale->formatNumber(input.icycle, 0);
      } else
        random = tsteps = ncycle = icycle = i18n("unknown");
      
      const KBSPredictorMonssterSeq &seq = predictorResult->mfold.monsster.seq;
      
      if(!seq.groups.isEmpty()) {
        groups = locale->formatNumber(seq.groups.count(), 0);
        atoms = locale->formatNumber(seq.atoms, 0);
        groups_tooltip = seq.toString();
      } else {
        groups = atoms = i18n("unknown");
        groups_tooltip = QString::null;
      }
      
      restraints = locale->formatNumber(predictorResult->mfold.monsster.restraints.count(), 0);
    }
    else
    {
      KLocale *locale = KGlobal::locale();
      
      const KBSPredictorProteinPDB &pdb = predictorResult->charmm.protein.pdb;
      
      if(!pdb.atom.isEmpty()) {
        groups = locale->formatNumber(pdb.groups, 0);
        atoms = locale->formatNumber(pdb.atom.count(), 0);
        groups_tooltip = pdb.toString();
      } else {
        groups = atoms = i18n("unknown");
        groups_tooltip = QString::null;
      }
      
      restraints = locale->formatNumber(predictorResult->charmm.protein.noe.count(), 0);
      
      random = QString::number(2 * predictorResult->charmm.seed.stream + 1);
      
      const KBSPredictorCharmmInp &inp = predictorResult->charmm.inp;
      
      tsteps = (inp.ntemps > 0) ? locale->formatNumber(inp.ntemps, 0) : i18n("unknown");
      ncycle = (inp.nsteps > 0) ? locale->formatNumber(inp.nsteps, 0) : i18n("unknown");

      if(inp.t.high > 0 && inp.t.low > 0)
        trange = i18n("%1K to %2K").arg(locale->formatNumber(inp.t.low, 0))
                                   .arg(locale->formatNumber(inp.t.high, 0));
      else
        trange = i18n("unknown");
    }
  else {
    random = tsteps = icycle = ncycle = groups = atoms = restraints = trange = i18n("unknown");
    groups_tooltip = QString::null;
  }
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
      if(MFOLD == m_appType)
      {
        KBSPredictorMFoldContent *content =
          static_cast<KBSPredictorMFoldContent*>(it.current()->content());
        
        content->random->setText(random);
        content->tsteps->setText(tsteps);
        content->icycle->setText(icycle);
        content->ncycle->setText(ncycle);
        content->groups->setText(groups);
        content->groups->setTooltip(groups_tooltip);
        content->atoms->setText(atoms);
        content->restraints->setText(restraints);
      }
      else
      {
        KBSPredictorCharmmContent *content = 
          static_cast<KBSPredictorCharmmContent*>(it.current()->content());
        
        content->random->setText(random);
        content->ntemps->setText(tsteps);
        content->nsteps->setText(ncycle);
        content->groups->setText(groups);
        content->groups->setTooltip(groups_tooltip);
        content->atoms->setText(atoms);
        content->restraints->setText(restraints);
        content->trange->setText(trange);
      }
}

void KBSPredictorFoldingPanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit)
    updateContent();
}

void KBSPredictorFoldingPanelNode::showMolecules()
{
  if(NULL == m_projectMonitor) return;
  
  if(NULL == m_molecules) {
    m_molecules = KBSPredictorMoleculesWindow::window(m_workunit);
    m_molecules->attachProjectMonitor(m_projectMonitor);
  }
  
  if(!m_molecules->isVisible()) m_molecules->show();
}

#include "kbspredictorfoldingpanelnode.moc"

