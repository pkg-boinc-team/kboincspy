/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPREDICTORMOLECULESWINDOW_H
#define KBSPREDICTORMOLECULESWINDOW_H

#include <qdict.h>
#include <qsignalmapper.h>

#include <kbsstandardwindow.h>

#include <kbspredictormoleculeview.h>

class KBSPredictorProjectMonitor;

class KBSPredictorMoleculesWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSPredictorMoleculesWindow *window(const QString &workunit);
    
    virtual QString workunit() const;
    
    virtual void attachProjectMonitor(KBSPredictorProjectMonitor *projectMonitor);
    
  protected slots:
    virtual void detachProjectMonitor();
  
  protected:
    KBSPredictorMoleculesWindow(const QString &workunit, QWidget *parent=0, const char *name=0);
    
    virtual QPixmap pixmap();
  
  private slots:
    void activateStyle(int style);
    void activateColoring(int coloring);
    
    void update();
    void update(const QString &workunit);
  
  private:
    void setupActions();
    void updateActions();
    
    void connectProjectMonitor(KBSPredictorProjectMonitor *projectMonitor);
    void disconnectProjectMonitor();
  
  protected:
    KBSPredictorMoleculeView *m_view;
    QPtrList<KBSPredictorProjectMonitor> m_projectMonitors;
  
  private:
    QString m_workunit;
    QSignalMapper *m_style, *m_coloring;
    
    KToggleAction *input_molecule, *output_molecule;
    KToggleAction *style[KBSPredictorMoleculeModel::BallAndStick+1];
    KToggleAction *coloring[KBSPredictorMoleculeModel::CPK+1];
    
    static QDict<KBSPredictorMoleculesWindow> s_windows;
};

#endif
