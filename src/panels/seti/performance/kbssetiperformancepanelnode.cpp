/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbsseticalibrator.h>
#include <kbssetiprojectmonitor.h>

#include <kbssetiperformancecontent.h>

#include "kbssetiperformancepanelnode.h"

class KBSSETIPerformancePanelFactory : KGenericFactory<KBSSETIPerformancePanelNode,KBSTreeNode>
{
  public:
    KBSSETIPerformancePanelFactory() : KGenericFactory<KBSSETIPerformancePanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbssetiperformancepanel, KBSSETIPerformancePanelFactory());

KBSSETIPerformancePanelNode::KBSSETIPerformancePanelNode(KBSTreeNode *parent, const char *name,
                                                         const QStringList &args)
                           : KBSPanelNode(parent, name),
                             m_prog(0.0), m_deltaProg(0.0), m_cpu(0.0), m_deltaCPU(0.0), m_ar(0.0),
                             m_task(args[0].toUInt(0, 10)), m_projectMonitor(NULL)
{
  setupMonitor();
}

QString KBSSETIPerformancePanelNode::name() const
{
  return i18n("Performance");
}
    
QStringList KBSSETIPerformancePanelNode::icons() const
{
  return QStringList("performance");
}

KBSPanel *KBSSETIPerformancePanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSSETIPerformanceContent *content = new KBSSETIPerformanceContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

unsigned KBSSETIPerformancePanelNode::task() const
{
  return m_task;
}

QString KBSSETIPerformancePanelNode::workunit() const
{
  return m_workunit;
}

QString KBSSETIPerformancePanelNode::result() const
{
  return m_result;
}

QString KBSSETIPerformancePanelNode::project() const
{
  return m_project;
}

KBSSETIProjectMonitor *KBSSETIPerformancePanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

void KBSSETIPerformancePanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->active_task_set.active_task[m_task]);
  m_result = state->active_task_set.active_task[m_task].result_name;
  m_workunit = state->result[m_result].wu_name;
  
  m_prog = state->active_task_set.active_task[m_task].fraction_done;
  m_cpu = state->active_task_set.active_task[m_task].current_cpu_time;
  
  if(m_project.isEmpty()) return;
  
  m_projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));

  const KBSSETIResult *setiResult = m_projectMonitor->result(m_workunit);
  if(NULL == setiResult) return;
  
  m_ar = setiResult->workunit_header.group_info.data_desc.true_angle_range;
  
  m_prog = KBSSETICalibrator::self()->calibrate(monitor()->url(), m_ar, m_prog);  
}

void KBSSETIPerformancePanelNode::setupContent(KBSSETIPerformanceContent *content)
{
  content->result->setName(i18n("Result name:"));
  content->result->setSqueezedText(m_result);
  content->rate_avg->setName(i18n("Average processing rate:"));
  content->speed_avg->setName(i18n("Average processing speed:"));
  content->rate_instant->setName(i18n("Instantaneous processing rate:"));
  content->speed_instant->setName(i18n("Instanteous processing speed:"));
  content->work->setName(i18n("Floating point operations to process:"));
  
  updateContent();
}

void KBSSETIPerformancePanelNode::updateContent()
{
  QString result_tip, rate_avg, rate_avg_tip, speed_avg,
          rate_instant, rate_instant_tip, speed_instant, work;
  
  KLocale *locale = KGlobal::locale();
    
  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  const KBSSETIResult *setiResult = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit)
                                                               : NULL;
  
  double prog, cpu;
  if(NULL != state) {
    result_tip = state->project[m_project].project_name;
    
    prog = state->active_task_set.active_task[m_task].fraction_done;
    cpu = state->active_task_set.active_task[m_task].current_cpu_time;
  } else
    prog = cpu = 0.0;
  
  double teraFLOPs = 0.0;
  
  if(NULL != state && NULL != setiResult)
  {
    if(0.0 == m_ar) {
      m_ar = setiResult->workunit_header.group_info.data_desc.true_angle_range;
      m_prog = KBSSETICalibrator::self()->calibrate(monitor()->url(), m_ar, m_prog);
    }
    prog = KBSSETICalibrator::self()->calibrate(monitor()->url(), m_ar, prog);
    teraFLOPs = KBSSETIDataDesc::teraFLOPs(m_ar);
    
    double deltaProg = m_deltaProg, deltaCPU = m_deltaCPU;
    if(prog > m_prog && cpu > m_cpu) {
      deltaProg = m_deltaProg = (prog - m_prog);
      deltaCPU = m_deltaCPU = (cpu - m_cpu);
    }
    m_prog = prog; m_cpu = cpu;
    
    if(prog > 0.0 && cpu > 0.0)
    {
      rate_avg = i18n("%1 hours / work unit")
                   .arg(locale->formatNumber(cpu / (prog * 3600), 2));
      rate_avg_tip = i18n("%1% / hour")
                       .arg(locale->formatNumber((prog * 1e2) / (cpu / 3600), 2));
      
      if(teraFLOPs > 0.0)
        speed_avg = i18n("%1 MegaFLOPS")
                      .arg(locale->formatNumber((teraFLOPs * 1e6) / (cpu / prog), 2));
      else
        speed_avg = i18n("unknown");
    }
    else
      rate_avg = speed_avg = i18n("unknown");
    
    if(deltaProg > 0.0 && deltaCPU > 0.0)
    {
      rate_instant = i18n("%1 hours / work unit")
                       .arg(locale->formatNumber(deltaCPU / (deltaProg * 3600), 2));
      rate_instant_tip = i18n("%1% / hour")
                           .arg(locale->formatNumber((deltaProg * 1e2) / (deltaCPU / 3600), 2));
      
      if(teraFLOPs > 0.0)
        speed_instant = i18n("%1 MegaFLOPS")
                          .arg(locale->formatNumber((teraFLOPs * 1e6) / (deltaCPU / deltaProg), 2));
      else
        speed_instant = i18n("unknown");
    }
    else
      rate_instant = speed_instant = i18n("unknown");
  }
  else
  {
    rate_avg = rate_instant = speed_avg = speed_instant = work = i18n("unknown");
    result_tip = rate_avg_tip = rate_instant_tip = QString::null;
  }
  
  work = (teraFLOPs > 0.0) ? i18n("%1 TeraFLOPs").arg(locale->formatNumber(teraFLOPs, 2))
                           : i18n("unknown");
  
  m_prog = prog; m_cpu = cpu;
    
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSSETIPerformanceContent *content = static_cast<KBSSETIPerformanceContent*>(it.current()->content());
      
      content->result->setTooltip(result_tip);
      content->rate_avg->setText(rate_avg);
      content->rate_avg->setTooltip(rate_avg_tip);
      content->rate_instant->setText(rate_instant);
      content->rate_instant->setTooltip(rate_instant_tip);
      content->speed_avg->setText(speed_avg);
      content->speed_instant->setText(speed_instant);
      content->work->setText(work);
    }
}

void KBSSETIPerformancePanelNode::updateContent(const QString &workunit)
{
  if(0.0 == m_ar && workunit == m_workunit) updateContent();
}
  
#include "kbssetiperformancepanelnode.moc"
