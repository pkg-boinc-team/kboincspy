#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbssetiperformancecontent.ui'
**
** Created: Mon Feb 6 18:29:10 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbssetiperformancecontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETIPerformanceContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIPerformanceContent::KBSSETIPerformanceContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIPerformanceContent" );
    KBSSETIPerformanceContentLayout = new QVBoxLayout( this, 0, 6, "KBSSETIPerformanceContentLayout"); 

    result = new KBSPanelField( this, "result" );
    KBSSETIPerformanceContentLayout->addWidget( result );

    rate_avg = new KBSPanelField( this, "rate_avg" );
    KBSSETIPerformanceContentLayout->addWidget( rate_avg );

    speed_avg = new KBSPanelField( this, "speed_avg" );
    KBSSETIPerformanceContentLayout->addWidget( speed_avg );

    rate_instant = new KBSPanelField( this, "rate_instant" );
    KBSSETIPerformanceContentLayout->addWidget( rate_instant );

    speed_instant = new KBSPanelField( this, "speed_instant" );
    KBSSETIPerformanceContentLayout->addWidget( speed_instant );

    work = new KBSPanelField( this, "work" );
    KBSSETIPerformanceContentLayout->addWidget( work );
    spacer = new QSpacerItem( 31, 30, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETIPerformanceContentLayout->addItem( spacer );
    languageChange();
    resize( QSize(124, 45).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIPerformanceContent::~KBSSETIPerformanceContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIPerformanceContent::languageChange()
{
    setCaption( tr2i18n( "KBSPerformanceContent" ) );
}

#include "kbssetiperformancecontent.moc"
