/****************************************************************************
** Form interface generated from reading ui file './kbssetiperformancecontent.ui'
**
** Created: Mon Feb 6 18:29:10 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSSETIPERFORMANCECONTENT_H
#define KBSSETIPERFORMANCECONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;

class KBSSETIPerformanceContent : public QWidget
{
    Q_OBJECT

public:
    KBSSETIPerformanceContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSSETIPerformanceContent();

    KBSPanelField* result;
    KBSPanelField* rate_avg;
    KBSPanelField* speed_avg;
    KBSPanelField* rate_instant;
    KBSPanelField* speed_instant;
    KBSPanelField* work;

protected:
    QVBoxLayout* KBSSETIPerformanceContentLayout;
    QSpacerItem* spacer;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSSETIPERFORMANCECONTENT_H
