#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsseticalibrationcontent.ui'
**
** Created: Mon Feb 6 18:29:09 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsseticalibrationcontent.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qlabel.h>
#include <qframe.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETICalibrationContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETICalibrationContent::KBSSETICalibrationContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETICalibrationContent" );
    KBSSETICalibrationContentLayout = new QVBoxLayout( this, 0, 6, "KBSSETICalibrationContentLayout"); 

    p_name = new QTabWidget( this, "p_name" );
    p_name->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)1, 0, 0, p_name->sizePolicy().hasHeightForWidth() ) );

    tab = new QWidget( p_name, "tab" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tabLayout"); 

    layout_table_low_ar = new QGridLayout( 0, 1, 1, 0, 6, "layout_table_low_ar"); 

    reported_0_0 = new QLabel( tab, "reported_0_0" );
    QFont reported_0_0_font(  reported_0_0->font() );
    reported_0_0_font.setPointSize( 8 );
    reported_0_0->setFont( reported_0_0_font ); 
    reported_0_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_0, 0, 1 );

    effective_0_4 = new QLabel( tab, "effective_0_4" );
    QFont effective_0_4_font(  effective_0_4->font() );
    effective_0_4_font.setPointSize( 8 );
    effective_0_4->setFont( effective_0_4_font ); 
    effective_0_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_4, 2, 5 );

    effective_0_2 = new QLabel( tab, "effective_0_2" );
    QFont effective_0_2_font(  effective_0_2->font() );
    effective_0_2_font.setPointSize( 8 );
    effective_0_2->setFont( effective_0_2_font ); 
    effective_0_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_2, 2, 3 );

    effective_0_3 = new QLabel( tab, "effective_0_3" );
    QFont effective_0_3_font(  effective_0_3->font() );
    effective_0_3_font.setPointSize( 8 );
    effective_0_3->setFont( effective_0_3_font ); 
    effective_0_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_3, 2, 4 );

    effective_low_ar = new QLabel( tab, "effective_low_ar" );
    QFont effective_low_ar_font(  effective_low_ar->font() );
    effective_low_ar_font.setPointSize( 8 );
    effective_low_ar_font.setBold( TRUE );
    effective_low_ar->setFont( effective_low_ar_font ); 

    layout_table_low_ar->addWidget( effective_low_ar, 2, 0 );

    reported_0_6 = new QLabel( tab, "reported_0_6" );
    QFont reported_0_6_font(  reported_0_6->font() );
    reported_0_6_font.setPointSize( 8 );
    reported_0_6->setFont( reported_0_6_font ); 
    reported_0_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_6, 0, 7 );

    reported_0_3 = new QLabel( tab, "reported_0_3" );
    QFont reported_0_3_font(  reported_0_3->font() );
    reported_0_3_font.setPointSize( 8 );
    reported_0_3->setFont( reported_0_3_font ); 
    reported_0_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_3, 0, 4 );

    reported_low_ar = new QLabel( tab, "reported_low_ar" );
    QFont reported_low_ar_font(  reported_low_ar->font() );
    reported_low_ar_font.setPointSize( 8 );
    reported_low_ar_font.setBold( TRUE );
    reported_low_ar->setFont( reported_low_ar_font ); 

    layout_table_low_ar->addWidget( reported_low_ar, 0, 0 );

    effective_0_0 = new QLabel( tab, "effective_0_0" );
    QFont effective_0_0_font(  effective_0_0->font() );
    effective_0_0_font.setPointSize( 8 );
    effective_0_0->setFont( effective_0_0_font ); 
    effective_0_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_0, 2, 1 );

    reported_0_2 = new QLabel( tab, "reported_0_2" );
    QFont reported_0_2_font(  reported_0_2->font() );
    reported_0_2_font.setPointSize( 8 );
    reported_0_2->setFont( reported_0_2_font ); 
    reported_0_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_2, 0, 3 );

    effective_0_6 = new QLabel( tab, "effective_0_6" );
    QFont effective_0_6_font(  effective_0_6->font() );
    effective_0_6_font.setPointSize( 8 );
    effective_0_6->setFont( effective_0_6_font ); 
    effective_0_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_6, 2, 7 );

    effective_0_5 = new QLabel( tab, "effective_0_5" );
    QFont effective_0_5_font(  effective_0_5->font() );
    effective_0_5_font.setPointSize( 8 );
    effective_0_5->setFont( effective_0_5_font ); 
    effective_0_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_5, 2, 6 );

    reported_0_5 = new QLabel( tab, "reported_0_5" );
    QFont reported_0_5_font(  reported_0_5->font() );
    reported_0_5_font.setPointSize( 8 );
    reported_0_5->setFont( reported_0_5_font ); 
    reported_0_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_5, 0, 6 );

    effective_0_1 = new QLabel( tab, "effective_0_1" );
    QFont effective_0_1_font(  effective_0_1->font() );
    effective_0_1_font.setPointSize( 8 );
    effective_0_1->setFont( effective_0_1_font ); 
    effective_0_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( effective_0_1, 2, 2 );

    reported_0_1 = new QLabel( tab, "reported_0_1" );
    QFont reported_0_1_font(  reported_0_1->font() );
    reported_0_1_font.setPointSize( 8 );
    reported_0_1->setFont( reported_0_1_font ); 
    reported_0_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_1, 0, 2 );

    reported_0_4 = new QLabel( tab, "reported_0_4" );
    QFont reported_0_4_font(  reported_0_4->font() );
    reported_0_4_font.setPointSize( 8 );
    reported_0_4->setFont( reported_0_4_font ); 
    reported_0_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_low_ar->addWidget( reported_0_4, 0, 5 );

    line_low_ar = new QFrame( tab, "line_low_ar" );
    QFont line_low_ar_font(  line_low_ar->font() );
    line_low_ar_font.setPointSize( 8 );
    line_low_ar->setFont( line_low_ar_font ); 
    line_low_ar->setFrameShape( QFrame::HLine );
    line_low_ar->setFrameShadow( QFrame::Sunken );
    line_low_ar->setFrameShape( QFrame::HLine );

    layout_table_low_ar->addMultiCellWidget( line_low_ar, 1, 1, 0, 7 );
    tabLayout->addLayout( layout_table_low_ar );

    layout_count_low_ar = new QHBoxLayout( 0, 0, 6, "layout_count_low_ar"); 
    spacer_left_low_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_low_ar->addItem( spacer_left_low_ar );

    label_0 = new QLabel( tab, "label_0" );
    QFont label_0_font(  label_0->font() );
    label_0_font.setPointSize( 8 );
    label_0_font.setItalic( TRUE );
    label_0->setFont( label_0_font ); 
    layout_count_low_ar->addWidget( label_0 );

    count_0 = new QLabel( tab, "count_0" );
    QFont count_0_font(  count_0->font() );
    count_0_font.setPointSize( 8 );
    count_0_font.setItalic( TRUE );
    count_0->setFont( count_0_font ); 
    layout_count_low_ar->addWidget( count_0 );
    spacer_right_low_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_low_ar->addItem( spacer_right_low_ar );
    tabLayout->addLayout( layout_count_low_ar );
    p_name->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( p_name, "tab_2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 11, 6, "tabLayout_2"); 

    layout_table_medium_ar = new QGridLayout( 0, 1, 1, 0, 6, "layout_table_medium_ar"); 

    effective_1_5 = new QLabel( tab_2, "effective_1_5" );
    QFont effective_1_5_font(  effective_1_5->font() );
    effective_1_5_font.setPointSize( 8 );
    effective_1_5->setFont( effective_1_5_font ); 
    effective_1_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_5, 2, 6 );

    reported_1_2 = new QLabel( tab_2, "reported_1_2" );
    QFont reported_1_2_font(  reported_1_2->font() );
    reported_1_2_font.setPointSize( 8 );
    reported_1_2->setFont( reported_1_2_font ); 
    reported_1_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_2, 0, 3 );

    effective_medium_ar = new QLabel( tab_2, "effective_medium_ar" );
    QFont effective_medium_ar_font(  effective_medium_ar->font() );
    effective_medium_ar_font.setPointSize( 8 );
    effective_medium_ar_font.setBold( TRUE );
    effective_medium_ar->setFont( effective_medium_ar_font ); 

    layout_table_medium_ar->addWidget( effective_medium_ar, 2, 0 );

    effective_1_0 = new QLabel( tab_2, "effective_1_0" );
    QFont effective_1_0_font(  effective_1_0->font() );
    effective_1_0_font.setPointSize( 8 );
    effective_1_0->setFont( effective_1_0_font ); 
    effective_1_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_0, 2, 1 );

    reported_1_0 = new QLabel( tab_2, "reported_1_0" );
    QFont reported_1_0_font(  reported_1_0->font() );
    reported_1_0_font.setPointSize( 8 );
    reported_1_0->setFont( reported_1_0_font ); 
    reported_1_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_0, 0, 1 );

    effective_1_1 = new QLabel( tab_2, "effective_1_1" );
    QFont effective_1_1_font(  effective_1_1->font() );
    effective_1_1_font.setPointSize( 8 );
    effective_1_1->setFont( effective_1_1_font ); 
    effective_1_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_1, 2, 2 );

    effective_1_3 = new QLabel( tab_2, "effective_1_3" );
    QFont effective_1_3_font(  effective_1_3->font() );
    effective_1_3_font.setPointSize( 8 );
    effective_1_3->setFont( effective_1_3_font ); 
    effective_1_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_3, 2, 4 );

    effective_1_4 = new QLabel( tab_2, "effective_1_4" );
    QFont effective_1_4_font(  effective_1_4->font() );
    effective_1_4_font.setPointSize( 8 );
    effective_1_4->setFont( effective_1_4_font ); 
    effective_1_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_4, 2, 5 );

    reported_1_5 = new QLabel( tab_2, "reported_1_5" );
    QFont reported_1_5_font(  reported_1_5->font() );
    reported_1_5_font.setPointSize( 8 );
    reported_1_5->setFont( reported_1_5_font ); 
    reported_1_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_5, 0, 6 );

    line_medium_ar = new QFrame( tab_2, "line_medium_ar" );
    QFont line_medium_ar_font(  line_medium_ar->font() );
    line_medium_ar_font.setPointSize( 8 );
    line_medium_ar->setFont( line_medium_ar_font ); 
    line_medium_ar->setFrameShape( QFrame::HLine );
    line_medium_ar->setFrameShadow( QFrame::Sunken );
    line_medium_ar->setFrameShape( QFrame::HLine );

    layout_table_medium_ar->addMultiCellWidget( line_medium_ar, 1, 1, 0, 7 );

    reported_1_4 = new QLabel( tab_2, "reported_1_4" );
    QFont reported_1_4_font(  reported_1_4->font() );
    reported_1_4_font.setPointSize( 8 );
    reported_1_4->setFont( reported_1_4_font ); 
    reported_1_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_4, 0, 5 );

    effective_1_6 = new QLabel( tab_2, "effective_1_6" );
    QFont effective_1_6_font(  effective_1_6->font() );
    effective_1_6_font.setPointSize( 8 );
    effective_1_6->setFont( effective_1_6_font ); 
    effective_1_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_6, 2, 7 );

    reported_1_6 = new QLabel( tab_2, "reported_1_6" );
    QFont reported_1_6_font(  reported_1_6->font() );
    reported_1_6_font.setPointSize( 8 );
    reported_1_6->setFont( reported_1_6_font ); 
    reported_1_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_6, 0, 7 );

    reported_1_1 = new QLabel( tab_2, "reported_1_1" );
    QFont reported_1_1_font(  reported_1_1->font() );
    reported_1_1_font.setPointSize( 8 );
    reported_1_1->setFont( reported_1_1_font ); 
    reported_1_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_1, 0, 2 );

    reported_medium_ar = new QLabel( tab_2, "reported_medium_ar" );
    QFont reported_medium_ar_font(  reported_medium_ar->font() );
    reported_medium_ar_font.setPointSize( 8 );
    reported_medium_ar_font.setBold( TRUE );
    reported_medium_ar->setFont( reported_medium_ar_font ); 

    layout_table_medium_ar->addWidget( reported_medium_ar, 0, 0 );

    effective_1_2 = new QLabel( tab_2, "effective_1_2" );
    QFont effective_1_2_font(  effective_1_2->font() );
    effective_1_2_font.setPointSize( 8 );
    effective_1_2->setFont( effective_1_2_font ); 
    effective_1_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( effective_1_2, 2, 3 );

    reported_1_3 = new QLabel( tab_2, "reported_1_3" );
    QFont reported_1_3_font(  reported_1_3->font() );
    reported_1_3_font.setPointSize( 8 );
    reported_1_3->setFont( reported_1_3_font ); 
    reported_1_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_medium_ar->addWidget( reported_1_3, 0, 4 );
    tabLayout_2->addLayout( layout_table_medium_ar );

    layout_count_medium_ar = new QHBoxLayout( 0, 0, 6, "layout_count_medium_ar"); 
    spacer_left_medium_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_medium_ar->addItem( spacer_left_medium_ar );

    label_1 = new QLabel( tab_2, "label_1" );
    QFont label_1_font(  label_1->font() );
    label_1_font.setPointSize( 8 );
    label_1_font.setItalic( TRUE );
    label_1->setFont( label_1_font ); 
    layout_count_medium_ar->addWidget( label_1 );

    count_1 = new QLabel( tab_2, "count_1" );
    QFont count_1_font(  count_1->font() );
    count_1_font.setPointSize( 8 );
    count_1_font.setItalic( TRUE );
    count_1->setFont( count_1_font ); 
    layout_count_medium_ar->addWidget( count_1 );
    spacer_right_medium_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_medium_ar->addItem( spacer_right_medium_ar );
    tabLayout_2->addLayout( layout_count_medium_ar );
    p_name->insertTab( tab_2, QString::fromLatin1("") );

    TabPage = new QWidget( p_name, "TabPage" );
    TabPageLayout = new QVBoxLayout( TabPage, 11, 6, "TabPageLayout"); 

    layout_table_high_ar = new QGridLayout( 0, 1, 1, 0, 6, "layout_table_high_ar"); 

    effective_2_5 = new QLabel( TabPage, "effective_2_5" );
    QFont effective_2_5_font(  effective_2_5->font() );
    effective_2_5_font.setPointSize( 8 );
    effective_2_5->setFont( effective_2_5_font ); 
    effective_2_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_5, 2, 6 );

    reported_2_2 = new QLabel( TabPage, "reported_2_2" );
    QFont reported_2_2_font(  reported_2_2->font() );
    reported_2_2_font.setPointSize( 8 );
    reported_2_2->setFont( reported_2_2_font ); 
    reported_2_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_2, 0, 3 );

    effective_high_ar = new QLabel( TabPage, "effective_high_ar" );
    QFont effective_high_ar_font(  effective_high_ar->font() );
    effective_high_ar_font.setPointSize( 8 );
    effective_high_ar_font.setBold( TRUE );
    effective_high_ar->setFont( effective_high_ar_font ); 

    layout_table_high_ar->addWidget( effective_high_ar, 2, 0 );

    effective_2_0 = new QLabel( TabPage, "effective_2_0" );
    QFont effective_2_0_font(  effective_2_0->font() );
    effective_2_0_font.setPointSize( 8 );
    effective_2_0->setFont( effective_2_0_font ); 
    effective_2_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_0, 2, 1 );

    reported_2_0 = new QLabel( TabPage, "reported_2_0" );
    QFont reported_2_0_font(  reported_2_0->font() );
    reported_2_0_font.setPointSize( 8 );
    reported_2_0->setFont( reported_2_0_font ); 
    reported_2_0->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_0, 0, 1 );

    effective_2_1 = new QLabel( TabPage, "effective_2_1" );
    QFont effective_2_1_font(  effective_2_1->font() );
    effective_2_1_font.setPointSize( 8 );
    effective_2_1->setFont( effective_2_1_font ); 
    effective_2_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_1, 2, 2 );

    effective_2_3 = new QLabel( TabPage, "effective_2_3" );
    QFont effective_2_3_font(  effective_2_3->font() );
    effective_2_3_font.setPointSize( 8 );
    effective_2_3->setFont( effective_2_3_font ); 
    effective_2_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_3, 2, 4 );

    effective_2_4 = new QLabel( TabPage, "effective_2_4" );
    QFont effective_2_4_font(  effective_2_4->font() );
    effective_2_4_font.setPointSize( 8 );
    effective_2_4->setFont( effective_2_4_font ); 
    effective_2_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_4, 2, 5 );

    reported_2_5 = new QLabel( TabPage, "reported_2_5" );
    QFont reported_2_5_font(  reported_2_5->font() );
    reported_2_5_font.setPointSize( 8 );
    reported_2_5->setFont( reported_2_5_font ); 
    reported_2_5->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_5, 0, 6 );

    line_high_ar = new QFrame( TabPage, "line_high_ar" );
    QFont line_high_ar_font(  line_high_ar->font() );
    line_high_ar_font.setPointSize( 8 );
    line_high_ar->setFont( line_high_ar_font ); 
    line_high_ar->setFrameShape( QFrame::HLine );
    line_high_ar->setFrameShadow( QFrame::Sunken );
    line_high_ar->setFrameShape( QFrame::HLine );

    layout_table_high_ar->addMultiCellWidget( line_high_ar, 1, 1, 0, 7 );

    reported_2_4 = new QLabel( TabPage, "reported_2_4" );
    QFont reported_2_4_font(  reported_2_4->font() );
    reported_2_4_font.setPointSize( 8 );
    reported_2_4->setFont( reported_2_4_font ); 
    reported_2_4->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_4, 0, 5 );

    effective_2_6 = new QLabel( TabPage, "effective_2_6" );
    QFont effective_2_6_font(  effective_2_6->font() );
    effective_2_6_font.setPointSize( 8 );
    effective_2_6->setFont( effective_2_6_font ); 
    effective_2_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_6, 2, 7 );

    reported_2_6 = new QLabel( TabPage, "reported_2_6" );
    QFont reported_2_6_font(  reported_2_6->font() );
    reported_2_6_font.setPointSize( 8 );
    reported_2_6->setFont( reported_2_6_font ); 
    reported_2_6->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_6, 0, 7 );

    reported_2_1 = new QLabel( TabPage, "reported_2_1" );
    QFont reported_2_1_font(  reported_2_1->font() );
    reported_2_1_font.setPointSize( 8 );
    reported_2_1->setFont( reported_2_1_font ); 
    reported_2_1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_1, 0, 2 );

    reported_high_ar = new QLabel( TabPage, "reported_high_ar" );
    QFont reported_high_ar_font(  reported_high_ar->font() );
    reported_high_ar_font.setPointSize( 8 );
    reported_high_ar_font.setBold( TRUE );
    reported_high_ar->setFont( reported_high_ar_font ); 

    layout_table_high_ar->addWidget( reported_high_ar, 0, 0 );

    effective_2_2 = new QLabel( TabPage, "effective_2_2" );
    QFont effective_2_2_font(  effective_2_2->font() );
    effective_2_2_font.setPointSize( 8 );
    effective_2_2->setFont( effective_2_2_font ); 
    effective_2_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( effective_2_2, 2, 3 );

    reported_2_3 = new QLabel( TabPage, "reported_2_3" );
    QFont reported_2_3_font(  reported_2_3->font() );
    reported_2_3_font.setPointSize( 8 );
    reported_2_3->setFont( reported_2_3_font ); 
    reported_2_3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout_table_high_ar->addWidget( reported_2_3, 0, 4 );
    TabPageLayout->addLayout( layout_table_high_ar );

    layout_count_high_ar = new QHBoxLayout( 0, 0, 6, "layout_count_high_ar"); 
    spacer_left_high_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_high_ar->addItem( spacer_left_high_ar );

    label_2 = new QLabel( TabPage, "label_2" );
    QFont label_2_font(  label_2->font() );
    label_2_font.setPointSize( 8 );
    label_2_font.setItalic( TRUE );
    label_2->setFont( label_2_font ); 
    layout_count_high_ar->addWidget( label_2 );

    count_2 = new QLabel( TabPage, "count_2" );
    QFont count_2_font(  count_2->font() );
    count_2_font.setPointSize( 8 );
    count_2_font.setItalic( TRUE );
    count_2->setFont( count_2_font ); 
    layout_count_high_ar->addWidget( count_2 );
    spacer_right_high_ar = new QSpacerItem( 50, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_count_high_ar->addItem( spacer_right_high_ar );
    TabPageLayout->addLayout( layout_count_high_ar );
    p_name->insertTab( TabPage, QString::fromLatin1("") );
    KBSSETICalibrationContentLayout->addWidget( p_name );
    spacer = new QSpacerItem( 20, 30, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETICalibrationContentLayout->addItem( spacer );

    layout7 = new QHBoxLayout( 0, 0, 6, "layout7"); 
    spacer_buttons = new QSpacerItem( 241, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout7->addItem( spacer_buttons );

    reset = new KPushButton( this, "reset" );
    layout7->addWidget( reset );
    KBSSETICalibrationContentLayout->addLayout( layout7 );
    languageChange();
    resize( QSize(331, 165).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETICalibrationContent::~KBSSETICalibrationContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETICalibrationContent::languageChange()
{
    setCaption( tr2i18n( "KBSCalibrationContent" ) );
    setIconText( QString::null );
    effective_low_ar->setText( tr2i18n( "Effective %" ) );
    reported_low_ar->setText( tr2i18n( "Reported %" ) );
    label_0->setText( tr2i18n( "Sample size:" ) );
    p_name->changeTab( tab, tr2i18n( "Low AR" ) );
    effective_medium_ar->setText( tr2i18n( "Effective %" ) );
    reported_medium_ar->setText( tr2i18n( "Reported %" ) );
    label_1->setText( tr2i18n( "Sample size:" ) );
    p_name->changeTab( tab_2, tr2i18n( "Medium AR" ) );
    effective_high_ar->setText( tr2i18n( "Effective %" ) );
    reported_high_ar->setText( tr2i18n( "Reported %" ) );
    label_2->setText( tr2i18n( "Sample size:" ) );
    p_name->changeTab( TabPage, tr2i18n( "High AR" ) );
    reset->setText( tr2i18n( "&Reset" ) );
    reset->setAccel( QKeySequence( tr2i18n( "Alt+R" ) ) );
}

#include "kbsseticalibrationcontent.moc"
