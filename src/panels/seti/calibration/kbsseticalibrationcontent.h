/****************************************************************************
** Form interface generated from reading ui file './kbsseticalibrationcontent.ui'
**
** Created: Mon Feb 6 18:29:09 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSSETICALIBRATIONCONTENT_H
#define KBSSETICALIBRATIONCONTENT_H

#include <qvariant.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QLabel;
class QFrame;
class KPushButton;

class KBSSETICalibrationContent : public QWidget
{
    Q_OBJECT

public:
    KBSSETICalibrationContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSSETICalibrationContent();

    QTabWidget* p_name;
    QWidget* tab;
    QLabel* reported_0_0;
    QLabel* effective_0_4;
    QLabel* effective_0_2;
    QLabel* effective_0_3;
    QLabel* effective_low_ar;
    QLabel* reported_0_6;
    QLabel* reported_0_3;
    QLabel* reported_low_ar;
    QLabel* effective_0_0;
    QLabel* reported_0_2;
    QLabel* effective_0_6;
    QLabel* effective_0_5;
    QLabel* reported_0_5;
    QLabel* effective_0_1;
    QLabel* reported_0_1;
    QLabel* reported_0_4;
    QFrame* line_low_ar;
    QLabel* label_0;
    QLabel* count_0;
    QWidget* tab_2;
    QLabel* effective_1_5;
    QLabel* reported_1_2;
    QLabel* effective_medium_ar;
    QLabel* effective_1_0;
    QLabel* reported_1_0;
    QLabel* effective_1_1;
    QLabel* effective_1_3;
    QLabel* effective_1_4;
    QLabel* reported_1_5;
    QFrame* line_medium_ar;
    QLabel* reported_1_4;
    QLabel* effective_1_6;
    QLabel* reported_1_6;
    QLabel* reported_1_1;
    QLabel* reported_medium_ar;
    QLabel* effective_1_2;
    QLabel* reported_1_3;
    QLabel* label_1;
    QLabel* count_1;
    QWidget* TabPage;
    QLabel* effective_2_5;
    QLabel* reported_2_2;
    QLabel* effective_high_ar;
    QLabel* effective_2_0;
    QLabel* reported_2_0;
    QLabel* effective_2_1;
    QLabel* effective_2_3;
    QLabel* effective_2_4;
    QLabel* reported_2_5;
    QFrame* line_high_ar;
    QLabel* reported_2_4;
    QLabel* effective_2_6;
    QLabel* reported_2_6;
    QLabel* reported_2_1;
    QLabel* reported_high_ar;
    QLabel* effective_2_2;
    QLabel* reported_2_3;
    QLabel* label_2;
    QLabel* count_2;
    KPushButton* reset;

protected:
    QVBoxLayout* KBSSETICalibrationContentLayout;
    QSpacerItem* spacer;
    QVBoxLayout* tabLayout;
    QGridLayout* layout_table_low_ar;
    QHBoxLayout* layout_count_low_ar;
    QSpacerItem* spacer_left_low_ar;
    QSpacerItem* spacer_right_low_ar;
    QVBoxLayout* tabLayout_2;
    QGridLayout* layout_table_medium_ar;
    QHBoxLayout* layout_count_medium_ar;
    QSpacerItem* spacer_left_medium_ar;
    QSpacerItem* spacer_right_medium_ar;
    QVBoxLayout* TabPageLayout;
    QGridLayout* layout_table_high_ar;
    QHBoxLayout* layout_count_high_ar;
    QSpacerItem* spacer_left_high_ar;
    QSpacerItem* spacer_right_high_ar;
    QHBoxLayout* layout7;
    QSpacerItem* spacer_buttons;

protected slots:
    virtual void languageChange();

};

#endif // KBSSETICALIBRATIONCONTENT_H
