/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kpushbutton.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>
#include <kbsrpcmonitor.h>

#include <kbsseticalibrator.h>
#include <kbsseticalibrationcontent.h>

#include "kbsseticalibrationpanelnode.h"

class KBSSETICalibrationPanelFactory : KGenericFactory<KBSSETICalibrationPanelNode,KBSTreeNode>
{
  public:
    KBSSETICalibrationPanelFactory() : KGenericFactory<KBSSETICalibrationPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsseticalibrationpanel, KBSSETICalibrationPanelFactory());

KBSSETICalibrationPanelNode::KBSSETICalibrationPanelNode(KBSTreeNode *parent,
                                                         const char *name,
                                                         const QStringList &arg)
                           : KBSPanelNode(parent, name), m_project(arg[0])
{
  connect(KBSSETICalibrator::self(), SIGNAL(calibrationUpdated()), this, SLOT(updateContent()));
}

QString KBSSETICalibrationPanelNode::name() const
{
  return i18n("Calibration");
}
    
QStringList KBSSETICalibrationPanelNode::icons() const
{
  return QStringList("calibration");
}

KBSPanel *KBSSETICalibrationPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSSETICalibrationContent *content = new KBSSETICalibrationContent(out);
  out->setContent(content);
  setupContent(content);
    
  return out;
}

QString KBSSETICalibrationPanelNode::project() const
{
  return m_project;
}

void KBSSETICalibrationPanelNode::setupContent(KBSSETICalibrationContent *content)
{
  if(NULL != monitor())
    connect(content->reset, SIGNAL(clicked()), this, SLOT(resetCalibration()));
  else
    content->reset->setEnabled(false);
  
  updateContent();
}

void KBSSETICalibrationPanelNode::updateContent()
{
  KURL url;
  if(NULL != monitor()) url = monitor()->url(); 
  const KBSSETICalibration calibration = KBSSETICalibrator::self()->calibration(url);
  const bool isAuto = KBSSETICalibrator::self()->isAuto();
  
  KLocale *locale = KGlobal::locale();
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSSETICalibrationContent *content = static_cast<KBSSETICalibrationContent*>(it.current()->content());
      
      {
        QMap<double,double> map = calibration.map[LowAR];
        QValueList<double> reported = calibration.map[LowAR].keys();
        qHeapSort(reported);
    
        content->reported_0_0->setText(locale->formatNumber(reported[0] * 1e2, 2));
        content->effective_0_0->setText(locale->formatNumber(map[reported[0]] * 1e2, 2));    
        content->reported_0_1->setText(locale->formatNumber(reported[1] * 1e2, 2));
        content->effective_0_1->setText(locale->formatNumber(map[reported[1]] * 1e2, 2));    
        content->reported_0_2->setText(locale->formatNumber(reported[2] * 1e2, 2));
        content->effective_0_2->setText(locale->formatNumber(map[reported[2]] * 1e2, 2));    
        content->reported_0_3->setText(locale->formatNumber(reported[3] * 1e2, 2));
        content->effective_0_3->setText(locale->formatNumber(map[reported[3]] * 1e2, 2));    
        content->reported_0_4->setText(locale->formatNumber(reported[4] * 1e2, 2));
        content->effective_0_4->setText(locale->formatNumber(map[reported[4]] * 1e2, 2));    
        content->reported_0_5->setText(locale->formatNumber(reported[5] * 1e2, 2));
        content->effective_0_5->setText(locale->formatNumber(map[reported[5]] * 1e2, 2));
        content->reported_0_6->setText(locale->formatNumber(1e2, 2));
        content->effective_0_6->setText(locale->formatNumber(1e2, 2));
        
        if(isAuto)
        {
          const double count = KBSSETICalibrator::self()->count(url, LowAR);
          
          content->label_0->setText(i18n("Sample size:"));
          content->count_0->setText(locale->formatNumber(count, 0));
        }
        else
        {
          content->label_0->setText(QString::null);
          content->count_0->setText(QString::null);
        }
      }   
      {
        QMap<double,double> map = calibration.map[MediumAR];
        QValueList<double> reported = calibration.map[MediumAR].keys();
        qHeapSort(reported);
    
        content->reported_1_0->setText(locale->formatNumber(reported[0] * 1e2, 2));
        content->effective_1_0->setText(locale->formatNumber(map[reported[0]] * 1e2, 2));    
        content->reported_1_1->setText(locale->formatNumber(reported[1] * 1e2, 2));
        content->effective_1_1->setText(locale->formatNumber(map[reported[1]] * 1e2, 2));    
        content->reported_1_2->setText(locale->formatNumber(reported[2] * 1e2, 2));
        content->effective_1_2->setText(locale->formatNumber(map[reported[2]] * 1e2, 2));    
        content->reported_1_3->setText(locale->formatNumber(reported[3] * 1e2, 2));
        content->effective_1_3->setText(locale->formatNumber(map[reported[3]] * 1e2, 2));    
        content->reported_1_4->setText(locale->formatNumber(reported[4] * 1e2, 2));
        content->effective_1_4->setText(locale->formatNumber(map[reported[4]] * 1e2, 2));    
        content->reported_1_5->setText(locale->formatNumber(reported[5] * 1e2, 2));
        content->effective_1_5->setText(locale->formatNumber(map[reported[5]] * 1e2, 2)); 
        content->reported_1_6->setText(locale->formatNumber(1e2, 2));
        content->effective_1_6->setText(locale->formatNumber(1e2, 2));
        
        if(isAuto)
        {
          const double count = KBSSETICalibrator::self()->count(url, MediumAR);
          
          content->label_1->setText(i18n("Sample size:"));
          content->count_1->setText(locale->formatNumber(count, 0));
        }
        else
        {
          content->label_1->setText(QString::null);
          content->count_1->setText(QString::null);
        }
      }   
      {
        QMap<double,double> map = calibration.map[HighAR];
        QValueList<double> reported = calibration.map[HighAR].keys();
        qHeapSort(reported);
        
        content->reported_2_0->setText(locale->formatNumber(reported[0] * 1e2, 2));
        content->effective_2_0->setText(locale->formatNumber(map[reported[0]] * 1e2, 2));    
        content->reported_2_1->setText(locale->formatNumber(reported[1] * 1e2, 2));
        content->effective_2_1->setText(locale->formatNumber(map[reported[1]] * 1e2, 2));    
        content->reported_2_2->setText(locale->formatNumber(reported[2] * 1e2, 2));
        content->effective_2_2->setText(locale->formatNumber(map[reported[2]] * 1e2, 2));    
        content->reported_2_3->setText(locale->formatNumber(reported[3] * 1e2, 2));
        content->effective_2_3->setText(locale->formatNumber(map[reported[3]] * 1e2, 2));    
        content->reported_2_4->setText(locale->formatNumber(reported[4] * 1e2, 2));
        content->effective_2_4->setText(locale->formatNumber(map[reported[4]] * 1e2, 2));    
        content->reported_2_5->setText(locale->formatNumber(reported[5] * 1e2, 2));
        content->effective_2_5->setText(locale->formatNumber(map[reported[5]] * 1e2, 2)); 
        content->reported_2_6->setText(locale->formatNumber(1e2, 2));
        content->effective_2_6->setText(locale->formatNumber(1e2, 2));
        
        if(isAuto)
        {
          const double count = KBSSETICalibrator::self()->count(url, HighAR);
          
          content->label_2->setText(i18n("Sample size:"));
          content->count_2->setText(locale->formatNumber(count, 0));
        }
        else
        {
          content->label_2->setText(QString::null);
          content->count_2->setText(QString::null);
        }
      }
      
      content->reset->setEnabled(isAuto);
    }
}

void KBSSETICalibrationPanelNode::resetCalibration()
{
  const QString host = monitor()->rpcMonitor()->host();
  const QString message = i18n("Reset calibration for host \"%1\"?").arg(host);
  
  const int index = (sender() != NULL) ? m_panels.findRef(static_cast<const KBSPanel*>(sender()->parent())) : -1;
  KBSPanel *content = (index >= 0) ? m_panels.at(index) : NULL;
  
  if(KMessageBox::warningYesNo(content, message) == KMessageBox::Yes) 
    KBSSETICalibrator::self()->resetCalibration(monitor()->url());
}

#include "kbsseticalibrationpanelnode.moc"
