/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIRESULTSDETAILSWINDOW_H
#define KBSSETIRESULTSDETAILSWINDOW_H

#include <qdict.h>

#include <kbssetidata.h>

#include <kbsstandardwindow.h>

class KBSSETIProjectMonitor;
class KBSSETIResultsDetailsContent;

class KBSSETIResultsDetailsWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSSETIResultsDetailsWindow *window(const QString &workunit);
    
    virtual QString workunit() const;
    
    virtual void attachProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
  
  protected slots:
    virtual void detachProjectMonitor();
  
  protected:
    KBSSETIResultsDetailsWindow(const QString &workunit, QWidget *parent=0, const char *name=0);
    
    virtual QString text();
    virtual QPixmap pixmap();
  
  private slots:
    void update();
    void update(const QString &workunit);
    
    void updateGaussian(int index);
    void updatePulse(int index);
    
    void slotContextMenu(KListView *list, QListViewItem *item, const QPoint &pos);
  
  private:
    void setupView();
    
    void connectProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
    void disconnectProjectMonitor();
  
  protected:
    KBSSETIResultsDetailsContent *m_view;
    QPtrList<KBSSETIProjectMonitor> m_projectMonitors;
  
  private:
    class SpikeItem : public KListViewItem {
      public:
        SpikeItem(QListView *parent, const QString &name,
                  const KBSSETISpike &spike, double start);
        virtual QString key(int column, bool ascending) const;
      private:
        QString m_name;
        KBSSETISpike m_spike;
        double m_start;
    };
    class TripletItem : public KListViewItem {
      public:
        TripletItem(QListView *parent, const QString &name,
                    const KBSSETITriplet &triplet, double start);
        virtual QString key(int column, bool ascending) const;
      private:
        QString m_name;
        KBSSETITriplet m_triplet;
        double m_start;
    };
  
  private:
    QString m_workunit;
    double m_start;
    
    static QDict<KBSSETIResultsDetailsWindow> s_windows;
};

#endif
