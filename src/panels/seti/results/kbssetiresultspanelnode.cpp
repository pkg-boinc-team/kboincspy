/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <kglobal.h>
#include <kiconloader.h>
#include <klocale.h>
#include <kpushbutton.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbssetiprojectmonitor.h>
#include <kbssetitaskmonitor.h>

#include <kbssetilogwindow.h>
#include <kbssetiresultscontent.h>
#include <kbssetiresultsdetailswindow.h>
#include <kbssetiresultsplot.h>

#include "kbssetiresultspanelnode.h"

class KBSSETIResultsPanelFactory : KGenericFactory<KBSSETIResultsPanelNode,KBSTreeNode>
{
  public:
    KBSSETIResultsPanelFactory() : KGenericFactory<KBSSETIResultsPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbssetiresultspanel, KBSSETIResultsPanelFactory());

const QPixmap KBSSETIResultsPanelNode::spike = UserIcon("seti/spike");
const QPixmap KBSSETIResultsPanelNode::gaussian = UserIcon("seti/gaussian");
const QPixmap KBSSETIResultsPanelNode::pulse = UserIcon("seti/pulse");
const QPixmap KBSSETIResultsPanelNode::triplet = UserIcon("seti/triplet");

KBSSETIResultsPanelNode::KBSSETIResultsPanelNode(KBSTreeNode *parent, const char *name,
                                                 const QStringList &args)
                       : KBSPanelNode(parent, name), m_workunit(args[0]),
                         m_projectMonitor(NULL), m_details(NULL)
{
  setupMonitor();
  
  if(NULL != m_projectMonitor)
    KBSSETILogWindow::self()->attachProjectMonitor(m_projectMonitor);
}

QString KBSSETIResultsPanelNode::name() const
{
  return i18n("Results");
}

QStringList KBSSETIResultsPanelNode::icons() const
{
  return QStringList("results");
}

KBSSETIProjectMonitor *KBSSETIResultsPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

KBSPanel *KBSSETIResultsPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSSETIResultsContent *content = new KBSSETIResultsContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

void KBSSETIResultsPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  
  m_projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSSETIResultsPanelNode::setupContent(KBSSETIResultsContent *content)
{
  content->results_plot->setWorkunit(m_workunit);
  content->results_plot->setProjectMonitor(m_projectMonitor);

  content->spikes_pixmap->setPixmap(spike);
  content->gaussians_pixmap->setPixmap(gaussian);
  content->pulses_pixmap->setPixmap(pulse);
  content->triplets_pixmap->setPixmap(triplet);
  
  content->spikes->setName(i18n("Spikes returned:"));
  content->gaussians->setName(i18n("Gaussians returned:"));
  content->pulses->setName(i18n("Pulses returned:"));
  content->triplets->setName(i18n("Triplets returned:"));
  
  connect(content->log_button, SIGNAL(clicked()), this, SLOT(showLog()));
  connect(content->details_button, SIGNAL(clicked()), this, SLOT(showDetails()));
  
  updateContent();
}

void KBSSETIResultsPanelNode::updateContent()
{
  QString spikes, gaussians, pulses, triplets;
  
  const KBSSETIResult *setiResult = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit)
                                                               : NULL;
  
  if(NULL != setiResult)
  {
    KLocale *locale = KGlobal::locale();
    
    spikes = locale->formatNumber(setiResult->spike.count(), 0);
    gaussians = locale->formatNumber(setiResult->gaussian.count(), 0);
    pulses = locale->formatNumber(setiResult->pulse.count(), 0);
    triplets = locale->formatNumber(setiResult->triplet.count(), 0);
  }
  else
    spikes = gaussians = pulses = triplets = "0";
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSSETIResultsContent *content = static_cast<KBSSETIResultsContent*>(it.current()->content());
      
      content->spikes->setText(spikes);
      content->gaussians->setText(gaussians);
      content->pulses->setText(pulses);
      content->triplets->setText(triplets);
      
      content->results_plot->update();
    }
}

void KBSSETIResultsPanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit)
    updateContent();
}

void KBSSETIResultsPanelNode::showLog()
{
  KBSSETILogWindow *log = KBSSETILogWindow::self();
  
  if(!log->isVisible()) log->show();
}

void KBSSETIResultsPanelNode::showDetails()
{
  if(NULL == m_projectMonitor) return;
  
  if(NULL == m_details) {
    m_details = KBSSETIResultsDetailsWindow::window(m_workunit);
    m_details->attachProjectMonitor(m_projectMonitor);
  }
  
  if(!m_details->isVisible()) m_details->show();
}

#include "kbssetiresultspanelnode.moc"

