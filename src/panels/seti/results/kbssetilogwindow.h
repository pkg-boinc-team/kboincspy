/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETILOGWINDOW_H
#define KBSSETILOGWINDOW_H

#include <qptrlist.h>

#include <klistview.h>

#include <kbssetilogmanager.h>
#include <kbsstandardwindow.h>

class KBSSETIProjectMonitor;

class KBSSETILogWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSSETILogWindow* self();
    virtual ~KBSSETILogWindow();
    
    virtual void setAutoSaveGeometry(const QString &group);
    
    virtual void attachProjectMonitor(KBSSETIProjectMonitor *monitor);
  
  protected slots:
    virtual void detachProjectMonitor(QObject *monitor);
    
    virtual void buildLog();
    virtual void updateLog();
  
  protected:
    KBSSETILogWindow(QWidget *parent=0, const char *name=0);
    
    virtual QString text();
  
  private slots:
    void slotContextMenu(KListView *list, QListViewItem *item, const QPoint &pos);
  
  protected:
    KListView *m_view;
    QValueList<int> m_columns;
    
    static QPtrList<KBSSETIProjectMonitor> s_monitors;
    
  private:
    class Item : public KListViewItem {
      public:
        Item(QListView *parent, const QValueList<int> &columns, const KBSLogDatum &datum);
        virtual QString key(int column, bool ascending) const;
      
      protected:
        QValueList<int> m_columns;
      
      private:
        QDateTime date;
        QString domain_name, wu_name;
        double start_ra, start_dec, angle_range, teraflops, cpu, prog;
        unsigned spike_count, gaussian_count, pulse_count, triplet_count;
        double bs_score, bg_score, bp_score, bt_score;
    };
    
    static KBSSETILogWindow *s_self;
};

#endif
