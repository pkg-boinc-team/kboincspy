/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qpopupmenu.h>

#include <kapplication.h>
#include <klocale.h>

#include <kbssetiprojectmonitor.h>

#include "kbssetilogwindow.h"

using namespace KBSSETI;

KBSSETILogWindow *KBSSETILogWindow::s_self = NULL;
QPtrList<KBSSETIProjectMonitor> KBSSETILogWindow::s_monitors = QPtrList<KBSSETIProjectMonitor>();

KBSSETILogWindow *KBSSETILogWindow::self()
{
  if(NULL == s_self) s_self = new KBSSETILogWindow();
  
  return s_self;
}

KBSSETILogWindow::KBSSETILogWindow(QWidget *parent, const char *name)
                : KBSStandardWindow(parent, name), m_view(new KListView(this))
{
  setCaption(i18n("SETI@home Log"));
  
  setCentralWidget(m_view);
  
  connect(m_view, SIGNAL(contextMenu(KListView *, QListViewItem *, const QPoint &)),
          this, SLOT(slotContextMenu(KListView *, QListViewItem *, const QPoint &))); 
  
  setAutoSaveGeometry("SETI@home Log");
  
  setupActions();
  
  KBSSETILogManager *logManager = KBSSETILogManager::self();
  connect(logManager, SIGNAL(logChanged()), this, SLOT(buildLog()));
  connect(logManager, SIGNAL(workunitsUpdated()), this, SLOT(updateLog()));
  
  buildLog();
}

KBSSETILogWindow::~KBSSETILogWindow()
{
  const QString group = autoSaveGroup();
  if(!group.isEmpty()) m_view->saveLayout(kapp->config(), group);
}

QString KBSSETILogWindow::text()
{
  QString text = "";
  
  QListViewItem *item = m_view->firstChild();
  while(NULL != item)
  {
    QStringList fields;
    for(int column = 0; column < m_view->columns(); ++column)
      fields << item->text(column);
    text.append(fields.join("\t") + "\n");
    
    item = item->nextSibling();
  }
  
  return text;
}

void KBSSETILogWindow::setAutoSaveGeometry(const QString &group)
{
  KBSStandardWindow::setAutoSaveGeometry(group);
  m_view->restoreLayout(kapp->config(), group);
}

void KBSSETILogWindow::attachProjectMonitor(KBSSETIProjectMonitor *monitor)
{
  if(NULL == monitor) return;
  
  if(s_monitors.containsRef(monitor)) return;
  
  s_monitors.append(monitor);
  connect(monitor, SIGNAL(destroyed(QObject *)), this, SLOT(detachProjectMonitor(QObject *)));
}

void KBSSETILogWindow::detachProjectMonitor(QObject *monitor)
{
  if(NULL == monitor) return;
  
  s_monitors.removeRef(static_cast<KBSSETIProjectMonitor*>(monitor));
  
  if(!s_monitors.isEmpty()) return;
  
  close();
  
  destroy();
  s_self = NULL;
}

void KBSSETILogWindow::buildLog()
{
  while(m_view->columns() > 0)
    m_view->removeColumn(0);
  m_columns.clear();
  
  QStringList keys = KBSSETILogManager::self()->keys();
  
  if(keys.contains("domain_name")) {
    m_view->addColumn(i18n("Host"));
    m_view->setColumnAlignment(m_columns.count(), AlignLeft);
    m_columns << 0;
  }
  if(keys.contains("date")) {
    m_view->addColumn(i18n("Time Done"));
    m_view->setColumnAlignment(m_columns.count(), AlignLeft);
    m_columns << 1;
  }
  if(keys.contains("wu_name")) {
    m_view->addColumn(i18n("Work Unit Name"));
    m_view->setColumnAlignment(m_columns.count(), AlignLeft);
    m_columns << 2;
  }
  if(keys.contains("start_ra")) {
    m_view->addColumn(i18n("Start RA"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 3;
  }
  if(keys.contains("start_dec")) {
    m_view->addColumn(i18n("Start Dec"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 4;
  }
  if(keys.contains("angle_range")) {
    m_view->addColumn(i18n("Angle Range"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 5;
  }
  if(keys.contains("teraflops") || keys.contains("angle_range")) {
    m_view->addColumn(i18n("TeraFLOPs"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 6;
  }
  if(keys.contains("cpu")) {
    m_view->addColumn(i18n("Process Time"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 7;
  }
  if(keys.contains("cpu")) {
    m_view->addColumn(i18n("% Done"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 8;
  }
  if(keys.contains("spike_count")) {
    m_view->addColumn(i18n("Spikes"));
    m_view->setColumnAlignment(m_columns.count(), AlignCenter);
    m_columns << 9;
  }
  if(keys.contains("bs_score")) {
    m_view->addColumn(i18n("Best Spike"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 10;
  }
  if(keys.contains("gaussian_count")) {
    m_view->addColumn(i18n("Gaussians"));
    m_view->setColumnAlignment(m_columns.count(), AlignCenter);
    m_columns << 11;
  }
  if(keys.contains("bg_score")) {
    m_view->addColumn(i18n("Best Gaussian"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 12;
  }
  if(keys.contains("pulse_count")) {
    m_view->addColumn(i18n("Pulses"));
    m_view->setColumnAlignment(m_columns.count(), AlignCenter);
    m_columns << 13;
  }
  if(keys.contains("bp_score")) {
    m_view->addColumn(i18n("Best Pulse"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 14;
  }
  if(keys.contains("triplet_count")) {
    m_view->addColumn(i18n("Triplets"));
    m_view->setColumnAlignment(m_columns.count(), AlignCenter);
    m_columns << 15;
  }
  if(keys.contains("bt_score")) {
    m_view->addColumn(i18n("Best Triplet"));
    m_view->setColumnAlignment(m_columns.count(), AlignRight);
    m_columns << 16;
  }
  
  m_view->setFocusPolicy(QWidget::NoFocus);
  m_view->setSelectionMode(QListView::NoSelection);
  m_view->setShowSortIndicator(true);
  
  if(m_columns.count() > 0) {
    m_view->setSorting(0, true);
    m_view->sort();
  }
  
  updateLog();
}

void KBSSETILogWindow::updateLog()
{
  KBSLogData workunits = KBSSETILogManager::self()->workunits();
  
  if(m_view->childCount() >= int(workunits.count()))
    m_view->clear();
    
  for(int i = m_view->childCount(); i < int(workunits.count()); ++i)
    new Item(m_view, m_columns, workunits[i]);
}

void KBSSETILogWindow::slotContextMenu(KListView *, QListViewItem *, const QPoint &pos)
{
  QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
  context->popup(pos);
}

KBSSETILogWindow::Item::Item(QListView *parent, const QValueList<int> &columns, const KBSLogDatum &datum)
                      : KListViewItem(parent), m_columns(columns)
{
  KLocale *locale = KGlobal::locale();
  int index;
  
  if((index = m_columns.findIndex(0)) >= 0)
  {
    domain_name = datum["domain_name"].toString();
    setText(index, domain_name);
  }
  
  if((index = m_columns.findIndex(1)) >= 0)
  {
    date = datum["date"].toDateTime();
    setText(index, locale->formatDateTime(date));
  }
  
  if((index = m_columns.findIndex(2)) >= 0)
  {
    wu_name = datum["wu_name"].toString();
    setText(index, wu_name);
  }
  
  if((index = m_columns.findIndex(3)) >= 0)
  {
    start_ra = datum["start_ra"].toDouble();
    setText(index, formatRA(start_ra));
  }
  
  if((index = m_columns.findIndex(4)) >= 0)
  {
    start_dec = datum["start_dec"].toDouble();
    setText(index, formatDec(start_dec));
  }
  
  if((index = m_columns.findIndex(5)) >= 0)
  {
    angle_range = datum["angle_range"].toDouble();
    setText(index, locale->formatNumber(angle_range, 3));
  }
  
  if((index = m_columns.findIndex(6)) >= 0)
  {
    teraflops = datum.contains("teraflops") ? datum["teraflops"].toDouble()
                                            : KBSSETIDataDesc::teraFLOPs(angle_range);
    setText(index, locale->formatNumber(teraflops, 3));
  }
  
  if((index = m_columns.findIndex(7)) >= 0)
  {
    cpu = datum["cpu"].toDouble();
    setText(index, KBSBOINC::formatTime(cpu));
  }
  
  if((index = m_columns.findIndex(8)) >= 0)
  {
    prog = datum.contains("prog") ? datum["prog"].toDouble() : 1.0;
    setText(index, QString("%1%").arg(locale->formatNumber(prog * 1e2, 2)));
  }
  
  if((index = m_columns.findIndex(9)) >= 0)
  {
    spike_count = datum["spike_count"].toUInt();
    setText(index, locale->formatNumber(spike_count, 0));
  }
  
  if((index = m_columns.findIndex(10)) >= 0)
  {
    bs_score = datum["bs_score"].toDouble();
    setText(index, locale->formatNumber(bs_score, 3));
  }
  
  if((index = m_columns.findIndex(11)) >= 0)
  {
    gaussian_count = datum["gaussian_count"].toUInt();
    setText(index, locale->formatNumber(gaussian_count, 0));
  }
  
  if((index = m_columns.findIndex(12)) >= 0)
  {
    bg_score = datum["bg_score"].toDouble();
    setText(index, locale->formatNumber(bg_score, 3));
  }
  
  if((index = m_columns.findIndex(13)) >= 0)
  {
    pulse_count = datum["pulse_count"].toUInt();
    setText(index, locale->formatNumber(pulse_count, 0));
  }
  
  if((index = m_columns.findIndex(14)) >= 0)
  {
    bp_score = datum["bp_score"].toDouble();
    setText(index, locale->formatNumber(bp_score, 3));
  }
  
  if((index = m_columns.findIndex(15)) >= 0)
  {
    triplet_count = datum["triplet_count"].toUInt();
    setText(index, locale->formatNumber(triplet_count, 0));
  }
  
  if((index = m_columns.findIndex(16)) >= 0)
  {
    bt_score = datum["bt_score"].toDouble();
    setText(index, locale->formatNumber(bt_score, 3));
  }
}

QString KBSSETILogWindow::Item::key(int column, bool ascending) const
{
  const QDateTime epoch(QDate(1990, 1, 1));
  QString out;

  switch(m_columns[column]) {
    case 0:
      return domain_name;
    case 1:
      return out.sprintf("%09d", epoch.secsTo(date));
    case 2:
      return wu_name;
    case 3:
      return out.sprintf("%05.0lf", start_ra * 1e3);
    case 4:
      return out.sprintf("%05.0lf", start_dec * 1e3);
    case 5:
      return out.sprintf("%05.0lf", angle_range * 1e3);
    case 6:
      return out.sprintf("%05.0lf", teraflops * 1e3);
    case 7:
      return out.sprintf("%08.0lf", cpu);
    case 8:
      return out.sprintf("%07.0lf", prog * 1e6);
    case 9:
      return out.sprintf("%05d", spike_count);
    case 10:
      return out.sprintf("%09.0lf", bs_score * 1e2);
    case 11:
      return out.sprintf("%05d", gaussian_count);
    case 12:
      return out.sprintf("%09.0lf", bg_score * 1e2);
    case 13:
      return out.sprintf("%05d", pulse_count);
    case 14:
      return out.sprintf("%09.0lf", bp_score * 1e2);
    case 15:
      return out.sprintf("%05d", triplet_count);
    case 16:
      return out.sprintf("%09.0lf", bt_score * 1e2);
    default:
      return QListViewItem::key(column, ascending);
  }
}

#include "kbssetilogwindow.moc"
