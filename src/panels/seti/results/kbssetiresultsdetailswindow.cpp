/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qpopupmenu.h>
#include <qtabwidget.h>

#include <kcombobox.h>
#include <kglobal.h>
#include <klistview.h>
#include <klocale.h>

#include <kbsboincmonitor.h>

#include <kbssetiprojectmonitor.h>
#include <kbssetisignalplot.h>

#include <kbssetiresultsdetailscontent.h>

#include "kbssetiresultsdetailswindow.h"

using namespace KBSBOINC;

QDict<KBSSETIResultsDetailsWindow> KBSSETIResultsDetailsWindow::s_windows
  = QDict<KBSSETIResultsDetailsWindow>();

KBSSETIResultsDetailsWindow *KBSSETIResultsDetailsWindow::window(const QString &workunit)
{
  KBSSETIResultsDetailsWindow *out = s_windows.find(workunit);
  
  if(NULL == out) {
    out = new KBSSETIResultsDetailsWindow(workunit);
    s_windows.insert(workunit, out);
  }
  
  return out;
}

KBSSETIResultsDetailsWindow::KBSSETIResultsDetailsWindow(const QString &workunit,
                                                         QWidget *parent, const char *name)
                           : KBSStandardWindow(parent, name),
                             m_view(new KBSSETIResultsDetailsContent(this)), m_workunit(workunit)
{
  setCaption(i18n("Results Details - %1").arg(workunit));
  
  setCentralWidget(m_view);
  setupView();
  
  setupActions();
}

QString KBSSETIResultsDetailsWindow::workunit() const
{
  return m_workunit;
}

void KBSSETIResultsDetailsWindow::attachProjectMonitor(KBSSETIProjectMonitor *projectMonitor)
{
  if(m_projectMonitors.containsRef(projectMonitor)) return;
  
  m_projectMonitors.append(projectMonitor);
  
  if(m_projectMonitors.count() == 1) {
    connectProjectMonitor(projectMonitor);
    update();
  }
}

void KBSSETIResultsDetailsWindow::detachProjectMonitor()
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();
  
  if(NULL != projectMonitor) {
    disconnectProjectMonitor();
    m_projectMonitors.removeRef(projectMonitor);
  }
  
  projectMonitor = m_projectMonitors.first();
    
  if(NULL != projectMonitor) {
    connectProjectMonitor(projectMonitor);
    update();
  } else {
    close();
    destroy();
    
    s_windows.remove(m_workunit);
  }
}

QString KBSSETIResultsDetailsWindow::text()
{
  KListView *view;
  switch(m_view->tab_signals->currentPageIndex()) {
    case 0:
      view =  m_view->spikes;
      break;
    case 3:
      view = m_view->triplets;
    default:
      return KBSStandardWindow::text();
  }
  
  QString text = "";
  
  QListViewItem *item = view->firstChild();
  while(NULL != item)
  {
    QStringList fields;
    for(int column = 0; column < view->columns(); ++column)
      fields << item->text(column);
    text.append(fields.join("\t") + "\n");
    
    item = item->nextSibling();
  }
  
  return text;
}

QPixmap KBSSETIResultsDetailsWindow::pixmap()
{
  switch(m_view->tab_signals->currentPageIndex()) {
    case 1:
      return m_view->gaussian_plot->pixmap();
    case 2:
      return m_view->pulse_plot->pixmap();
    default:
      return KBSStandardWindow::pixmap();
  }
}

void KBSSETIResultsDetailsWindow::update()
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();
  if(NULL == projectMonitor) return;
  
  const KBSSETIResult *setiResult = projectMonitor->result(m_workunit);
  if(NULL == setiResult) return;
  
  const double start = setiResult->workunit_header.group_info.data_desc.time_recorded_jd;
  
  
  m_view->spikes->clear();
  
  if(setiResult->state.best_spike.spike.time_jd > 0)
    new SpikeItem(m_view->spikes, i18n("Best score"), setiResult->state.best_spike.spike, start);
  
  for(unsigned i = 0; i < setiResult->spike.count(); ++i)
    new SpikeItem(m_view->spikes, i18n("Returned %1").arg(i+1), setiResult->spike[i], start);
  
  m_view->spikes->sort();
  
  
  for(unsigned i = m_view->gaussians->count(); i < setiResult->gaussian.count()+1; ++i)
    m_view->gaussians->insertItem(i18n("Returned %1").arg(i), i);
  
  updateGaussian(m_view->gaussians->currentItem());
  
    
  for(unsigned i = m_view->pulses->count(); i < setiResult->pulse.count()+1; ++i)
    m_view->pulses->insertItem(i18n("Returned %1").arg(i), i);
  
  updatePulse(m_view->pulses->currentItem());
  
  
  m_view->triplets->clear();
  
  if(setiResult->state.best_triplet.triplet.time_jd > 0)
    new TripletItem(m_view->triplets, i18n("Best score"), setiResult->state.best_triplet.triplet, start);
  
  for(unsigned i = 0; i < setiResult->triplet.count(); ++i)
    new TripletItem(m_view->triplets, i18n("Returned %1").arg(i+1), setiResult->triplet[i], start);
  
  m_view->triplets->sort();  
}

void KBSSETIResultsDetailsWindow::update(const QString &workunit)
{
  if(workunit == m_workunit) update();
}

void KBSSETIResultsDetailsWindow::updateGaussian(int index)
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();    
  const KBSSETIResult *setiResult = (NULL != projectMonitor) ? projectMonitor->result(m_workunit)
                                                             : NULL;

  if(NULL == setiResult || index < 0)
    m_view->gaussian_plot->clearData();
  else if(0 == index)
    if(setiResult->state.best_gaussian.gaussian.time_jd > 0)
      m_view->gaussian_plot->setData(setiResult->workunit_header, setiResult->state.best_gaussian.gaussian);
    else
      m_view->gaussian_plot->clearData();
  else if(index > 0)
    if(unsigned(index) < setiResult->gaussian.count()+1)
      m_view->gaussian_plot->setData(setiResult->workunit_header, setiResult->gaussian[index-1]);
    else
      m_view->gaussian_plot->clearData();
}

void KBSSETIResultsDetailsWindow::updatePulse(int index)
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();
  const KBSSETIResult *setiResult = (NULL != projectMonitor) ? projectMonitor->result(m_workunit)
                                                             : NULL;

  if(NULL == setiResult || index < 0)
    m_view->pulse_plot->clearData();
  else if(0 == index )
    if(setiResult->state.best_pulse.pulse.time_jd > 0)
      m_view->pulse_plot->setData(setiResult->workunit_header, setiResult->state.best_pulse.pulse);
    else
      m_view->pulse_plot->clearData();
  else if(index > 0)
    if(unsigned(index) < setiResult->pulse.count()+1)
      m_view->pulse_plot->setData(setiResult->workunit_header, setiResult->pulse[index-1]);
    else
      m_view->pulse_plot->clearData();
}

void KBSSETIResultsDetailsWindow::setupView()
{
  m_view->spikes->setFocusPolicy(QWidget::NoFocus);
  m_view->spikes->setSelectionMode(QListView::NoSelection);
  m_view->spikes->setShowSortIndicator(true);
  m_view->spikes->setSorting(0);
  
  connect(m_view->spikes, SIGNAL(contextMenu(KListView *, QListViewItem *, const QPoint &)),
          this, SLOT(slotContextMenu(KListView *, QListViewItem *, const QPoint &))); 

  connect(m_view->gaussians, SIGNAL(activated(int)), this, SLOT(updateGaussian(int)));
  
  connect(m_view->pulses, SIGNAL(activated(int)), this, SLOT(updatePulse(int)));
  
  m_view->triplets->setFocusPolicy(QWidget::NoFocus);
  m_view->triplets->setSelectionMode(QListView::NoSelection);
  m_view->triplets->setShowSortIndicator(true);
  m_view->triplets->setSorting(0);
  
  connect(m_view->triplets, SIGNAL(contextMenu(KListView *, QListViewItem *, const QPoint &)),
          this, SLOT(slotContextMenu(KListView *, QListViewItem *, const QPoint &)));
}

void KBSSETIResultsDetailsWindow::slotContextMenu(KListView *, QListViewItem *, const QPoint &pos)
{
  QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
  context->popup(pos);
}

void KBSSETIResultsDetailsWindow::connectProjectMonitor(KBSSETIProjectMonitor *projectMonitor)
{
  connect(projectMonitor, SIGNAL(updatedResult(const QString &)), this, SLOT(update(const QString &)));
  connect(projectMonitor, SIGNAL(destroyed()), this, SLOT(detachProjectMonitor()));
  
  KBSBOINCMonitor *boincMonitor = projectMonitor->boincMonitor();
  
  connect(boincMonitor, SIGNAL(stateUpdated()), this, SLOT(update()));
}

void KBSSETIResultsDetailsWindow::disconnectProjectMonitor()
{
  disconnect(this, SLOT(update(const QString &)));
  disconnect(this, SLOT(detachProjectMonitor()));
  disconnect(this, SLOT(update()));
}

KBSSETIResultsDetailsWindow::SpikeItem::SpikeItem(QListView *parent, const QString &name,
                                                  const KBSSETISpike &spike, double start)
                                      : KListViewItem(parent, name), m_name(name),
                                        m_spike(spike), m_start(start)
{
  KLocale *locale = KGlobal::locale();
  
  setText(0, name);
  setText(1, locale->formatNumber(spike.peak_power, 2));
  setText(2, locale->formatNumber(spike.score(), 3));
  setText(3, locale->formatNumber(spike.signal_ratio(), 3));
  setText(4, locale->formatNumber(spike.resolution(), 3));
  setText(5, locale->formatNumber(spike.freq, 2));
  setText(6, formatTime((spike.time_jd - start) * 24 * 60 * 60));
  setText(7, locale->formatNumber(spike.chirp_rate, 4));
}

QString KBSSETIResultsDetailsWindow::SpikeItem::key(int column, bool ascending) const
{
  switch(column) {
    case 0:
      return m_name;
    case 1:
      return QString().sprintf("%08.0lf", m_spike.peak_power * 1e2);
    case 2:
      return QString().sprintf("%09.0lf", m_spike.score() * 1e2);
    case 3:
      return QString().sprintf("%09.0lf", m_spike.signal_ratio() * 1e2);
    case 4:
      return QString().sprintf("%05.0lf", m_spike.resolution() * 1e2);
    case 5:
      return QString().sprintf("%014.0lf", m_spike.freq * 1e2);
    case 6:
      return QString().sprintf("%09.0lf", fabs(m_spike.time_jd - m_start) * 24 * 60 * 60 * 1e2);
    case 7:
      return QString().sprintf("%07.0lf", 50.0 + m_spike.chirp_rate * 1e4);
    default:
      return QListViewItem::key(column, ascending) ;
  }
}

KBSSETIResultsDetailsWindow::TripletItem::TripletItem(QListView *parent, const QString &name,
                                                      const KBSSETITriplet &triplet, double start)
                                        : KListViewItem(parent, name), m_name(name),
                                          m_triplet(triplet), m_start(start)
{
  KLocale *locale = KGlobal::locale();
  
  setText(0, name);
  setText(1, locale->formatNumber(triplet.peak_power, 2));
  setText(2, locale->formatNumber(triplet.score(), 3));
  setText(3, locale->formatNumber(triplet.period, 3));
  setText(4, locale->formatNumber(triplet.resolution(), 3));
  setText(5, locale->formatNumber(triplet.freq, 2));
  setText(6, formatTime(fabs(triplet.time_jd - start) * 24 * 60 * 60));
  setText(7, locale->formatNumber(triplet.chirp_rate, 4));
}

QString KBSSETIResultsDetailsWindow::TripletItem::key(int column, bool ascending) const
{
  switch(column) {
    case 0:
      return m_name;
    case 1:
      return QString().sprintf("%08.0lf", m_triplet.peak_power * 1e2);
    case 2:
      return QString().sprintf("%09.0lf", m_triplet.score() * 1e2);
    case 3:
      return QString().sprintf("%09.0lf", m_triplet.period * 1e2);
    case 4:
      return QString().sprintf("%05.0lf", m_triplet.resolution() * 1e2);
    case 5:
      return QString().sprintf("%014.0lf", m_triplet.freq * 1e2);
    case 6:
      return QString().sprintf("%05.0lf", fabs(m_triplet.time_jd - m_start) * 24 * 60 * 60);
    case 7:
      return QString().sprintf("%07.0lf", 50.0 + m_triplet.chirp_rate * 1e4);
    default:
      return QListViewItem::key(column, ascending) ;
  }
}

#include "kbssetiresultsdetailswindow.moc"
