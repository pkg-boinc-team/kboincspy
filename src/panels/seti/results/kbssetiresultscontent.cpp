#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbssetiresultscontent.ui'
**
** Created: Mon Feb 6 18:29:12 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbssetiresultscontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <kbssetiresultsplot.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETIResultsContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIResultsContent::KBSSETIResultsContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIResultsContent" );
    KBSSETIResultsContentLayout = new QVBoxLayout( this, 0, 6, "KBSSETIResultsContentLayout"); 

    layout_main = new QHBoxLayout( 0, 0, 6, "layout_main"); 

    results_plot = new KBSSETIResultsPlot( this, "results_plot" );
    results_plot->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 1, results_plot->sizePolicy().hasHeightForWidth() ) );
    layout_main->addWidget( results_plot );

    layout_signals = new QVBoxLayout( 0, 0, 6, "layout_signals"); 
    spacer_signals_top = new QSpacerItem( 20, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout_signals->addItem( spacer_signals_top );

    layout_spikes = new QHBoxLayout( 0, 0, 6, "layout_spikes"); 

    spikes_pixmap = new QLabel( this, "spikes_pixmap" );
    spikes_pixmap->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, spikes_pixmap->sizePolicy().hasHeightForWidth() ) );
    spikes_pixmap->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    spikes_pixmap->setScaledContents( FALSE );
    spikes_pixmap->setAlignment( int( QLabel::AlignCenter ) );
    layout_spikes->addWidget( spikes_pixmap );

    spikes = new KBSPanelField( this, "spikes" );
    spikes->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, spikes->sizePolicy().hasHeightForWidth() ) );
    layout_spikes->addWidget( spikes );
    layout_signals->addLayout( layout_spikes );

    layout_gaussians = new QHBoxLayout( 0, 0, 6, "layout_gaussians"); 

    gaussians_pixmap = new QLabel( this, "gaussians_pixmap" );
    gaussians_pixmap->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, gaussians_pixmap->sizePolicy().hasHeightForWidth() ) );
    gaussians_pixmap->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    gaussians_pixmap->setScaledContents( FALSE );
    gaussians_pixmap->setAlignment( int( QLabel::AlignCenter ) );
    layout_gaussians->addWidget( gaussians_pixmap );

    gaussians = new KBSPanelField( this, "gaussians" );
    gaussians->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, gaussians->sizePolicy().hasHeightForWidth() ) );
    layout_gaussians->addWidget( gaussians );
    layout_signals->addLayout( layout_gaussians );

    layout_pulses = new QHBoxLayout( 0, 0, 6, "layout_pulses"); 

    pulses_pixmap = new QLabel( this, "pulses_pixmap" );
    pulses_pixmap->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, pulses_pixmap->sizePolicy().hasHeightForWidth() ) );
    pulses_pixmap->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    pulses_pixmap->setScaledContents( FALSE );
    pulses_pixmap->setAlignment( int( QLabel::AlignCenter ) );
    layout_pulses->addWidget( pulses_pixmap );

    pulses = new KBSPanelField( this, "pulses" );
    pulses->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, pulses->sizePolicy().hasHeightForWidth() ) );
    layout_pulses->addWidget( pulses );
    layout_signals->addLayout( layout_pulses );

    layout9 = new QHBoxLayout( 0, 0, 6, "layout9"); 

    triplets_pixmap = new QLabel( this, "triplets_pixmap" );
    triplets_pixmap->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, triplets_pixmap->sizePolicy().hasHeightForWidth() ) );
    triplets_pixmap->setPaletteBackgroundColor( QColor( 0, 0, 0 ) );
    triplets_pixmap->setScaledContents( FALSE );
    triplets_pixmap->setAlignment( int( QLabel::AlignCenter ) );
    layout9->addWidget( triplets_pixmap );

    triplets = new KBSPanelField( this, "triplets" );
    triplets->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, triplets->sizePolicy().hasHeightForWidth() ) );
    layout9->addWidget( triplets );
    layout_signals->addLayout( layout9 );
    spacer_signals_bottom = new QSpacerItem( 20, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout_signals->addItem( spacer_signals_bottom );
    layout_main->addLayout( layout_signals );
    KBSSETIResultsContentLayout->addLayout( layout_main );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_buttons = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_buttons );

    log_button = new KPushButton( this, "log_button" );
    layout_buttons->addWidget( log_button );

    details_button = new KPushButton( this, "details_button" );
    layout_buttons->addWidget( details_button );
    KBSSETIResultsContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(170, 83).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIResultsContent::~KBSSETIResultsContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIResultsContent::languageChange()
{
    setCaption( tr2i18n( "KBSSETIResultsContent" ) );
    setIconText( QString::null );
    log_button->setText( tr2i18n( "&View Log" ) );
    log_button->setAccel( QKeySequence( tr2i18n( "Alt+V" ) ) );
    details_button->setText( tr2i18n( "&Details" ) );
    details_button->setAccel( QKeySequence( tr2i18n( "Alt+D" ) ) );
}

#include "kbssetiresultscontent.moc"
