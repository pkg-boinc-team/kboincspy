#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbssetiresultsdetailscontent.ui'
**
** Created: Mon Feb 6 18:29:12 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbssetiresultsdetailscontent.h"

#include <qvariant.h>
#include <kbssetisignalplot.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qheader.h>
#include <klistview.h>
#include <kcombobox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETIResultsDetailsContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIResultsDetailsContent::KBSSETIResultsDetailsContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIResultsDetailsContent" );
    KBSSETIResultsDetailsContentLayout = new QVBoxLayout( this, 11, 6, "KBSSETIResultsDetailsContentLayout"); 

    tab_signals = new QTabWidget( this, "tab_signals" );

    tab = new QWidget( tab_signals, "tab" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tabLayout"); 

    spikes = new KListView( tab, "spikes" );
    spikes->addColumn( tr2i18n( "Spike" ) );
    spikes->addColumn( tr2i18n( "Power" ) );
    spikes->addColumn( tr2i18n( "Score" ) );
    spikes->addColumn( tr2i18n( "Signal ratio" ) );
    spikes->addColumn( tr2i18n( "Resolution" ) );
    spikes->addColumn( tr2i18n( "Frequency" ) );
    spikes->addColumn( tr2i18n( "Time" ) );
    spikes->addColumn( tr2i18n( "Drift rate" ) );
    tabLayout->addWidget( spikes );
    tab_signals->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( tab_signals, "tab_2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 11, 6, "tabLayout_2"); 

    layout_gaussians = new QHBoxLayout( 0, 0, 6, "layout_gaussians"); 

    gaussians = new KComboBox( FALSE, tab_2, "gaussians" );
    layout_gaussians->addWidget( gaussians );
    spacer_gaussians = new QSpacerItem( 371, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_gaussians->addItem( spacer_gaussians );
    tabLayout_2->addLayout( layout_gaussians );

    gaussian_plot = new KBSSETISignalPlot( tab_2, "gaussian_plot" );
    tabLayout_2->addWidget( gaussian_plot );
    tab_signals->insertTab( tab_2, QString::fromLatin1("") );

    TabPage = new QWidget( tab_signals, "TabPage" );
    TabPageLayout = new QVBoxLayout( TabPage, 11, 6, "TabPageLayout"); 

    layout_pulses = new QHBoxLayout( 0, 0, 6, "layout_pulses"); 

    pulses = new KComboBox( FALSE, TabPage, "pulses" );
    layout_pulses->addWidget( pulses );
    spacer_pulses = new QSpacerItem( 311, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_pulses->addItem( spacer_pulses );
    TabPageLayout->addLayout( layout_pulses );

    pulse_plot = new KBSSETISignalPlot( TabPage, "pulse_plot" );
    TabPageLayout->addWidget( pulse_plot );
    tab_signals->insertTab( TabPage, QString::fromLatin1("") );

    TabPage_2 = new QWidget( tab_signals, "TabPage_2" );
    TabPageLayout_2 = new QVBoxLayout( TabPage_2, 11, 6, "TabPageLayout_2"); 

    triplets = new KListView( TabPage_2, "triplets" );
    triplets->addColumn( tr2i18n( "Triplet" ) );
    triplets->addColumn( tr2i18n( "Power" ) );
    triplets->addColumn( tr2i18n( "Score" ) );
    triplets->addColumn( tr2i18n( "Period" ) );
    triplets->addColumn( tr2i18n( "Resolution" ) );
    triplets->addColumn( tr2i18n( "Frequency" ) );
    triplets->addColumn( tr2i18n( "Time" ) );
    triplets->addColumn( tr2i18n( "Drift rate" ) );
    TabPageLayout_2->addWidget( triplets );
    tab_signals->insertTab( TabPage_2, QString::fromLatin1("") );
    KBSSETIResultsDetailsContentLayout->addWidget( tab_signals );
    languageChange();
    resize( QSize(445, 289).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIResultsDetailsContent::~KBSSETIResultsDetailsContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIResultsDetailsContent::languageChange()
{
    setCaption( tr2i18n( "KBSSETIResultsDetailsContent" ) );
    spikes->header()->setLabel( 0, tr2i18n( "Spike" ) );
    spikes->header()->setLabel( 1, tr2i18n( "Power" ) );
    spikes->header()->setLabel( 2, tr2i18n( "Score" ) );
    spikes->header()->setLabel( 3, tr2i18n( "Signal ratio" ) );
    spikes->header()->setLabel( 4, tr2i18n( "Resolution" ) );
    spikes->header()->setLabel( 5, tr2i18n( "Frequency" ) );
    spikes->header()->setLabel( 6, tr2i18n( "Time" ) );
    spikes->header()->setLabel( 7, tr2i18n( "Drift rate" ) );
    tab_signals->changeTab( tab, tr2i18n( "&Spikes" ) );
    gaussians->clear();
    gaussians->insertItem( tr2i18n( "Best score" ) );
    tab_signals->changeTab( tab_2, tr2i18n( "&Gaussians" ) );
    pulses->clear();
    pulses->insertItem( tr2i18n( "Best score" ) );
    tab_signals->changeTab( TabPage, tr2i18n( "&Pulses" ) );
    triplets->header()->setLabel( 0, tr2i18n( "Triplet" ) );
    triplets->header()->setLabel( 1, tr2i18n( "Power" ) );
    triplets->header()->setLabel( 2, tr2i18n( "Score" ) );
    triplets->header()->setLabel( 3, tr2i18n( "Period" ) );
    triplets->header()->setLabel( 4, tr2i18n( "Resolution" ) );
    triplets->header()->setLabel( 5, tr2i18n( "Frequency" ) );
    triplets->header()->setLabel( 6, tr2i18n( "Time" ) );
    triplets->header()->setLabel( 7, tr2i18n( "Drift rate" ) );
    tab_signals->changeTab( TabPage_2, tr2i18n( "&Triplets" ) );
}

#include "kbssetiresultsdetailscontent.moc"
