/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIPROGRESSPANELNODE_H
#define KBSSETIPROGRESSPANELNODE_H

#include <kbspanelnode.h>

class KBSSETIProjectMonitor;
class KBSSETIProgressContent;

class KBSSETIProgressPanelNode : public KBSPanelNode
{
  Q_OBJECT
  public:
    KBSSETIProgressPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args);
    
    virtual QString name() const;
    virtual QStringList icons() const;

    virtual KBSPanel *createPanel(QWidget *parent=0);
    
    virtual QString result() const;
    virtual QString workunit() const;
    virtual QString project() const;
    
    virtual KBSSETIProjectMonitor *projectMonitor() const;
    
  private:
    void setupMonitor();
    void setupContent(KBSSETIProgressContent *panel);
    
  private slots:
    void updateContent();
    void updateContent(const QString &workunit);
    
  protected:
    QString m_project, m_workunit, m_result;
    KBSSETIProjectMonitor *m_projectMonitor;
    double m_ar;
};

#endif
