/****************************************************************************
** Form interface generated from reading ui file './kbssetiprogresscontent.ui'
**
** Created: Mon Feb 6 18:29:11 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSSETIPROGRESSCONTENT_H
#define KBSSETIPROGRESSCONTENT_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class KBSPanelField;
class QProgressBar;

class KBSSETIProgressContent : public QWidget
{
    Q_OBJECT

public:
    KBSSETIProgressContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSSETIProgressContent();

    KBSPanelField* app;
    KBSPanelField* status;
    KBSPanelField* cpu_time;
    KBSPanelField* rate;
    KBSPanelField* credit;
    KBSPanelField* completed;
    KBSPanelField* total_time;
    KBSPanelField* remaining_time;
    KBSPanelField* total_credit;
    QProgressBar* progress;
    KBSPanelField* report_deadline;

protected:
    QVBoxLayout* KBSSETIProgressContentLayout;
    QSpacerItem* spacer_top;
    QSpacerItem* spacer_bottom;
    QHBoxLayout* layout_group;
    QSpacerItem* spacer_middle;
    QVBoxLayout* layout_left;
    QVBoxLayout* layout_right;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // KBSSETIPROGRESSCONTENT_H
