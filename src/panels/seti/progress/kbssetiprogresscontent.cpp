#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbssetiprogresscontent.ui'
**
** Created: Mon Feb 6 18:29:11 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbssetiprogresscontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <qpushbutton.h>
#include <qprogressbar.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETIProgressContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIProgressContent::KBSSETIProgressContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIProgressContent" );
    KBSSETIProgressContentLayout = new QVBoxLayout( this, 0, 6, "KBSSETIProgressContentLayout"); 

    app = new KBSPanelField( this, "app" );
    KBSSETIProgressContentLayout->addWidget( app );

    layout_group = new QHBoxLayout( 0, 0, 6, "layout_group"); 

    layout_left = new QVBoxLayout( 0, 0, 6, "layout_left"); 

    status = new KBSPanelField( this, "status" );
    layout_left->addWidget( status );

    cpu_time = new KBSPanelField( this, "cpu_time" );
    layout_left->addWidget( cpu_time );

    rate = new KBSPanelField( this, "rate" );
    layout_left->addWidget( rate );

    credit = new KBSPanelField( this, "credit" );
    layout_left->addWidget( credit );
    layout_group->addLayout( layout_left );
    spacer_middle = new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout_group->addItem( spacer_middle );

    layout_right = new QVBoxLayout( 0, 0, 6, "layout_right"); 

    completed = new KBSPanelField( this, "completed" );
    layout_right->addWidget( completed );

    total_time = new KBSPanelField( this, "total_time" );
    layout_right->addWidget( total_time );

    remaining_time = new KBSPanelField( this, "remaining_time" );
    layout_right->addWidget( remaining_time );

    total_credit = new KBSPanelField( this, "total_credit" );
    layout_right->addWidget( total_credit );
    layout_group->addLayout( layout_right );
    KBSSETIProgressContentLayout->addLayout( layout_group );
    spacer_top = new QSpacerItem( 40, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETIProgressContentLayout->addItem( spacer_top );

    progress = new QProgressBar( this, "progress" );
    progress->setProgress( 0 );
    KBSSETIProgressContentLayout->addWidget( progress );
    spacer_bottom = new QSpacerItem( 40, 50, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETIProgressContentLayout->addItem( spacer_bottom );

    report_deadline = new KBSPanelField( this, "report_deadline" );
    KBSSETIProgressContentLayout->addWidget( report_deadline );
    languageChange();
    resize( QSize(124, 86).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIProgressContent::~KBSSETIProgressContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIProgressContent::languageChange()
{
    setCaption( tr2i18n( "Processor" ) );
    setIconText( tr2i18n( "exec" ) );
}

#include "kbssetiprogresscontent.moc"
