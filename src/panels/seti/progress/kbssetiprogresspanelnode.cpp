/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qprogressbar.h>

#include <kgenericfactory.h>
#include <klocale.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbsseticalibrator.h>
#include <kbssetiprojectmonitor.h>

#include <kbssetiprogresscontent.h>

#include <kbsboincdata.h>

#include "kbssetiprogresspanelnode.h"

using namespace KBSBOINC;

class KBSSETIProgressPanelFactory : KGenericFactory<KBSSETIProgressPanelNode,KBSTreeNode>
{
  public:
    KBSSETIProgressPanelFactory() : KGenericFactory<KBSSETIProgressPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbssetiprogresspanel, KBSSETIProgressPanelFactory());

KBSSETIProgressPanelNode::KBSSETIProgressPanelNode(KBSTreeNode *parent, const char *name, const QStringList &args)
                    : KBSPanelNode(parent, name), m_workunit(args[0]), m_ar(0.0)
{
  setupMonitor();
}

QString KBSSETIProgressPanelNode::name() const
{
  return i18n("Progress");
}

QStringList KBSSETIProgressPanelNode::icons() const
{
  return QStringList("progress");
}

KBSPanel *KBSSETIProgressPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSSETIProgressContent *content = new KBSSETIProgressContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

QString KBSSETIProgressPanelNode::workunit() const
{
  return m_workunit;
}

QString KBSSETIProgressPanelNode::result() const
{
  return m_result;
}

QString KBSSETIProgressPanelNode::project() const
{
  return m_project;
}

KBSSETIProjectMonitor *KBSSETIProgressPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

void KBSSETIProgressPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  m_result = state->workunit[m_workunit].result_name;
  
  if(m_project.isEmpty()) return;
  
  m_projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSSETIProgressPanelNode::setupContent(KBSSETIProgressContent *content)
{
  content->app->setName(i18n("Application:"));
  content->status->setName(i18n("Status:"));
  content->completed->setName(i18n("Completed:"));
  content->cpu_time->setName(i18n("CPU time:"));
  content->total_time->setName(i18n("Estimated total time:"));
  content->rate->setName(i18n("Rate:"));
  content->remaining_time->setName(i18n("Estimated time left:"));
  content->credit->setName(i18n("Accrued credit:"));
  content->total_credit->setName(i18n("Estimated total credit:"));
  content->report_deadline->setName(i18n("Report deadline:"));
  
  updateContent();
}

void KBSSETIProgressPanelNode::updateContent()
{
  int progress;
  bool expired;
  QString app, app_tip, status, completed, cpu_time, total_time, rate, remaining_time,
          credit, total_credit, report_deadline;
  
  const KBSBOINCClientState *state = (NULL != monitor()) ? monitor()->state() : NULL;
  const KBSSETIResult *setiResult = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit)
                                                               : NULL;
  
  if(NULL != state)
  {
    app = i18n("%1 version %2")
            .arg(state->workunit[m_workunit].app_name)
            .arg(double(state->workunit[m_workunit].version_num) / 1e2, 0, 'f', 2);
    
    app_tip = state->project[m_project].project_name;
    
    QString result = state->workunit[m_workunit].result_name;
    int task = result.isEmpty() ? -1 : state->active_task_set.index(result);
    
    double secs, prog;
    if(task >= 0) {
      secs = state->active_task_set.active_task[task].current_cpu_time;
      prog = state->active_task_set.active_task[task].fraction_done;
      status = (state->active_task_set.active_task[task].scheduler_state > 1) ? i18n("active")
                                                                              : i18n("paused");
    } else if(!result.isEmpty())
      switch(state->result[result].state) {
        case 0:
          secs = prog = 0.0;
          status = i18n("new file");
          break;
        case 1:
          secs = prog = 0.0;
          status = i18n("downloading");
          break;
        case 2:
          secs = prog = 0.0;
          status = i18n("ready");
          break;
        case 3:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          status = i18n("completed");
          break;
        case 4:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          status = i18n("uploading");
          break;
        case 5:
          secs = state->result[result].final_cpu_time;
          prog = 1.0;
          status = i18n("uploaded");
          break;
        default:
          secs = prog = 0.0;
          status = i18n("unknown");
          break;
      }
    else {
      secs = prog = 0.0;
      status = i18n("new file");
    }
    
    if(NULL != setiResult)
      m_ar = setiResult->workunit_header.group_info.data_desc.true_angle_range;
    
    if(m_ar > 0.0)
      prog = KBSSETICalibrator::self()->calibrate(monitor()->url(), m_ar, prog);
    
    double secs_est = 0.0;
    if(state->host_info.p.fpops > 0.0)
      secs_est = state->workunit[m_workunit].rsc.fpops_est / state->host_info.p.fpops;
    
    KLocale *locale = KGlobal::locale();

    completed = i18n("%1%").arg(locale->formatNumber(prog * 1e2));
    cpu_time = formatTime(secs);
    
    if(prog > 0.0) {
      total_time = formatTime(secs / prog);
      remaining_time = formatTime(secs * (1 / prog - 1));
    } else if(secs_est > 0.0)
      total_time = remaining_time = formatTime(secs_est);
    else
      total_time = remaining_time = i18n("unknown");
    
    if(secs >= 1) 
      rate = i18n("%1% / hour").arg(locale->formatNumber(3600.0 * 1e2 * prog / secs, 2));
    else if(secs_est >= 1)
      rate = i18n("%1% / hour").arg(locale->formatNumber(3600.0 * 1e2 / secs_est, 2));
    else
      rate = i18n("unknown");
    
    progress = int(prog * 1e2 + 0.005);
    
    if(prog > 0.0)
      expired = QDateTime::currentDateTime().addSecs(int(secs * (1 / prog - 1)))
                  >= state->result[result].report_deadline;
    else if(secs_est > secs)
      expired = QDateTime::currentDateTime().addSecs(int(secs_est - secs))
                  >= state->result[result].report_deadline;
    else
      expired = false;
    
    
    credit = locale->formatNumber(state->host_info.credit_per_cpu_sec() * secs, 2);
    
    if(prog > 0.0) 
      total_credit = locale->formatNumber(state->host_info.credit_per_cpu_sec() * secs / prog, 2);
    else if(secs_est > 0.0)
      total_credit = locale->formatNumber(3600.0 * 1e2 / secs_est, 2);
    else
      total_credit = i18n("unknown");
    
    report_deadline = result.isEmpty() ? i18n("unknown")
                                       : locale->formatDateTime(state->result[result].report_deadline, false);
  }
  else {
    progress = 0;
    expired = false;
    app = status = completed = cpu_time = total_time = rate = remaining_time = report_deadline = i18n("unknown");
    app_tip = QString::null;
  }
  
  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSSETIProgressContent *content = static_cast<KBSSETIProgressContent*>(it.current()->content());
      
      content->app->setSqueezedText(app);
      content->app->setTooltip(app_tip);
      content->status->setText(status);
      content->completed->setText(completed);
      content->cpu_time->setText(cpu_time);
      content->total_time->setText(total_time);
      content->rate->setText(rate);
      content->remaining_time->setText(remaining_time);
      content->progress->setProgress(progress);
      content->credit->setText(credit);
      content->total_credit->setText(total_credit);
      content->report_deadline->setText(report_deadline);
      content->report_deadline->setTextColor(expired ? Qt::red : content->status->textColor());
    }
}

void KBSSETIProgressPanelNode::updateContent(const QString &workunit)
{
  if(0.0 == m_ar && workunit == m_workunit) updateContent();
}
  
#include "kbssetiprogresspanelnode.moc"
