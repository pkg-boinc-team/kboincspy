#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbssetiworkunitcontent.ui'
**
** Created: Mon Feb 6 18:29:13 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbssetiworkunitcontent.h"

#include <qvariant.h>
#include <kbspanelfield.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETIWorkunitContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIWorkunitContent::KBSSETIWorkunitContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIWorkunitContent" );
    KBSSETIWorkunitContentLayout = new QVBoxLayout( this, 0, 6, "KBSSETIWorkunitContentLayout"); 

    wu_name = new KBSPanelField( this, "wu_name" );
    KBSSETIWorkunitContentLayout->addWidget( wu_name );

    time_recorded = new KBSPanelField( this, "time_recorded" );
    KBSSETIWorkunitContentLayout->addWidget( time_recorded );

    position = new KBSPanelField( this, "position" );
    KBSSETIWorkunitContentLayout->addWidget( position );

    constellation = new KBSPanelField( this, "constellation" );
    KBSSETIWorkunitContentLayout->addWidget( constellation );

    receiver = new KBSPanelField( this, "receiver" );
    KBSSETIWorkunitContentLayout->addWidget( receiver );

    base_frequency = new KBSPanelField( this, "base_frequency" );
    KBSSETIWorkunitContentLayout->addWidget( base_frequency );
    spacer = new QSpacerItem( 16, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETIWorkunitContentLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_skymap = new QSpacerItem( 160, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_skymap );

    skymap_button = new KPushButton( this, "skymap_button" );
    layout_buttons->addWidget( skymap_button );

    telescope_path_button = new KPushButton( this, "telescope_path_button" );
    layout_buttons->addWidget( telescope_path_button );
    KBSSETIWorkunitContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(220, 78).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIWorkunitContent::~KBSSETIWorkunitContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIWorkunitContent::languageChange()
{
    setCaption( tr2i18n( "KBSSETIWorkunitContent" ) );
    setIconText( QString::null );
    skymap_button->setText( tr2i18n( "&Sky Map" ) );
    skymap_button->setAccel( QKeySequence( tr2i18n( "Alt+S" ) ) );
    telescope_path_button->setText( tr2i18n( "&Telescope Path" ) );
    telescope_path_button->setAccel( QKeySequence( tr2i18n( "Alt+T" ) ) );
}

#include "kbssetiworkunitcontent.moc"
