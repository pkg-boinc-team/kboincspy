/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <klocale.h>

#include <kbsboincmonitor.h>

#include <kbssetiprojectmonitor.h>
#include <kbssetisignalplot.h>
#include <kbssetidata.h>

#include "kbssetitelescopepathwindow.h"

QDict<KBSSETITelescopePathWindow> KBSSETITelescopePathWindow::s_windows
  = QDict<KBSSETITelescopePathWindow>();

KBSSETITelescopePathWindow *KBSSETITelescopePathWindow::window(const QString &workunit)
{
  KBSSETITelescopePathWindow *out = s_windows.find(workunit);
  
  if(NULL == out) {
    out = new KBSSETITelescopePathWindow(workunit);
    s_windows.insert(workunit, out);
  }
  
  return out;
}

KBSSETITelescopePathWindow::KBSSETITelescopePathWindow(const QString &workunit,
                                                       QWidget *parent, const char *name)
                          : KBSStandardWindow(parent, name),
                            m_view(new KBSSETISignalPlot(this)), m_workunit(workunit)
{
  setCaption(i18n("Telescope Path - %1").arg(workunit));
  
  setCentralWidget(m_view);
  
  setupActions();
}

QString KBSSETITelescopePathWindow::workunit() const
{
  return m_workunit;
}

void KBSSETITelescopePathWindow::attachProjectMonitor(KBSSETIProjectMonitor *projectMonitor)
{
  if(m_projectMonitors.containsRef(projectMonitor)) return;
  
  m_projectMonitors.append(projectMonitor);
  
  if(m_projectMonitors.count() == 1) {
    connectProjectMonitor(projectMonitor);
    update();
  }
}

void KBSSETITelescopePathWindow::detachProjectMonitor()
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();
  
  if(NULL != projectMonitor) {
    disconnectProjectMonitor();
    m_projectMonitors.removeRef(projectMonitor);
  }
  
  projectMonitor = m_projectMonitors.first();
    
  if(NULL != projectMonitor) {
    connectProjectMonitor(projectMonitor);
    update();
  } else {
    close();
    destroy();
      
    s_windows.remove(m_workunit);     
  }
}

QPixmap KBSSETITelescopePathWindow::pixmap()
{
  return m_view->pixmap();
}

void KBSSETITelescopePathWindow::update()
{
  KBSSETIProjectMonitor *projectMonitor = m_projectMonitors.first();
  if(NULL == projectMonitor) return;
  
  const KBSSETIResult *result = projectMonitor->result(m_workunit);
  
  if(NULL != result)
    m_view->setData(result->workunit_header);
}

void KBSSETITelescopePathWindow::update(const QString &workunit)
{
  if(workunit == m_workunit) update();
}

void KBSSETITelescopePathWindow::connectProjectMonitor(KBSSETIProjectMonitor *projectMonitor)
{
  connect(projectMonitor, SIGNAL(updatedResult(const QString &)), this, SLOT(update(const QString &)));
  connect(projectMonitor, SIGNAL(destroyed()), this, SLOT(detachProjectMonitor()));
  
  KBSBOINCMonitor *boincMonitor = projectMonitor->boincMonitor();
  
  connect(boincMonitor, SIGNAL(stateUpdated()), this, SLOT(update()));
}

void KBSSETITelescopePathWindow::disconnectProjectMonitor()
{
  disconnect(this, SLOT(update(const QString &)));
  disconnect(this, SLOT(detachProjectMonitor()));
  disconnect(this, SLOT(update()));
}

#include "kbssetitelescopepathwindow.moc"
