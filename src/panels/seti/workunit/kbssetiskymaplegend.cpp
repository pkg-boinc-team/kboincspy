/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlabel.h>
#include <qlayout.h>
#include <qscrollview.h>

#include <klocale.h>
#include <krun.h>
#include <kurllabel.h>

#include <kbssetiskymapwindow.h>

#include "kbssetiskymaplegend.h"

KBSSETISkyMapLegend::KBSSETISkyMapLegend(KBSSETISkyMapWindow *parent, const char *name)
                   : KBSStandardWindow(parent, name)
{
  setCaption(i18n("SETI@home Sky Map Legend"));
  
  setupView();
  
  setAutoSaveGeometry("SETI@home Sky Map Legend");
  
  setupActions();
}

const KBSSETISkyMapWindow *KBSSETISkyMapLegend::skyMap() const
{
  return static_cast<const KBSSETISkyMapWindow*>(parent());
}

QString KBSSETISkyMapLegend::text()
{
  QString out;
  
  const unsigned constellations = skyMap()->constellations();
  for(unsigned i = 0; i < constellations; ++i)
    out.append(i18n("%1\t%2\n").arg(skyMap()->constellation(i))
                               .arg(skyMap()->constellationName(i)));
  
  return out;
}

void KBSSETISkyMapLegend::setupView()
{
  QScrollView *scrollView = new QScrollView(this);
  scrollView->setResizePolicy(QScrollView::AutoOneFit);
  setCentralWidget(scrollView);

  QWidget *view = new QWidget(scrollView);
  QVBoxLayout *layout = new QVBoxLayout(view);
  scrollView->addChild(view);

  QLabel *label;
  KURLLabel *urlLabel;

  label = new QLabel(i18n("Internet links courtesy of"), view);
  label->setAlignment(AlignCenter);
  layout->addWidget(label);

  urlLabel = new KURLLabel(skyMap()->constellationURL().prettyURL(),
                           skyMap()->constellationName(), view);
  urlLabel->setAlignment(AlignCenter);
  urlLabel->setUseTips();
  urlLabel->setTipText(i18n("Click to visit web site"));
  layout->addWidget(urlLabel);

  connect(urlLabel, SIGNAL(leftClickedURL(const QString &)), this, SLOT(handleURL(const QString &)));

  const unsigned constellations = skyMap()->constellations();
  QGridLayout *grid = new QGridLayout(layout, constellations, 2, 2);
  grid->setMargin(5);
  grid->addColSpacing(0, 40);
  grid->setColStretch(1, 1);

  for(unsigned i = 0; i < constellations; i++)
  {
    label = new QLabel(skyMap()->constellation(i), view);
    grid->addWidget(label, i, 0);

    urlLabel = new KURLLabel(skyMap()->constellationURL(i).prettyURL(),
                             skyMap()->constellationName(i), view);
    urlLabel->setUseTips();
    urlLabel->setTipText(i18n("Click for more information"));
    grid->addWidget(urlLabel, i, 1);

    connect(urlLabel, SIGNAL(leftClickedURL(const QString &)), this, SLOT(handleURL(const QString &)));
  }
}

void KBSSETISkyMapLegend::handleURL(const QString &url)
{
  KRun::runURL(url, "text/html", false, false);
}

#include "kbssetiskymaplegend.moc"
