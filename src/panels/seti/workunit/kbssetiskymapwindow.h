/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETISKYMAPWINDOW_H
#define KBSSETISKYMAPWINDOW_H

#include <qdict.h>
#include <qlabel.h>
#include <qptrdict.h>
#include <qptrlist.h>

#include <kactionclasses.h>
#include <kurl.h>

#include <kbsstandardwindow.h>
 
struct KBSSETIConstellation
{
  QString abbrev,
          name,
          id;
  double ra,
        dec;
};

class KBSBOINCMonitor;
class KBSSETIProjectMonitor;
class KBSSETISkyMapLegend;

class KBSSETISkyMapTarget : public QLabel
{
  Q_OBJECT
  public:
    KBSSETISkyMapTarget(const QString &workunit, QWidget *parent=0);
    
    virtual QString workunit() const;
    
    virtual void addProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
    virtual void removeProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
  
  private slots:
    void update();
    void update(const QString &workunit);
    
  private:
    void connectProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
    void disconnectProjectMonitor(KBSSETIProjectMonitor *projectMonitor);
    
  protected:
    QPtrList<KBSSETIProjectMonitor> m_projectMonitors;
    QPtrDict<KBSBOINCMonitor> m_boincMonitors;
    
  private:
    QString m_workunit;

};

class KBSSETISkyMapWindow : public KBSStandardWindow
{
  Q_OBJECT
  public:
    static KBSSETISkyMapWindow *self();
    
    static unsigned constellations();
    static QString constellation(unsigned index);
    static QString constellationName(int index=-1);
    static KURL constellationURL(int index=-1);
    static unsigned findNearestConstellation(double ra, double dec);
    
    virtual bool isHistoryVisible() const;
    virtual void setHistoryVisible(bool set);
    
    virtual void addWorkunit(KBSSETIProjectMonitor *projectMonitor, const QString &workunit);
    virtual void removeWorkunit(KBSSETIProjectMonitor *projectMonitor, const QString &workunit);

    friend class KBSSETISkyMapTarget;
  
  protected:    
    KBSSETISkyMapWindow(QWidget *parent=0, const char *name=0);
    
    virtual QPixmap pixmap();
    
    static QPoint position(double ra, double dec);
    static double distance(const double ra1, const double dec1, const double ra2, const double dec2);

  private slots:
    void buildHistory();
    void updateHistory();
    
    void toggleHistory();
    void showLegend();
    
  private:
    void setupActions();
        
  protected:
    QDict<KBSSETISkyMapTarget> m_targets;
    QPtrList<QLabel> m_history;
    
  private:
    QWidget *m_view;
    bool m_historyVisible;
    KBSSETISkyMapLegend *m_legend;   
    KToggleAction *show_history;
    
    static KBSSETISkyMapWindow *s_self;
    
    static const KBSSETIConstellation s_constellation[];
    
    static const QPixmap s_skyMap, s_targetIdle, s_targetHistory;
    static const QMovie s_targetRunning;
    static const QSize s_targetRunningSize;
};

#endif
