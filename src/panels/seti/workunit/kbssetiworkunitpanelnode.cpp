/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>
#include <kpushbutton.h>

#include <kbsboincmonitor.h>
#include <kbspanel.h>
#include <kbspanelfield.h>

#include <kbssetiprojectmonitor.h>
#include <kbssetitaskmonitor.h>
#include <kbssetiskymapwindow.h>
#include <kbssetitelescopepathwindow.h>
#include <kbssetiworkunitcontent.h>

#include <kbsboincdata.h>
#include <kbssetidata.h>

#include "kbssetiworkunitpanelnode.h"

using namespace KBSSETI;

const QString SETIAreciboName = I18N_NOOP("Arecibo Radio Observatory"); 
const QString SETIAreciboURL  = I18N_NOOP("http://www.naic.edu/");

class KBSSETIWorkunitPanelFactory : KGenericFactory<KBSSETIWorkunitPanelNode,KBSTreeNode>
{
  public:
    KBSSETIWorkunitPanelFactory() : KGenericFactory<KBSSETIWorkunitPanelNode,KBSTreeNode>() {};
};

K_EXPORT_COMPONENT_FACTORY(libkbssetiworkunitpanel, KBSSETIWorkunitPanelFactory());

KBSSETIWorkunitPanelNode::KBSSETIWorkunitPanelNode(KBSTreeNode *parent, const char *name,
                                                   const QStringList &args)
                        : KBSPanelNode(parent, name),
                          m_workunit(args[0]), m_projectMonitor(NULL),
                          m_telescopePath(NULL)
{
  setupMonitor();
  
  if(NULL != m_projectMonitor)
    KBSSETISkyMapWindow::self()->addWorkunit(m_projectMonitor, m_workunit);
}

KBSSETIWorkunitPanelNode::~KBSSETIWorkunitPanelNode()
{
  if(NULL != m_projectMonitor)
    KBSSETISkyMapWindow::self()->removeWorkunit(m_projectMonitor, m_workunit);
}

QString KBSSETIWorkunitPanelNode::name() const
{
  return i18n("Work Unit");
}

QStringList KBSSETIWorkunitPanelNode::icons() const
{
  return QStringList("workunit");
}

KBSPanel *KBSSETIWorkunitPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *out = KBSPanelNode::createPanel(parent);
  KBSSETIWorkunitContent *content = new KBSSETIWorkunitContent(out);
  out->setContent(content);
  setupContent(content);
  
  return out;
}

QString KBSSETIWorkunitPanelNode::project() const
{
  return m_project;
}

QString KBSSETIWorkunitPanelNode::workunit() const
{
  return m_workunit;
}

KBSSETIProjectMonitor *KBSSETIWorkunitPanelNode::projectMonitor() const
{
  return m_projectMonitor;
}

void KBSSETIWorkunitPanelNode::setupMonitor()
{
  if(NULL == monitor()) return;
  
  connect(monitor(), SIGNAL(stateUpdated()), this, SLOT(updateContent()));
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_project = monitor()->project(state->workunit[m_workunit]);
  if(m_project.isEmpty()) return;
  
  m_projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor()->projectMonitor(m_project));
  if(NULL == m_projectMonitor) return;
  
  connect(m_projectMonitor, SIGNAL(updatedResult(const QString &)),
          this, SLOT(updateContent(const QString &)));
}

void KBSSETIWorkunitPanelNode::setupContent(KBSSETIWorkunitContent *content)
{
  content->wu_name->setName(i18n("Work unit name:"));
  content->wu_name->setSqueezedText(m_workunit);
  content->time_recorded->setName(i18n("Recorded:"));
  content->position->setName(i18n("Position:"));
  content->constellation->setName(i18n("Nearest constellation:"));
  content->receiver->setName(i18n("Source:"));
  content->base_frequency->setName(i18n("Base frequency:"));
  
  connect(content->skymap_button, SIGNAL(clicked()), this, SLOT(showSkyMap()));
  connect(content->telescope_path_button, SIGNAL(clicked()), this, SLOT(showTelescopePath()));
  
  updateContent();
}

void KBSSETIWorkunitPanelNode::updateContent()
{
  KURL constellation_url, receiver_url;
  QString time_recorded, position, constellation, constellation_aux, receiver, base_frequency;
  
  const KBSSETIResult *setiResult = (NULL != m_projectMonitor) ? m_projectMonitor->result(m_workunit)
                                                               : NULL;
  
  if(NULL != setiResult)
  {
    KLocale *locale = KGlobal::locale();
    const KBSSETIGroupInfo &group_info = setiResult->workunit_header.group_info;
    const KBSSETISubbandDesc &subband_desc = setiResult->workunit_header.subband_desc;

    time_recorded = locale->formatDateTime(group_info.data_desc.time_recorded, false);
    
    const double ra = group_info.data_desc.start.ra;
    const double dec = group_info.data_desc.start.dec;
    position = i18n("%1 RA, %2 Dec, %3 AR")
                 .arg(formatRA(ra)).arg(formatDec(dec))
                 .arg(locale->formatNumber(group_info.data_desc.true_angle_range, 3));
    
    const unsigned constellation_id = KBSSETISkyMapWindow::findNearestConstellation(ra, dec);
    constellation = KBSSETISkyMapWindow::constellationName(constellation_id);
    constellation_url = KBSSETISkyMapWindow::constellationURL(constellation_id);
    constellation_aux = QString("(%1)").arg(KBSSETISkyMapWindow::constellation(constellation_id));
    
    if(group_info.receiver_cfg.name.startsWith("ao")) {
      receiver = i18n(SETIAreciboName);
      receiver_url = i18n(SETIAreciboURL);
    } else
      receiver = i18n("unknown");
    
    base_frequency = i18n("%1 GHz").arg(locale->formatNumber(subband_desc.base / 1e9, 5)); 
  }
  else
    time_recorded = position = constellation = receiver = base_frequency = i18n("unknown");

  for(QPtrListIterator<KBSPanel> it(m_panels); NULL != it.current(); ++it)
    if(NULL != it.current()->content())
    {
      KBSSETIWorkunitContent *content = static_cast<KBSSETIWorkunitContent*>(it.current()->content());
      
      content->time_recorded->setText(time_recorded);
      content->position->setText(position);
            
      if(constellation_url.isValid()) {
        content->constellation->setURL(constellation_url.prettyURL(), constellation,
                                       i18n("Click to visit the web page"));
        content->constellation->setAux(constellation_aux);
      } else
        content->constellation->setText(constellation);
      
      if(receiver_url.isValid())
        content->receiver->setURL(receiver_url.prettyURL(), receiver,
                                  i18n("Click to visit the web page"));
      else
        content->receiver->setText(receiver);
      
      content->base_frequency->setText(base_frequency);
    }
}

void KBSSETIWorkunitPanelNode::updateContent(const QString &workunit)
{
  if(workunit == m_workunit) updateContent();
}

void KBSSETIWorkunitPanelNode::showSkyMap()
{
  KBSSETISkyMapWindow *skymap = KBSSETISkyMapWindow::self();
  
  if(!skymap->isVisible()) skymap->show();
}

void KBSSETIWorkunitPanelNode::showTelescopePath()
{
  if(NULL == m_projectMonitor) return;
  
  if(NULL == m_telescopePath) {
    m_telescopePath = KBSSETITelescopePathWindow::window(m_workunit);
    m_telescopePath->attachProjectMonitor(m_projectMonitor);
  }
  
  if(!m_telescopePath->isVisible()) m_telescopePath->show();
}

#include "kbssetiworkunitpanelnode.moc"
