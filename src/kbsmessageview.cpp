/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlayout.h>
#include <qpopupmenu.h>

#include <kapplication.h>
#include <kglobal.h>
#include <kiconloader.h>
#include <klocale.h>
#include <kstatusbar.h>

#include <kbsboincmonitor.h>
#include <kbsrpcmonitor.h>

#include "kbsmessageview.h"

KBSMessageView::KBSMessageView(KBSBOINCMonitor *monitor, QWidget *parent, const char *name)
              : KBSStandardWindow(parent, name),
                m_view(new KListView(this)), m_monitor(monitor)
{
  KBSRPCMonitor *rpcMonitor = monitor->rpcMonitor();
  
  connect(rpcMonitor, SIGNAL(messagesUpdated()), this, SLOT(updateMessages()));
  
  const QString host = rpcMonitor->host();
  
  setCaption(i18n("Messages - %1").arg(host));
  
  setCentralWidget(m_view);
  setupView();
  
  setAutoSaveGeometry(QString("%1 - Messages").arg(host));
  
  setupActions();
  
  updateMessages();
}

KBSMessageView::~KBSMessageView()
{
  const QString group = autoSaveGroup();
  if(!group.isEmpty()) m_view->saveLayout(kapp->config(), group);
}

KBSBOINCMonitor *KBSMessageView::monitor()
{
  return m_monitor;
}

QString KBSMessageView::text()
{
  QString text = "";
  
  QListViewItem *item = m_view->firstChild();
  while(NULL != item)
  {
    QStringList fields;
    for(int column = 0; column < m_view->columns(); ++column)
      fields << item->text(column);
    text.append(fields.join("\t") + "\n");
    
    item = item->nextSibling();
  }
  
  return text;
}

void KBSMessageView::setAutoSaveGeometry(const QString &group)
{
  KBSStandardWindow::setAutoSaveGeometry(group);
  m_view->restoreLayout(kapp->config(), group);
}

void KBSMessageView::updateMessages()
{
  const QValueList<KBSBOINCMsg> messages = m_monitor->rpcMonitor()->messages()->msg;
  
  if(m_view->childCount() >= int(messages.count()))
    m_view->clear();
  
  QListViewItem *last = NULL;
  for(int i = m_view->childCount(); i < int(messages.count()); ++i)
    last = new Item(m_view, messages[i]);
  
  if(NULL != last)
    m_view->ensureItemVisible(last);
}

void KBSMessageView::slotContextMenu(KListView *, QListViewItem *, const QPoint &pos)
{
  QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
  context->popup(pos);
}

void KBSMessageView::setupView()
{
  m_view->addColumn(i18n("Project"));
  m_view->setColumnAlignment(0, AlignLeft);
  m_view->addColumn(i18n("Message"));  
  m_view->setColumnAlignment(1, AlignLeft);
  m_view->addColumn(i18n("Time"));
  m_view->setColumnAlignment(2, AlignLeft);
  
  m_view->setFocusPolicy(QWidget::NoFocus);
  m_view->setSelectionMode(QListView::NoSelection);
  
  m_view->setShowSortIndicator(true);
  m_view->setSorting(2);
  m_view->sort();
 
  connect(m_view, SIGNAL(contextMenu(KListView *, QListViewItem *, const QPoint &)),
          this, SLOT(slotContextMenu(KListView *, QListViewItem *, const QPoint &))); 
}

KBSMessageView::Item::Item(QListView *parent, const KBSBOINCMsg &msg)
                    : KListViewItem(parent), m_msg(msg)
{
  setText(0, msg.project);
  setPixmap(1, msg.pri > 1 ? SmallIcon("messagebox_critical") : SmallIcon("messagebox_info"));
  setText(1, msg.body);
  setText(2, KGlobal::locale()->formatDateTime(msg.time));
}

QString KBSMessageView::Item::key(int column, bool ascending) const
{
  switch(column) {
    case 0:
      return m_msg.project;
    case 1:
      return m_msg.body;
    case 2:
      return QString().sprintf("%09u", m_msg.seqno);
    default:
      return QListViewItem::key(column, ascending);
  }
}

#include "kbsmessageview.moc"
