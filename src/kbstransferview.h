/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSTRANSFERVIEW_H
#define KBSTRANSFERVIEW_H

#include <qmap.h>

#include <kaction.h>
#include <klistview.h>
#include <kmainwindow.h>

class KBSBOINCMonitor;
class KBSRPCMonitor;

class KBSTransferView : public KMainWindow
{
  Q_OBJECT
  public:
    KBSTransferView(KBSBOINCMonitor *monitor, QWidget *parent=0,
                    const char *name=0, WFlags f=WType_TopLevel);
    virtual ~KBSTransferView();
  
  protected:
    virtual KBSBOINCMonitor *monitor();
  
    virtual void setAutoSaveGeometry(const QString &group);
  
  private slots:
    void updateFileTransfers();
    void updateState();
    
    void transferRetry();
    void transferAbort();
    
    void slotContextMenu(QListViewItem *item, const QPoint &pos, int col);
  
  private:
    void setupView();
    void setupActions();
    
    void readGeometry(const QString &group);
    void writeGeometry(const QString &group);
  
  protected:
    KListView *m_view;

  private:
    class Item : public KListViewItem {
      public:
        Item(QListView *parent);
        
        virtual const KBSBOINCFileTransfer &transfer() const;
        virtual void setTransfer(const KBSBOINCFileTransfer &transfer);
        
        virtual bool canRetry() const;
        virtual bool canAbort() const;
        
        virtual bool isSuspended() const;
        virtual void setSuspended(bool suspended);
        
        virtual QString sizeRatio() const;
        virtual QString speed() const;
        virtual QString status() const;
        
        virtual void paintCell(QPainter *p, const QColorGroup &cg,
                               int column, int width, int align);
        
        virtual QString key(int column, bool ascending) const;
      
      private:
        KBSBOINCFileTransfer m_transfer;
        bool m_suspended;
    };

    KBSBOINCMonitor *m_monitor;
    KAction *transfer_retry, *transfer_abort;
};

#endif
