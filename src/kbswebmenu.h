/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSWEBMENU_H
#define KBSWEBMENU_H

#include <kpopupmenu.h>

class KBOINCMonitor;
class KBSWebToolBar;

class KBSWebMenu : public KPopupMenu
{
  Q_OBJECT
  public:
    KBSWebMenu(const QString &project, KBSWebToolBar *parent);
     
    QString project() const;
    KBSBOINCMonitor *monitor() const;
  
  public slots:
    void update();
  
  private slots:
    void slotActivated(int id);
  
  private:
    QString m_project;
    KBSBOINCMonitor *m_monitor;
};

#endif
