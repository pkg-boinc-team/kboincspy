/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qcheckbox.h>
#include <qgroupbox.h>

#include <kaccel.h>
#include <kapplication.h>
#include <kconfigdialog.h>
#include <kfiledialog.h>
#include <klineedit.h>
#include <klocale.h>
#include <kmenubar.h>
#include <kmessagebox.h>
#include <knotifydialog.h>
#include <kpassdlg.h>

#include <kbsconfigpage.h>
#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbshostnode.h>
#include <kbslocationdialog.h>
#include <kbsmessageview.h>
#include <kbspanel.h>
#include <kbspanelview.h>
#include <kbspreferences.h>
#include <kbsprojectwizard.h>
#include <kbsprojectnode.h>
#include <kbsprojectplugin.h>
#include <kbsproxydialog.h>
#include <kbsrpcmonitor.h>
#include <kbssystemtray.h>
#include <kbstransferview.h>
#include <kbstreeview.h>
#include <kbsview.h>
#include <kbswebtoolbar.h>
#include <kbsworkunitnode.h>

#include <kbsboincdata.h>

#include "kboincspy.h"

KMainWindow *kmain = NULL;

KBoincSpy::KBoincSpy()
         : KMainWindow(0, "KBoincSpy"),
           m_document(new KBSDocument(this)),
           m_view(new KBSView(m_document, this)),
           m_tray(new KBSSystemTray(this)),
           m_toolbar(NULL),
           m_show(false), m_close(false),
           m_host(NULL), m_project(NULL), m_workunit(NULL), m_panel(NULL)
{
  if(NULL == kmain) kmain = this;
  
  setCentralWidget(m_view);
  setupActions();
  setAutoSaveSettings("KBoincSpy", false);
  
  connect(m_document, SIGNAL(childRemoved(KBSTreeNode *)), this, SLOT(removeViews(KBSTreeNode *)));
  
  connect(m_view, SIGNAL(currentTreeViewChanged()), this, SLOT(updateTreeViewSelection()));
  
  for(unsigned treeView = KBSView::NestedView; treeView <= KBSView::FlattenedView; ++treeView)
    connect(m_view->treeView(KBSView::TreeView(treeView)), SIGNAL(selectionChanged()),
            this, SLOT(updateTreeViewSelection()));
  
  for(unsigned panelView = KBSView::SimpleView; panelView <= KBSView::TabbedView; ++panelView) 
    connect(m_view->panelView(KBSView::PanelView(panelView)), SIGNAL(currentPanelChanged()),
            this, SLOT(updatePanelVisible()));
  
  readConfig();
  
  if(m_show) show();
}

KBoincSpy::~KBoincSpy()
{
  for(QPtrDictIterator<KBSMessageView> it(m_messages); it.current() != NULL; ++it) {
    it.current()->close();
    delete it.current();
  }
  m_messages.clear();
  
  for(QPtrDictIterator<KBSTransferView> it(m_transfers); it.current() != NULL; ++it) {
    it.current()->close();
    delete it.current();
  }
  m_transfers.clear();
}

KBSDocument *KBoincSpy::document() const
{
  return m_document;
}

QSize KBoincSpy::sizeHint() const
{
  return QSize(420, 200);
}

void KBoincSpy::applyPreferences()
{
  KBSPreferences *preferences = static_cast<KBSPreferences*>(m_document->preferences());
  
  if(preferences->use_tray()) {
    m_tray->show();
    m_show = !preferences->startup_tray();
  } else {
    m_tray->hide();
    show();
  }
}

void KBoincSpy::quit()
{
  m_close = true;
  close();
}

void KBoincSpy::readConfig()
{
  KConfig *config = kapp->config();
  if(NULL == config) return;
  
  readGeometry(config, "KBoincSpy");
  
  config->setGroup("KBoincSpy");
  
  tabbed_view->setChecked(config->readBoolEntry("Tabbed view", false));
  activateTabbedView();
  
  if(config->readBoolEntry("Icon Mode", true))
    activateIconView();
  else
    activateTreeView();
  
  unsigned size = config->readUnsignedNumEntry("Icon Size", KIcon::SizeMedium);
  switch(size) {
    case KIcon::SizeHuge:
      activateHugeIcons();
      break;
    case KIcon::SizeLarge:
      activateLargeIcons();
      break;
    case KIcon::SizeMedium:
      activateMediumIcons();
      break;
    default:
      activateSmallIcons();
      break;
  }
  
  m_view->readConfig(config);

  m_document->readConfig(config);
  
  applyPreferences();
}

void KBoincSpy::writeConfig()
{
  KConfig *config = kapp->config();
  if(NULL == config) return;
  
  m_document->writeConfig(config);

  m_view->writeConfig(config);
 
  config->setGroup("KBoincSpy");
  
  config->writeEntry("Tabbed view",
    m_view->currentPanelView() == m_view->panelView(KBSView::TabbedView));
   
  KBSFlattenedTreeView *iconView =
    static_cast<KBSFlattenedTreeView*>(m_view->treeView(KBSView::FlattenedView));
  
  config->writeEntry("Icon Mode", m_view->currentTreeView() == iconView);
  config->writeEntry("Icon Size", iconView->iconSize());
  
  writeGeometry(config, "KBoincSpy");
}

bool KBoincSpy::queryClose()
{
  if(!m_close && !kapp->sessionSaving()) {
    if(!m_tray->isVisible())
      kapp->quit();
    else
      hide();
    return false;
  }
  
  writeConfig();
  
  return true;
}

void KBoincSpy::setupActions()
{
  setStandardToolBarMenuEnabled(true);
  
  new KAction(i18n("&Add Location..."), KStdAccel::shortcut(KStdAccel::Open),
              this, SLOT(locationAdd()), actionCollection(), "location_add");
  
  new KAction(i18n("&Remove Location"), 0,
              this, SLOT(locationRemove()), actionCollection(), "location_remove");
  
  KStdAction::quit(this, SLOT(quit()), actionCollection());
  
  
  KStdAction::copy(this, SLOT(editCopy()), actionCollection());
  
  
  tabbed_view = new KToggleAction(i18n("Use &Tabs"), 0,
                                  this, SLOT(activateTabbedView()),
                                  actionCollection(), "view_tabbed");
  
  icon_view = new KRadioAction(i18n("&Icon View"), 0,
                               this, SLOT(activateIconView()),
                               actionCollection(), "view_iconview");
  icon_view->setExclusiveGroup("viewmode");
  
  tree_view = new KRadioAction(i18n("&Tree View"), 0,
                               this, SLOT(activateTreeView()),
                               actionCollection(), "view_treeview");
  tree_view->setExclusiveGroup("viewmode");
  
  icon_small = new KRadioAction(i18n("&Small"), 0,
                                this, SLOT(activateSmallIcons()),
                                actionCollection(), "view_smallicons");
  icon_small->setExclusiveGroup("iconsize");
  
  icon_medium = new KRadioAction(i18n("&Medium"), 0,
                                 this, SLOT(activateMediumIcons()),
                                 actionCollection(), "view_mediumicons");
  icon_medium->setExclusiveGroup("iconsize");
  
  icon_large = new KRadioAction(i18n("&Large"), 0,
                                this, SLOT(activateLargeIcons()),
                                actionCollection(), "view_largeicons");
  icon_large->setExclusiveGroup("iconsize");
  
  icon_huge = new KRadioAction(i18n("&Huge"), 0,
                               this, SLOT(activateHugeIcons()),
                               actionCollection(), "view_hugeicons");
  icon_huge->setExclusiveGroup("iconsize");
  
  
  new KAction(i18n("Run &Benchmarks"), 0, this, SLOT(runBenchmarks()),
              actionCollection(), "run_benchmarks");
  
  run_auto = new KRadioAction(i18n("Run Based on &Preferences"), 0,
                              this, SLOT(activateRunAuto()),
                              actionCollection(), "run_auto");
  run_auto->setExclusiveGroup("runmode");
  
  run_always = new KRadioAction(i18n("Run &Always"), 0,
                                this, SLOT(activateRunAlways()),
                                actionCollection(), "run_always");
  run_always->setExclusiveGroup("runmode");
  
  run_never = new KRadioAction(i18n("&Suspend"), 0,
                               this, SLOT(activateRunNever()),
                               actionCollection(), "run_never");
  run_never->setExclusiveGroup("runmode");
  
  new KAction(i18n("Show &Messages"), 0, this, SLOT(clientMessages()),
              actionCollection(), "client_messages");
  
  new KAction(i18n("Show File &Transfers"), 0, this, SLOT(fileTransfers()),
              actionCollection(), "file_transfers");
  
  network_connect = new KToggleAction(i18n("Work &Offline"), 0,
                                      this, SLOT(activateNetworkConnect()),
                                      actionCollection(), "network_connect");
  
  new KAction(i18n("&Proxy Server..."), 0, this, SLOT(networkProxy()),
              actionCollection(), "network_proxy");
  
  
  new KAction(i18n("&Attach to Project..."), 0, this, SLOT(projectAttach()),
              actionCollection(), "project_attach");
  
  project_update = new KAction(i18n("&Update Project"), 0,
                               this, SLOT(projectUpdate()),
                               actionCollection(), "project_update");
  project_actions.append(project_update);
  
  project_suspend = new KAction(i18n("&Suspend Project"), 0,
                                this, SLOT(activateProjectSuspend()),
                                actionCollection(), "project_suspend");
  project_actions.append(project_suspend);
  
  project_reset = new KAction(i18n("&Reset Project"), 0,
                              this, SLOT(projectReset()),
                              actionCollection(), "project_reset");
  project_actions.append(project_reset);
  
  project_detach = new KAction(i18n("&Detach from Project"), 0,
                               this, SLOT(projectDetach()),
                               actionCollection(), "project_detach");
  project_actions.append(project_detach);
  
  project_extinguish = new KToggleAction(i18n("Don't &Get New Work"), 0,
                                         this, SLOT(activateProjectExtinguish()),
                                         actionCollection(), "project_extinguish");
  project_actions.append(project_extinguish);
  
  
  show_graphics = new KAction(i18n("Show &Graphics"), 0,
                                this, SLOT(showGraphics()),
				actionCollection(), "show_graphics");
  result_actions.append(show_graphics);
  
  result_suspend = new KAction(i18n("&Suspend Work Unit"), 0,
                               this, SLOT(activateResultSuspend()),
                               actionCollection(), "result_suspend");
  result_actions.append(result_suspend);
  
  result_abort = new KAction(i18n("&Abort Work Unit"), 0, this, SLOT(resultAbort()),
                             actionCollection(), "result_abort");
  result_actions.append(result_abort);
  
  
  KBSTabbedPanelView *view = static_cast<KBSTabbedPanelView*>(m_view->panelView(KBSView::TabbedView));
  
  new KAction(i18n("&Close"), 0, view, SLOT(closeThisPanel()),
              actionCollection(), "close_tab");
  new KAction(i18n("Close &All"), 0, view, SLOT(closeAllPanels()),
              actionCollection(), "close_all_tabs");
  new KAction(i18n("Close &Others"), 0, view, SLOT(closeOtherPanels()),
              actionCollection(), "close_other_tabs");
  
  show_menubar = KStdAction::showMenubar(this, SLOT(optionsShowMenubar()), actionCollection());
  KStdAction::configureNotifications(this, SLOT(optionsConfigureNotifications()), actionCollection());
  KStdAction::preferences(this, SLOT(optionsPreferences()), actionCollection());
  
  createGUI("kboincspyui.rc", false);
  
  updateMenuBarStatus();
  
  activateTabbedView();
  
  activateTreeView();
  activateSmallIcons();
  
  updateTreeViewSelection();
  updatePanelVisible();
  
  delete toolBar();
  m_toolbar = new KBSWebToolBar(this, "webToolBar");
}

void KBoincSpy::readGeometry(KConfig *config, const QString &group)
{
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
  
  QRect geometry;
  
  geometry.setTop(config->readNumEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), -1));
  if(geometry.top() < 0) return;
  
  geometry.setLeft(config->readNumEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), -1));
  if(geometry.left() < 0) return;
  
  geometry.setHeight(config->readNumEntry(QString("Height %1").arg(desk.height()), 0));
  if(geometry.height() <= 0) return;
  
  geometry.setWidth(config->readNumEntry(QString("Width %1").arg(desk.width()), -1));
  if(geometry.width() < 0) return;
  
  setGeometry(geometry);
}

void KBoincSpy::writeGeometry(KConfig *config, const QString &group)
{
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
  
  config->writeEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), geometry().top());
  config->writeEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), geometry().left());
  config->writeEntry(QString("Height %1").arg(desk.height()), geometry().height());
  config->writeEntry(QString("Width %1").arg(desk.width()), geometry().width());
}

void KBoincSpy::locationAdd()
{
  KBSLocationDialog *dialog = new KBSLocationDialog(this);
  if(dialog->exec() == KDialog::Accepted)
    m_document->connectTo(dialog->location());
  delete dialog;
}

void KBoincSpy::locationRemove()
{
  if(NULL == m_host) return;
  
  const QString message = i18n("Are you sure you want to remove this location?");
  if(KMessageBox::warningYesNo(this, message) == KMessageBox::Yes) 
    m_document->disconnectFrom(m_host->monitor()->location());
  
  m_host = NULL;
}

void KBoincSpy::editCopy()
{
  if(NULL == m_panel) return;
  
  m_panel->editCopy();
}

void KBoincSpy::activateTabbedView()
{
  if(tabbed_view->isChecked())
    m_view->setCurrentPanelView(KBSView::TabbedView);
  else
    m_view->setCurrentPanelView(KBSView::SimpleView);
  
  updatePanelVisible();
}

void KBoincSpy::activateIconView()
{
  m_view->setCurrentTreeView(KBSView::FlattenedView);
  
  icon_view->setChecked(true);
  stateChanged("icon_view");
}

void KBoincSpy::activateTreeView()
{
  m_view->setCurrentTreeView(KBSView::NestedView);
  
  tree_view->setChecked(true);
  stateChanged("icon_view", StateReverse);
}

void KBoincSpy::activateSmallIcons()
{
  KBSFlattenedTreeView *iconView =
    static_cast<KBSFlattenedTreeView*>(m_view->treeView(KBSView::FlattenedView)); 
  iconView->setIconSize(KIcon::SizeSmall);
  
  icon_small->setChecked(true);
}

void KBoincSpy::activateMediumIcons()
{
  KBSFlattenedTreeView *iconView =
    static_cast<KBSFlattenedTreeView*>(m_view->treeView(KBSView::FlattenedView)); 
  iconView->setIconSize(KIcon::SizeMedium);
  
  icon_medium->setChecked(true);
}

void KBoincSpy::activateLargeIcons()
{
  KBSFlattenedTreeView *iconView =
    static_cast<KBSFlattenedTreeView*>(m_view->treeView(KBSView::FlattenedView)); 
  iconView->setIconSize(KIcon::SizeLarge);
  
  icon_large->setChecked(true);
}

void KBoincSpy::activateHugeIcons()
{
  KBSFlattenedTreeView *iconView =
    static_cast<KBSFlattenedTreeView*>(m_view->treeView(KBSView::FlattenedView)); 
  iconView->setIconSize(KIcon::SizeHuge);
  
  icon_huge->setChecked(true);
}

void KBoincSpy::activateRunAuto()
{
  if(NULL == m_host) return;
  
  m_host->monitor()->rpcMonitor()->setRunMode(RunAuto);
}

void KBoincSpy::activateRunAlways()
{
  if(NULL == m_host) return;
  
  m_host->monitor()->rpcMonitor()->setRunMode(RunAlways);
}

void KBoincSpy::activateRunNever()
{
  if(NULL == m_host) return;
  
  m_host->monitor()->rpcMonitor()->setRunMode(RunNever);
}

void KBoincSpy::clientMessages()
{
  if(NULL == m_host) return;
  
  KBSMessageView *view = m_messages.find(m_host);
  
  if(NULL == view) {
    view = new KBSMessageView(m_host->monitor(), this, m_host->name());
    m_messages.insert(m_host, view);
  }
  
  if(!view->isVisible()) view->show();
}

void KBoincSpy::fileTransfers()
{
  if(NULL == m_host) return;

  KBSTransferView *view = m_transfers.find(m_host);

  if(NULL == view) {
    view = new KBSTransferView(m_host->monitor(), this, m_host->name());
    m_transfers.insert(m_host, view);
  }

  if(!view->isVisible()) view->show();
}

void KBoincSpy::activateNetworkConnect()
{
  if(NULL == m_host) return;
  
  if(network_connect->isChecked())
    m_host->monitor()->rpcMonitor()->setNetworkMode(ConnectNever);
  else
    m_host->monitor()->rpcMonitor()->setNetworkMode(ConnectAlways);
}

void KBoincSpy::runBenchmarks()
{
  if(NULL == m_host) return;
  
  m_host->monitor()->rpcMonitor()->runBenchmarks();
}

void KBoincSpy::networkProxy()
{
  if(NULL == m_host) return;
  
  KBSProxyDialog *dialog = new KBSProxyDialog(this);
  
  const KBSBOINCClientState *state = m_host->monitor()->state();
  if(NULL != state)
  {
    dialog->socks->setChecked(!state->proxy_info.socks.server.name.isEmpty());
    dialog->socks_server->setText(state->proxy_info.socks.server.name);
    dialog->socks_port->setText(QString::number(state->proxy_info.socks.server.port));
    dialog->socks_auth->setChecked(!state->proxy_info.socks.user.name.isEmpty());
    dialog->socks_user->setText(state->proxy_info.socks.user.name);
    dialog->socks_password->setText(state->proxy_info.socks.user.passwd);
    dialog->http->setChecked(!state->proxy_info.http.server.name.isEmpty());
    dialog->http_server->setText(state->proxy_info.http.server.name);
    dialog->http_port->setText(QString::number(state->proxy_info.http.server.port));
    dialog->http_auth->setChecked(!state->proxy_info.http.user.name.isEmpty());
    dialog->http_user->setText(state->proxy_info.http.user.name);
    dialog->http_password->setText(state->proxy_info.http.user.passwd);
  }
  
  if(dialog->exec() == KDialog::Accepted)
  {
    KBSBOINCProxyInfo proxy_info;
    
    proxy_info.socks.server.name = dialog->socks->isChecked() ? dialog->socks_server->text()
                                                              : QString::null;
    
    proxy_info.socks.server.port = dialog->socks_port->text().toUInt(0, 10);
    
    proxy_info.socks.user.name = dialog->socks_auth->isChecked() ? dialog->socks_user->text()
                                                                 : QString::null;
    
    proxy_info.socks.user.passwd = dialog->socks_password->text();
    
    proxy_info.http.server.name = dialog->http->isChecked() ? dialog->http_server->text()
                                                            : QString::null;
    
    proxy_info.http.server.port = dialog->http_port->text().toUInt(0, 10);
    
    proxy_info.http.user.name = dialog->http_auth->isChecked() ? dialog->http_user->text()
                                                               : QString::null;
    
    proxy_info.http.user.passwd = dialog->http_password->text();
    
    
    m_host->monitor()->rpcMonitor()->setProxyInfo(proxy_info);
  }
}

void KBoincSpy::projectAttach()
{
  if(NULL == m_host) return;
  
  KBSProjectWizard wizard(m_host->monitor(), this);
  wizard.exec();
}

void KBoincSpy::projectDetach()
{
  if(NULL == m_host || NULL == m_project) return;
  
  const QString message = i18n("Are you sure you want to detach host %1\nfrom project %2?")
                            .arg(m_host->name()).arg(m_project->name());
  if(KMessageBox::warningYesNo(this, message) == KMessageBox::Yes) 
    m_host->monitor()->rpcMonitor()->detachProject(m_project->projectURL());
}

void KBoincSpy::projectReset()
{
  if(NULL == m_host || NULL == m_project) return;
  
  const QString message = i18n("Are you sure you want to reset project %2\non host %1?")
                            .arg(m_host->name()).arg(m_project->name());
  if(KMessageBox::warningYesNo(this, message) == KMessageBox::Yes) 
    m_host->monitor()->rpcMonitor()->resetProject(m_project->projectURL());
}

void KBoincSpy::projectUpdate()
{
  if(NULL == m_host || NULL == m_project) return;
  
  m_host->monitor()->rpcMonitor()->updateProject(m_project->projectURL());
}

void KBoincSpy::activateProjectSuspend()
{
  if(NULL == m_host || NULL == m_project) return;
  
  m_host->monitor()->rpcMonitor()->suspendProject(m_project->projectURL(),
                                                  !m_project->isSuspended());
}

void KBoincSpy::activateProjectExtinguish()
{
  if(NULL == m_host || NULL == m_project) return;
  
  m_host->monitor()->rpcMonitor()->extinguishProject(m_project->projectURL(),
                                                     project_extinguish->isChecked());
}

void KBoincSpy::showGraphics()
{
  if(NULL == m_host || NULL == m_workunit) return;
  
  m_host->monitor()->rpcMonitor()->showGraphics(m_workunit->projectURL(),
                                                m_workunit->result());
}

void KBoincSpy::resultAbort()
{
  if(NULL == m_host || NULL == m_workunit) return;
  
  const QString message = i18n("Are you sure you want to abort workunit %2\non host %1?")
                            .arg(m_host->name()).arg(m_workunit->name());
  m_host->monitor()->rpcMonitor()->abortResult(m_workunit->project(), m_workunit->result());
}

void KBoincSpy::activateResultSuspend()
{
  if(NULL == m_host || NULL == m_workunit) return;
  
  m_host->monitor()->rpcMonitor()->suspendResult(m_workunit->projectURL(),
                                                 m_workunit->result(),
                                                 !m_workunit->isSuspended());
}

void KBoincSpy::optionsShowMenubar()
{
  if(menuBar()->isVisibleTo(this)) {
    menuBar()->hide();    
  } else
    menuBar()->show();
  
  updateMenuBarStatus();
}

void KBoincSpy::optionsConfigureNotifications()
{
  (void) KNotifyDialog::configure(this);
}

void KBoincSpy::optionsPreferences()
{
  if(KConfigDialog::showDialog("preferences")) return;
  
  KConfigDialog *dialog = new KConfigDialog(this, "preferences", m_document->preferences());
  connect(dialog, SIGNAL(settingsChanged()), m_document, SLOT(applyPreferences()));
  connect(dialog, SIGNAL(settingsChanged()), this, SLOT(applyPreferences()));
  
  KBSConfigPage *page = new KBSConfigPage(0, "kboincspy");
  dialog->addPage(page, page->iconText(), page->name(), page->caption());
  
  QPtrList<KBSProjectPlugin> plugins = m_document->plugins();
  for(QPtrListIterator<KBSProjectPlugin> it(plugins); it.current() != NULL; ++it)
  {
    KConfigSkeleton *preferences = it.current()->preferences();
    if(NULL == preferences) continue;
    
    QPtrList<QWidget> pages = it.current()->createConfigPages();
    if(pages.isEmpty()) continue;
    
    for(QPtrListIterator<QWidget> page(pages); NULL != page.current(); ++page)
    {
      QWidget *widget = page.current();
      dialog->addPage(widget, preferences, widget->iconText(), widget->name(), widget->caption());
    }
  }
  
  dialog->setMaximumSize(0, 0);
  dialog->show();  
}

void KBoincSpy::removeViews(KBSTreeNode *host)
{
  KBSMessageView *messageView = m_messages.find(host);
  if(NULL != messageView) {
    messageView->close();
    m_messages.remove(host);
    delete messageView;
  }

  KBSTransferView *transferView = m_transfers.find(host);
  if(NULL != transferView) {
    transferView->close();
    m_transfers.remove(host);
    delete transferView;
  }
}

void KBoincSpy::updateMenuBarStatus()
{
  unplugActionList("show_menubar");
  if(!menuBar()->isVisibleTo(this)) {
    QPtrList<KAction> actions;
    actions.append(show_menubar);
    plugActionList("show_menubar", actions);
  }  
}

void KBoincSpy::updateTreeViewSelection()
{
  if(NULL != m_host) disconnect(this, SLOT(updateRunMode()));
  if(NULL != m_host) disconnect(this, SLOT(updateNetworkMode()));
  if(NULL != m_project) disconnect(this, SLOT(updateProjectState(KBSTreeNode *)));
  if(NULL != m_workunit) disconnect(this, SLOT(updateResultState(KBSTreeNode *)));
  
  KBSTreeNode *node = m_view->currentTreeView()->selection();
  
  m_project = (NULL != node) ? static_cast<KBSProjectNode*>(node->findAncestor("KBSProjectNode"))
                             : NULL;
  
  m_host = (NULL != node) ? static_cast<KBSHostNode*>(node->findAncestor("KBSHostNode"))
                          : NULL;
  
  m_workunit = (NULL != node) ? static_cast<KBSWorkunitNode*>(node->findAncestor("KBSWorkunitNode"))
                              : NULL;
  
  bool canRPC = (NULL != m_host) ? m_host->monitor()->rpcMonitor()->canRPC() : false;
  
  if(NULL != m_host)
    stateChanged("host_selected");
  else
    stateChanged("host_selected", StateReverse);
  
  if(canRPC)
    stateChanged("host_selected_guirpc");
  else
    stateChanged("host_selected_guirpc", StateReverse);
    
  if((NULL != m_project) && canRPC)
    stateChanged("project_selected_guirpc");
  else
    stateChanged("project_selected_guirpc", StateReverse);
  
  if((NULL != m_workunit) && !m_workunit->result().isEmpty() && canRPC) {
    stateChanged("result_selected_guirpc");
    if(NULL == m_host || !m_host->monitor()->isLocal() || !m_workunit->hasGraphics())
      show_graphics->setEnabled(false);
  } else
    stateChanged("result_selected_guirpc", StateReverse);
  
  unplugActionList("node_actions");
  if(NULL != m_workunit)
    plugActionList("node_actions", result_actions);
  else if(NULL != m_project)
    plugActionList("node_actions", project_actions);
  
  if(NULL != m_host)
  {
    updateRunMode();
    updateNetworkMode();
    
    connect(m_host->monitor()->rpcMonitor(), SIGNAL(updated()),
            this, SLOT(updateNetworkMode()));
    connect(m_host->monitor()->rpcMonitor(), SIGNAL(updated()),
            this, SLOT(updateRunMode()));
  }
  
  if(NULL != m_project)
  {
    updateProjectState(m_project);
    
    connect(m_project, SIGNAL(nodeChanged(KBSTreeNode *)),
            this, SLOT(updateProjectState(KBSTreeNode *)));
  }
  
  if(NULL != m_workunit)
  {
    updateResultState(m_workunit);
    
    connect(m_workunit, SIGNAL(nodeChanged(KBSTreeNode *)),
            this, SLOT(updateResultState(KBSTreeNode *)));
  }
  
  if(NULL != m_toolbar)
    m_toolbar->setMonitor(m_host ? m_host->monitor() : NULL);
}

void KBoincSpy::updatePanelVisible()
{
  m_panel = m_view->currentPanelView()->currentPanel();
  
  if(NULL != m_panel)
    stateChanged("panel_visible");
  else
    stateChanged("panel_visible", StateReverse);
}

void KBoincSpy::updateRunMode()
{
  KBSBOINCRunMode runMode = (NULL != m_host) ? m_host->monitor()->rpcMonitor()->runMode()
                                             : RunAuto;
  
  switch(runMode) {
    case RunAlways:
      run_always->setChecked(true);
      break;
    case RunNever:
      run_never->setChecked(true);
      break;
    default:
      run_auto->setChecked(true);
      break;
  }
}

void KBoincSpy::updateNetworkMode()
{
  KBSBOINCNetworkMode networkMode = (NULL != m_host) ? m_host->monitor()->rpcMonitor()->networkMode()
                                                     : ConnectAlways;
  
  network_connect->setChecked(ConnectNever == networkMode);
}

void KBoincSpy::updateProjectState(KBSTreeNode *node)
{
  if(node == m_project) {
    if(m_project->isSuspended())
      project_suspend->setText(i18n("Re&sume Project"));
    else
      project_suspend->setText(i18n("&Suspend Project"));
    project_extinguish->setChecked(m_project->isExtinguished());
  }
}
    
void KBoincSpy::updateResultState(KBSTreeNode *node)
{
  if(node == m_workunit)
    if(m_workunit->isSuspended())
      result_suspend->setText(i18n("Re&sume Work Unit"));
    else
      result_suspend->setText(i18n("&Suspend Work Unit"));
}
    
#include "kboincspy.moc"
