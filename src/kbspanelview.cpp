/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qiconset.h>
#include <qlayout.h>
#include <qpopupmenu.h>
#include <qptrlist.h>

#include <kiconloader.h>
#include <kxmlguifactory.h>

#include <kboincspy.h>
#include <kbspanel.h>
#include <kbspanelnode.h>

#include "kbspanelview.h"

KBSPanelView::KBSPanelView(KBSTreeNode *root, QWidget *parent, const char *name)
            : QWidget(parent, name), m_root(root), m_front(false)
{
  addConnections(m_root);
}

bool KBSPanelView::isFrontView() const
{
  return m_front;
}

void KBSPanelView::setFrontView(bool set)
{
  if(m_front == set) return;
  m_front = set;
  
  if(m_front) showAllPanels();
  else hideAllPanels();
}

void KBSPanelView::openPanel(KBSTreeNode *node)
{
  if(m_front && node->inherits("KBSPanelNode"))
    showPanel(static_cast<KBSPanelNode*>(node));
}

void KBSPanelView::closePanel(KBSTreeNode *node)
{
  if(node->inherits("KBSPanelNode"))
    hidePanel(static_cast<KBSPanelNode*>(node));
}

void KBSPanelView::addConnections(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode"))
  {
    connect(node, SIGNAL(childInserted(KBSTreeNode *)),
            this, SLOT(slotChildInserted(KBSTreeNode *)));
    connect(node, SIGNAL(childRemoved(KBSTreeNode *)),
            this, SLOT(slotChildRemoved(KBSTreeNode *)));
  
    const unsigned children = node->children();
    for(unsigned i = 0; i < children; i++)
      addConnections(node->child(i));
  } else if(m_front)
    showPanel(static_cast<KBSPanelNode*>(node));
}

void KBSPanelView::removeConnections(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode"))
  {
    disconnect(node, SIGNAL(childInserted(KBSTreeNode *)),
               this, SLOT(slotChildInserted(KBSTreeNode *)));
    disconnect(node, SIGNAL(childRemoved(KBSTreeNode *)),
               this, SLOT(slotChildRemoved(KBSTreeNode *)));
    
    const unsigned children = node->children();
    for(unsigned i = 0; i < children; i++)
      removeConnections(node->child(i));
  } else
    hidePanel(static_cast<KBSPanelNode*>(node));
}

void KBSPanelView::slotChildInserted(KBSTreeNode *child)
{
  addConnections(child);
}

void KBSPanelView::slotChildRemoved(KBSTreeNode *child)
{
  removeConnections(child);
}

KBSSimplePanelView::KBSSimplePanelView(KBSTreeNode *root, QWidget *parent, const char *name)
                  : KBSPanelView(root, parent, name), KXMLGUIClient(), m_visible(NULL)
{
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->setAutoAdd(true);
  
  layout->addSpacing(8);
  
  m_view = new QWidget(this);
  (new QHBoxLayout(m_view))->setAutoAdd(true);
  
  m_opened.clear();
}

KBSPanel *KBSSimplePanelView::currentPanel()
{
  return m_visible;
}

void KBSSimplePanelView::readConfig(KConfig *config)
{
  config->setGroup("KBSSimplePanelView");
  
  hideAllPanels();
  
  m_opened = KBSNamedPath(config->readEntry(QString("Opened panel")));
  
  if(m_front) showAllPanels();
}

void KBSSimplePanelView::writeConfig(KConfig *config)
{
  config->setGroup("KBSSimplePanelView");
  
  config->writeEntry(QString("Opened panel"), m_opened.toString());
}

void KBSSimplePanelView::openPanel(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode")) return;
  
  KBSNamedPath namedPath(m_root, node->path());
  if(namedPath.isEmpty()) return;
  
  m_opened = namedPath;
  
  KBSPanelView::openPanel(node);
}

void KBSSimplePanelView::closePanel(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode")) return;
  
  if(m_opened != KBSNamedPath::fromPath(m_root, node->path())) return;
  
  KBSPanelView::closePanel(node);
}

void KBSSimplePanelView::showPanel(KBSPanelNode *panel)
{
  const KBSNamedPath namedPath(m_root, panel->path());
  if(namedPath != m_opened) return;
  
  if(NULL != m_visible)
    if(m_visible->node() == panel) return;
    else hidePanel(m_visible->node());
  
  m_visible = panel->createPanel(m_view);
  m_visible->setContextMenu(static_cast<QPopupMenu*>(kmain->guiFactory()->container("panelview_popup", kmain)));
  m_visible->show();
    
  emit currentPanelChanged();
}

void KBSSimplePanelView::hidePanel(KBSPanelNode *panel)
{
  if(NULL == m_visible || m_visible->node() != panel) return;
  
  m_visible->hide();
  delete m_visible;
  m_visible = NULL;
  
  emit currentPanelChanged();
}

void KBSSimplePanelView::showAllPanels()
{
  if(m_opened.isEmpty()) return;
  
  KBSTreeNode *node = m_root->descendant(m_opened.toPath(m_root));
  if(NULL == node || !node->inherits("KBSPanelNode")) return;
  
  showPanel(static_cast<KBSPanelNode*>(node));
}

void KBSSimplePanelView::hideAllPanels()
{
  if(NULL != m_visible)
    hidePanel(m_visible->node());
}

KBSTabbedPanelView::KBSTabbedPanelView(KBSTreeNode *root, QWidget *parent, const char *name)
                  : KBSPanelView(root, parent, name), KXMLGUIClient(),
                    m_active(NULL), m_mapper(new QSignalMapper(this))
{
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->setAutoAdd(true);
  
  m_view = new KTabWidget(this);
  m_view->hide();
  
  m_close = new QToolButton(m_view);
  m_close->setIconSet(SmallIconSet("tab_remove"));
  m_view->setCornerWidget(m_close, TopRight);
  
  connect(m_view, SIGNAL(closeRequest(QWidget *)), this, SLOT(slotCloseRequest(QWidget *)));
  connect(m_view, SIGNAL(contextMenu(QWidget *, const QPoint &)),
          this, SLOT(slotContextMenu(QWidget *, const QPoint &)));
  
  connect(m_close, SIGNAL(clicked()), this, SLOT(closeCurrentPanel()));
  
  connect(m_mapper, SIGNAL(mapped(int)), m_view, SLOT(setCurrentPage(int)));
}

KBSPanel *KBSTabbedPanelView::currentPanel()
{
  return static_cast<KBSPanel*>(m_view->currentPage());
}

void KBSTabbedPanelView::readConfig(KConfig *config)
{  
  config->setGroup("KBSTabbedPanelView");
  
  hideAllPanels();
  
  m_opened.clear();
  QStringList namedPaths = config->readListEntry("Opened panels");
  for(QStringList::const_iterator namedPath = namedPaths.begin();
      namedPath != namedPaths.end(); ++namedPath)
    m_opened << KBSNamedPath::fromString(*namedPath);
  
  if(m_front) showAllPanels();
}

void KBSTabbedPanelView::writeConfig(KConfig *config)
{
  config->setGroup("KBSTabbedPanelView");
  
  QStringList namedPaths;
  for(QValueList<KBSNamedPath>::const_iterator namedPath = m_opened.begin();
      namedPath != m_opened.end(); ++namedPath)
    if(!(*namedPath).isEmpty())
      namedPaths << (*namedPath).toString();
  
  config->writeEntry("Opened panels", namedPaths);
}

void KBSTabbedPanelView::openPanel(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode")) return;
  
  KBSNamedPath namedPath(m_root, node->path());
  if(namedPath.isEmpty()) return;
  
  if(!m_opened.contains(namedPath))
    m_opened.append(namedPath);
  
  KBSPanelView::openPanel(node);
}

void KBSTabbedPanelView::closePanel(KBSTreeNode *node)
{
  if(!node->inherits("KBSPanelNode")) return;
  
  KBSNamedPath namedPath(m_root, node->path());
  if(namedPath.isEmpty()) return;
  
  if(m_opened.contains(namedPath))
    m_opened.remove(namedPath);
  
  KBSPanelView::closePanel(node);
}

void KBSTabbedPanelView::closeThisPanel()
{
  if(NULL == m_active) return;
  closePanel(m_active->node());
  m_active = NULL;
}

void KBSTabbedPanelView::closeAllPanels()
{
  QPtrDict<KBSPanel> panels(m_visible);
  
  for(QPtrDictIterator<KBSPanel> it(panels); it.current() != NULL; ++it)
    closePanel(static_cast<KBSTreeNode*>(it.currentKey()));
}

void KBSTabbedPanelView::closeOtherPanels()
{
  if(NULL == m_active) return;
  
  QPtrDict<KBSPanel> panels(m_visible);
  panels.remove(m_active->node());
  
  for(QPtrDictIterator<KBSPanel> it(panels); it.current() != NULL; ++it)
    closePanel(static_cast<KBSTreeNode*>(it.currentKey()));
}

void KBSTabbedPanelView::closeCurrentPanel()
{
  m_active = currentPanel();
  closeThisPanel();
}

void KBSTabbedPanelView::showPanel(KBSPanelNode *panel)
{
  const KBSNamedPath namedPath(m_root, panel->path());
  if(!m_opened.contains(namedPath)) return;
  
  KBSPanel *tab = m_visible.find(panel);
  
  if(NULL == tab)
  {
    tab = panel->createPanel(this);
    tab->setContextMenu(static_cast<QPopupMenu*>(kmain->guiFactory()->container("panelview_popup", kmain)));
    
    m_view->insertTab(tab, panel->name(), orderedTabIndex(panel));
    m_view->setTabIconSet(tab, CompositeIconSet(panel->icons()));
    m_view->setTabToolTip(tab, namedPath.toString());
    m_visible.insert(panel, tab);
  }
  
  m_view->showPage(tab);
  
  if(m_view->count() > 0) m_view->show();
  
  emit currentPanelChanged();
}

void KBSTabbedPanelView::hidePanel(KBSPanelNode *panel)
{
  KBSPanel *tab = m_visible.find(panel);
  if(NULL == tab) return;
  
  m_visible.remove(panel);
  delete tab;
  
  if(m_view->count() == 0) m_view->hide();
  
  emit currentPanelChanged();
}

void KBSTabbedPanelView::showAllPanels()
{
  for(QValueList<KBSNamedPath>::const_iterator namedPath = m_opened.begin();
      namedPath != m_opened.end(); ++namedPath)
    if(!(*namedPath).isEmpty())
    {
      KBSPanelNode *panel = static_cast<KBSPanelNode*>(m_root->descendant((*namedPath).toPath(m_root)));
      showPanel(panel);
    }
}

void KBSTabbedPanelView::hideAllPanels()
{
  QPtrDict<KBSPanel> panels(m_visible);
  
  for(QPtrDictIterator<KBSPanel> it(panels); it.current() != NULL; ++it)
    hidePanel(static_cast<KBSPanelNode*>(it.currentKey()));
}

unsigned KBSTabbedPanelView::orderedTabIndex(const KBSPanelNode *panel)
{
  const QString panelPath = KBSNamedPath(m_root, panel->path()).toString();
  
  unsigned out = 0;
  for(QPtrDictIterator<KBSPanel> it(m_visible); it.current() != NULL; ++it)
  {
    KBSPanelNode *node = static_cast<KBSPanelNode*>(it.currentKey());
    const QString nodePath = KBSNamedPath(m_root, node->path()).toString();
    if(panelPath >= nodePath) out++;
  }

  return out;
}

void KBSTabbedPanelView::slotCloseRequest(QWidget *tab)
{
  if(!tab->inherits("KBSPanel")) return;
  
  closePanel(static_cast<KBSPanel*>(tab)->node());
}

void KBSTabbedPanelView::slotContextMenu(QWidget *tab, const QPoint &point)
{
  if(!tab->inherits("KBSPanel")) return;
  m_active = static_cast<KBSPanel*>(tab);
  
  KAction *close_other_tabs = kmain->action("close_other_tabs");
  close_other_tabs->setEnabled(m_view->count() > 1);
  
  kmain->unplugActionList("tabs_list");
  for(QPtrListIterator<KAction> it(m_actions); it.current() != NULL; ++it)
    delete it.current();
  m_actions.clear();
  
  for(int i = 0; i < m_view->count(); ++i)
  {
    KBSPanel *panel = static_cast<KBSPanel*>(m_view->page(i));
    KBSNamedPath namedPath(m_root, panel->node()->path());
    KAction *action = new KAction(namedPath.toString(), 0, m_mapper, SLOT(map()), this);
    
    action->setEnabled(m_view->currentPageIndex() != i);
    
    m_mapper->setMapping(action, i);
    m_actions.append(action);
  }
  kmain->plugActionList("tabs_list", m_actions);
  
  QPopupMenu *menu = static_cast<QPopupMenu*>(kmain->guiFactory()->container("tabs_popup", kmain));
  menu->popup(point);
}

#include "kbspanelview.moc"
