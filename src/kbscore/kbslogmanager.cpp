/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboinclogx.h>
#include <kbslogmonitor.h>

#include "kbslogmanager.h"

KBSLogManager *KBSLogManager::s_self = NULL;

KBSLogManager *KBSLogManager::self()
{
  if(NULL == s_self)
    s_self = new KBSLogManager();
  
  return s_self;
}

KBSLogManager::KBSLogManager(QObject *parent, const char *name)
              : QObject(parent, name), m_format(0), m_mask(0), m_interval(0)
{
}

KURL KBSLogManager::url() const
{
  return m_url;
}

void KBSLogManager::setURL(const KURL &url)
{
  if(url == m_url) return;
  m_url = url;
  
  for(unsigned format = 0; format < formats(); ++format)
  {
    if(m_monitors.find(format) != NULL)
      destroyLogMonitor(format);
    
    if((m_mask & (1 << format)) || (m_format == format))
      createLogMonitor(format);
  }
  
  emit logChanged();
}

int KBSLogManager::interval() const
{
  return m_interval;
}

void KBSLogManager::setInterval(int interval)
{
  if(interval == m_interval) return;
  
  m_interval = interval;
  emit intervalChanged(interval);
}

QStringList KBSLogManager::keys() const
{
  const KBSLogMonitor *monitor = m_monitors.find(m_format);
  return (NULL != monitor) ? monitor->keys() : QStringList();
}

bool KBSLogManager::hasResults() const
{
  const KBSLogMonitor *monitor = m_monitors.find(m_format);
  return (NULL != monitor) ? monitor->hasResults() : false;
}

KBSLogData KBSLogManager::workunits() const
{
  const KBSLogMonitor *monitor = m_monitors.find(m_format);
  return((NULL != monitor) ? monitor->workunits() : KBSLogData());
}

KBSLogData KBSLogManager::results(const QString &workunit) const
{
  const KBSLogMonitor *monitor = m_monitors.find(m_format);
  return (NULL != monitor) ? monitor->results(workunit) : KBSLogData();
}

unsigned KBSLogManager::currentFormat() const
{
  return m_format;
}

void KBSLogManager::setCurrentFormat(unsigned format)
{
  if(format == m_format) return;
  
  KBSLogMonitor *monitor;
   
  monitor = m_monitors.find(m_format);
  if(NULL != monitor)
    if(m_mask & (1 << m_format)) {
      disconnect(monitor, SIGNAL(workunitsUpdated()));
      disconnect(monitor, SIGNAL(resultsUpdated()));
    }
    else
      destroyLogMonitor(m_format);
  
  m_format = format;
  
  monitor = m_monitors.find(format);
  if(NULL == monitor)
    createLogMonitor(format);
  else {
    connect(monitor, SIGNAL(workunitsUpdated()), this, SIGNAL(workunitsUpdated()));
    connect(monitor, SIGNAL(resultsUpdated()), this, SIGNAL(resultsUpdated()));
  }
    
  emit logChanged();
}

unsigned KBSLogManager::writeMask() const
{
  return m_mask;
}

void KBSLogManager::setWriteMask(unsigned mask)
{
  m_mask = mask;
  
  for(unsigned format = 0; format < formats(); ++format)
  {
    KBSLogMonitor *monitor = m_monitors.find(format);
    
    if(mask & (1 << format)) {
      if(NULL == monitor) createLogMonitor(format);
    } else if(format != m_format) {
      if(NULL != monitor) destroyLogMonitor(format);
    }
  }
}

void KBSLogManager::logWorkunit(KBSProjectMonitor *projectMonitor, const QString &workunit)
{  
  if(this != s_self)
    s_self->logWorkunit(projectMonitor, workunit);
  
  for(unsigned format = 0; format < formats(); ++format)
  {
    KBSLogMonitor *monitor = m_monitors.find(format);
    if(NULL == monitor) continue;
    
    if(m_mask & (1 << format))
      monitor->logWorkunit(projectMonitor, workunit);
  }
}
    
unsigned KBSLogManager::formats() const
{
  return 1;
}

KBSLogMonitor *KBSLogManager::createLogMonitor(unsigned format, const KURL &url, QObject *parent)
{
  return (format == 0) ? new KBSBOINCLogX(url, parent) : NULL;
}

KBSLogMonitor *KBSLogManager::createLogMonitor(unsigned format)
{
  if(!m_url.isValid()) return NULL;
  
  KBSLogMonitor *monitor = createLogMonitor(format, m_url, this);
  if(NULL == monitor) return NULL;
        
  monitor->setInterval(m_interval);
  connect(this, SIGNAL(intervalChanged(int)), monitor, SLOT(setInterval(int)));
      
  if(m_format == format) {
    connect(monitor, SIGNAL(workunitsUpdated()), this, SIGNAL(workunitsUpdated()));
    connect(monitor, SIGNAL(resultsUpdated()), this, SIGNAL(resultsUpdated()));
  }
        
  m_monitors.insert(format, monitor);
  
  return monitor;
}

void KBSLogManager::destroyLogMonitor(unsigned format)
{
  KBSLogMonitor *monitor = m_monitors.take(format);
  if(NULL == monitor) return;
  
  delete monitor; 
}

#include "kbslogmanager.moc"
