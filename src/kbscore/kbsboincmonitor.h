/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSBOINCMONITOR_H
#define KBSBOINCMONITOR_H

#include <qdict.h>
#include <qdom.h>
#include <qintdict.h>
#include <qmap.h>
#include <qstringlist.h>

#include <qprocess.h>

#include <kbsdatamonitor.h>

#include <kbsboincdata.h>

class KBSProjectMonitor;
class KBSRPCMonitor;
class KBSTaskMonitor;
class KBSTreeNode;

struct KBSLocation {
  KURL url;
  QString host;
  unsigned port;
  
  static QString defaultHost(const KURL &url);
  static unsigned defaultPort;
  
  QString defaultHost() const;
};

bool operator==(const KBSLocation &location1, const KBSLocation &location2);

class KBSBOINCMonitor : public KBSDataMonitor
{
  Q_OBJECT
  public:
    KBSBOINCMonitor(const KBSLocation &location, KBSTreeNode *parent=0, const char *name=0);
    virtual ~KBSBOINCMonitor();
    
    virtual bool isLocal() const;
    virtual KBSLocation location() const;
    
    virtual void exec(const QString &client, bool killOnExit=true);
    
    virtual const KBSBOINCClientState *state() const;
    virtual const KBSBOINCAccount *account(const QString &project) const;
    virtual const KBSBOINCProjectStatistics *statistics(const QString &project) const;
    
    virtual QString project(const KBSBOINCProject &project) const;
    virtual QString project(const KBSBOINCApp &app) const;
    virtual QString project(const KBSBOINCWorkunit &workunit) const;
    virtual QString project(const KBSBOINCResult &result) const;
    virtual QString project(const KBSBOINCActiveTask &task) const;
    virtual QString project(const KBSBOINCAccount &account) const;
    virtual QString project(const KBSBOINCProjectStatistics &statistics) const;
    
    virtual QString app(const KBSBOINCApp &app) const;
    virtual QString app(const KBSBOINCWorkunit &workunit) const;
    virtual QString app(const KBSBOINCResult &result) const;
    virtual QString app(const KBSBOINCActiveTask &task) const;
    
    virtual QString workunit(const KBSBOINCWorkunit &workunit) const;
    virtual QString workunit(const KBSBOINCResult &result) const;
    virtual QString workunit(const KBSBOINCActiveTask &task) const;
    
    virtual QString result(const KBSBOINCResult &result) const;
    virtual QString result(const KBSBOINCActiveTask &task) const;
  
  public:
    virtual KBSRPCMonitor *rpcMonitor() const;
    virtual KBSProjectMonitor *projectMonitor(const QString &project) const;
    virtual KBSTaskMonitor *taskMonitor(unsigned task) const;
    
    friend class KBSProjectMonitor;
    
  signals:
    void projectsAdded(const QStringList &projects);
    void projectsRemoved(const QStringList &projects);
    void appsAdded(const QStringList &apps);
    void appsRemoved(const QStringList &apps);
    void workunitsAdded(const QStringList &workunits);
    void workunitsRemoved(const QStringList &workunits);
    void resultsAdded(const QStringList &results);
    void resultsRemoved(const QStringList &results);
    void resultsCompleted(const QStringList &results);
     
    void workunitActivated(unsigned task, const QString &workunit, bool activated);
    void resultActivated(unsigned task, const QString &result, bool activated);
    
    void stateUpdated();
    void accountUpdated(const QString &project);
    void statisticsUpdated(const QString &project);
  
  protected:
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    static QString formatAccountFileName(const QString &project);
    static QString parseAccountFileName(const QString &fileName);
    
    static QString formatStatisticsFileName(const QString &project);
    static QString parseStatisticsFileName(const QString &fileName);
  
  private:
    bool parseClientStateDocument(const QDomDocument &document);
    bool parseAccountDocument(const QDomDocument &document, KBSBOINCAccount &account);
    bool parseStatisticsDocument(const QDomDocument &document, KBSBOINCProjectStatistics &statistics);
    
    bool validateResults();
    
    void notify(const QString &message, const QString &text);
    
    KURL::List collectURLs(const KBSBOINCApp &app) const;
    KURL::List collectURLs(const KBSBOINCWorkunit &workunit, bool recursive=true) const;
    KURL::List collectURLs(const KBSBOINCResult &result, bool recursive=true) const;
    
    static double matchURL(const KURL &url, const KURL &scheduler_url);
  
  private slots:
    void addProjectFiles(const QStringList &projects);
    void removeProjectFiles(const QStringList &projects);
    
    void addProjectMonitors(const QStringList &projects);
    void removeProjectMonitors(const QStringList &projects);
  
    void updateTaskMonitor(unsigned task, const QString &result, bool add);
    
    void updateFile(const QString &fileName);
  
  protected:
    KBSBOINCClientState m_state;
    QDict<KBSBOINCAccount> m_accounts;
    QDict<KBSBOINCProjectStatistics> m_statistics;
  
  protected:
    KBSLocation m_location;
    KBSRPCMonitor *m_rpcMonitor;
    QDict<KBSProjectMonitor> m_projectMonitors;
    QIntDict<KBSTaskMonitor> m_taskMonitors;
    QProcess *m_client;
  
  private:
    bool m_startup;
    QStringList m_projects, m_apps, m_workunits, m_results, m_complete;
    QMap<unsigned,KBSBOINCActiveTask> m_tasks;
    QValueList<unsigned> m_running;
    bool m_killClient;
};

#endif
