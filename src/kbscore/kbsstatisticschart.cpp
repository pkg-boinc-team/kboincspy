/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qpainter.h>

#include <kglobal.h>
#include <klocale.h>

#include "kbsstatisticschart.h"

unsigned KBSStatisticsChart::s_border = 6;

KBSStatisticsChart::KBSStatisticsChart(Type type, QWidget *parent, const char *name)
                  : QWidget(parent, name), m_today(QDate::currentDate()), m_type(type)
{
  QFont font = this->font();
  font.setPointSize(9);
  setFont(font);
  
  computeBoundaries();
  
  // optimize redrawing
  setBackgroundMode(NoBackground);
  
  // check the current date every hour
  startTimer(60*60*1000);
}

KBSStatisticsChart::Type KBSStatisticsChart::type() const
{
  return m_type;
}

QDate KBSStatisticsChart::start() const
{
  return m_data.isEmpty() ? QDate() : m_data.first().day;
}

QDate KBSStatisticsChart::end() const
{
  const unsigned size = m_data.size();
  return (size > 1) ? m_data[size-2].day : start();
}

void KBSStatisticsChart::setData(const QValueList<KBSBOINCDailyStatistics> &data)
{
  m_data = QValueList<KBSBOINCDailyStatistics>(data);
  
  computeBoundaries();
  
  repaint();
}

QPixmap KBSStatisticsChart::pixmap()
{
  return QPixmap::grabWidget(this);
}

void KBSStatisticsChart::paintEvent(QPaintEvent *)
{
  QPixmap buffer(size());
  buffer.fill(black);
  
  QPainter painter(&buffer);
  painter.setFont(font());
  
  if(m_data.size() > 1)
  {
    KLocale *locale = KGlobal::locale();
    
    QFontMetrics metrics = this->fontMetrics();
    const int lineHeight = metrics.lineSpacing();
    
    const int mainWidth = width() - m_margin.left() - m_margin.right(),
              mainHeight = height() - m_margin.top() - m_margin.bottom();
    
    const unsigned count = start().daysTo(end())+1;
    
    QDate day[count+1];
    day[0] = m_data.first().day;
    for(unsigned i = 1; i < count; ++i)
      day[i] = day[i-1].addDays(1);
    day[count] = m_data.last().day;
    
    int x[count+1];
    for(unsigned i = 0; i <= count; ++i)
      x[i] = i * mainWidth / count;
    
    painter.save();
    painter.translate(m_margin.left(), m_margin.top());
    for(unsigned i = 0; i < 10; ++i)
    {
      const int y = i * mainHeight / 10;
      
      painter.setPen(i % 2 ? darkGray.dark() : darkGray);
      painter.drawLine(0, y, mainWidth, y);
    }
    painter.restore();
    
    painter.setPen(QPen(magenta, 2));
    
    painter.save();
    painter.translate(0, m_margin.top());
    for(unsigned i = 0; i <= 10; i += 2)
    {
      const QRect r(s_border, 
                    -lineHeight / 2,
                    m_margin.left() - 2 * s_border,
                    (i * mainHeight / 10) + lineHeight);
      const QString label = locale->formatNumber(m_min[Daily] + (10 - i) * m_step[Daily], 0);
      
      painter.drawText(r, AlignRight | AlignBottom, label);
    }
    painter.translate(0, mainHeight);
    painter.rotate(-90);
    {
      const QRect r(0, s_border, mainHeight, m_margin.left() - 2 * s_border);
      
      painter.drawText(r, AlignHCenter | AlignTop, i18n("Avg. Daily Credits"));
    }
    painter.restore();
    
    painter.save();
    painter.translate(m_margin.left(), m_margin.top());
    {
      const double scale = mainHeight / (10 * m_step[Daily]);
      QValueList<KBSBOINCDailyStatistics>::const_iterator datum = m_data.begin();
      int previous = 0, current;
      
      for(unsigned i = 0; i <= count; ++i)
      {
        if((*datum).day == day[i])
        {
          const double credit = (Host == m_type) ? (*(datum++)).host.expavg_credit
                                                 : (*(datum++)).user.expavg_credit;
          
          current = mainHeight - int((credit - m_min[Daily]) * scale);
        }
        else
          current = previous;
        
        if(i > 0) painter.drawLine(x[i-1], previous, x[i], current);
        
        previous = current;
      }
    }
    painter.restore();
    
    painter.setPen(QPen(cyan, 2));
    
    painter.save();
    painter.translate(width() - m_margin.right(), m_margin.top());
    for(unsigned i = 0; i <= 10; i += 2)
    {
      const QRect r(s_border,
                    -lineHeight / 2,
                    m_margin.right() - 2 * s_border,
                    (i * mainHeight / 10) + lineHeight);
      const QString label = locale->formatNumber(m_min[Total] + (10 - i) * m_step[Total], 0);
      
      painter.drawText(r, AlignLeft | AlignBottom, label);
    }
    painter.translate(0, mainHeight);
    painter.rotate(-90);
    {
      const QRect r(0, s_border, mainHeight, m_margin.right() - 2 * s_border);
      
      painter.drawText(r, AlignHCenter | AlignBottom, i18n("Total Credits"));
    }
    painter.restore();
    
    painter.save();
    painter.translate(m_margin.left(), m_margin.top());
    {
      const double scale = mainHeight / (10 * m_step[Total]);
      QValueList<KBSBOINCDailyStatistics>::const_iterator datum = m_data.begin();
      int previous = 0, current;
      
      for(unsigned i = 0; i <= count; ++i)
      {
        if((*datum).day == day[i])
        {
          const double credit = (Host == m_type) ? (*(datum++)).host.total_credit
                                                 : (*(datum++)).user.total_credit;
          
          current = mainHeight - int((credit - m_min[Total]) * scale);
        }
        else
          current = previous;
        
        if(i > 0) painter.drawLine(x[i-1], previous, x[i], current);
        
        previous = current;
      }
    }
    painter.restore();
    
    painter.setPen(white);
    
    painter.save();
    painter.translate(m_margin.left(), height() - m_margin.bottom());
    {
      const QRect r(0, 0, mainWidth, m_margin.bottom() - s_border);
      
      painter.drawText(r, AlignHCenter | AlignBottom, i18n("Days"));
    }
    for(unsigned i = 0; i <= count; ++i)
    {
      const QString label = (i < count) ? QString::number(m_today.daysTo(day[i])) : "...";
      const int w = metrics.width(label);
      const QRect r(x[i] - w/2, s_border, w, lineHeight);
      
      painter.drawText(r, AlignHCenter | AlignBottom, label);
    }
    painter.restore();
    
    painter.save();
    painter.translate(m_margin.left(), m_margin.top());
    painter.drawLine(0, 0, 0, mainHeight);
    painter.drawLine(0, mainHeight, mainWidth, mainHeight);
    painter.drawLine(mainWidth, mainHeight, mainWidth, 0);
    painter.restore();
  }
  else
  {
    painter.setPen(white);
    painter.drawText(rect(), AlignCenter, i18n("No Data"));
  }

  painter.end();
  
  painter.begin(this);
  painter.drawPixmap(0, 0, buffer);
}

void KBSStatisticsChart::timerEvent(QTimerEvent *)
{
  if(QDate::currentDate() == m_today) return;
  m_today = QDate::currentDate();
  
  repaint();
}

void KBSStatisticsChart::computeBoundaries()
{
  double max[DataSets];
  
  // compute minimum and maximum of each dataset
  if(!m_data.isEmpty())
  {
    m_min[Total] = (Host == m_type) ? m_data.first().host.total_credit
                                    : m_data.first().user.total_credit;
    
    max[Total] = (Host == m_type) ? m_data.last().host.total_credit
                                  : m_data.last().user.total_credit;

    m_min[Daily] = max[Daily] = 0;
    for(QValueList<KBSBOINCDailyStatistics>::const_iterator datum = m_data.begin();
        datum != m_data.end(); ++datum)
    {
      if(Host == m_type && (*datum).host.expavg_credit > max[Daily])
        max[Daily] = (*datum).host.expavg_credit;
      else if(User == m_type && (*datum).user.expavg_credit > max[Daily])
        max[Daily] = (*datum).user.expavg_credit;
    }
  }
  else
    m_min[Total] = m_min[Daily] = max[Total] = max[Daily] = 0;
  
  // expand boundaries to nicer numbers
  for(unsigned i = 0; i < DataSets; ++i)
  {
    double unit;
    
    if(m_min[i] > 10) {
      unit = pow(10, floor(log10(m_min[i])));
      m_min[i] = floor(m_min[i]/unit) * unit;
    } else
      m_min[i] = 0;
    
    m_step[i] = computeStep(m_min[i], max[i]);
  }
  
  // compute borders
  int top, left, bottom, right;
  
  KLocale *locale = KGlobal::locale();
  
  QFontMetrics metrics = this->fontMetrics();
  const int lineHeight = metrics.lineSpacing();
  
  top = s_border + lineHeight / 2;
  
  left = 0;
  for(unsigned i = 0; i <= 10; ++i)
  {
    const int w = metrics.width(locale->formatNumber(m_min[Daily] + i * m_step[Daily]));
    if(left < w) left = w;
  }
  left += 2 * s_border + lineHeight;
  
  bottom = 3 * s_border + 2 * lineHeight;
  
  right = 0;
  for(unsigned i = 0; i <= 10; ++i)
  {
    const int w = metrics.width(locale->formatNumber(m_min[Total] + i * m_step[Total]));
    if(right < w) right = w;
  }
  right += 2 * s_border + lineHeight;
  
  m_margin = QRect(QPoint(left, top), QPoint(right, bottom));
  
  if(m_data.size() > 1)
  {
    const unsigned count = start().daysTo(end())+1;
    const int cellSize = metrics.width("-___");
    
    setMinimumSize(left + count * cellSize + right, top + 10 * cellSize + bottom);
  }
  else
    setMinimumSize(150, 100);
}

double KBSStatisticsChart::computeStep(double min, double max)
{
  const double range = max - min;
  
  double out;
  for(out = 1; ; out *= 10)
  {
    if(out >= 100 && range <= 2.5*out) return out/4;
    if(out >= 10 && range <= 5*out) return out/2;
    if(range <= 10*out) return out;
  }
}

#include "kbsstatisticschart.moc"
