/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>
#include <kbsprojectmonitor.h>

#include "kbsboinclogx.h"

using namespace KBSBOINC;

const QString KBSBOINCLogX::s_version = "1.11";
const QString KBSBOINCLogX::s_filename = "boinc.csv";

KBSBOINCLogX::KBSBOINCLogX(const KURL &url, QObject *parent, const char *name)
            : KBSLogMonitor(url, parent, name)
{
  initKeys();
  
  addLogFile(s_filename);
  
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

QStringList KBSBOINCLogX::keys() const
{
  return m_keys;
}

bool KBSBOINCLogX::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(s_filename == file->fileName)
    return parseLogDocument(lines);
  else        
    return false;
}

KBSLogDatum KBSBOINCLogX::formatWorkunitDatum(KBSProjectMonitor *projectMonitor,
                                             const QString &workunit) const
{
  const QString project = projectMonitor->project();
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogDatum();
  
  const QString result = state->workunit[workunit].result_name;
    
  KBSLogDatum out;
  
  out["date"] = formatLogEntryDate(QDateTime::currentDateTime());
  out["project_name"] = state->project[project].project_name;
  out["app_name"] = state->workunit[workunit].app_name;
  out["domain_name"] = state->host_info.domain_name;
  out["p_ncpus"] = state->host_info.p.ncpus;
  out["p_vendor"] = state->host_info.p.vendor;
  out["p_model"] = state->host_info.p.model;
  out["p_fpops"] = state->host_info.p.fpops;
  out["p_iops"] = state->host_info.p.iops;
  out["p_membw"] = state->host_info.p.membw;
  out["p_calculated"] = state->host_info.p.calculated;
  out["os_name"] = state->host_info.os.name;
  out["os_version"] = state->host_info.os.version;
  out["m_nbytes"] = state->host_info.m.nbytes;
  out["m_cache"] = state->host_info.m.cache;
  out["m_swap"] = state->host_info.m.swap;
  out["d_total"] = state->host_info.d.total;
  out["d_free"] = state->host_info.d.free;
  out["on_frac"] = state->time_stats.on_frac;
  out["connected_frac"] = state->time_stats.connected_frac;
  out["active_frac"] = state->time_stats.active_frac;
  out["last_update"] = formatUNIXDate(state->time_stats.last_update);
  out["bwup"] = state->net_stats.bwup;
  out["bwdown"] = state->net_stats.bwdown;
  out["user_name"] = state->project[project].user_name;
  out["team_name"] = state->project[project].team_name;
  out["user_total_credit"] = state->project[project].user.total_credit;
  out["user_expavg_credit"] = state->project[project].user.expavg_credit;
  out["user_create_time"] = formatUNIXDate(state->project[project].user.create_time);
  out["rpc_seqno"] = state->project[project].rpc_seqno;
  out["hostid"] = state->project[project].hostid;
  out["host_total_credit"] = state->project[project].host.total_credit;
  out["host_expavg_credit"] = state->project[project].host.expavg_credit;
  out["host_create_time"] = formatUNIXDate(state->project[project].host.create_time);
  out["exp_avg_cpu"] = state->project[project].exp_avg.cpu;
  out["exp_avg_mod_time"] = state->project[project].exp_avg.mod_time;
  out["host_venue"] = state->host_venue;
  out["boinc_version"] = formatVersion(state->core_client.major_version, state->core_client.minor_version);
  out["logX_version"] = s_version;
  out["app_version"] = formatVersion(state->workunit[workunit].version_num);
  out["wu_name"] = workunit;
  if(state->result[result].file_ref.count() > 0)
    out["result_name"] = state->result[result].file_ref.first().file_name;
  else
    out["result_name"] = result;
  out["cpu"] = state->result[result].final_cpu_time;
  out["fpops_est"] = state->workunit[workunit].rsc.fpops_est;
  out["error"] = "false";
  // out["error_txt"] = ...
  
  return out;
}

QMap<QString,KBSLogData> KBSBOINCLogX::formatWorkunit(KBSProjectMonitor *monitor,
                                                     const QString &workunit) const
{
  QMap<QString,KBSLogData> out;
  
  out[s_filename] << formatWorkunitDatum(monitor, workunit);
    
  return out;
}

void KBSBOINCLogX::appendHeader(const KBSFileInfo *info, QIODevice *io)
{
  QTextStream text(io);
  
  if(info->fileName == s_filename)
    text << formatCSVKeys(m_keys) << "\r\n";
}

void KBSBOINCLogX::appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum)
{
  QTextStream text(io);
  
  if(info->fileName == s_filename)
    text << formatCSVDatum(datum, m_keys) << "\r\n";
}

void KBSBOINCLogX::initKeys()
{
  m_keys.clear(); m_keys
    << "date" << "project_name" << "app_name" << "domain_name" << "p_ncpus"
    << "p_vendor" << "p_model" << "p_fpops" << "p_iops" << "p_membw"
    << "p_calculated" << "os_name" << "os_version" << "m_nbytes" << "m_cache"
    << "m_swap" << "d_total" << "d_free" << "on_frac" << "connected_frac"
    << "active_frac" << "last_update" << "bwup" << "bwdown" << "user_name"
    << "team_name" << "user_total_credit" << "user_expavg_credit"
    << "user_create_time" << "rpc_seqno" << "hostid" << "host_total_credit"
    << "host_expavg_credit" << "host_create_time" << "exp_avg_cpu"
    << "exp_avg_mod_time" << "host_venue" << "boinc_version" << "logX_version"
    << "app_version" << "wu_name" << "result_name" << "cpu" << "fpops_est"
    << "error" << "error_txt";
}

bool KBSBOINCLogX::parseLogDocument(const QStringList &lines)
{
  QStringList::const_iterator line = lines.begin();
  
  if(lines.end() == line) return true;
  const unsigned keys = m_keys.count();
  m_keys = parseCSVKeys(*line);
  if(keys > m_keys.count()) return false;
  ++line;
  
  for(unsigned i = 0; i < m_workunits.count(); ++i)
    if(lines.end() != line) ++line;
    else return true;
    
  while(lines.end() != line) {
    KBSLogDatum datum = parseCSVDatum(*line, m_keys);
    ++line;
    
    datum["date"] = parseLogEntryDate(datum["date"].toString());
    datum["last_update"] = parseUNIXDate(datum["last_update"].toDouble());
    datum["user_create_time"] = parseUNIXDate(datum["user_create_time"].toDouble());
    datum["host_create_time"] = parseUNIXDate(datum["host_create_time"].toDouble());
    
    m_workunits << datum;
  }
  
  qDebug("... parse OK");
  
  return true;
}

void KBSBOINCLogX::updateFile(const QString &)
{
  emit workunitsUpdated();
}

#include "kbsboinclogx.moc"
