/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPANELFIELD_H
#define KBSPANELFIELD_H

#include <qlabel.h>
#include <qlayout.h>
#include <qwidget.h>

class KBSPanelField : public QWidget
{
  Q_OBJECT
  public:
    KBSPanelField(QWidget *parent = 0, const char *name = 0);
    
    QString name() const;
    void setName(const QString &name);
    
    bool isText() const;
    
    QString text() const;
    void setText(const QString &text);
    void setSqueezedText(const QString &text);
    
    QString url() const;
    QString urlText() const;
    QString urlTooltip() const;
    void setURL(const QString &url, const QString &text, const QString &tooltip=QString::null);
    
    QString aux() const;
    void setAux(const QString &text);
  
    QString tooltip() const;
    void setTooltip(const QString &tooltip);
    
    QColor textColor() const;
    void setTextColor(const QColor &color);
    
  protected:
    enum Type {None, Text, SqueezedText, URL};
    void setType(Type type);
    
  private slots:
    void handleURL(const QString &url);
    
  protected:
    Type m_type;
    QBoxLayout *m_layout;
    QLabel *m_name, *m_value, *m_aux;
};

#endif
