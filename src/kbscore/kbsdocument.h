/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSDOCUMENT_H
#define KBSDOCUMENT_H

#include <qdict.h>
#include <qmap.h>
#include <qptrlist.h>
#include <qstringlist.h>
#include <qwidget.h>

#include <kconfig.h>
#include <kconfigskeleton.h>
#include <kurl.h>

#include <kbspreferences.h>
#include <kbstree.h>

class KBSBOINCMonitor;
class KBSLocation;
class KBSProjectMonitor;
class KBSProjectPlugin;
class KBSTaskMonitor;

class KBSDocument : public KBSTreeNode
{
  Q_OBJECT
  public:
    KBSDocument(QObject *parent=0, const char *name=0);
    virtual ~KBSDocument();
    
    virtual void connectTo(const KBSLocation &location);
    virtual void disconnectFrom(const KBSLocation &location);
    
    virtual int interval() const;
    
    virtual QPtrList<KBSProjectPlugin> plugins() const;
    virtual KBSProjectPlugin *plugin(const QString &project) const;
    
    virtual KConfigSkeleton *preferences();
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  public slots:
    virtual void applyPreferences();
  
  signals:
    void intervalChanged(int interval);
  
  private:
    void loadPlugins();
  
  protected:
    QMap<KURL,KBSLocation> m_locations;
    QDict<KBSProjectPlugin> m_plugins;
    KBSPreferences m_preferences;
};

#endif
