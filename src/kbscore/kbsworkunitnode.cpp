/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdict.h>
#include <qregexp.h>

#include <klibloader.h>
#include <klocale.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbshostnode.h>
#include <kbspanelnode.h>

#include "kbsworkunitnode.h"

KBSWorkunitNode::KBSWorkunitNode(const QString &workunit, KBSTreeNode *parent, const char *name)
               : KBSTreeNode(parent, name),
                 m_status(0), m_unit(0), m_suspended(false), m_aborted(false), m_graphics(false),
                 m_workunit(workunit)
{
  setupMonitor();
  addPlugins();
}

unsigned KBSWorkunitNode::type() const
{
  return 3;
}

QString KBSWorkunitNode::name() const
{
  return KBSWorkunitNode::name(m_workunit, m_monitor);
}

QStringList KBSWorkunitNode::icons() const
{
  QStringList out;
  
  if(m_suspended)
    return QStringList("progress_suspend");
  
  out << "progress_background";
  
  if(m_status >= 0)
    switch(m_status) {
      case 1:
        out << "progress_000" << "progress_download";
        break;
      case 3:
        out << "progress_100";
        break;
      case 4:
        out << "progress_100" << "progress_upload";
        break;
      case 5:
        out << "progress_100" << "progress_complete";
        break;
      default:
        out << "progress_000";
        break;
    }
  else
    out << QString().sprintf("progress_%.3u", m_unit);
    
  out << "progress_frame";
  
  if(m_status < 0)
    switch(-m_status) {
      case 1:
        out << "progress_pause";
        break;
      default:
        out << "progress_play";
        break;
    }
  else
    out << "progress_stop";
    
  return out;
}

QString KBSWorkunitNode::project() const
{
  return m_project;
}

KURL KBSWorkunitNode::projectURL() const
{
  return m_projectURL;
}

QString KBSWorkunitNode::application() const
{
  return m_application;
}

QString KBSWorkunitNode::workunit() const
{
  return m_workunit;
}

QString KBSWorkunitNode::result() const
{
  return m_result;
}

QString KBSWorkunitNode::name(const QString &workunit, KBSBOINCMonitor *)
{
  return workunit;
}

bool KBSWorkunitNode::isSuspended() const
{
  return m_suspended;
}

bool KBSWorkunitNode::isAborted() const
{
  return m_aborted;
}

bool KBSWorkunitNode::hasGraphics() const
{
  return m_graphics;
}

KBSBOINCMonitor *KBSWorkunitNode::monitor() const
{
  return m_monitor;
}

unsigned KBSWorkunitNode::unit(double fraction)
{
  return (fraction >= 0.0) ? unsigned(fraction * 20.0) * 5 : 0;
}

void KBSWorkunitNode::update()
{
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  const QString result_name = state->workunit[m_workunit].result_name;
  
  int status;
  unsigned unit;
  bool suspended, aborted, graphics;
  if(!result_name.isEmpty())
  {
    const KBSBOINCResult &result = state->result[result_name];
    const int task_index = state->active_task_set.index(result_name);
    
    if(task_index >= 0)
    {
      const KBSBOINCActiveTask &task = state->active_task_set.active_task[task_index];
      
      status = -task.scheduler_state;
      unit = this->unit(task.fraction_done);
      graphics = task.supports_graphics;
    }
    else
    {
      status = result.state;
      unit = (status >= 3) ? 100 : 0;
      graphics = false;
    }
    
    suspended = result.suspended_via_gui;
    aborted = result.aborted_via_gui;
  }
  else
  {
    status = unit = 0;
    suspended = aborted = graphics = false;
  }
  
  bool changed = false;
  
  if(m_result != result_name) {
    m_result = result_name;
    changed = true;
  }
  if(m_status != status) {
    m_status = status;
    changed = true;
  }
  if(m_unit != unit) {
    m_unit = unit;
    changed = true;
  }
  if(m_suspended != suspended) {
    m_suspended = suspended;
    changed = true;
  }
  if(m_aborted != aborted) {
    m_aborted = aborted;
    changed = true;
  }
  if(m_graphics != graphics) {
    m_graphics = graphics;
    changed = true;
  }
  
  if(changed) emit nodeChanged(this);
}

void KBSWorkunitNode::setupMonitor()
{
  KBSHostNode *host = static_cast<KBSHostNode*>(findAncestor("KBSHostNode"));
  m_monitor = (NULL != host) ? host->monitor() : NULL;
  if(NULL == m_monitor) return;
  
  connect(m_monitor, SIGNAL(stateUpdated()), this, SLOT(update()));
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  m_application = state->workunit[m_workunit].app_name;
  m_project = m_monitor->project(state->workunit[m_workunit]);
  m_projectURL = state->project[m_project].master_url;
  
  update();
}

void KBSWorkunitNode::addPlugins()
{
  if(m_project.isEmpty()) return;
  
  QString constraints;
  KTrader::OfferList offers;
  QDict<KBSPanelNode> plugins;
  
  // first load plugins specific to this project...
  constraints = "([X-KDE-Target] == 'Workunit') and ('%1' in [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints.arg(m_project));
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    const QRegExp app = (*offer)->property("X-KDE-Application").toString();
    if(!app.isEmpty() && app.search(application()) == -1)
      continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_workunit);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
  
  // ... and then generic ones valid for all projects
  constraints = "([X-KDE-Target] == 'Workunit') and (not exist [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints);
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_workunit);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
}

#include "kbsworkunitnode.moc"
