/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdom.h>
#include <qpixmap.h>
#include <qstringlist.h>

#include <klibloader.h>
#include <klocale.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbspanelnode.h>
#include <kbsprojectnode.h>
#include <kbsrpcmonitor.h>
#include <kbstasknode.h>

#include "kbshostnode.h"

KBSHostNode::KBSHostNode(const KBSLocation& location, KBSTreeNode *parent, const char *name)
           : KBSTreeNode(parent, name),
             m_monitor(new KBSBOINCMonitor(location, this)),
             m_connected(m_monitor->rpcMonitor()->canRPC())
{
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL != state) {
    addProjects(state->project.keys());
    updateTasks();
  }
  
  connect(m_monitor, SIGNAL(projectsAdded(const QStringList &)),
          this, SLOT(addProjects(const QStringList &)));
  connect(m_monitor, SIGNAL(projectsRemoved(const QStringList &)),
          this, SLOT(removeProjects(const QStringList &)));
  
  connect(m_monitor, SIGNAL(stateUpdated()), this, SLOT(updateTasks()));
  
  connect(m_monitor->rpcMonitor(), SIGNAL(updated()),
          this, SLOT(updateConnection()));
  
  addPlugins(); 
}

unsigned KBSHostNode::type() const
{
  return 1;
}

QString KBSHostNode::name() const
{
  return KBSHostNode::name(m_monitor->location().host);
}

QStringList KBSHostNode::icons() const
{
  return QStringList(m_connected ? "location" : "location_disconnected");
}

KBSBOINCMonitor *KBSHostNode::monitor()
{
  return m_monitor;
}

bool KBSHostNode::isConnected() const
{
  return m_connected;
}

QString KBSHostNode::name(const QString &host)
{
  return host;
}

void KBSHostNode::addProjects(const QStringList &projects)
{
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    KBSProjectNode *node = new KBSProjectNode(*project, this);
    insertChild(node);
    
    m_projects.insert(*project, node);
  }
}

void KBSHostNode::removeProjects(const QStringList &projects)
{
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    KBSProjectNode *node = m_projects.find(*project);
    if(NULL == node) continue;
    
    m_projects.remove(*project);
    removeChild(node);
  }
}

void KBSHostNode::updateTasks()
{
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  const QMap<unsigned,KBSBOINCActiveTask> tasks(state->active_task_set.active_task);
  unsigned num = 0;
  
  for(QMap<unsigned,KBSBOINCActiveTask>::const_iterator task = tasks.begin();
      task != tasks.end(); ++task)
    if((*task).scheduler_state > 1)
    {
      const QString workunit = state->result[(*task).result_name].wu_name;
      KBSTaskNode *node = m_tasks.find(num);
      
      if(NULL == node)
        addTask(num, workunit);
      else if(workunit != node->workunit()) {
        removeTask(num);
        addTask(num, workunit);
      }
      
      ++num;
    }
  
  while(m_tasks.count() > num)
    removeTask(m_tasks.count()-1);
}

void KBSHostNode::updateConnection()
{
  const bool connected = m_monitor->rpcMonitor()->canRPC();
  
  if(connected != m_connected) {
    m_connected = connected;
    emit nodeChanged(this);
  }
}

void KBSHostNode::addTask(unsigned num, const QString &workunit)
{
  KBSTaskNode *node = new KBSTaskNode(num, workunit, this);
  insertChild(node);
  
  m_tasks.insert(num, node);
}

void KBSHostNode::removeTask(unsigned num)
{
  KBSTaskNode *node = m_tasks.find(num);
  if(NULL == node) return;
  
  m_tasks.remove(num);
  removeChild(node);
}

void KBSHostNode::addPlugins()
{
  const QString constraints = "[X-KDE-Target] == 'Host'";
  KTrader::OfferList offers = KTrader::self()->query("KBSPanelNode", constraints);
  QDict<KBSPanelNode> plugins;

  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
}

#include "kbshostnode.moc"
