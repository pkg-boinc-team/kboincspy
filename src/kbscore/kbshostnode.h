/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSHOSTNODE_H
#define KBSHOSTNODE_H

#include <qdict.h>
#include <qintdict.h>
#include <qstringlist.h>

#include <kurl.h>

#include <kbstree.h>

class KBSBOINCMonitor;
class KBSLocation;
class KBSProjectNode;
class KBSTaskNode;

class KBSHostNode : public KBSTreeNode
{
  Q_OBJECT
  public:
    KBSHostNode(const KBSLocation& location, KBSTreeNode *parent, const char *name=0);
    
    virtual unsigned type() const;
    virtual QString name() const;
    virtual QStringList icons() const;
    
    virtual KBSBOINCMonitor *monitor();
    
    virtual bool isConnected() const;
    
    static QString name(const QString& host);
  
  private slots:
    void addProjects(const QStringList &projects);
    void removeProjects(const QStringList &projects);
    void updateTasks();
    void updateConnection();
  
  private:
    void setupMonitor(const KBSLocation &location);
    void addTask(unsigned num, const QString &workunit);
    void removeTask(unsigned task);
    void addPlugins();
  
  protected:
    KBSBOINCMonitor *m_monitor;
    QDict<KBSProjectNode> m_projects;
    QIntDict<KBSTaskNode> m_tasks;
    bool m_connected;
};

#endif
