/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLOGMANAGER_H
#define KBSLOGMANAGER_H

#include <qintdict.h>
#include <qobject.h>

#include <kurl.h>

#include <kbslogmonitor.h>

class KBSProjectMonitor;

class KBSLogManager : public QObject
{
  Q_OBJECT
  public:
    static KBSLogManager *self();

    virtual KURL url() const;
    virtual void setURL(const KURL &url);
    
    virtual int interval() const;
    
    virtual QStringList keys() const;
    virtual bool hasResults() const;
    
    virtual KBSLogData workunits() const;
    virtual KBSLogData results(const QString &workunit) const;
    
    virtual unsigned currentFormat() const;
    virtual void setCurrentFormat(unsigned format);
    
    virtual unsigned writeMask() const;
    virtual void setWriteMask(unsigned format);
    
    virtual void logWorkunit(KBSProjectMonitor *projectMonitor, const QString &workunit);
    
  public slots:
    virtual void setInterval(int interval);
    
  signals:
    void intervalChanged(int interval);
    void logChanged();
    void workunitsUpdated();
    void resultsUpdated();
    
  protected:
    KBSLogManager(QObject *parent=0, const char *name=0);
    
    virtual unsigned formats() const;
    virtual KBSLogMonitor *createLogMonitor(unsigned format, const KURL &url, QObject *parent=0);
  
  private:
    KBSLogMonitor *createLogMonitor(unsigned format);
    void destroyLogMonitor(unsigned format);
    
  protected:
    QIntDict<KBSLogMonitor> m_monitors;
    unsigned m_format, m_mask;
    int m_interval;
    KURL m_url;
    
  private:
    static KBSLogManager *s_self;
};

#endif
