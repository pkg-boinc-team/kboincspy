/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlayout.h>
#include <qtooltip.h>

#include <krun.h>
#include <ksqueezedtextlabel.h>
#include <kurllabel.h>

#include "kbspanelfield.h"

KBSPanelField::KBSPanelField(QWidget *parent, const char *name)
             : QWidget(parent, name), m_type(None), m_value(NULL), m_aux(NULL)
{
  m_layout = new QHBoxLayout(this, 0, 4);
  
  m_name = new QLabel(this, "name");
  m_name->setAlignment(AlignRight);
  m_layout->addWidget(m_name, 0);
}

QString KBSPanelField::name() const
{
  return m_name->text();
}

void KBSPanelField::setName(const QString &name)
{
  m_name->setText(name);
}

bool KBSPanelField::isText() const
{
  return(Text == m_type || SqueezedText == m_type);
}

QString KBSPanelField::text() const
{
  return isText() ? m_value->text() : QString::null;
}

void KBSPanelField::setText(const QString &text)
{
  setType(Text);
  
  m_value->setText(text);
}

void KBSPanelField::setSqueezedText(const QString &text)
{
  setType(SqueezedText);
  
  m_value->setText(text);
}

QString KBSPanelField::url() const
{
  return (URL == m_type) ? static_cast<KURLLabel*>(m_value)->url() : QString::null;
}

QString KBSPanelField::urlText() const
{
  return (URL == m_type) ? m_value->text() : QString::null;
}

QString KBSPanelField::urlTooltip() const
{
  return (URL == m_type) ? static_cast<KURLLabel*>(m_value)->tipText() : QString::null;
}

void KBSPanelField::setURL(const QString &url, const QString &text, const QString &tooltip)
{
  setType(URL);
  
  KURLLabel *value = static_cast<KURLLabel*>(m_value);
  
  value->setURL(url);
  value->setText(text);
  value->setUseTips(!tooltip.isEmpty());
  value->setTipText(tooltip);
}

QString KBSPanelField::aux() const
{
  return (URL == m_type) ? m_aux->text() : QString::null;
}

void KBSPanelField::setAux(const QString &text)
{
  setType(URL);

  m_aux->setText(text);
}

QString KBSPanelField::tooltip() const
{
  return (Text == m_type) ? QToolTip::textFor(m_value) : QString::null;
}
  
void KBSPanelField::setTooltip(const QString &tooltip)
{
  if(Text != m_type) return;
  
  QToolTip::remove(m_value);
  if(!tooltip.isEmpty())
    QToolTip::add(m_value, tooltip);
}

QColor KBSPanelField::textColor() const
{
  switch(m_type) {
    case Text:
    case SqueezedText:
      return m_value->paletteForegroundColor();
    case URL:
      return m_aux->paletteForegroundColor();
    default:
      return QColor();
  }
}

void KBSPanelField::setTextColor(const QColor &color)
{
  switch(m_type) {
    case Text:
    case SqueezedText:
      m_value->setPaletteForegroundColor(color);
      break;
    case URL:
      m_aux->setPaletteForegroundColor(color);
      break;
    default:
      break;
  }
}

void KBSPanelField::setType(Type type)
{
  if(type == m_type) return;
  
  if(URL == m_type) {
    delete m_aux;
    m_aux = NULL;
  }
  if(None != m_type) {
    delete m_value;
    m_value = NULL;
  }
  
  m_type = type;
  
  switch(type) {
    case Text:
      m_value = new QLabel(this);
      m_value->setAlignment(AlignLeft);
      m_layout->addWidget(m_value, 1);
      m_value->show();
      break;
    case SqueezedText:
      m_value = new KSqueezedTextLabel(this);
      m_value->setAlignment(AlignLeft);
      m_layout->addWidget(m_value, 1);
      m_value->show();
      break;
    case URL:
      m_value = new KURLLabel(this);
      connect(m_value, SIGNAL(leftClickedURL(const QString &)), this, SLOT(handleURL(const QString &)));
      m_layout->addWidget(m_value, 0);
      m_value->show();
      m_aux = new QLabel(this);
      m_aux->setAlignment(AlignLeft);
      m_layout->addWidget(m_aux, 1);
      m_aux->show();
      break;
    default:
      break;
  }
}
 
void KBSPanelField::handleURL(const QString &url)
{
  KRun::runURL(url, "text/html", false, false);
}

#include "kbspanelfield.moc"
