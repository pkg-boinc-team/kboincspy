/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qclipboard.h>
#include <qfile.h>
#include <qimage.h>
#include <qtextstream.h>

#include <kapplication.h>
#include <kfiledialog.h>
#include <kio/netaccess.h>
#include <klocale.h>
#include <kmenubar.h>
#include <kmessagebox.h>
#include <ktempfile.h>
#include <ktoolbar.h>

#include "kbsstandardwindow.h"

KBSStandardWindow::KBSStandardWindow(QWidget *parent, const char *name, WFlags f)
                 : KMainWindow(parent, name, f)
{
}

KBSStandardWindow::~KBSStandardWindow()
{
  const QString group = autoSaveGroup();
  if(!group.isEmpty()) writeGeometry(group);
}

QString KBSStandardWindow::text()
{
  return QString::null;
}

QPixmap KBSStandardWindow::pixmap()
{
  return QPixmap();
}

void KBSStandardWindow::setAutoSaveGeometry(const QString &group)
{
  setAutoSaveSettings(group, false);
  if(!group.isEmpty()) readGeometry(group);
}

bool KBSStandardWindow::eventFilter(QObject *obj, QEvent *e)
{
  if(e->type() == QEvent::MouseButtonPress)
  {
    QWidget *target = (QWidget *) obj;
    QMouseEvent *click = (QMouseEvent *) e;

    if(click->button() == RightButton) {
      QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
      context->popup(target->mapToGlobal(click->pos()));
      return true;
    }
  }

  return false;
}

void KBSStandardWindow::setupActions()
{
  setStandardToolBarMenuEnabled(false);
  
  KStdAction::copy(this, SLOT(editCopy()), actionCollection())->setText(i18n("&Copy to Clipboard"));  
  KStdAction::save(this, SLOT(fileSave()), actionCollection())->setText(i18n("&Save to File..."));
  KStdAction::close(this, SLOT(close()), actionCollection())->setText(i18n("Close &Window"));

  createGUI("kbsstdwindowui.rc", false);

  delete menuBar();
  delete toolBar();
}

bool KBSStandardWindow::queryClose()
{
  if(!kapp->sessionSaving()) {
    hide();
    return false;
  }
  else
    return true;
}

void KBSStandardWindow::editCopy()
{
  QPixmap pixmap = this->pixmap();
  if(!pixmap.isNull()) {
    KApplication::clipboard()->setPixmap(pixmap);
    return;
  }
  
  QString text = this->text();
  if(!text.isNull())
    KApplication::clipboard()->setText(text);
}

void KBSStandardWindow::fileSave()
{
  QPixmap pixmap = this->pixmap();
  if(!pixmap.isNull())
  {
    QStringList filter;
    filter << "image/x-bmp" << "image/png";
    
    QStrList formats = QImageIO::outputFormats();
    if(formats.contains("GIF")) filter << "image/gif";
    if(formats.contains("JPEG")) filter << "image/jpeg";    
    
    KFileDialog fileDialog(QString::null, QString::null, this, "save", true);
    fileDialog.setOperationMode(KFileDialog::Saving);
    fileDialog.setMimeFilter(filter, "image/png");
    fileDialog.setCaption(i18n("Save Picture"));
    fileDialog.setKeepLocation(true);
    fileDialog.setMode(KFile::File);
    
    while(true)
    {
      if(fileDialog.exec() == 0) return;
    
      KURL fileURL = fileDialog.selectedURL();
      if(!fileURL.isValid()) return;
      fileURL.adjustPath(-1);
      
      const QString fileFilter = fileDialog.currentFilter();
      const QString fileType = (  fileFilter == "image/x-bmp" ) ? "BMP"
                             : (  fileFilter == "image/gif"   ) ? "GIF"
                             : (  fileFilter == "image/jpeg"  ) ? "JPEG"
                             : /* fileFilter == "image/png"  */   "PNG";
      
      if(KIO::NetAccess::exists(fileURL, false, this)) {
        const QString warning = i18n("File %1 exists. Overwrite?").arg(fileURL.fileName());
        if(KMessageBox::warningYesNo(this, warning) != KMessageBox::Yes)
          continue;
      }
      
      if(fileURL.isLocalFile())
        pixmap.save(fileURL.path(), fileType);
      else
      {
        KTempFile fileTemp;
        fileTemp.setAutoDelete(true);
    
        pixmap.save(fileTemp.name(), fileType);
        KIO::NetAccess::upload(fileTemp.name(), fileURL, this);
      }
      
      return;
    }
  }
  
  QString text = this->text();
  if(!text.isNull())
    while(true)
    {
      KURL fileURL = KFileDialog::getSaveURL(QString::null, "text/plain", this, i18n("Save Text"));
      if(!fileURL.isValid()) return;
      fileURL.adjustPath(-1);
      
      if(KIO::NetAccess::exists(fileURL, false, this)) {
        const QString warning = i18n("File %1 exists. Overwrite?").arg(fileURL.fileName());
        if(KMessageBox::warningYesNo(this, warning) != KMessageBox::Yes)
          continue;
      }
      
      KTempFile fileTemp;
      fileTemp.setAutoDelete(true);
      
      const QString fileName = fileURL.isLocalFile() ? fileURL.path() : fileTemp.name();
      QFile file(fileName);
      
      if(file.open(IO_WriteOnly | IO_Truncate)) {
        const QString terminator = text.endsWith("\n") ? QString::null : "\n";
        QTextStream(&file) << text << terminator;
        file.close();
      }
      
      if(!fileURL.isLocalFile())
        KIO::NetAccess::upload(fileTemp.name(), fileURL, this);
        
      return;
    }
}

void KBSStandardWindow::readGeometry(const QString &group)
{
  KConfig *config = kapp->config();
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
                 
  QRect geometry;
  
  geometry.setTop(config->readNumEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), -1));
  if(geometry.top() < 0) return;
  
  geometry.setLeft(config->readNumEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), -1));
  if(geometry.left() < 0) return;
  
  geometry.setHeight(config->readNumEntry(QString("Height %1").arg(desk.height()), 0));
  if(geometry.height() <= 0) return;
  
  geometry.setWidth(config->readNumEntry(QString("Width %1").arg(desk.width()), -1));
  if(geometry.width() < 0) return;
  
  setGeometry(geometry);
}

void KBSStandardWindow::writeGeometry(const QString &group)
{
  KConfig *config = kapp->config();
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
  
  config->writeEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), geometry().top());
  config->writeEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), geometry().left());
  config->writeEntry(QString("Height %1").arg(desk.height()), geometry().height());
  config->writeEntry(QString("Width %1").arg(desk.width()), geometry().width());
}

#include "kbsstandardwindow.moc"
