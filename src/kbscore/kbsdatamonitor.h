/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSDATAMONITOR_H
#define KBSDATAMONITOR_H

#include <qobject.h>

#include <qdatetime.h>
#include <qdict.h>
#include <qdom.h>
#include <qobject.h>
#include <qptrdict.h>
#include <qstringlist.h>
#include <qvaluelist.h>

#include <kio/jobclasses.h>
#include <ktempfile.h>
#include <kurl.h>

struct KBSFileInfo
{
  QString fileName;
  bool initialized,
       monitored,
       exists;
  QDateTime timestamp;
  unsigned size;
  bool ok;
};

class KBSDataMonitor : public QObject
{
  Q_OBJECT
  public:
    virtual ~KBSDataMonitor();
    
    virtual KURL url() const;
    virtual int interval() const;
    
  public slots:
    virtual void setInterval(int interval);
    virtual void checkFiles();
    
  signals:
    void intervalChanged(int interval);
    void fileUpdated(const QString &file);

  protected:
    KBSDataMonitor(const KURL &url, QObject *parent=0, const char *name=0);
    
    virtual void timerEvent(QTimerEvent *);
    
    virtual void addFile(const QString &fileName);
    virtual void removeFile(const QString &fileName);
    virtual void setMonitoring(const QString &fileName, bool monitored);
   
    virtual const KBSFileInfo *file(const QString &fileName) const;
    
    static bool readFile(const QString &fileName, QString &content);
    static bool readFile(const QString &fileName, QDomDocument &content);
    static bool readFile(const QString &fileName, QStringList &content,
                         const QString &delimiter=QString::null);
    
    static bool readDevice(QIODevice *device, QString &content);
    static bool readDevice(QIODevice *device, QDomDocument &content);
    static bool readDevice(QIODevice *device, QStringList &content,
                           const QString &delimiter=QString::null);
    
    virtual void checkFile(KBSFileInfo *file);
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
  protected slots:
    virtual void checkFile(const QString &fileName);
    
  private:
    void updateLocalFileInfo(const QString &fileName, KBSFileInfo *info);
    
    void queueStatJob(const QString &fileName);
    void queueCopyJob(const QString &fileName);
    
    void commenceStatJob(const QString &fileName);
    void commenceCopyJob(const QString &fileName);    
    
  private slots:
    void statResult(KIO::Job *job);
    void copyResult(KIO::Job *job);

  private:
    KURL m_url;
    int m_interval, m_timer;
    QDict<KBSFileInfo> m_files;
    KIO::Job *m_job;
    KTempFile *m_tmp;
    QStringList m_stat, m_copy;
};

#endif
