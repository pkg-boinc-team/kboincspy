/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSCACHENODE_H
#define KBSCACHENODE_H

#include <qdict.h>
#include <qstringlist.h>

#include <kbstree.h>

class KBSBOINCMonitor;
class KBSWorkunitNode;

class KBSCacheNode : public KBSTreeNode
{
  Q_OBJECT
  public:
    KBSCacheNode(const QString &project, KBSTreeNode *parent, const char *name=0);
    
    virtual unsigned type() const;
    virtual QString name() const;
    virtual QStringList icons() const;
    
    virtual QString project() const;
  
  protected:    
    virtual KBSBOINCMonitor *monitor();
  
  private slots:
    void addWorkunits(const QStringList &workunits);
    void removeWorkunits(const QStringList &workunits);
  
  private:
    void setupMonitor();
    void addPlugins();
  
  protected:
     QDict<KBSWorkunitNode> m_workunits;
     
  private:
    QString m_project;
    KBSBOINCMonitor *m_monitor;
};

#endif
