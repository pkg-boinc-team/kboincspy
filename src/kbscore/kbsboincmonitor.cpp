/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <unistd.h>

#include <kapplication.h>
#include <klocale.h>
#include <knotifyclient.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbsprojectmonitor.h>
#include <kbsprojectplugin.h>
#include <kbsrpcmonitor.h>
#include <kbstaskmonitor.h>
#include <kbstree.h>

using namespace KBSBOINC;

const QString KBSBOINCClientStateFile = "client_state.xml";

const QString KBSBOINCRPCAuthenticationFile = "gui_rpc_auth.cfg";

const QString KBSBOINCAccountFilePrefix = "account_";
const QString KBSBOINCAccountFileSuffix = ".xml";

const unsigned KBSBOINCAccountFilePrefixLength = KBSBOINCAccountFilePrefix.length();
const unsigned KBSBOINCAccountFileSuffixLength = KBSBOINCAccountFileSuffix.length();

const QString KBSBOINCStatisticsFilePrefix = "statistics_";
const QString KBSBOINCStatisticsFileSuffix = ".xml";

const unsigned KBSBOINCStatisticsFilePrefixLength = KBSBOINCStatisticsFilePrefix.length();
const unsigned KBSBOINCStatisticsFileSuffixLength = KBSBOINCStatisticsFileSuffix.length();

QString KBSLocation::defaultHost(const KURL &url)
{
  const QString host = url.host();
  return host.isEmpty() ? "localhost" : host;
}

unsigned KBSLocation::defaultPort = 0;

QString KBSLocation::defaultHost() const
{
  return defaultHost(url);
}

bool operator==(const KBSLocation &location1, const KBSLocation &location2)
{
  return(location1.url == location2.url);
}

KBSBOINCMonitor::KBSBOINCMonitor(const KBSLocation &location, KBSTreeNode *parent, const char *name)
               : KBSDataMonitor(location.url, parent, name), m_location(location),
                 m_rpcMonitor(new KBSRPCMonitor(location.host, this)), m_client(NULL),
                 m_startup(true), m_killClient(false)
{
  m_rpcMonitor->setPort(location.port);
  
  connect(this, SIGNAL(fileUpdated(const QString &)),
          this, SLOT(updateFile(const QString &)));
  
  connect(this, SIGNAL(projectsAdded(const QStringList &)),
          this, SLOT(addProjectFiles(const QStringList &)));
  connect(this, SIGNAL(projectsRemoved(const QStringList &)),
          this, SLOT(removeProjectFiles(const QStringList &)));
  
  connect(this, SIGNAL(projectsAdded(const QStringList &)),
          this, SLOT(addProjectMonitors(const QStringList &)));
  connect(this, SIGNAL(projectsRemoved(const QStringList &)),
          this, SLOT(removeProjectMonitors(const QStringList &)));
  
  connect(this, SIGNAL(resultActivated(unsigned, const QString &, bool)),
          this, SLOT(updateTaskMonitor(unsigned, const QString &, bool)));
  
  addFile(KBSBOINCClientStateFile);
  addFile(KBSBOINCRPCAuthenticationFile);
}

KBSBOINCMonitor::~KBSBOINCMonitor()
{
  for(QIntDictIterator<KBSTaskMonitor> it(m_taskMonitors); it.current() != NULL; ++it)
    delete it.current();
  m_taskMonitors.clear();
  
  for(QDictIterator<KBSProjectMonitor> it(m_projectMonitors); it.current() != NULL; ++it)
    delete it.current();
  m_projectMonitors.clear();
  
  for(QDictIterator<KBSBOINCAccount> it(m_accounts); it.current() != NULL; ++it)
    delete it.current();
  m_accounts.clear();
  
  for(QDictIterator<KBSBOINCProjectStatistics> it(m_statistics); it.current() != NULL; ++it)
    delete it.current();
  m_statistics.clear();
  
  if(!m_killClient || !m_client->isRunning()) return;
  
  if(m_rpcMonitor->canRPC())
    m_rpcMonitor->quit();
  
  // failsafe method
  m_client->tryTerminate();
  usleep(200);
  m_client->tryTerminate();
}

bool KBSBOINCMonitor::isLocal() const
{
  return(m_location.host == "localhost" || m_location.host == "127.0.0.1");
}

KBSLocation KBSBOINCMonitor::location() const
{
  return m_location;
}

void KBSBOINCMonitor::exec(const QString &client, bool killOnExit)
{
  if(NULL != m_client || !isLocal()) return;
  
  m_client = new QProcess(this);
  
  m_client->setWorkingDirectory(m_location.url.path(+1));
  
  const KURL executable(m_location.url, client);
  m_client->addArgument(executable.path(-1));
  
  m_client->start();
  
  m_killClient = killOnExit;
}

const KBSBOINCClientState *KBSBOINCMonitor::state() const
{
  return file(KBSBOINCClientStateFile)->ok ? &m_state : NULL;
}
    
const KBSBOINCAccount *KBSBOINCMonitor::account(const QString &project) const
{
  return file(formatAccountFileName(project))->ok ? m_accounts.find(project) : NULL;
}

const KBSBOINCProjectStatistics *KBSBOINCMonitor::statistics(const QString &project) const
{
  return file(formatStatisticsFileName(project))->ok ? m_statistics.find(project) : NULL;
}

QString KBSBOINCMonitor::project(const KBSBOINCProject &project) const
{
  return parseProjectName(project.master_url);
}

QString KBSBOINCMonitor::project(const KBSBOINCApp &app) const
{
  double max = 0.0;
  QString out = QString::null;
  
  const KURL::List urls = collectURLs(app);
  for(KURL::List::const_iterator url = urls.begin(); url != urls.end(); ++url)
  {
    if(!(*url).isValid()) continue;
    
    for(QMap<QString,KBSBOINCProject>::const_iterator project = m_state.project.begin();
        project != m_state.project.end(); ++project)
    {
      const double score = matchURL(*url, (*project).scheduler_url);
      if(score > max) { max = score; out = project.key(); }
    }
  }
  
  return out;
}

QString KBSBOINCMonitor::project(const KBSBOINCWorkunit &workunit) const
{
  double max = 0.0;
  QString out = QString::null;
  
  const KURL::List urls = collectURLs(workunit);
  for(KURL::List::const_iterator url = urls.begin(); url != urls.end(); ++url)
  {
    if(!(*url).isValid()) continue;
    
    for(QMap<QString,KBSBOINCProject>::const_iterator project = m_state.project.begin();
        project != m_state.project.end(); ++project)
    {
      const double score = matchURL(*url, (*project).scheduler_url);
      if(score > max) { max = score; out = project.key(); }
    }
  }
  
  return out;
}

QString KBSBOINCMonitor::project(const KBSBOINCResult &result) const
{
  double max = 0.0;
  QString out = QString::null;
  
  const KURL::List urls = collectURLs(result);
  for(KURL::List::const_iterator url = urls.begin(); url != urls.end(); ++url)
  {
    if(!(*url).isValid()) continue;
    
    for(QMap<QString,KBSBOINCProject>::const_iterator project = m_state.project.begin();
        project != m_state.project.end(); ++project)
    {
      const double score = matchURL(*url, (*project).scheduler_url);
      if(score > max) { max = score; out = project.key(); }
    }
  }
  
  return out;
}

QString KBSBOINCMonitor::project(const KBSBOINCActiveTask &task) const
{
  return parseProjectName(task.project_master_url);
}

QString KBSBOINCMonitor::project(const KBSBOINCAccount &account) const
{
  return parseProjectName(account.master_url);
}

QString KBSBOINCMonitor::project(const KBSBOINCProjectStatistics &statistics) const
{
  return parseProjectName(statistics.master_url);
}

QString KBSBOINCMonitor::app(const KBSBOINCApp &app) const
{
  return app.name;
}

QString KBSBOINCMonitor::app(const KBSBOINCWorkunit &workunit) const
{
  return workunit.app_name;
}

QString KBSBOINCMonitor::app(const KBSBOINCResult &result) const
{
  if(m_state.workunit.contains(result.wu_name))
    return app(m_state.workunit[result.wu_name]);
  else
    return QString::null;
}

QString KBSBOINCMonitor::app(const KBSBOINCActiveTask &task) const
{
  if(m_state.result.contains(task.result_name))
    return app(m_state.result[task.result_name]);
  else
    return QString::null;
}
    
QString KBSBOINCMonitor::workunit(const KBSBOINCWorkunit &workunit) const
{
  return workunit.name;
}

QString KBSBOINCMonitor::workunit(const KBSBOINCResult &result) const
{
  return result.wu_name;
}

QString KBSBOINCMonitor::workunit(const KBSBOINCActiveTask &task) const
{
  if(m_state.result.contains(task.result_name))
    return workunit(m_state.result[task.result_name]);
  else
    return QString::null;
}

QString KBSBOINCMonitor::result(const KBSBOINCResult &result) const
{
  return result.name;
}

QString KBSBOINCMonitor::result(const KBSBOINCActiveTask &task) const
{
  return task.result_name;
}

KBSRPCMonitor *KBSBOINCMonitor::rpcMonitor() const
{
  return m_rpcMonitor;
}

KBSProjectMonitor *KBSBOINCMonitor::projectMonitor(const QString &project) const
{
  return m_projectMonitors.find(project);
}

KBSTaskMonitor *KBSBOINCMonitor::taskMonitor(unsigned task) const
{
  return m_taskMonitors.find(task);
}

bool KBSBOINCMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  if(KBSBOINCClientStateFile == file->fileName)
  {
    QDomDocument document(file->fileName);
    if(!readFile(fileName, document)) return false;
    
    return parseClientStateDocument(document);
  }
  else if(KBSBOINCRPCAuthenticationFile == file->fileName)
  {
    QStringList lines;
    if(!readFile(fileName, lines)) return false;
    
    const QString password = (lines.count() > 0) ? lines[0].stripWhiteSpace() : "";
    m_rpcMonitor->setPassword(password);
    
    qDebug("... parse OK");
    
    return true;
  }
  else
  {
    QString project;

    project = parseAccountFileName(file->fileName);
    if(!project.isNull())
    {
      KBSBOINCAccount *account = m_accounts.find(project);
      if(NULL == account) return false;
      
      QDomDocument document(file->fileName);
      if(!readFile(fileName, document)) return false;
      
      return parseAccountDocument(document, *account);
    }

    project = parseStatisticsFileName(file->fileName);
    if(!project.isNull())
    {
      KBSBOINCProjectStatistics *statistics = m_statistics.find(project);
      if(NULL == statistics) return false;
      
      QDomDocument document(file->fileName);
      if(!readFile(fileName, document)) return false;
      
      return parseStatisticsDocument(document, *statistics);
    }

    return false;
  }
}

QString KBSBOINCMonitor::formatAccountFileName(const QString &project)
{
  return(KBSBOINCAccountFilePrefix + project + KBSBOINCAccountFileSuffix);
}

QString KBSBOINCMonitor::formatStatisticsFileName(const QString &project)
{
  return(KBSBOINCStatisticsFilePrefix + project + KBSBOINCStatisticsFileSuffix);
}

QString KBSBOINCMonitor::parseAccountFileName(const QString &fileName)
{
  if(fileName.startsWith(KBSBOINCAccountFilePrefix)
     && fileName.endsWith(KBSBOINCAccountFileSuffix))
    return fileName.mid(KBSBOINCAccountFilePrefixLength,
                        fileName.length() - KBSBOINCAccountFilePrefixLength
                                          - KBSBOINCAccountFileSuffixLength); 
  else
    return QString::null;
}

QString KBSBOINCMonitor::parseStatisticsFileName(const QString &fileName)
{
  if(fileName.startsWith(KBSBOINCStatisticsFilePrefix)
     && fileName.endsWith(KBSBOINCStatisticsFileSuffix))
    return fileName.mid(KBSBOINCStatisticsFilePrefixLength,
                        fileName.length() - KBSBOINCStatisticsFilePrefixLength
                                          - KBSBOINCStatisticsFileSuffixLength); 
  else
    return QString::null;
}

bool KBSBOINCMonitor::parseClientStateDocument(const QDomDocument &document)
{
  QDomNode child = document.firstChild();
  while(!child.isNull()) {
    if(child.isElement()) {
      QDomElement element = child.toElement();
      
      if(element.nodeName() == "client_state") {
        if(!m_state.parse(element)) return false;
      }
    }
    child = child.nextSibling();
  }
  
  if(!validateResults()) return false;
  
  qDebug("... parse OK");
  
  return true;
}

bool KBSBOINCMonitor::parseAccountDocument(const QDomDocument &document, KBSBOINCAccount &account)
{
  QDomNode child = document.firstChild();
  while(!child.isNull()) {
    if(child.isElement()) {
      QDomElement element = child.toElement();
      
      if(element.nodeName() == "account") {
        if(!account.parse(element)) return false;
      }
    }
    child = child.nextSibling();
  }
  
  emit accountUpdated(project(account));
  
  qDebug("... parse OK");
  return true;
}

bool KBSBOINCMonitor::parseStatisticsDocument(const QDomDocument &document,
                                              KBSBOINCProjectStatistics &statistics)
{
  QDomNode child = document.firstChild();
  while(!child.isNull()) {
    if(child.isElement()) {
      QDomElement element = child.toElement();
      
      if(element.nodeName() == "project_statistics") {
        if(!statistics.parse(element)) return false;
      }
    }
    child = child.nextSibling();
  }
  
  emit statisticsUpdated(project(statistics));
  
  qDebug("... parse OK");
  return true;
}

bool KBSBOINCMonitor::validateResults()
{
  QStringList workunits = m_state.workunit.keys();
  for(QStringList::const_iterator it = workunits.constBegin();
      it != workunits.constEnd(); ++it)
    m_state.workunit[*it].result_name = QString::null;
  
  QStringList results = m_state.result.keys();
  for(QStringList::const_iterator it = results.constBegin();
      it != results.constEnd(); ++it)
  {
    const QString wu_name = m_state.result[*it].wu_name;
    if(!wu_name.isEmpty())
      if(workunits.contains(wu_name))
        m_state.workunit[wu_name].result_name = *it;
      else
        return false;
  }
  
  return true;
}

void KBSBOINCMonitor::notify(const QString &message, const QString &text)
{
  KNotifyClient::event(kapp->mainWidget()->winId(), message,
                       i18n("Host %1: %2").arg(m_location.host).arg(text));
}

KURL::List KBSBOINCMonitor::collectURLs(const KBSBOINCApp &app) const
{
  KURL::List out;

  const QString name = app.name;
  if(m_state.app_version.contains(name))
    for(QValueList<KBSBOINCAppVersion>::const_iterator app_version = m_state.app_version[name].begin();
        app_version != m_state.app_version[name].end(); ++app_version)
      for(QValueList<KBSBOINCFileRef>::const_iterator file_ref = (*app_version).file_ref.begin();
          file_ref != (*app_version).file_ref.end(); ++file_ref)
        if(m_state.file_info.contains((*file_ref).file_name))
          out += m_state.file_info[(*file_ref).file_name].url;
  
  return out;
}

KURL::List KBSBOINCMonitor::collectURLs(const KBSBOINCWorkunit &workunit, bool recursive) const
{
  KURL::List out;
  
  if(recursive)
  {
    if(!workunit.app_name.isEmpty() && m_state.app.contains(workunit.app_name))
      out += collectURLs(m_state.app[workunit.app_name]);
    
    if(!workunit.result_name.isEmpty() && m_state.result.contains(workunit.result_name))
      out += collectURLs(m_state.result[workunit.app_name], false);
  }
  
  for(QValueList<KBSBOINCFileRef>::const_iterator file_ref = workunit.file_ref.begin();
      file_ref != workunit.file_ref.end(); ++file_ref)
    if(m_state.file_info.contains((*file_ref).file_name))
      out += m_state.file_info[(*file_ref).file_name].url;
  
  return out;
}

KURL::List KBSBOINCMonitor::collectURLs(const KBSBOINCResult &result, bool recursive) const
{
  KURL::List out;
  
  if(recursive)
    if(!result.wu_name.isEmpty() && m_state.workunit.contains(result.wu_name))
      out += collectURLs(m_state.workunit[result.wu_name]);
  
  for(QValueList<KBSBOINCFileRef>::const_iterator file_ref = result.file_ref.begin();
      file_ref != result.file_ref.end(); ++file_ref)
    if(m_state.file_info.contains((*file_ref).file_name))
      out += m_state.file_info[(*file_ref).file_name].url;
  
  return out;
}

double KBSBOINCMonitor::matchURL(const KURL &url, const KURL &scheduler_url)
{
  double result = 0.0;
  QString host = url.host(),
          scheduler_host = scheduler_url.host();
  
  if(host != scheduler_host)
  {
    unsigned const length = host.length(),
                   scheduler_length = scheduler_host.length();
    
    unsigned i = length, j = scheduler_length;
    
    while(i > 0 && j > 0 && host.at(--i) == scheduler_host.at(--j))
      ;
    
    result += (host.mid(i+1).contains('.')+1)/double(host.contains('.')+1);
  }
  else
    result += 2.0;
  
  QString path = url.path(),
          scheduler_path = scheduler_url.path();

  if(path != scheduler_path)
  {
    unsigned const length = path.length(),
                   scheduler_length = scheduler_path.length();
    unsigned i = 0, j = 0;
    
    while(i < length && j < scheduler_length && path.at(i++) == scheduler_path.at(j++))
      ;
    
    
    result += 1e-3 * i/double(length);
  }
  else
    result += 2e-3;
  
  return result;
}

void KBSBOINCMonitor::addProjectFiles(const QStringList &projects)
{
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    m_accounts.insert(*project, new KBSBOINCAccount());
    addFile(formatAccountFileName(*project));
    
    m_statistics.insert(*project, new KBSBOINCProjectStatistics());
    addFile(formatStatisticsFileName(*project));
  }
}

void KBSBOINCMonitor::removeProjectFiles(const QStringList &projects)
{
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    KBSBOINCAccount *account = m_accounts.take(*project);
    if(NULL != account) delete account;
    removeFile(formatAccountFileName(*project));
    
    KBSBOINCProjectStatistics *statistics = m_statistics.take(*project);
    if(NULL != statistics) delete statistics;
    removeFile(formatStatisticsFileName(*project));
  }
}

void KBSBOINCMonitor::addProjectMonitors(const QStringList &projects)
{
  KBSTreeNode *node = static_cast<KBSTreeNode*>(parent());
  if(NULL == node) return;
  
  KBSDocument *document = static_cast<KBSDocument*>(node->findAncestor("KBSDocument"));
  if(NULL == document) return;
  
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    KBSProjectPlugin *plugin = document->plugin(*project);
    if(NULL == plugin) continue;
    
    KBSProjectMonitor *monitor = plugin->createProjectMonitor(*project, this);
    if(NULL == monitor) continue;
    
    m_projectMonitors.insert(*project, monitor);
  }
}

void KBSBOINCMonitor::removeProjectMonitors(const QStringList &projects)
{
  for(QStringList::const_iterator project = projects.constBegin();
      project != projects.constEnd(); ++project)
  {
    KBSProjectMonitor *monitor = m_projectMonitors.take(*project);
    if(NULL == monitor) continue;
    
    delete monitor;
  }
}

void KBSBOINCMonitor::updateTaskMonitor(unsigned task, const QString &result, bool add)
{
  if(add) {
    const QString project = this->project(m_state.result[result]);
    if(project.isEmpty()) return;
  
    KBSTreeNode *node = static_cast<KBSTreeNode*>(parent());
    if(NULL == node) return;
  
    KBSDocument *document = static_cast<KBSDocument*>(node->findAncestor("KBSDocument"));
    if(NULL == document) return;
    
    KBSProjectPlugin *plugin = document->plugin(project);
    if(NULL == plugin) return;
    
    KBSTaskMonitor *monitor = plugin->createTaskMonitor(task, this);
    if(NULL == monitor) return;
    
    m_taskMonitors.insert(task, monitor);
  }
  else
  {
    KBSTaskMonitor *monitor = m_taskMonitors.take(task);
    if(NULL != monitor) delete monitor;
  }
}

void KBSBOINCMonitor::updateFile(const QString &fileName)
{
  if(!(file(fileName)->ok)) return;
  
  if(KBSBOINCClientStateFile == fileName)
  {
    {
      const unsigned version = m_state.core_client.major_version * 100 + m_state.core_client.minor_version;
      
      if(0 == m_location.port)
        m_rpcMonitor->setPort(version >= 420 && version < 503 ? 1043 : 31416);
    }
    {
      QStringList added, names, removed = m_projects;
      
      m_projects = m_state.project.keys();
      for(QStringList::iterator project = m_projects.begin();
          project != m_projects.end(); ++project)
        if(removed.contains(*project))
          removed.remove(*project);
        else {
          added << *project;
          names << m_state.project[*project].project_name;
        }
      if(!added.isEmpty()) emit projectsAdded(added);
      if(!removed.isEmpty()) emit projectsRemoved(removed);
      
      if(!(m_startup || names.isEmpty()))
        notify("ProjectAttached",
               i18n("project %1 has been attached",
                    "projects %1 have been attached",
                    added.count())
                 .arg(names.join(", ")));
    }
    {
      QStringList added, removed = m_apps;
      
      m_apps = m_state.app.keys();
      for(QStringList::iterator app = m_apps.begin();
          app != m_apps.end(); ++app)
        if(removed.contains(*app))
          removed.remove(*app);
        else
          added << *app;
      if(!added.isEmpty()) emit appsAdded(added);
      if(!removed.isEmpty()) emit appsRemoved(removed);
    }
    {
      QStringList added, removed = m_workunits;
      
      m_workunits = m_state.workunit.keys();
      for(QStringList::iterator workunit = m_workunits.begin();
          workunit != m_workunits.end(); ++workunit)
        if(removed.contains(*workunit))
          removed.remove(*workunit);
        else
          added << *workunit;
      if(!added.isEmpty()) emit workunitsAdded(added);
      if(!removed.isEmpty()) emit workunitsRemoved(removed);
      
      if(!(m_startup || added.isEmpty()))
        notify("WorkunitDownloading",
               i18n("work unit %1 is being downloaded",
                    "workunits %1 are being downloaded",
                    added.count())
                 .arg(added.join(", ")));
    }
    {
      QStringList added, removed = m_results;
      
      m_results = m_state.result.keys();
      for(QStringList::iterator result = m_results.begin();
          result != m_results.end(); ++result)
        if(removed.contains(*result))
          removed.remove(*result);
        else
          added << *result;
      if(!added.isEmpty()) emit resultsAdded(added);
      if(!removed.isEmpty()) emit resultsRemoved(removed);
    }
    {
      QStringList added, workunits, removed = m_complete;
      
      m_complete.clear();
      for(QStringList::iterator result = m_results.begin();
          result != m_results.end(); ++result)
        if(m_state.result[*result].ready_to_report)
        {
          m_complete << *result;
          if(removed.contains(*result))
            removed.remove(*result);
          else {
            added << *result;
            workunits << m_state.result[*result].wu_name;
          }
        }
      if(!(m_startup || added.isEmpty())) emit resultsCompleted(added);
      
      if(!(m_startup || workunits.isEmpty()))
        notify("WorkunitCompleted",
               i18n("work unit %1 has been completed",
                    "workunits %1 have been completed",
                    workunits.count())
                 .arg(workunits.join(", "))); 
    }
    {
      QMap<unsigned,KBSBOINCActiveTask> removed = m_tasks;
      QValueList<unsigned> running;
      QStringList switched;
      
      m_tasks = m_state.active_task_set.active_task;
      for(QMap<unsigned,KBSBOINCActiveTask>::iterator task = m_tasks.begin();
          task != m_tasks.end(); ++task)
      {
        const QString newResult = (*task).result_name;
        
        if((*task).scheduler_state > 1)
          running << task.key();
        
        if(removed.contains(task.key()))
        {
          const QString oldResult = removed[task.key()].result_name;
        
          if(oldResult != newResult)
          {
            emit workunitActivated(task.key(), m_state.result[oldResult].wu_name, false);
            emit workunitActivated(task.key(), m_state.result[newResult].wu_name, true);
            emit resultActivated(task.key(), oldResult, false);
            emit resultActivated(task.key(), newResult, true);
            
            switched << QString::number(task.key());
          }
          else if(m_running.contains(task.key()) ^ running.contains(task.key()))
            switched << QString::number(task.key());
          
          removed.remove(task.key());
        }
        else
        {
          emit workunitActivated(task.key(), m_state.result[newResult].wu_name, true);
          emit resultActivated(task.key(), newResult, true);
            
          switched << QString::number(task.key());
        }
      }
      for(QMap<unsigned,KBSBOINCActiveTask>::iterator task = removed.begin();
          task != removed.end(); ++task)
      {
        const QString oldResult = removed[task.key()].result_name;
          
        emit workunitActivated(task.key(), m_state.result[oldResult].wu_name, false);
        emit resultActivated(task.key(), oldResult, false);
           
        switched << QString::number(task.key());
      }
      
      if(!(m_startup || switched.isEmpty()))
        notify("TaskSwitch",
               i18n("there was a task switch on slot %1",
                    "there were task switches involving slots %1",
                    switched.count())
                 .arg(switched.join(", "))); 
        
      m_running = running;
    }
    
    m_startup = false;
    
    emit stateUpdated();
  }
  else
  {
    QString project;
    
    project = parseAccountFileName(fileName);
    if(!project.isEmpty() && NULL != m_accounts.find(project))
      emit accountUpdated(project);
    
    project = parseStatisticsFileName(fileName);
    if(!project.isEmpty() && NULL != m_statistics.find(project))
      emit statisticsUpdated(project);
  }
}

#include "kbsboincmonitor.moc"
