/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>

#include "kbstaskmonitor.h"

KBSTaskMonitor::KBSTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name)
              : KBSDataMonitor(KURL(parent->url(), QString("slots/%1/").arg(task)), parent, name),
                m_task(task)
{
  connect(parent, SIGNAL(intervalChanged(int)), this, SLOT(setInterval(int)));

  setInterval(parent->interval());
  
  const KBSBOINCClientState *state = parent->state();
  if(NULL == state) return;
  
  m_project = parent->project(state->active_task_set.active_task[task]);
  m_result = state->active_task_set.active_task[task].result_name;
  m_workunit = state->result[m_result].wu_name;
}

QString KBSTaskMonitor::project() const
{
  return m_project;
}

QString KBSTaskMonitor::workunit() const
{
  return m_workunit;
}

QString KBSTaskMonitor::result() const
{
  return m_result;
}

unsigned KBSTaskMonitor::task() const
{
  return m_task;
}

KBSBOINCMonitor *KBSTaskMonitor::boincMonitor()
{
  return static_cast<KBSBOINCMonitor*>(parent());
}

#include "kbstaskmonitor.moc"
