/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPROJECTMONITOR_H
#define KBSPROJECTMONITOR_H

#include <qmap.h>
#include <qstringlist.h>

#include <kbsdatamonitor.h>

#include <kbsboincdata.h>

struct KBSFileMetaInfo
{
  QStringList workunits,
              results;
  QString open_name;
  unsigned active_sets;
};

class KBSBOINCMonitor;
class KBSLogManager;

class KBSProjectMonitor : public KBSDataMonitor
{
  Q_OBJECT
  public:
    virtual QString project() const;
  
    virtual KBSBOINCMonitor *boincMonitor() const;
    
    virtual KBSLogManager *logManager() const;
    
  signals:
    void updatedResult(const QString &workunit);

  protected:
    KBSProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    
    virtual bool parseable(const QString &openName) const;
    
    virtual bool validWorkunit(const QString &workunit) const;
    virtual bool validResult(const QString &result) const;
    
  private slots:
    void addWorkunits(const QStringList &workunits);
    void addResults(const QStringList &results);
    void removeWorkunits(const QStringList &workunits);    
    void removeResults(const QStringList &results);    
    void logResults(const QStringList &results);
    void activateResult(unsigned, const QString &result, bool active);
  
  private:
    bool validSet(const QString &set) const;
  
  protected:
    QMap<QString,KBSFileMetaInfo> m_meta;
    
  private:
    QString m_project;
    QMap<QString,QStringList> m_sets;
};

#endif
