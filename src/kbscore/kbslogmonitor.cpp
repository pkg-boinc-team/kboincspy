/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdatetime.h>
#include <qfile.h>
#include <qregexp.h>

#include <kio/job.h>
#include <krfcdate.h>

#include <kbsboincmonitor.h>
#include <kbsprojectmonitor.h>

#include <kbsboincdata.h>

#include "kbslogmonitor.h"

using namespace KBSBOINC;

KBSLogMonitor::KBSLogMonitor(const KURL &url, QObject *parent, const char *name)
             : KBSDataMonitor(url, parent, name), m_tmp(NULL), m_job(NULL)
{
}

QStringList KBSLogMonitor::keys() const
{
  return QStringList();
}

bool KBSLogMonitor::hasResults() const
{
  return false;
}

KBSLogData KBSLogMonitor::workunits() const
{
  return m_workunits;
}

KBSLogData KBSLogMonitor::results(const QString &workunit) const
{
  return m_results[workunit];
}

void KBSLogMonitor::logWorkunit(KBSProjectMonitor *monitor, const QString &workunit)
{
  QMap<QString,KBSLogData> log = formatWorkunit(monitor, workunit);
  QStringList fileNames(log.keys());
  
  if(url().isLocalFile())
    for(QStringList::iterator fileName = fileNames.begin(); fileName != fileNames.end(); ++fileName)
    {
      const KBSFileInfo *info = file(*fileName);
      if(NULL == info) continue;
  
      KURL fileURL(url(), *fileName);
  
      QFile file(fileURL.path(-1));
      if(file.open(IO_WriteOnly | IO_Append))
      {
        if(!info->exists || m_workunits.isEmpty())
          appendHeader(info, &file);
          
        KBSLogData data =  log[*fileName];
        for(KBSLogData::iterator datum = data.begin(); datum != data.end(); ++ datum)
          appendWorkunit(info, &file, *datum);
        
        file.close();
      }
    }
  else
  {
    for(QStringList::iterator fileName = fileNames.begin(); fileName != fileNames.end(); ++fileName)
    {
      KBSLogData data = log[*fileName];
      for(KBSLogData::iterator datum = data.begin(); datum != data.end(); ++datum)
        m_queue[*fileName] << *datum;
    
      if(NULL == m_job)
        commenceLogReadJob(*fileName);
    }
  }
}

void KBSLogMonitor::addLogFile(const QString &fileName)
{
  m_files << fileName;
  
  addFile(fileName);
}

QMap<QString,KBSLogData> KBSLogMonitor::formatWorkunit(KBSProjectMonitor *, const QString &) const
{
  return QMap<QString,KBSLogData>();
}
    
void KBSLogMonitor::appendHeader(const KBSFileInfo *, QIODevice *)
{
}

void KBSLogMonitor::appendWorkunit(const KBSFileInfo *, QIODevice *, const KBSLogDatum &)
{
}

QDateTime KBSLogMonitor::parseSETIClassicDate(const QString &string)
{
  return string.contains(' ') ? parseJulianDate(string.left(string.find(' '))) : parseJulianDate(string);
}

QString KBSLogMonitor::formatSETIClassicDate(const QDateTime &date)
{
  return QString("%1 (%2)").arg(formatJulianDate(date), 0, 'f', 5).arg(date.toString());
}

QString KBSLogMonitor::formatSETIClassicDate(double date)
{
  return QString("%1 (%2)").arg(date, 0, 'f', 5).arg(parseJulianDate(date).toString());
}

unsigned KBSLogMonitor::parseMajorVersion(const QString &string)
{
  return string.left(string.find('.')).toUInt(0, 10);
}

unsigned KBSLogMonitor::parseMinorVersion(const QString &string)
{
  return string.mid(string.find('.')+1).toUInt(0, 10);
}

QString KBSLogMonitor::formatVersion(unsigned major, unsigned minor)
{
  return QString().sprintf("%u.%02u", major, minor);
}

unsigned KBSLogMonitor::parseVersion (const QString &string)
{
  return QString(string).remove('.').toUInt(0, 10);
}

QString KBSLogMonitor::formatVersion(unsigned version)
{
  return formatVersion(version / 100, version % 100);
}

QValueList<QVariant> KBSLogMonitor::parsePotData(const QString &string)
{
  QValueList<QVariant> out;
  
  const unsigned count = string.length()/2;
  for(unsigned i = 0; i < count; i++)
    out << string.mid(i*2, 2).toUInt(0, 16);
  
  return out;
}

QString KBSLogMonitor::formatPotData(const QValueList<unsigned> &list)
{
  QString out;
  for(QValueList<unsigned>::const_iterator it = list.constBegin(); it != list.constEnd(); ++it)
    out.append(QString::number(*it, 16).rightJustify(2, '0'));
  
  return out;
}

QDateTime KBSLogMonitor::parseLogEntryDate(const QString &string)
{
  QDateTime out;
  
  out.setDate(QDate(string.mid(0, 4).toInt(0, 10),
                    string.mid(5, 2).toInt(0, 10),
                    string.mid(8, 2).toInt(0, 10)));
  out.setTime(QTime(string.mid(11, 2).toInt(0, 10),
                    string.mid(14, 2).toInt(0, 10),
                    string.mid(17, 2).toInt(0, 10)));
  
  return out;
}

QString KBSLogMonitor::formatLogEntryDate(const QDateTime &date)
{
  return QString().sprintf("%04d.%02d.%02d %02d:%02d:%02d",
                           date.date().year(), date.date().month(), date.date().day(),
                           date.time().hour(), date.time().minute(), date.time().second());
}

QStringList KBSLogMonitor::parseCSVKeys(const QString &line, const QChar &sep)
{
  QString in(line);
  QStringList out;
  
  int next;
  while(!in.isEmpty())
  {
    if(in.startsWith("\""))
    {
      const int start = 1;
      int end = in.find("\"" + QString(sep), start);
      if(end < 0) {
        end = in.endsWith("\"") ? in.length()-1 : in.length();
        next = in.length();
      } else
        next = end+2;
      
      QString value = in.mid(start, end-start);
      value.replace("\"\"", "\"");
    
      out << value;
    }
    else
    {
      const int end = in.find(sep);
      next = (end >= 0) ? end+1 : in.length(); 
    }
    
    in = in.mid(next);
  }
  
  return out;
}

QString KBSLogMonitor::formatCSVKeys(const QStringList &keys, const QChar &sep)
{
  QString out;
  
  for(QStringList::const_iterator key = keys.begin(); key != keys.end(); ++key)
  {
    if(!out.isEmpty()) out.append(sep);
    
    QString item = *key;
    item.replace("\"", "\"\"");
          
    out.append(QString("\"%1\"").arg(item));
  }
  
  return out;
}

KBSLogDatum KBSLogMonitor::parseCSVDatum(const QString &line, const QStringList &keys, const QChar &sep)
{
  QString in(line);
  KBSLogDatum out;
  
  int next;
  QStringList::const_iterator key = keys.begin();
  while(!in.isEmpty() && key != keys.end())
  {
    if(in.startsWith("\""))
    {
      const int start = 1;
      int end = in.find("\"" + QString(sep), start);
      if(end < 0) {
        end = in.endsWith("\"") ? in.length()-1 : in.length();
        next = in.length();
      } else
        next = end+2;
      
      QString value = in.mid(start, end-start);
      value.replace("\"\"", "\"");
      
      out[*key] = value;
    }
    else
    {
      const int start = 0;
      int end = in.find(sep);
      if(end < 0)
        end = in.length();
      next = end+1;
      
      QString value = in.mid(start, end-start);
      bool ok;
      
      out[*key] = value.toUInt(&ok, 10);
      if(!ok) out[*key] = value.toInt(&ok, 10);
      if(!ok) out[*key] = value.toDouble(&ok);
      if(!ok) out[*key] = value;
    }
    
    in = in.mid(next);
    ++key;
  }
  
  return out;
}

QString KBSLogMonitor::formatCSVDatum(const KBSLogDatum &datum, const QStringList &keys, const QChar &sep)
{
  QStringList out;
  
  for(QStringList::const_iterator key = keys.begin(); key != keys.end(); ++key)
    switch(datum[*key].type())
    {
      case QVariant::CString:
      case QVariant::String:
        {
          QString value = datum[*key].toString();
          value.replace("\"", "\"\"");
          
          out << QString("\"%1\"").arg(value);
        }
        break;
      case QVariant::Int:
        out << QString::number(datum[*key].toInt());
        break;
      case QVariant::UInt:
        out << QString::number(datum[*key].toUInt());
        break;
      case QVariant::Double:
        out << QString::number(datum[*key].toDouble(), 'f', 9);
        break;
      default:
        out << QString::null;
        break;
    }
  
  return out.join(sep);
}

QStringList KBSLogMonitor::remapKeys(const QStringList &keys, const KBSKeyMap &map)
{
  QStringList out;
  
  for(QStringList::const_iterator it = keys.begin(); it != keys.end(); ++it)
    out << (map.contains(*it) ? map[*it] : *it);
  
  return out;
}

KBSLogDatum KBSLogMonitor::remapCSVDatum(const KBSLogDatum &datum, const KBSKeyMap &map)
{
  KBSLogDatum out;
  
  for(KBSLogDatum::const_iterator it = datum.begin(); it != datum.end(); ++it)
    if(map.contains(it.key()))
      out[map[it.key()]] = it.data();
    else
      out[it.key()] = it.data();
  
  return out;   
}

void KBSLogMonitor::commenceLogReadJob(const QString &fileName)
{
  const KBSFileInfo *info = file(fileName);
  if(NULL == info) return;
  
  m_tmp = new KTempFile();
  m_tmp->setAutoDelete(true);
  
  if(info->exists)
  {
    KURL fileURL(url());
    fileURL.addPath(fileName);
  
    m_job = KIO::file_copy(fileURL, m_tmp->name(), -1, true, false, false);
    connect(m_job, SIGNAL(result(KIO::Job *)), this, SLOT(readResult(KIO::Job *)));
  }
  else
    commenceLogWriteJob(fileName);
}

void KBSLogMonitor::commenceLogWriteJob(const QString &fileName)
{
  if(NULL == m_tmp) return;
  
  const KBSFileInfo *info = file(fileName);
  if(NULL == info) return;
  
  if(m_queue.contains(fileName))
  {
    KBSLogData data = m_queue[fileName];
    m_queue.remove(fileName);
    
    QFile file(m_tmp->name());
    if(file.open(IO_WriteOnly | IO_Append))
    {
      if(!info->exists || m_workunits.isEmpty())
        appendHeader(info, &file);
       for(KBSLogData::iterator datum = data.begin(); datum != data.end(); ++datum)
        appendWorkunit(info, &file, *datum);
      file.close();
    }
  }

  KURL fileURL(url());
  fileURL.addPath(fileName);
  
  m_job = KIO::file_copy(m_tmp->name(), fileURL, -1, true, false, false);
  connect(m_job, SIGNAL(result(KIO::Job *)), this, SLOT(writeResult(KIO::Job *)));
}
    
void KBSLogMonitor::readResult(KIO::Job *job)
{
  if(job != m_job) return;
  
  KIO::FileCopyJob *copyJob = static_cast<KIO::FileCopyJob*>(job);
  const QString fileName = copyJob->srcURL().fileName();
  
  if(!job->error())
    commenceLogWriteJob(fileName);
  else
  {
    delete m_tmp;
    m_tmp = NULL;
  
    m_job = NULL;
  }
}

void KBSLogMonitor::writeResult(KIO::Job *job)
{
  if(job != m_job) return;
  
  delete m_tmp;
  m_tmp = NULL;
  
  m_job = NULL;
  
  if(!job->error() && !m_queue.isEmpty())
    commenceLogReadJob(m_queue.keys().first());
}

#include "kbslogmonitor.moc"
