/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <klibloader.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbshostnode.h>
#include <kbslogmanager.h>
#include <kbsprojectmonitor.h>
#include <kbsprojectplugin.h>
#include <kbsrpcmonitor.h>

#include "kbsdocument.h"

KBSDocument::KBSDocument(QObject *parent, const char *name)
           : KBSTreeNode(parent, name)
{
  loadPlugins();
  
  KBSLogManager *log = KBSLogManager::self();
  
  log->setInterval(interval());
  connect(this, SIGNAL(intervalChanged(int)), log, SLOT(setInterval(int)));
}

KBSDocument::~KBSDocument()
{
  QPtrList<KBSProjectPlugin> plugins = this->plugins();
  for(QPtrListIterator<KBSProjectPlugin> it(plugins); NULL != it.current(); ++it)
    delete it.current();
  m_plugins.clear();
}

void KBSDocument::connectTo(const KBSLocation &location)
{
  if(m_locations.contains(location.url)) return;
  
  m_locations[location.url] = location;
  
  KBSHostNode *node = new KBSHostNode(location, this);
  KBSBOINCMonitor *monitor = node->monitor();
  
  monitor->setInterval(m_preferences.fam());
  connect(this, SIGNAL(intervalChanged(int)), monitor, SLOT(setInterval(int)));
  
  monitor->rpcMonitor()->setInterval(m_preferences.rpc());
  
  if(m_preferences.client_exec())
    monitor->exec(m_preferences.client(), m_preferences.client_kill());
  
  insertChild(node);
}

void KBSDocument::disconnectFrom(const KBSLocation &location)
{
  m_locations.remove(location.url);
  
  for(unsigned i = 0; i < children(); ++i)
  {
    KBSTreeNode *node = child(i);
    
    if(node->inherits("KBSHostNode"))
      if(static_cast<KBSHostNode*>(node)->monitor()->location() == location) {
        removeChild(node);
        return;
      }
  }
}

int KBSDocument::interval() const
{
  return m_preferences.fam();
}

QPtrList<KBSProjectPlugin> KBSDocument::plugins() const
{
  QPtrList<KBSProjectPlugin> out;
  for(QDictIterator<KBSProjectPlugin> it(m_plugins); it.current() != NULL; ++it)
    if(!out.containsRef(it.current()))
      out.append(it.current());
  
  return out;
}

KBSProjectPlugin *KBSDocument::plugin(const QString &project) const
{
  return m_plugins.find(project);
}

KConfigSkeleton *KBSDocument::preferences()
{
  return &m_preferences;
}

void KBSDocument::applyPreferences()
{
  emit intervalChanged(m_preferences.fam());
  
  for(unsigned i = 0; i < children(); ++i)
    if(child(i)->inherits("KBSHostNode")) {
      KBSRPCMonitor *rpcMonitor = static_cast<KBSHostNode*>(child(i))->monitor()->rpcMonitor();
      rpcMonitor->setInterval(m_preferences.rpc());
    }
  
  KBSLogManager *log = KBSLogManager::self();
  
  log->setURL(m_preferences.location());
  log->setWriteMask(m_preferences.write() ? 1 : 0);
  
  QPtrList<KBSProjectPlugin> plugins = this->plugins();
  for(QPtrListIterator<KBSProjectPlugin> it(plugins); NULL != it.current(); ++it)
    it.current()->applyPreferences();
}

void KBSDocument::readConfig(KConfig *config)
{
  config->setGroup("KBSDocument");
  
  m_preferences.readConfig();
  applyPreferences();
  
  qDebug("client = %s", m_preferences.client().latin1());
  
  QValueList<KBSLocation> locations;
  const unsigned count = config->readNumEntry("Locations", 0);
  
  for(unsigned i = 0; i < count; ++i)
  {
    const QString prefix = QString("Location %1 ").arg(i);
    KBSLocation location;
    
    location.url = KURL(config->readEntry(prefix + "URL"));
    if(!location.url.isValid()) continue;
    location.url.adjustPath(+1);
    
    location.host = config->readEntry(prefix + "host", location.defaultHost());
    
    location.port = config->readNumEntry(prefix + "port", location.defaultPort);
    
    locations << location;
  }
  
  for(QValueList<KBSLocation>::iterator location = locations.begin();
      location != locations.end(); ++location)
    connectTo(*location);
  
  QPtrList<KBSProjectPlugin> plugins = this->plugins();
  for(QPtrListIterator<KBSProjectPlugin> it(plugins); NULL != it.current(); ++it)
    it.current()->readConfig(config);
}

void KBSDocument::writeConfig(KConfig *config)
{
  config->setGroup("KBSDocument");
  
  m_preferences.writeConfig();
  
  config->writeEntry("Locations", unsigned(m_locations.count()));
  
  unsigned i;
  QMap<KURL,KBSLocation>::iterator location;
  for(i = 0, location = m_locations.begin(); location != m_locations.end(); ++i, ++location)
  {
    const QString prefix = QString("Location %1 ").arg(i);
    
    config->writeEntry(prefix + "URL", (*location).url.prettyURL(+1));
    config->writeEntry(prefix + "host", (*location).host);
    config->writeEntry(prefix + "port", (*location).port);
  }
  
  QPtrList<KBSProjectPlugin> plugins = this->plugins();
  for(QPtrListIterator<KBSProjectPlugin> it(plugins); NULL != it.current(); ++it)
    it.current()->writeConfig(config);
}

void KBSDocument::loadPlugins()
{
  KTrader::OfferList offers = KTrader::self()->query("KBSMonitor");
  for(KTrader::OfferListIterator offer = offers.begin();
      offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->name();
    
    const QStringList projects = (*offer)->property("X-KDE-Projects").toStringList();
    if(projects.isEmpty()) continue;
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) {
      qDebug("Error loading plugin %s: %s", name.latin1(),
                                            KLibLoader::self()->lastErrorMessage().latin1());
      continue;
    }
    
    KBSProjectPlugin *plugin = static_cast<KBSProjectPlugin*>(factory->create(this, name,
                                                                              "KBSProjectPlugin"));
    
    for(QStringList::const_iterator project = projects.constBegin();
        project != projects.constEnd(); ++project)
    {
      m_plugins.insert(*project, plugin);
      qDebug("Plugin %s for project %s loaded successfully", name.latin1(), (*project).latin1());
    }
  }
}

#include "kbsdocument.moc"
