/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSTASKMONITOR_H
#define KBSTASKMONITOR_H

#include <kbsdatamonitor.h>

class KBSBOINCMonitor;

class KBSTaskMonitor : public KBSDataMonitor
{
  Q_OBJECT
  public:
    virtual QString project() const;
    virtual QString workunit() const;
    virtual QString result() const;
    virtual unsigned task() const;
  
    virtual KBSBOINCMonitor *boincMonitor();
    
  signals:
    void updatedState();
    
  protected:
    KBSTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name=0);
    
  private:
    QString m_project, m_workunit, m_result;
    unsigned m_task;
};

#endif
