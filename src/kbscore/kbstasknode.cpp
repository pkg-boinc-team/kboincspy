/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qregexp.h>

#include <klibloader.h>
#include <klocale.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbspanelnode.h>

#include "kbstasknode.h"

KBSTaskNode::KBSTaskNode(unsigned num, const QString &workunit, KBSTreeNode *parent, const char *name)
           : KBSWorkunitNode(workunit, parent, name), m_num(num)
{
  addPlugins();
}

QString KBSTaskNode::name() const
{
  return KBSTaskNode::name(m_num, monitor());
}

QString KBSTaskNode::name(unsigned num, KBSBOINCMonitor *)
{
  return i18n("CPU %1").arg(num + 1);
}

int KBSTaskNode::task() const
{
  return m_task;
}

QString KBSTaskNode::result() const
{
  return m_result;
}

void KBSTaskNode::addPlugins()
{
  if(NULL == monitor() || project().isEmpty()) return;
  
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  m_result = state->workunit[workunit()].result_name;
  if(m_result.isEmpty()) return;
  
  m_task = state->active_task_set.index(m_result);
  if(m_task < 0) return;

  QString constraints;
  KTrader::OfferList offers;
  QDict<KBSPanelNode> plugins;
  
  // first load plugins specific to this project...
  constraints = "([X-KDE-Target] == 'Task') and ('%1' in [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints.arg(project()));
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    const QRegExp app = (*offer)->property("X-KDE-Application").toString();
    if(!app.isEmpty() && app.search(application()) == -1)
      continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(QString::number(m_task));
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
  
  // ... and then generic ones valid for all projects
  constraints = "([X-KDE-Target] == 'Task') and (not exist [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints);
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(QString::number(m_task));
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
}

#include "kbstasknode.moc"
