/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSTATISTICSCHART_H
#define KBSSTATISTICSCHART_H

#include <qdatetime.h>
#include <qpixmap.h>
#include <qvaluelist.h>
#include <qwidget.h>

#include <kbsboincdata.h>

class KBSStatisticsChart : public QWidget
{
  Q_OBJECT
  public:
    enum Type {Host, User};
    KBSStatisticsChart(Type type, QWidget *parent=0, const char *name=0);
    
    virtual Type type() const;
    
    virtual QDate start() const;
    virtual QDate end() const;
    virtual void setData(const QValueList<KBSBOINCDailyStatistics> &data);
    
    virtual QPixmap pixmap();
  
  protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void timerEvent(QTimerEvent *event);
  
  private:
    void computeBoundaries();
    
    static double computeStep(double min, double max);
  
  protected:
    QDate m_today;
    enum DataSet {Daily, Total, DataSets};
    double m_min[DataSets], m_step[DataSets];
    QValueList<KBSBOINCDailyStatistics> m_data;
  
  private:
    Type m_type;
    QRect m_margin;
    
    static unsigned s_border;
};

#endif
