/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSTREE_H
#define KBSTREE_H

#include <qiconset.h>
#include <qobject.h>
#include <qpixmap.h>
#include <qptrlist.h>
#include <qstringlist.h>

QPixmap CompositePixmap(const QStringList &icons, int size=0);
QIconSet CompositeIconSet(const QStringList &icons);

typedef QValueList<unsigned> KBSTreePath;

class KBSTreeNode;

class KBSTreeNodeList : public QPtrList<KBSTreeNode>
{
  public:
    KBSTreeNodeList();
    
  protected:
    virtual int compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2);
};

class KBSTreeNode : public QObject
{
  Q_OBJECT
  public:
    KBSTreeNode(QObject *parent=0, const char *name=0);
    virtual ~KBSTreeNode();
    
    virtual unsigned children() const;
    virtual KBSTreeNode *child(unsigned pos) const;
    
    virtual int childIndex(const KBSTreeNode *child) const;
    virtual int childIndex(const QString &name) const;
    
    virtual bool isRoot() const;
    virtual KBSTreeNode *root();
    virtual bool isAncestor(const KBSTreeNode *node) const;
    virtual KBSTreeNode *findAncestor(const QString &className);
    
    virtual KBSTreePath path() const;
    virtual KBSTreeNode *descendant(const KBSTreePath &path);
    
    virtual unsigned type() const;
    virtual QString name() const;
    virtual QStringList icons() const;
  
  signals:
    void nodeChanged(KBSTreeNode *node);
    void childInserted(KBSTreeNode *node);
    void childRemoved(KBSTreeNode *node);
  
  protected:
    virtual void insertChild(KBSTreeNode *node);
    virtual void removeChild(KBSTreeNode *node, bool free=true);
    virtual void removeChild(unsigned index, bool free=true);
    virtual void removeAllChildren(bool free=true);
  
  protected:
    KBSTreeNodeList m_children;
};

class KBSNamedPath : public QStringList
{
  public:
    KBSNamedPath();
    KBSNamedPath(const QString &string);
    KBSNamedPath(const KBSTreeNode *root, const KBSTreePath &path);
    virtual ~KBSNamedPath();
    
    virtual QString toString() const;
    virtual KBSTreePath toPath(const KBSTreeNode *root, bool *error=0) const;
    
    static KBSNamedPath fromString(const QString &string);
    static KBSNamedPath fromPath(const KBSTreeNode *root, const KBSTreePath &path);
};

#endif
