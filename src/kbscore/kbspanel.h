/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPANEL_H
#define KBSPANEL_H

#include <qlabel.h>
#include <qlayout.h>
#include <qpopupmenu.h>
#include <qstringlist.h>
#include <qwidget.h>

class KBSPanelNode;

class KBSPanel : public QWidget
{
  Q_OBJECT
  public:
    KBSPanel(KBSPanelNode *node, QWidget *parent=0, const char *name=0);
    
    virtual KBSPanelNode *node() const;
    
    virtual QStringList icons() const;
    virtual void setIcons(const QStringList &icons);
    
    virtual QString header() const;
    virtual void setHeader(const QString &header);
    
    virtual QWidget *content() const;
    virtual void setContent(QWidget *content);
    
    virtual QStringList text() const;
    
    virtual QPopupMenu *contextMenu() const;
    virtual void setContextMenu(QPopupMenu *context);
    virtual bool eventFilter(QObject *obj, QEvent *e);
    
  protected:
    
  public slots:
    virtual void editCopy();
    
  protected:
    QStringList m_icons;
    KBSPanelNode *m_node;
    QLabel *m_icon, *m_header;
    QWidget *m_content;
    QPopupMenu *m_context;
    
  private:
    QGridLayout *m_layout;
};

#endif
