/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>
#include <kbslogmanager.h>

#include "kbsprojectmonitor.h"

KBSProjectMonitor::KBSProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name)
                 : KBSDataMonitor(KURL(parent->url(), QString("projects/%1/").arg(project)), parent, name),
                   m_project(project)
{
  connect(parent, SIGNAL(workunitsAdded(const QStringList &)),
          this, SLOT(addWorkunits(const QStringList &)));
  connect(parent, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  
  connect(parent, SIGNAL(resultsAdded(const QStringList &)),
          this, SLOT(addResults(const QStringList &)));
  connect(parent, SIGNAL(resultsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));

  connect(parent, SIGNAL(resultsCompleted(const QStringList &)),
          this, SLOT(logResults(const QStringList &)));
  
  connect(parent, SIGNAL(resultActivated(unsigned, const QString &, bool)),
          this, SLOT(activateResult(unsigned, const QString &, bool)));
  
  connect(parent, SIGNAL(intervalChanged(int)), this, SLOT(setInterval(int)));
  
  setInterval(parent->interval());
  
  const KBSBOINCClientState *state = parent->state();
  if(NULL != state)
  {
    addWorkunits(state->workunit.keys());
    addResults(state->result.keys());
    for(QMap<unsigned,KBSBOINCActiveTask>::const_iterator task = state->active_task_set.active_task.begin();
        task != state->active_task_set.active_task.end(); ++task)
      activateResult(task.key(), (*task).result_name, true);
  }
}


QString KBSProjectMonitor::project() const
{
  return m_project;
}

KBSBOINCMonitor *KBSProjectMonitor::boincMonitor() const
{
  return static_cast<KBSBOINCMonitor*>(parent());
}

KBSLogManager *KBSProjectMonitor::logManager() const
{
  return KBSLogManager::self();
}

bool KBSProjectMonitor::parseable(const QString &) const
{
  return false;
}

bool KBSProjectMonitor::validWorkunit(const QString &workunit) const
{
  return validSet(workunit);
}

bool KBSProjectMonitor::validResult(const QString &result) const
{
  return validSet(result);
}

void KBSProjectMonitor::addWorkunits(const QStringList &workunits)
{
  const KBSBOINCClientState *state = boincMonitor()->state();
  if(NULL == state) return;
  
  for(QStringList::const_iterator set = workunits.constBegin();
      set != workunits.constEnd(); ++set)
  {
    const KBSBOINCWorkunit workunit = state->workunit[*set];
    if(boincMonitor()->project(workunit) != m_project) continue;
    
    for(QValueList<KBSBOINCFileRef>::const_iterator fileRef = workunit.file_ref.constBegin();
        fileRef != workunit.file_ref.constEnd(); ++fileRef)
    {
      const QString openName = (*fileRef).open_name;
      
      if(!parseable(openName)) continue;
      
      const QString fileName = (*fileRef).file_name;
      
      if(!m_meta.contains(fileName)) {
        m_meta[fileName].workunits.clear();
        m_meta[fileName].results.clear();
        m_meta[fileName].open_name = openName;
        m_meta[fileName].active_sets = 0;
      }
      
      m_meta[fileName].workunits << *set;
      
      m_sets[*set] << fileName;
      
      addFile(fileName);
      setMonitoring(fileName, false);
    }
  }
}

void KBSProjectMonitor::addResults(const QStringList &results)
{
  const KBSBOINCMonitor *boincMonitor = static_cast<KBSBOINCMonitor*>(parent());
  const KBSBOINCClientState client_state = boincMonitor->m_state;
  
  for(QStringList::const_iterator set = results.constBegin();
      set != results.constEnd(); ++set)
  {
    const KBSBOINCResult result = client_state.result[*set];
    if(boincMonitor->project(result) != m_project) continue;
    
    for(QValueList<KBSBOINCFileRef>::const_iterator fileRef = result.file_ref.constBegin();
        fileRef != result.file_ref.constEnd(); ++fileRef)
    {
      const QString openName = (*fileRef).open_name;
      
      if(!parseable(openName)) continue;
      
      const QString fileName = (*fileRef).file_name;
      
      if(!m_meta.contains(fileName)) {
        m_meta[fileName].workunits.clear();
        m_meta[fileName].results.clear();
        m_meta[fileName].open_name = openName;
        m_meta[fileName].active_sets = 0;
      }
      
      m_meta[fileName].workunits << result.wu_name;
      m_meta[fileName].results << *set;
      
      m_sets[*set] << fileName;
      
      addFile(fileName);
      setMonitoring(fileName, false);
    }
  }
}

void KBSProjectMonitor::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator set = workunits.begin(); set != workunits.constEnd(); ++set)
    if(m_sets.contains(*set))
    {
      const QStringList fileNames = m_sets[*set];
      for(QStringList::const_iterator fileName = fileNames.constBegin();
          fileName != fileNames.constEnd(); ++fileName)
      {
        m_meta[*fileName].workunits.remove(*set);
        
        if(m_meta[*fileName].workunits.isEmpty()
        && m_meta[*fileName].results.isEmpty())
        {
          removeFile(*fileName);
          m_meta.remove(*fileName);
        }
      }
      
      m_sets.remove(*set);
    }
}

void KBSProjectMonitor::removeResults(const QStringList &results)
{
  const KBSBOINCMonitor *boincMonitor = static_cast<KBSBOINCMonitor*>(parent());
  const KBSBOINCClientState client_state = boincMonitor->m_state;
  
  for(QStringList::const_iterator set = results.begin(); set != results.end(); ++set)
    if(m_sets.contains(*set))
    {
      const KBSBOINCResult result = client_state.result[*set];
      if(boincMonitor->project(result) != m_project) continue;
      
      const QStringList fileNames = m_sets[*set];
      for(QStringList::const_iterator fileName = fileNames.constBegin();
          fileName != fileNames.constEnd(); ++fileName)
      {
        m_meta[*fileName].workunits.remove(result.wu_name);
        m_meta[*fileName].results.remove(*set);
        
        if(m_meta[*fileName].workunits.isEmpty()
        && m_meta[*fileName].results.isEmpty())
        {
          removeFile(*fileName);        
          m_meta.remove(*fileName);
        }
      }
      
      m_sets.remove(*set);
    }
}

void KBSProjectMonitor::logResults(const QStringList &results)
{
  const KBSBOINCClientState *state = boincMonitor()->state();
  if(NULL == state) return;
  
  if(logManager() == NULL) return;
  for(QStringList::const_iterator result = results.constBegin(); result != results.constEnd(); ++result)
    if(boincMonitor()->project(state->result[*result]) == m_project)
      logManager()->logWorkunit(this, state->result[*result].wu_name);
}

void KBSProjectMonitor::activateResult(unsigned, const QString &result, bool activate)
{
  if(m_sets.contains(result))
  {
    const QStringList fileNames = m_sets[result];
    for(QStringList::const_iterator fileName = fileNames.constBegin();
        fileName != fileNames.constEnd(); ++fileName)
      if(activate && m_meta[*fileName].active_sets++ == 0)
        setMonitoring(*fileName, true);
      else if(!activate && --m_meta[*fileName].active_sets == 0)
        setMonitoring(*fileName, false);
  }
}

bool KBSProjectMonitor::validSet(const QString &set) const
{
  if(!m_sets.contains(set)) return false;
  
  const QStringList fileNames = m_sets[set];
  for(QStringList::const_iterator fileName = fileNames.constBegin();
      fileName != fileNames.constEnd(); ++fileName)
  {
    const KBSFileInfo *info = file(*fileName);
    if(!info->ok) return false;
  }
  
  return true;
}

#include "kbsprojectmonitor.moc"
