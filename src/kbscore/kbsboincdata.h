/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSBOINCDATA_H
#define KBSBOINCDATA_H

#include <qdatetime.h>
#include <qdom.h>
#include <qmap.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qvaluelist.h>

#include <kurl.h>

namespace KBSBOINC
{
  QDateTime parseJulianDate(double d);
  QDateTime parseJulianDate(const QString &text);
  double formatJulianDate(const QDateTime &date);
  
  QDateTime parseUNIXDate(double d);
  QDateTime parseUNIXDate(const QString &text);
  double formatUNIXDate(const QDateTime &date);
  
  QString formatBytes(double bytes);
  QString formatTime(double secs);
  
  QString parseProjectName(const KURL &url);
  KURL formatProjectName(const QString &name);
}

struct KBSBOINCHostInfo
{
  unsigned timezone;                  // client timezone
  QString domain_name,                // client domain name
          ip_addr;                    // client IP address
  QString host_cpid;                  // host ID
  struct {
    unsigned ncpus;                   // number of CPUs
    QString vendor,                   // CPU vendor ('vendor' string reported by CPU)
            model;                    // CPU model ('model' string reported by CPU)
    double fpops,                     // floating point operations per second
           iops,                      // integer operations per second
           membw;                     // memory bandwith
    unsigned fpop_err,                // floating point errors
             iop_err,                 // integer errors
             membw_err;               // memory bandwith errors
    double calculated;                // benchmarked performance
  } p;
  struct {
    QString name,                     // operating system name
            version;                  // operating system version
  } os;
  struct {
    double nbytes,                    // total memory
           cache,                     // cache memory
           swap;                      // swap memory
  } m;
  struct {
    double total,                     // total disk space
           free;                      // free disk space
  } d;
  
  bool parse(const QDomElement &node);
  
  double credit_per_cpu_sec() const;
  static double credit_per_cpu_sec(double fpops, double iops);
};

struct KBSBOINCTimeStats
{
  double on_frac,                     // time percentage computer on
         connected_frac,              // time percentage computer connected
         active_frac,                 // time percentage BOINC running
         cpu_efficiency;
  QDateTime last_update;              // date & time of last connection
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCNetStats
{
  double bwup,                        // upload bandwidth
         bwdown;                      // download bandwidth
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCCredits
{
  double total_credit,                // total credit
         expavg_credit;               // expected average daily credit
  QDateTime create_time;              // registration date & time
};

struct KBSBOINCProject
{
  KURL scheduler_url,                 // URL of the BOINC scheduler
       master_url;                    // URL of the project page
  QString project_name,               // name of the project
          user_name,                  // name of the user (relative to this project)
          team_name,                  // name of the team the user belongs to
          email_hash,                 // hash key of the email address of the user
          cross_project_id;           // unique ID to identify this user across multiple projects
  KBSBOINCCredits user;               // user credits
  unsigned rpc_seqno,                 // RPC sequence number
           hostid;                    // host computer ID
  KBSBOINCCredits host;               // host computer credits
  struct {
    double cpu,                       //
           mod_time;                  //
  } exp_avg;
  unsigned nrpc_failures,             // number of failed fetches from scheduler URL
           master_fetch_failures,     // number of failed fetches from master URL
           min_rpc_time;              //
  double short_term_debt,
         long_term_debt;
  bool suspended_via_gui,
       dont_request_more_work;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCFileInfo
{
  QString name;                       // file name
  double nbytes,                      // current size in bytes
         max_nbytes;                  // maximum allowed size in bytes
  unsigned status;                    // file status
  QValueList<KURL> url;               // file URLs
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCApp
{
  QString name;
    
  bool parse(const QDomElement &node);
};

struct KBSBOINCFileRef
{
  QString file_name,
          open_name;
  bool main_program;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCAppVersion
{
  QString app_name;
  unsigned version_num;
  QValueList<KBSBOINCFileRef> file_ref;
  
  bool parse(const QDomElement &node);
};

typedef QValueList<KBSBOINCAppVersion> KBSBOINCAppVersionList;

struct KBSBOINCWorkunit
{
  QString name,
          app_name;
  unsigned version_num;
  QString command_line;
  QString env_vars;
  struct {
    double fpops_est,
           fpops_bound,
           memory_bound,
           disk_bound;
  } rsc;
  QValueList<KBSBOINCFileRef>file_ref;
  QString result_name;
    
  bool parse(const QDomElement &node);
};

struct KBSBOINCResult
{
  QString name;
  double final_cpu_time;
  unsigned exit_status,
           state;
  bool ready_to_report;
  QString wu_name;
  QDateTime report_deadline;
  QValueList<KBSBOINCFileRef> file_ref;
  bool suspended_via_gui,
       aborted_via_gui;
    
  bool parse(const QDomElement &node);
};

struct KBSBOINCActiveTask
{
  KURL project_master_url;
  QString result_name;
  unsigned app_version_num,
           slot,
           scheduler_state;
  double checkpoint_cpu_time,
         fraction_done,
         current_cpu_time,
         vm_bytes,
         rss_bytes;
  bool supports_graphics;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCActiveTaskSet
{
  QMap<unsigned,KBSBOINCActiveTask> active_task;
  
  bool parse(const QDomElement &node);
  
  int index(const QString &result) const;
};

struct KBSBOINCProxyInfo
{
  unsigned socks_version;
  struct {
    struct {
      QString name;
      unsigned port;
    } server;
    struct {
      QString name,
              passwd;
    } user;
  } socks, http;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCClientState
{
  KBSBOINCHostInfo host_info;
  KBSBOINCTimeStats time_stats;
  KBSBOINCNetStats net_stats;
  QMap<QString,KBSBOINCProject> project;
  QMap<QString,KBSBOINCApp> app;
  QMap<QString,KBSBOINCFileInfo> file_info;
  QMap<QString,KBSBOINCAppVersionList> app_version;
  QMap<QString,KBSBOINCWorkunit> workunit;
  QMap<QString,KBSBOINCResult> result;
  KBSBOINCActiveTaskSet active_task_set;
  QString platform_name;
  struct {
    unsigned major_version,
             minor_version,
             release;
  } core_client;
  struct {
    unsigned period;
    double work_done_this_period;
  } cpu_sched;
  KBSBOINCProxyInfo proxy_info;
  QString host_venue;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCProjectPreferences
{
  unsigned resource_share;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCGuiUrl
{
  QString name,
          description;
  KURL url;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCGuiUrls
{
  QValueList<KBSBOINCGuiUrl> gui_url;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCAccount
{
  KURL master_url;
  QString authenticator,
          project_name;
  KBSBOINCProjectPreferences project_preferences;
  KBSBOINCGuiUrls gui_urls;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCDailyStatistics
{
  QDate day;
  struct {
    double total_credit,
           expavg_credit;
  } user, host;
  
  bool parse(const QDomElement &node);
};

bool operator<(const KBSBOINCDailyStatistics &stat1, const KBSBOINCDailyStatistics &stat2);

struct KBSBOINCProjectStatistics
{
  KURL master_url;
  QValueList<KBSBOINCDailyStatistics> daily_statistics;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCMsg
{
  unsigned pri,
           seqno;
  QString body;
  QDateTime time;
  QString project;
  
  bool parse(const QDomElement &node);
};

bool operator<(const KBSBOINCMsg &msg1, const KBSBOINCMsg &msg2);

struct KBSBOINCMsgs
{
  QValueList<KBSBOINCMsg> msg;
  
  bool parse(const QDomElement &node);
};
  
enum KBSBOINCRunMode {RunAuto, RunAlways, RunNever};

enum KBSBOINCNetworkMode {ConnectAlways, ConnectNever};

struct KBSBOINCPersistentFileXfer
{
  unsigned num_retries;
  QDateTime first_request_time,
            next_request_time;
  double time_so_far;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCFileXfer
{
  double bytes_xferred,
         file_offset,
         xfer_speed;
  QString hostname;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCFileTransfer
{
  KURL project_url;
  QString project_name,
          name;
  double nbytes,
         max_nbytes;
  KBSBOINCPersistentFileXfer persistent_file_xfer;
  KBSBOINCFileXfer file_xfer;
  int status;
  bool generated_locally,
       uploaded,
       upload_when_present,
       sticky,
       marked_for_delete;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCFileTransfers
{
  QMap<QString,KBSBOINCFileTransfer> file_transfer;
  
  bool parse(const QDomElement &node);
};

struct KBSBOINCProjectConfig
{
  QString name;
  bool account_manager,
       uses_username,
       account_creation_disabled,
       client_account_creation_disabled;
  unsigned min_passwd_length;
  
  bool parse(const QDomElement &node);
};

#endif
