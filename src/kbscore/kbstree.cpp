/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qimage.h>
#include <qpainter.h>
#include <qregexp.h>

#include <kiconloader.h>

#include "kbstree.h"

QPixmap CompositePixmap(const QStringList &icons, int size)
{
  if(icons.isEmpty()) return QPixmap();
  
  QStringList::const_iterator icon = icons.begin();
  QPixmap out = SmallIcon(*icon, size);
  ++icon;
  
  if(icons.end() == icon) return out;
  
  QPainter painter(&out);
  while(icon != icons.end()) {
    painter.drawPixmap(0, 0, SmallIcon(*icon, size));
    ++icon;
  }
  painter.end();
  
  return out;
}

QIconSet CompositeIconSet(const QStringList &icons)
{
  return QIconSet(CompositePixmap(icons, KIcon::SizeSmall),
                  CompositePixmap(icons, KIcon::SizeMedium));
}

KBSTreeNodeList::KBSTreeNodeList() : QPtrList<KBSTreeNode>()
{
}

int KBSTreeNodeList::compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2)
{
  KBSTreeNode *node1 = static_cast<KBSTreeNode *>(item1),
              *node2 = static_cast<KBSTreeNode *>(item2);
  
  if(node1->type() != node2->type())
    return (node1->type() - node2->type());
  else             
    return QString::compare(node1->name(), node2->name());
}

KBSTreeNode::KBSTreeNode(QObject *parent, const char *name)
           : QObject(parent, name)
{
}

KBSTreeNode::~KBSTreeNode()
{
  removeAllChildren();
}

unsigned KBSTreeNode::children() const
{
  return m_children.count();
}

KBSTreeNode *KBSTreeNode::child(unsigned pos) const
{
  if(pos >= m_children.count()) {
    qDebug("index out of count");
    return NULL;
  }
  
  QPtrListIterator<KBSTreeNode> it(m_children);
  if(pos > 0) it += pos;
  
  return *it;
}

int KBSTreeNode::childIndex(const KBSTreeNode *child) const
{
  QPtrListIterator<KBSTreeNode> it(m_children);
  unsigned pos = 0;  
  
  while(*it) {
    if(*it == child) return pos;
    ++it; ++pos;
  }
  
  return -1;
}

int KBSTreeNode::childIndex(const QString &name) const
{
  QPtrListIterator<KBSTreeNode> it(m_children);
  unsigned pos = 0;
    
  while(*it) {
    if((*it)->name() == name) return pos;
    ++it; ++pos;
  }
  
  return -1;
}

bool KBSTreeNode::isRoot() const
{
  return (parent() != NULL) ? !(parent()->inherits("KBSTreeNode")) : true;
}

KBSTreeNode *KBSTreeNode::root()
{
  return isRoot() ? this : static_cast<KBSTreeNode*>(parent())->root();
}

bool KBSTreeNode::isAncestor(const KBSTreeNode *node) const
{
  if(node->isRoot()) return false;
  
  const KBSTreeNode *ancestor = node;
  do {
    ancestor = static_cast<const KBSTreeNode *>(ancestor->parent());
    if(ancestor == this) return true;
  } while(!ancestor->isRoot());
  
  return false;
}

KBSTreeNode *KBSTreeNode::findAncestor(const QString &className)
{
  KBSTreeNode *node = this;
  while(true)
    if(node->inherits(className))
      return node;
    else if(node->isRoot())
      return NULL;
    else
      node = static_cast<KBSTreeNode*>(node->parent());
}
  
KBSTreePath KBSTreeNode::path() const
{
  if(NULL != parent() && parent()->inherits("KBSTreeNode")) {
    const KBSTreeNode *parent = static_cast<KBSTreeNode *>(this->parent());
    
    return (parent->path() << parent->childIndex(this));
  } else
    return KBSTreePath();
}

KBSTreeNode *KBSTreeNode::descendant(const KBSTreePath &path)
{
  KBSTreeNode *node = this;
  for(KBSTreePath::const_iterator it = path.constBegin();
      it != path.constEnd(); ++it)
    if(node->children() > *it)
      node = node->child(*it);
    else
      return NULL;
  
  return node;
}

unsigned KBSTreeNode::type() const
{
  return 0;
}

QString KBSTreeNode::name() const
{
  return QString::null;
}

QStringList KBSTreeNode::icons() const
{
  return QStringList();
}

void KBSTreeNode::insertChild(KBSTreeNode *node)
{
  if(node->parent() != this) QObject::insertChild(node);
  m_children.inSort(node);
  emit childInserted(node);
}

void KBSTreeNode::removeChild(KBSTreeNode *node, bool free)
{
  if(!m_children.containsRef(node)) return;
  
  emit childRemoved(node);
  m_children.removeRef(node);
  QObject::removeChild(node);
  if(free) delete node;
}

void KBSTreeNode::removeChild(unsigned index, bool free)
{
  KBSTreeNode *node = m_children.at(index);
  if(NULL == node) return;
  
  emit childRemoved(node);
  m_children.remove(index);
  QObject::removeChild(node);
  if(free) delete node;
}

void KBSTreeNode::removeAllChildren(bool free)
{
  while(children() > 0)
    removeChild(child(0), free);
}

KBSNamedPath::KBSNamedPath() : QStringList()
{
}

KBSNamedPath::KBSNamedPath(const QString &string)
{
  unsigned start = 0;
  while(start < string.length())
  {
    int end = string.find(QRegExp("/[^/]"), start);
    if(end < 0) end = string.length();
    
    append(string.mid(start, end - start).replace("//", "/"));
    
    start = end + 1;
  }
}

KBSNamedPath::KBSNamedPath(const KBSTreeNode *root, const KBSTreePath &path)
{
  const KBSTreeNode *node = root;
  for(KBSTreePath::const_iterator it = path.begin(); it != path.end(); ++it)
  {
    node = node->child(*it);
    append(node->name());
  }
}

KBSNamedPath::~KBSNamedPath()
{
}

QString KBSNamedPath::toString() const
{
  QStringList names(*this);
  for(QStringList::iterator name = names.begin(); name != names.end(); ++name)
    (*name).replace("/", "//");
    
  return names.join("/");
}

KBSTreePath KBSNamedPath::toPath(const KBSTreeNode *root, bool *error) const
{
  KBSTreePath out;
  const KBSTreeNode *node = root;
  
  for(const_iterator it = constBegin(); it != constEnd(); ++it)
  {
    const int index = node->childIndex(*it);
    if(index < 0) goto error;
    
    node = node->child(index);
    out << index;
  }
  
  if(NULL != error) *error = false;
  return out;
  
error:    
  if(NULL != error) *error = true;
  return KBSTreePath();
}

KBSNamedPath KBSNamedPath::fromString(const QString &string)
{
  return KBSNamedPath(string);
}

KBSNamedPath KBSNamedPath::fromPath(const KBSTreeNode *root, const KBSTreePath &path)
{
  return KBSNamedPath(root, path);
}

#include "kbstree.moc"
