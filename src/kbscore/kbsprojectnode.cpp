/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdict.h>

#include <kglobal.h>
#include <kiconloader.h>
#include <klibloader.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbscachenode.h>
#include <kbshostnode.h>
#include <kbspanelnode.h>
 
#include "kbsprojectnode.h"

KBSProjectNode::KBSProjectNode(const QString& project, KBSTreeNode *parent, const char *name)
              : KBSTreeNode(parent, name),
                m_suspended(false), m_extinguished(false), m_project(project)
{
  setupMonitor();
  
  insertChild(new KBSCacheNode(project, this));
  
  addPlugins();
}

unsigned KBSProjectNode::type() const
{
  return 2;
}

QString KBSProjectNode::name() const
{
  return KBSProjectNode::name(m_project, m_monitor);
}

QStringList KBSProjectNode::icons() const
{
  QStringList out;
  
  out << "project_frame";
  
  if(m_suspended || m_extinguished)
    out << "project_left_disabled";
  else if(m_workunit[Input].isEmpty())
    out << "project_left_empty";
  else
    out << "project_left_normal";
  
  if(m_suspended)
    out << "project_right_disabled";
  else if(m_workunit[Output].isEmpty())
    out << "project_right_empty";
  else
    out << "project_right_normal";
  
  if(m_suspended)
    out << "project_top_disabled";
  else if(m_workunit[Task].isEmpty())
    out << "project_top_empty";
  else
    out << "project_top_normal";
  
  return out;
}

QString KBSProjectNode::project() const
{
  return m_project;
}

KURL KBSProjectNode::projectURL() const
{
  return m_projectURL;
}

bool KBSProjectNode::isSuspended() const
{
  return m_suspended;
}

bool KBSProjectNode::isExtinguished() const
{
  return m_extinguished;
}

QString KBSProjectNode::name(const QString &project, KBSBOINCMonitor *monitor)
{
  if(NULL == monitor) return project;
  
  const KBSBOINCClientState *state = monitor->state();
  if(NULL == state) return project;
  
  const QString project_name = state->project[project].project_name;
  return project_name.isEmpty() ? project : project_name;
}

KBSBOINCMonitor *KBSProjectNode::monitor()
{
  return m_monitor;
}

void KBSProjectNode::update()
{
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  const KBSBOINCProject &project = state->project[m_project];
  
  const bool suspended = project.suspended_via_gui,
             extinguished = project.dont_request_more_work;
  
  bool changed = false;
  
  if(m_suspended != suspended) {
    m_suspended = suspended;
    changed = true;
  }
  if(m_extinguished != extinguished) {
    m_extinguished = extinguished;
    changed = true;
  }
  
  if(changed) emit nodeChanged(this);
} 

void KBSProjectNode::addWorkunits(const QStringList &workunits)
{
  bool changed = false;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++workunit)
    if(insertWorkunit(*workunit))
      changed = true;
  
  if(changed) emit nodeChanged(this);
}

void KBSProjectNode::removeWorkunits(const QStringList &workunits)
{
  bool changed = false;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++workunit)
    if(deleteWorkunit(*workunit))
      changed = true;
  
  if(changed) emit nodeChanged(this);
}

void KBSProjectNode::activateWorkunit(unsigned, const QString &workunit, bool)
{
  if(deleteWorkunit(workunit)) {
    insertWorkunit(workunit);
    emit nodeChanged(this);
  }
}

void KBSProjectNode::setupMonitor()
{
  KBSHostNode *host = static_cast<KBSHostNode*>(findAncestor("KBSHostNode"));
  m_monitor = (NULL != host) ? host->monitor() : NULL;
  if(NULL == m_monitor) return;
  
  connect(m_monitor, SIGNAL(stateUpdated()), this, SLOT(update()));
  connect(m_monitor, SIGNAL(workunitsAdded(const QStringList &)),
          this, SLOT(addWorkunits(const QStringList &)));
  connect(m_monitor, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  connect(m_monitor, SIGNAL(workunitActivated(unsigned, const QString &, bool)),
          this, SLOT(activateWorkunit(unsigned, const QString &, bool)));
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  const KBSBOINCProject &project = state->project[m_project];
  
  m_projectURL = project.master_url;
  m_suspended = project.suspended_via_gui;
  m_extinguished = project.dont_request_more_work;
  
  const QStringList workunits = state->workunit.keys();
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++workunit)
    insertWorkunit(*workunit);
}

void KBSProjectNode::addPlugins()
{
  QString constraints;
  KTrader::OfferList offers;
  QDict<KBSPanelNode> plugins;
  
  // first load plugins specific to this project...
  constraints = "([X-KDE-Target] == 'Project') and ('%1' in [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints.arg(m_project));
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_project);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
  
  // ... and then generic ones valid for all projects
  constraints = "([X-KDE-Target] == 'Project') and (not exist [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints);
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_project);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
}

bool KBSProjectNode::insertWorkunit(const QString &name)
{
  for(unsigned set = Input; set < Sets; set++)
    if(m_workunit[set].contains(name)) return false;
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return false;

  if(!state->workunit.contains(name)) return false;
  
  const KBSBOINCWorkunit &workunit = state->workunit[name];
  if(m_monitor->project(workunit) != m_project) return false;
  
  unsigned set = Input;

  const QString result_name = workunit.result_name;
  if(!result_name.isEmpty())
  {
    const KBSBOINCResult &result = state->result[result_name];
    const int task_index = state->active_task_set.index(result_name);
    
    if(task_index >= 0) set = Task;
    else if(result.state >= 3) set = Output;
  }
  
  m_workunit[set].append(name);
  
  return true;
}

bool KBSProjectNode::deleteWorkunit(const QString &name)
{
  for(unsigned set = Input; set < Sets; ++set)
    if(m_workunit[set].remove(name)) return true;
  
  return false;
}

#include "kbsprojectnode.moc"
