/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <limits.h>

#include <kbsboincmonitor.h>
#include <kbshostnode.h>
#include <kbspanel.h>

#include "kbspanelnode.h"

KBSPanelNode::KBSPanelNode(KBSTreeNode *parent, const char *name)
            : KBSTreeNode(parent, name)
{
  KBSHostNode *host = static_cast<KBSHostNode*>(findAncestor("KBSHostNode"));
  m_monitor = (NULL != host) ? host->monitor() : NULL;  
}

unsigned KBSPanelNode::type() const
{
  return INT_MAX;
}

KBSPanel *KBSPanelNode::createPanel(QWidget *parent)
{
  KBSPanel *panel = new KBSPanel(this, parent);  
  panel->setHeader(this->name());
  panel->setIcons(this->icons());

  m_panels.append(panel);
  connect(panel, SIGNAL(destroyed(QObject *)), this, SLOT(slotDestroyed(QObject *)));
    
  return panel;
}

KBSBOINCMonitor *KBSPanelNode::monitor()
{
  return m_monitor;
}

void KBSPanelNode::slotDestroyed(QObject *panel)
{
  m_panels.removeRef(static_cast<KBSPanel*>(panel));
}

#include "kbspanelnode.moc"
