/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <kglobal.h>
#include <klocale.h>
#include <krfcdate.h>

#include "kbsboincdata.h"

QDateTime KBSBOINC::parseJulianDate(double d)
{
  const double epoch = 58574100.0;
  QDateTime out;
  
  out.setTime_t(unsigned((d * 24 - epoch) * 60 * 60), Qt::UTC);
  out = out.addSecs(KRFCDate::localUTCOffset() * 60);
  
  return out;
}

QDateTime KBSBOINC::parseJulianDate(const QString &text)
{
  return parseJulianDate(text.toDouble());
}

double KBSBOINC::formatJulianDate(const QDateTime &date)
{
  const double epoch = 58574102.0;
  double out;
  
  out =  double(date.toTime_t());
  out /= 60.0;
  out -= KRFCDate::localUTCOffset();
  out /= 60.0;
  out += epoch;
  out /= 24.0;
  
  return out;
}

QDateTime KBSBOINC::parseUNIXDate(double d)
{
  QDateTime out;
  
  out.setTime_t(unsigned(d), Qt::UTC);
  out = out.addSecs(KRFCDate::localUTCOffset() * 60);

  return out;
}

QDateTime KBSBOINC::parseUNIXDate(const QString &text)
{
  return parseUNIXDate(text.toDouble());
}

double KBSBOINC::formatUNIXDate(const QDateTime &date)
{
  return(double(date.toTime_t()) - KRFCDate::localUTCOffset() * 60);
}

QString KBSBOINC::formatBytes(double bytes)
{
  QString format = i18n("%1 Bytes");

  if(bytes >= 1024 && floor(bytes/1024) == bytes/1024)
  {
    bytes /= 1024;
    format = i18n("%1 KB");
  }
  if(bytes >= 1024 && floor(bytes/1024) == bytes/1024)
  {
    bytes /= 1024;
    format = i18n("%1 MB");
  }
  if(bytes >= 1024 && floor(bytes/1024) == bytes/1024)
  {
    bytes /= 1024;
    format = i18n("%1 GB");
  }
  if(bytes >= 1024 && floor(bytes/1024) == bytes/1024)
  {
    bytes /= 1024;
    format = i18n("%1 TB");
  }

  return format.arg(KGlobal::locale()->formatNumber(bytes, 0));
}

QString KBSBOINC::formatTime(double secs)
{
  const int h = int(secs / 3600.0);
  const int m = int((secs - h * 3600.0) / 60.0);
  const int s = int(secs - h * 3600.0 - m * 60.0);

  return(QString().sprintf("%d:%.2d:%.2d", h, m, s));
}

QString KBSBOINC::parseProjectName(const KURL &url)
{
  if(!url.isValid()) return QString::null;
  
  QString out = url.host();
  
  QString suffix = url.path(-1);
  suffix.replace('/', '_');
  
  if("_" != suffix)
    out = out + suffix;
    
  return out;
}

using namespace KBSBOINC;

bool KBSBOINCHostInfo::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "timezone")
        timezone = element.text().toUInt(0, 10);
      else if(elementName == "domain_name")
        domain_name = element.text();
      else if(elementName == "ip_addr")
        ip_addr = element.text();
      else if(elementName == "host_cpid")
        host_cpid = element.text();
      else if(elementName == "p_ncpus")
        p.ncpus = element.text().toUInt(0, 10);
      else if(elementName == "p_vendor")
        p.vendor = element.text();
      else if(elementName == "p_model")
        p.model = element.text();
      else if(elementName == "p_fpops")
        p.fpops = element.text().toDouble();
      else if(elementName == "p_iops")
        p.iops = element.text().toDouble();
      else if(elementName == "p_membw")
        p.membw = element.text().toDouble();
      else if(elementName == "p_fpop_err")
        p.fpop_err = element.text().toUInt(0, 10);
      else if(elementName == "p_iop_err")
        p.iop_err = element.text().toUInt(0, 10);
      else if(elementName == "p_membw_err")
        p.membw_err = element.text().toUInt(0, 10);
      else if(elementName == "p_calculated")
        p.calculated = element.text().toDouble();
      else if(elementName == "os_name")
        os.name = element.text();
      else if(elementName == "os_version")
        os.version = element.text();
      else if(elementName == "m_nbytes")
        m.nbytes = element.text().toDouble();
      else if(elementName == "m_cache")
        m.cache = element.text().toDouble();
      else if(elementName == "m_swap")
        m.swap = element.text().toDouble();
      else if(elementName == "d_total")
        d.total = element.text().toDouble();
      else if(elementName == "d_free")
        d.free = element.text().toDouble();
    }
  
  return true;
}

double KBSBOINCHostInfo::credit_per_cpu_sec() const
{
  return credit_per_cpu_sec(p.fpops, p.iops);
}

double KBSBOINCHostInfo::credit_per_cpu_sec(double fpops, double iops)
{
  const double cobblestone_factor = 100;
  
  return((fpops / 1e9 + iops / 1e9) * cobblestone_factor / (2 * 24 * 60 * 60));
}

bool KBSBOINCTimeStats::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "on_frac")
        on_frac = element.text().toDouble();
      else if(elementName == "connected_frac")
        connected_frac = element.text().toDouble();
      else if(elementName == "active_frac")
        active_frac = element.text().toDouble();
      else if(elementName == "cpu_efficiency")
        cpu_efficiency = element.text().toDouble();
      else if(elementName == "last_update")
        last_update = parseUNIXDate(element.text());
    }
  
  return true;
}

bool KBSBOINCNetStats::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "bwup")
        bwup = element.text().toDouble();
      else if(elementName == "bwdown")
        bwdown = element.text().toDouble();
    }
  
  return true;
}

bool KBSBOINCProject::parse(const QDomElement &node)
{
  short_term_debt = long_term_debt = 0.0;
  suspended_via_gui = dont_request_more_work = false;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "scheduler_url")
        scheduler_url = KURL(element.text());
      else if(elementName == "master_url")
        master_url = KURL(element.text());
      else if(elementName == "project_name")
        project_name = element.text();
      else if(elementName == "user_name")
        user_name = element.text();
      else if(elementName == "team_name")
        team_name = element.text();
      else if(elementName == "email_hash")
        email_hash = element.text();
      else if(elementName == "cross_project_id")
        cross_project_id = element.text();
      else if(elementName == "user_total_credit")
        user.total_credit = element.text().toDouble();
      else if(elementName == "user_expavg_credit")
        user.expavg_credit = element.text().toDouble();
      else if(elementName == "user_create_time")
        user.create_time = parseUNIXDate(element.text());
      else if(elementName == "rpc_seqno")
        rpc_seqno = element.text().toUInt(0, 10);
      else if(elementName == "hostid")
        hostid = element.text().toUInt(0, 10);
      else if(elementName == "host_total_credit")
        host.total_credit = element.text().toDouble();
      else if(elementName == "host_expavg_credit")
        host.expavg_credit = element.text().toDouble();
      else if(elementName == "host_create_time")
        host.create_time = parseUNIXDate(element.text());
      else if(elementName == "exp_avg_cpu")
        exp_avg.cpu = element.text().toDouble();
      else if(elementName == "exp_avg_mod_time")
        exp_avg.mod_time = element.text().toDouble();
      else if(elementName == "nrpc_failures")
        nrpc_failures = element.text().toUInt(0, 10);
      else if(elementName == "master_fetch_failures")
        master_fetch_failures = element.text().toUInt(0, 10);
      else if(elementName == "min_rpc_time")
        min_rpc_time = element.text().toUInt(0, 10);
      else if(elementName == "debt")
        long_term_debt = element.text().toDouble();
      else if(elementName == "short_term_debt")
        long_term_debt = element.text().toDouble();
      else if(elementName == "long_term_debt")
        long_term_debt = element.text().toDouble();
      else if(elementName == "suspended_via_gui")
        suspended_via_gui = true;
      else if(elementName == "dont_request_more_work")
        dont_request_more_work = true;
    }
  
  return true;
}

bool KBSBOINCApp::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
    }

  return true;
}

bool KBSBOINCFileInfo::parse(const QDomElement &node)
{
  url.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "nbytes")
        nbytes = element.text().toDouble();
      else if(elementName == "max_nbytes")
        max_nbytes = element.text().toDouble();
      else if(elementName == "status")
        status = element.text().toUInt(0, 10);
      else if(elementName == "url")
        url << KURL(element.text());
    }
  
  return true;
}

bool KBSBOINCFileRef::parse(const QDomElement &node)
{
  open_name = QString::null;
  main_program = false;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "file_name")
        file_name = element.text();
      else if(elementName == "open_name")
        open_name = element.text();
      else if(elementName == "main_program")
        main_program = true;
    }
  
  return true;
}

bool KBSBOINCAppVersion::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "app_name")
        app_name = element.text();
      else if(elementName == "version_num")
        version_num = element.text().toUInt(0, 10);
      else if(elementName == "file_ref") {
        KBSBOINCFileRef item;
        
        if(item.parse(element)) file_ref << item;
        else return false;
      }
    }
  
  return true;
}

bool KBSBOINCWorkunit::parse(const QDomElement &node)
{
  rsc.fpops_est = 0.0;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "app_name")
        app_name = element.text();
      else if(elementName == "version_num")
        version_num = element.text().toUInt(0, 10);
      else if(elementName == "command_line")
        command_line = element.text();
      else if(elementName == "env_vars")
        env_vars = element.text();
      else if(elementName == "rsc_fpops_est")
        rsc.fpops_est = element.text().toDouble();
      else if(elementName == "rsc_fpops_bound")
        rsc.fpops_bound = element.text().toDouble();
      else if(elementName == "rsc_memory_bound")
        rsc.memory_bound = element.text().toDouble();
      else if(elementName == "rsc_disk_bound")
        rsc.disk_bound = element.text().toDouble();
      else if(elementName == "file_ref") {
        KBSBOINCFileRef item;
        
        if(item.parse(element)) file_ref << item;
        else return false;
      }
    }
  
  return true;
}

bool KBSBOINCResult::parse(const QDomElement &node)
{
  ready_to_report = suspended_via_gui = aborted_via_gui = false;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "final_cpu_time")
        final_cpu_time = element.text().toDouble();
      else if(elementName == "exit_status")
        exit_status = element.text().toUInt(0, 10);
      else if(elementName == "state")
        state = element.text().toUInt(0, 10);
      else if(elementName == "ready_to_report")
        ready_to_report = true;
      else if(elementName == "wu_name")
        wu_name = element.text();
      else if(elementName == "report_deadline")
        report_deadline = parseUNIXDate(element.text());
      else if(elementName == "file_ref") {
        KBSBOINCFileRef item;
        
        if(item.parse(element)) file_ref << item;
        else return false;
      } else if (elementName == "suspended_via_gui")
        suspended_via_gui = true;
      else if(elementName == "aborted_via_gui")
        aborted_via_gui = true;
    }
    
  return true;
}

bool KBSBOINCActiveTask::parse(const QDomElement &node)
{
  scheduler_state = 2;
  vm_bytes = rss_bytes = 0.0;
  supports_graphics = false;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "project_master_url")
        project_master_url = KURL(element.text());
      else if(elementName == "result_name")
        result_name = element.text();
      else if(elementName == "app_version_num")
        app_version_num = element.text().toUInt(0, 10);
      else if(elementName == "slot")
        slot = element.text().toUInt(0, 10);
      else if(elementName == "scheduler_state")
        scheduler_state = element.text().toUInt(0, 10);
      else if(elementName == "checkpoint_cpu_time")
        checkpoint_cpu_time = element.text().toDouble();
      else if(elementName == "fraction_done")
        fraction_done = element.text().toDouble();
      else if(elementName == "current_cpu_time")
        current_cpu_time = element.text().toDouble();
      else if(elementName == "vm_bytes")
        vm_bytes = element.text().toDouble();
      else if(elementName == "rss_bytes")
        rss_bytes = element.text().toDouble();
      else if(elementName == "supports_graphics")
        supports_graphics = true;
    }
  
  return true;
}

bool KBSBOINCActiveTaskSet::parse(const QDomElement &node)
{
  active_task.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "active_task") {
        KBSBOINCActiveTask item;
        
        if(item.parse(element)) active_task[item.slot] = item;
        else return false;
      }
    }
  
  return true;
}

int KBSBOINCActiveTaskSet::index(const QString &result) const
{
  if(result.isEmpty()) return -1;
  
  for(QMap<unsigned,KBSBOINCActiveTask>::const_iterator task = active_task.begin();
      task != active_task.end(); ++task)
    if(result == task.data().result_name)
      return task.key();
  
  return -1;
}

bool KBSBOINCProxyInfo::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "socks_version")
        socks_version = element.text().toUInt(0, 10);
      else if(elementName == "socks_server_name")
        socks.server.name = element.text();
      else if(elementName == "socks_server_port")
        socks.server.port = element.text().toUInt(0, 10);
      else if(elementName == "http_server_name")
        http.server.name = element.text();
      else if(elementName == "http_server_port")
        http.server.port = element.text().toUInt(0, 10);
      else if(elementName == "socks5_user_name")
        socks.user.name = element.text();
      else if(elementName == "socks5_user_passwd")
        socks.user.passwd = element.text();
      else if(elementName == "http_user_name")
        http.user.name = element.text();
      else if(elementName == "http_user_passwd")
        http.user.passwd = element.text();
    }
  
  return true;
}

bool KBSBOINCClientState::parse(const QDomElement &node)
{
  project.clear();
  app.clear();
  file_info.clear();
  app_version.clear();
  workunit.clear();
  result.clear();
  
  core_client.release = 0;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "host_info") {
        if(!host_info.parse(element)) return false;
      } else if(elementName == "time_stats") {
        if(!time_stats.parse(element)) return false;
      } else if(elementName == "net_stats") {
        if(!net_stats.parse(element)) return false;
      } else if(elementName == "project") {
        KBSBOINCProject item;
        
        if(!item.parse(element)) return false;
        
        const QString name = parseProjectName(item.master_url);
        if(name.isEmpty()) return false;
        
        if(!item.project_name.isEmpty())
          project[name] = item;
      } else if(elementName == "app") {
        KBSBOINCApp item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.name;
        if(name.isEmpty()) return false;
        
        app[name] = item;
      } else if(elementName == "file_info") {
        KBSBOINCFileInfo item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.name;
        if(name.isEmpty()) return false;
        
        file_info[name] = item;
      } else if(elementName == "app_version") {
        KBSBOINCAppVersion item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.app_name;
        if(name.isEmpty()) return false;
        
        app_version[name] << item;
      } else if(elementName == "workunit") {
        KBSBOINCWorkunit item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.name;
        if(name.isEmpty()) return false;
        
        workunit[name] = item;
      } else if(elementName == "result") {
        KBSBOINCResult item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.name;
        if(name.isEmpty()) return false;
        
        result[name] = item;
      } else if(elementName == "active_task_set") {
        if(!active_task_set.parse(element)) return false;
      } else if(elementName == "platform_name")
        platform_name = element.text();
      else if(elementName == "core_client_major_version")
        core_client.major_version = element.text().toUInt(0, 10);
      else if(elementName == "core_client_minor_version")
        core_client.minor_version = element.text().toUInt(0, 10);
      else if(elementName == "core_client_release")
        core_client.release = element.text().toUInt(0, 10);
      else if(elementName == "cpu_sched_period")
        cpu_sched.period = element.text().toUInt(0, 10);
      else if(elementName == "cpu_sched_work_done_this_period")
        cpu_sched.work_done_this_period = element.text().toDouble();
      else if(elementName == "proxy_info") {
        if(!proxy_info.parse(element)) return false;
      } else if(elementName == "host_venue")
        host_venue = element.text();
    }
  
  return true;
}

bool KBSBOINCProjectPreferences::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "resource_share")
        resource_share = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSBOINCGuiUrl::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "description")
        description = element.text();
      else if(elementName == "url")
        url = KURL(element.text());
    }
  
  return true;
}

bool KBSBOINCGuiUrls::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "gui_url") {
        KBSBOINCGuiUrl item;
        
        if(!item.parse(element)) return false;
        
        gui_url << item;
      }
    }
  
  return true;
}

bool KBSBOINCAccount::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "master_url")
        master_url = KURL(element.text());
      else if(elementName == "authenticator")
        authenticator = element.text();
      else if(elementName == "project_name")
        project_name = element.text();
      else if(elementName == "project_preferences") {
        if(!project_preferences.parse(element)) return false;
      }
      else if(elementName == "gui_urls") {
        if(!gui_urls.parse(element)) return false;
      }
    }
  
  return true;
}

bool KBSBOINCDailyStatistics::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "day")
        day = parseUNIXDate(element.text()).date();
      else if(elementName == "user_total_credit")
        user.total_credit = element.text().toDouble();
      else if(elementName == "user_expavg_credit")
        user.expavg_credit = element.text().toDouble();
      else if(elementName == "host_total_credit")
        host.total_credit = element.text().toDouble();
      else if(elementName == "host_expavg_credit")
        host.expavg_credit = element.text().toDouble();
    }
  
  return true;
}

bool operator<(const KBSBOINCDailyStatistics &stat1, const KBSBOINCDailyStatistics &stat2)
{
  return stat1.day < stat2.day;
}

bool KBSBOINCProjectStatistics::parse(const QDomElement &node)
{
  daily_statistics.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "master_url")
        master_url = KURL(element.text());
      else if(elementName == "daily_statistics") {
        KBSBOINCDailyStatistics item;
        
        if(!item.parse(element)) return false;
        daily_statistics << item;
      }
    }
  
  qHeapSort(daily_statistics);
  
  return true;
}

bool KBSBOINCMsg::parse(const QDomElement &node)
{
  project = QString::null;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "pri")
        pri = element.text().toUInt(0, 10);
      else if(elementName == "seqno")
        seqno = element.text().toUInt(0, 10);
      else if(elementName == "body")
        body = element.text().stripWhiteSpace();
      else if(elementName == "time") 
        time = parseUNIXDate(element.text().toDouble());
      else if(elementName == "project")
        project = element.text().stripWhiteSpace();
    }
  
  return true;
}

bool operator<(const KBSBOINCMsg &msg1, const KBSBOINCMsg &msg2)
{
  return(msg1.seqno < msg2.seqno);
}

bool KBSBOINCMsgs::parse(const QDomElement &node)
{
  msg.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower();
      
      if(elementName == "msg") {
        KBSBOINCMsg item;
        
        if(item.parse(element)) msg << item;
        else return false;
      }
    }
  
  qHeapSort(msg);
  
  return true;
}

bool KBSBOINCPersistentFileXfer::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "num_retries")
        num_retries = element.text().toUInt(0, 10);
      else if(elementName == "first_request_time")
        first_request_time = parseUNIXDate(element.text());
      else if(elementName == "next_request_time")
        next_request_time = parseUNIXDate(element.text());
      else if(elementName == "time_so_far")
        time_so_far = element.text().toDouble();
    }
  
  return true;
}

bool KBSBOINCFileXfer::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "bytes_xferred")
        bytes_xferred = element.text().toDouble();
      else if(elementName == "file_offset")
        file_offset = element.text().toDouble();
      else if(elementName == "xfer_speed")
        xfer_speed = element.text().toDouble();
      else if(elementName == "hostname")
        hostname = element.text().stripWhiteSpace();
    }
  
  return true;
}

bool KBSBOINCFileTransfer::parse(const QDomElement &node)
{
  status = 0;
  project_name = file_xfer.hostname = QString::null;
  file_xfer.bytes_xferred = file_xfer.file_offset = file_xfer.xfer_speed = 0.0;
  generated_locally = uploaded = upload_when_present = sticky = marked_for_delete = false;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "project_url")
        project_url = KURL(element.text());
      else if(elementName == "project_name")
        project_name = element.text();
      else if(elementName == "name")
        name = element.text();
      else if(elementName == "nbytes") 
        nbytes = element.text().toDouble();
      else if(elementName == "max_nbytes") 
        max_nbytes = element.text().toDouble();
      else if(elementName == "persistent_file_xfer") {
        if(!persistent_file_xfer.parse(element)) return false;
      } else if(elementName == "file_xfer") {
        if(!file_xfer.parse(element)) return false;
      } else if(elementName == "status")
        status = element.text().toInt(0, 10);
      else if(elementName == "generated_locally")
        generated_locally = true;
      else if(elementName == "uploaded")
        uploaded = true;
      else if(elementName == "upload_when_present")
        upload_when_present = true;
      else if(elementName == "sticky")
        sticky = true;
      else if(elementName == "marked_for_delete")
        marked_for_delete = true;
    }
  
  return true;
}
  
bool KBSBOINCFileTransfers::parse(const QDomElement &node)
{
  file_transfer.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower();
      
      if(elementName == "file_transfer") {
        KBSBOINCFileTransfer item;
        
        if(!item.parse(element)) return false;
        
        const QString name = item.name;
        if(name.isEmpty()) return false;
        
        file_transfer[name] = item;
      }
    }
  
  return true;
}

bool KBSBOINCProjectConfig::parse(const QDomElement &node)
{
  account_manager =
  uses_username =
  account_creation_disabled =
  client_account_creation_disabled = false;
  min_passwd_length = 0;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower();
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "account_manager")
        account_manager = true;
      else if(elementName == "uses_username")
        uses_username = true;
      else if(elementName == "account_creation_disabled")
        account_creation_disabled = true;
      else if(elementName == "client_account_creation_disabled")
        client_account_creation_disabled = true;
      else if(elementName == "min_passwd_length")
        min_passwd_length = element.text().toUInt(0, 10);
    }
  
  return true;
}
