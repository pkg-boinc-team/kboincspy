/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qclipboard.h>
#include <qlayout.h>
#include <qobjectlist.h>

#include <kapplication.h>
#include <kiconloader.h>
#include <klocale.h>

#include <kbstree.h>
#include <kbspanelfield.h>

#include "kbspanel.h"

KBSPanel::KBSPanel(KBSPanelNode *node, QWidget *parent, const char *name)
        : QWidget(parent, name), m_node(node), m_content(NULL), m_context(NULL)
{
  m_layout = new QGridLayout(this, 2, 2);
  m_layout->setRowStretch(1, 1);
  m_layout->setColStretch(1, 1);
  m_layout->setMargin(8);
  m_layout->setSpacing(8);

  m_icon = new QLabel(this, "icon");
  m_layout->addWidget(m_icon, 0, 0);

  QFont headerFont = this->font();
  headerFont.setPointSize(20);
  headerFont.setBold(true);

  m_header = new QLabel(this, "header");
  m_header->setFont(headerFont);
  m_layout->addWidget(m_header, 0, 1);
}

KBSPanelNode *KBSPanel::node() const
{
  return m_node;
}

QStringList KBSPanel::icons() const
{
  return m_icons;
}

void KBSPanel::setIcons(const QStringList &icons)
{
  m_icons = icons;
  
  m_icon->setPixmap(CompositePixmap(icons, KIcon::SizeLarge));
}

QString KBSPanel::header() const
{
  return m_header->text();
}

void KBSPanel::setHeader(const QString &header)
{
  m_header->setText(header);
}

QWidget *KBSPanel::content() const
{
  return m_content;
}

void KBSPanel::setContent(QWidget *widget)
{
  if(NULL != m_content) delete m_content;
  m_content = widget;
  
  if(widget->parent() != this)
    widget->reparent(this, 0, QPoint(), true);
  widget->installEventFilter(this);
  m_layout->addMultiCellWidget(widget, 1, 1, 0, 1);
}

QStringList KBSPanel::text() const
{
  QStringList out;
  
  if(NULL == m_content) return out;
  QObjectList *list = m_content->queryList("KBSPanelField");
  for(QObjectListIt it(*list); NULL != it.current(); ++it)
  {
    KBSPanelField *field = static_cast<KBSPanelField*>(*it);
    if(!field->isVisibleTo(m_content)) continue;
     
    const QString name = field->name();
    if(name.isEmpty()) continue;
    
    const QString text = field->text();
    const QString urlText = field->urlText();
    const QString aux = field->aux();
    
    if(text.isEmpty())
      if(aux.isEmpty())
        if(urlText.isEmpty())
          out << name;
        else
          out << i18n("%1 %2").arg(name).arg(urlText);
      else
        out << i18n("%1 %2 %3").arg(name).arg(urlText).arg(aux);
    else
      out << i18n("%1 %2").arg(name).arg(text);
  }
  delete list;
  
  return out;
}

QPopupMenu *KBSPanel::contextMenu() const
{
  return m_context;
}

void KBSPanel::setContextMenu(QPopupMenu *context)
{
  m_context = context;
}

bool KBSPanel::eventFilter(QObject *obj, QEvent *e)
{
  if(e->type() == QEvent::MouseButtonPress)
  {
    QWidget *target = (QWidget *) obj;
    QMouseEvent *click = (QMouseEvent *) e;

    if(click->button() == RightButton && NULL != m_context) {
      m_context->popup(target->mapToGlobal(click->pos()));
      return(true);
    }
  }

  return(false);
}

void KBSPanel::editCopy()
{
  QStringList fields = text();
  if(fields.isEmpty()) return;
  
  KApplication::clipboard()->setText(fields.join("\n").append("\n"));
}

#include "kbspanel.moc"
