/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <unistd.h>

#include <qregexp.h>
#include <qtimer.h>

#include <klocale.h>
#include <kmdcodec.h>

#include <kbsboincmonitor.h>

#include <kbsboincdata.h>

#include "kbsrpcmonitor.h"

KBSRPCMonitor::KBSRPCMonitor(const QString &host, KBSBOINCMonitor *parent, const char *name)
             : QObject(parent, name),
               m_runMode(RunAuto), m_networkMode(ConnectAlways), m_seqno(-1),
               m_host(host),
               m_socket(new QSocket(this)), m_status(Disconnected),
               m_interval(0), m_port(0),
               m_password("")
{
  connect(m_socket, SIGNAL(connected()), this, SLOT(slotConnected()));
  connect(m_socket, SIGNAL(connectionClosed()), this, SLOT(slotConnectionClosed()));
  connect(m_socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
  connect(m_socket, SIGNAL(error(int)), this, SLOT(slotError(int)));
  
  getRunMode();
  getNetworkMode();
  getMessages();
  getFileTransfers();
}

bool KBSRPCMonitor::canRPC() const
{
  return(!m_msgs.msg.isEmpty());
}

int KBSRPCMonitor::interval() const
{
  return m_interval;
}

void KBSRPCMonitor::setInterval(int interval)
{
  if(interval > 0 && interval < 100) interval = 100;
  
  if(interval == m_interval) return;
  
  if(interval > 0)
  {
    m_interval = interval;
    m_timer = startTimer(interval);
    sendQueued();
  }
  else if(interval < 0)
  {
    m_interval = 0;
    killTimer(m_timer);
  }

  emit intervalChanged(interval);
}

QString KBSRPCMonitor::host() const
{
  return m_host;
}

void KBSRPCMonitor::setHost(const QString &host)
{
  if(host == m_host) return;
  
  m_host = host;
  
  if(m_status > Disconnected) {
    m_socket->close();
    m_status = Disconnected;
  }
}

unsigned KBSRPCMonitor::port() const
{
  return m_port;
}

void KBSRPCMonitor::setPort(unsigned port)
{
  if(port == m_port) return;
  
  m_port = port;
  
  if(m_status > Disconnected) {
    m_socket->close();
    m_status = Disconnected;
  }
}

QString KBSRPCMonitor::password() const
{
  return m_password;
}

void KBSRPCMonitor::setPassword(const QString &password)
{
  if(password == m_password) return;
  
  m_password = password;
  
  if(m_status > Disconnected) {
    m_socket->close();
    m_status = Disconnected;
  }
}

void KBSRPCMonitor::quit()
{
  if(m_status < Idle || Authenticating == m_status) return;
  
  QDomDocument command;
  
  QDomElement quit = command.createElement("quit");
  command.appendChild(quit);
  
  sendImmediate(command);
  
  usleep(200);
  
  resetConnection();
}

KBSBOINCRunMode KBSRPCMonitor::runMode() const
{
  return m_runMode;
}

void KBSRPCMonitor::setRunMode(KBSBOINCRunMode mode)
{
  QDomDocument command;
  
  QDomElement set_run_mode = command.createElement("set_run_mode");
  command.appendChild(set_run_mode);
  
  QString tag;
  switch(mode) {
    case RunAlways:
      tag = "always";
      break;
    case RunNever:
      tag = "never";
      break;    
    default:
      tag = "auto";
      break;
  }
  set_run_mode.appendChild(command.createElement(tag));
  
  sendCommand(command);
}

KBSBOINCNetworkMode KBSRPCMonitor::networkMode() const
{
  return m_networkMode;
}

void KBSRPCMonitor::setNetworkMode(KBSBOINCNetworkMode mode)
{
  QDomDocument command;
  
  QDomElement set_network_mode = command.createElement("set_network_mode");
  command.appendChild(set_network_mode);
  
  QString tag;
  switch(mode) {
    case ConnectNever:
      tag = "never";
      break;    
    default:
      tag = "always";
      break;
  }
  set_network_mode.appendChild(command.createElement(tag));
  
  sendCommand(command);
}

const KBSBOINCMsgs *KBSRPCMonitor::messages() const
{
  return &m_msgs;
}

const KBSBOINCFileTransfers *KBSRPCMonitor::fileTransfers() const
{
  return &m_fileTransfers;
}

void KBSRPCMonitor::runBenchmarks()
{
  QDomDocument command;
  
  QDomElement run_benchmarks = command.createElement("run_benchmarks");
  command.appendChild(run_benchmarks);
  
  sendCommand(command);
}

void KBSRPCMonitor::setProxyInfo(const KBSBOINCProxyInfo &info)
{
  QDomDocument command;
  
  QDomElement set_proxy_info = command.createElement("set_proxy_info");
  command.appendChild(set_proxy_info);
  
  QDomElement socks_proxy_server_name = command.createElement("socks_proxy_server_name");
  set_proxy_info.appendChild(socks_proxy_server_name);
  socks_proxy_server_name.appendChild(command.createTextNode(info.socks.server.name));
  
  QDomElement socks_proxy_server_port = command.createElement("socks_proxy_server_port");
  set_proxy_info.appendChild(socks_proxy_server_port);
  socks_proxy_server_port.appendChild(command.createTextNode(QString::number(info.socks.server.port)));
  
  QDomElement http_proxy_server_name = command.createElement("http_proxy_server_name");
  set_proxy_info.appendChild(http_proxy_server_name);
  http_proxy_server_name.appendChild(command.createTextNode(info.http.server.name));
  
  QDomElement http_proxy_server_port = command.createElement("http_proxy_server_port");
  set_proxy_info.appendChild(http_proxy_server_port);
  http_proxy_server_port.appendChild(command.createTextNode(QString::number(info.http.server.port)));
  
  QDomElement socks_proxy_user_name = command.createElement("socks_proxy_user_name");
  set_proxy_info.appendChild(socks_proxy_user_name);
  socks_proxy_user_name.appendChild(command.createTextNode(info.socks.user.name));
  
  QDomElement socks_proxy_user_passwd = command.createElement("socks_proxy_user_passwd");
  set_proxy_info.appendChild(socks_proxy_user_passwd);
  socks_proxy_user_passwd.appendChild(command.createTextNode(info.socks.user.passwd));
  
  QDomElement http_proxy_user_name = command.createElement("http_proxy_user_name");
  set_proxy_info.appendChild(http_proxy_user_name);
  http_proxy_user_name.appendChild(command.createTextNode(info.http.user.name));
  
  QDomElement http_proxy_user_passwd = command.createElement("http_proxy_user_passwd");
  set_proxy_info.appendChild(http_proxy_user_passwd);
  http_proxy_user_passwd.appendChild(command.createTextNode(info.http.user.passwd));
  
  sendCommand(command);
}

void KBSRPCMonitor::lookupWebsite(const QString &name)
{
  QDomDocument blocking, polling;
  
  QDomElement lookup_website = blocking.createElement("lookup_website");
  blocking.appendChild(lookup_website);
  
  QDomElement site = blocking.createElement(name);
  lookup_website.appendChild(site);
  
  sendCommand(blocking, true);
  
  QDomElement lookup_website_poll = polling.createElement("lookup_website_poll");
  polling.appendChild(lookup_website_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::getProjectConfig(const KURL &url)
{
  QDomDocument blocking, polling;
  
  QDomElement get_project_config = blocking.createElement("get_project_config");
  blocking.appendChild(get_project_config);
  
  QDomElement project_url = blocking.createElement("url");
  get_project_config.appendChild(project_url);
  project_url.appendChild(blocking.createTextNode(url.prettyURL(+1)));
  
  sendCommand(blocking, true);
  
  QDomElement get_project_config_poll = polling.createElement("get_project_config_poll");
  polling.appendChild(get_project_config_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::createAccount(const KURL &project_url, const QString &email,
                                  const QString &username, const QString &password)
{
  QDomDocument blocking, polling;
  
  QDomElement create_account = blocking.createElement("create_account");
  blocking.appendChild(create_account);
  
  QDomElement url = blocking.createElement("url");
  create_account.appendChild(url);
  url.appendChild(blocking.createTextNode(project_url.prettyURL(+1)));
  
  QDomElement email_addr = blocking.createElement("email_addr");
  create_account.appendChild(email_addr);
  email_addr.appendChild(blocking.createTextNode(email));
  
  const QString digest = KMD5(password + email).hexDigest();
  
  QDomElement passwd_hash = blocking.createElement("passwd_hash");
  create_account.appendChild(passwd_hash);
  passwd_hash.appendChild(blocking.createTextNode(digest));
  
  QDomElement user_name = blocking.createElement("user_name");
  create_account.appendChild(user_name);
  user_name.appendChild(blocking.createTextNode(username));
  
  sendCommand(blocking, true);
  
  QDomElement create_account_poll = polling.createElement("create_account_poll");
  polling.appendChild(create_account_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::lookupAccount(const KURL &project_url, const QString &email,
                                  const QString &username, const QString &password)
{
  QDomDocument blocking, polling;
  
  QDomElement lookup_account = blocking.createElement("lookup_account");
  blocking.appendChild(lookup_account);
  
  QDomElement url = blocking.createElement("url");
  lookup_account.appendChild(url);
  url.appendChild(blocking.createTextNode(project_url.prettyURL(+1)));
  
  QDomElement email_addr = blocking.createElement("email_addr");
  lookup_account.appendChild(email_addr);
  email_addr.appendChild(blocking.createTextNode(email));
  
  const QString digest = KMD5(password + email).hexDigest();
  
  QDomElement passwd_hash = blocking.createElement("passwd_hash");
  lookup_account.appendChild(passwd_hash);
  passwd_hash.appendChild(blocking.createTextNode(digest));
  
  QDomElement user_name = blocking.createElement("user_name");
  lookup_account.appendChild(user_name);
  user_name.appendChild(blocking.createTextNode(username));
  
  sendCommand(blocking, true);
  
  QDomElement lookup_account_poll = polling.createElement("lookup_account_poll");
  polling.appendChild(lookup_account_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::accountManagerRPC(const KURL &acct_mgr_url,
                                      const QString &login, const QString &passwd)
{
  QDomDocument blocking, polling;
  
  QDomElement acct_mgr_rpc = blocking.createElement("acct_mgr_rpc");
  blocking.appendChild(acct_mgr_rpc);
  
  QDomElement url = blocking.createElement("url");
  acct_mgr_rpc.appendChild(url);
  url.appendChild(blocking.createTextNode(acct_mgr_url.prettyURL(+1)));
  
  QDomElement name = blocking.createElement("name");
  acct_mgr_rpc.appendChild(name);
  name.appendChild(blocking.createTextNode(login));
  
  QDomElement password = blocking.createElement("password");
  acct_mgr_rpc.appendChild(password);
  password.appendChild(blocking.createTextNode(passwd));
  
  sendCommand(blocking, true);
  
  QDomElement acct_mgr_rpc_poll = polling.createElement("acct_mgr_rpc_poll");
  polling.appendChild(acct_mgr_rpc_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::attachProject(const KURL &url, const QString &account_key)
{
  QDomDocument blocking, polling;
  
  QDomElement project_attach = blocking.createElement("project_attach");
  blocking.appendChild(project_attach);
  
  QDomElement project_url = blocking.createElement("project_url");
  project_attach.appendChild(project_url);
  project_url.appendChild(blocking.createTextNode(url.prettyURL(+1)));
  
  QDomElement authenticator = blocking.createElement("authenticator");
  project_attach.appendChild(authenticator);
  authenticator.appendChild(blocking.createTextNode(account_key));
  
  sendCommand(blocking, true);
  
  QDomElement project_attach_poll = polling.createElement("project_attach_poll");
  polling.appendChild(project_attach_poll);
  
  sendCommand(polling, false);
}

void KBSRPCMonitor::detachProject(const KURL &url)
{
  projectCommand("project_detach", url);
}

void KBSRPCMonitor::resetProject(const KURL &url)
{
  projectCommand("project_reset", url);
}

void KBSRPCMonitor::updateProject(const KURL &url)
{
  projectCommand("project_update", url);
}

void KBSRPCMonitor::suspendProject(const KURL &url, bool suspend)
{
  projectCommand(suspend ? "project_suspend" : "project_resume", url);
}

void KBSRPCMonitor::extinguishProject(const KURL &url, bool extinguish)
{
  projectCommand(extinguish ? "project_nomorework" : "project_allowmorework", url);
}

void KBSRPCMonitor::abortResult(const KURL &url, const QString &result)
{
  resultCommand("abort_result", url, result);
}

void KBSRPCMonitor::suspendResult(const KURL &url, const QString &result, bool suspend)
{
  resultCommand(suspend ? "suspend_result" : "resume_result", url, result);
}

void KBSRPCMonitor::showGraphics(const KURL &url, const QString &result)
{
  QDomDocument command;
  
  QDomElement result_show_graphics = command.createElement("result_show_graphics");
  command.appendChild(result_show_graphics);
  
  QDomElement project_url = command.createElement("project_url");
  result_show_graphics.appendChild(project_url);
  project_url.appendChild(command.createTextNode(url.prettyURL(+1)));
  
  QDomElement result_name = command.createElement("result_name");
  result_show_graphics.appendChild(result_name);
  result_name.appendChild(command.createTextNode(result));
  
  sendCommand(command);
}

void KBSRPCMonitor::abortFileTransfer(const KURL &url, const QString &name)
{
  fileTransferCommand("abort_file_transfer", url, name);
}

void KBSRPCMonitor::retryFileTransfer(const KURL &url, const QString &name)
{
  fileTransferCommand("retry_file_transfer", url, name);
}

void KBSRPCMonitor::sendCommand(const QDomDocument &command, bool blocking)
{
  if(!blocking)
  {
    const QString tag = command.firstChild().toElement().nodeName();
    if(tag.isEmpty()) return;
    
    m_polling.insert(tag, command);
  }
  else if(!m_blocking.contains(command))
  {
    m_blocking << command;
    sendQueued();
  }
}

KBSBOINCMonitor *KBSRPCMonitor::monitor()
{
  return static_cast<KBSBOINCMonitor*>(parent());
}

void KBSRPCMonitor::projectCommand(const QString &tag, const KURL &url)
{
  QDomDocument command;
  
  QDomElement root = command.createElement(tag);
  command.appendChild(root);
  
  QDomElement project_url = command.createElement("project_url");
  root.appendChild(project_url);
  project_url.appendChild(command.createTextNode(url.prettyURL(+1)));
  
  sendCommand(command);
  QTimer::singleShot(1500, monitor(), SLOT(checkFiles()));
}

void KBSRPCMonitor::resultCommand(const QString &tag, const KURL &url, const QString &result)
{
  QDomDocument command;
  
  QDomElement root = command.createElement(tag);
  command.appendChild(root);
  
  QDomElement project_url = command.createElement("project_url");
  root.appendChild(project_url);
  project_url.appendChild(command.createTextNode(url.prettyURL(+1)));
  
  QDomElement name = command.createElement("name");
  root.appendChild(name);
  name.appendChild(command.createTextNode(result));
  
  sendCommand(command);
  QTimer::singleShot(1500, monitor(), SLOT(checkFiles()));
}

void KBSRPCMonitor::fileTransferCommand(const QString &tag, const KURL &url, const QString &name)
{
  QDomDocument command;
  
  QDomElement root = command.createElement(tag);
  command.appendChild(root);
  
  QDomElement project_url = command.createElement("project_url");
  root.appendChild(project_url);
  project_url.appendChild(command.createTextNode(url.prettyURL(+1)));
  
  QDomElement filename = command.createElement("filename");
  root.appendChild(filename);
  filename.appendChild(command.createTextNode(name));
  
  sendCommand(command);
}

void KBSRPCMonitor::getRunMode()
{
  QDomDocument command;
  
  command.appendChild(command.createElement("get_run_mode"));
  
  sendCommand(command, false);
}

void KBSRPCMonitor::getNetworkMode()
{
  QDomDocument command;
  
  QDomElement get_network_mode = command.createElement("get_network_mode");
  command.appendChild(get_network_mode);
  get_network_mode.appendChild(command.createTextNode(""));
  
  sendCommand(command, false);
}

void KBSRPCMonitor::getMessages()
{
  QDomDocument command;
  
  QDomElement get_messages = command.createElement("get_messages");
  command.appendChild(get_messages);
  
  QDomElement nmessages = command.createElement("nmessages");
  get_messages.appendChild(nmessages);
  nmessages.appendChild(command.createTextNode(QString::number(32767)));
  
  if(m_seqno >= 0) {
    QDomElement seqno = command.createElement("seqno");
    get_messages.appendChild(seqno);
    seqno.appendChild(command.createTextNode(QString::number(m_seqno)));
  }
  
  sendCommand(command, false);
}

void KBSRPCMonitor::getFileTransfers()
{
  QDomDocument command;
  
  QDomElement get_file_transfers = command.createElement("get_file_transfers");
  command.appendChild(get_file_transfers);
  get_file_transfers.appendChild(command.createTextNode(""));
  
  sendCommand(command, false);
}

void KBSRPCMonitor::timerEvent(QTimerEvent *e)
{
  if(e->timerId() != m_timer || 0 == m_port) return;
  
  // send polling commands if there's nothing else to do
  if(m_blocking.isEmpty()) m_blocking = m_polling.values();
  
  sendQueued();
}

void KBSRPCMonitor::startConnection()
{
  if(0 == m_port || Disconnected != m_status) return;
  
  m_status = Connecting;
  m_nonce = QString::null;
  
  m_socket->connectToHost(m_host, m_port);
}

void KBSRPCMonitor::resetConnection()
{
  m_status = Disconnected;
  
  m_blocking.clear();
  m_output = QString::null;
  
  m_socket->close();
}

void KBSRPCMonitor::sendQueued()
{
  if(Disconnected == m_status) return startConnection();
  
  if(Idle != m_status) return;
  
  if(!m_password.isEmpty() && m_nonce.isEmpty()) return sendAuth1();
  
  if(m_blocking.isEmpty()) return;
  
  m_command = m_blocking.first();
  m_blocking.remove(m_command);
  
  sendImmediate(m_command);
}

void KBSRPCMonitor::sendImmediate(const QDomDocument &command)
{
  if(m_status < Idle) return;
  
  m_status = Active;
  
  QTextStream stream(m_socket);
  stream << command.toString() << "\n";
  m_socket->flush();
}

void KBSRPCMonitor::sendAuth1()
{
  QDomDocument command;
  
  QDomElement auth1 = command.createElement("auth1");
  command.appendChild(auth1);
  auth1.appendChild(command.createTextNode(""));
  
  m_status = Authenticating;
  
  QTextStream stream(m_socket);
  stream << command.toString() << "\n\n";
  m_socket->flush();
}

void KBSRPCMonitor::sendAuth2()
{
  QDomDocument command;
  
  QDomElement auth2 = command.createElement("auth2");
  command.appendChild(auth2);
  
  QDomElement nonce_hash = command.createElement("nonce_hash");
  auth2.appendChild(nonce_hash);
  
  const QString digest = KMD5(m_nonce + m_password).hexDigest();
  nonce_hash.appendChild(command.createTextNode(digest));
  
  sendImmediate(command);
}

void KBSRPCMonitor::massageFileTransfers(KBSBOINCFileTransfers &fileTransfers)
{
  const KBSBOINCClientState *state = monitor()->state();
  if(NULL == state) return;
  
  QMap<QString,KBSBOINCFileTransfer>::iterator transfer;
  for(transfer = fileTransfers.file_transfer.begin();
      transfer != fileTransfers.file_transfer.end(); ++transfer)
    if((*transfer).project_name.isEmpty())
    {
      const QString project = KBSBOINC::parseProjectName((*transfer).project_url);
      if(project.isEmpty()) continue;
      
      if(state->project.contains(project))
        (*transfer).project_name = state->project[project].project_name;
    }
}

void KBSRPCMonitor::slotConnected()
{
  m_status = Idle;
  
  sendQueued();
}

void KBSRPCMonitor::slotConnectionClosed()
{
  resetConnection();
  
  if(!m_msgs.msg.isEmpty())
  {
    m_msgs.msg.clear();
    m_seqno = -1;
    getMessages();
    
    emit updated();
    emit messagesUpdated();
  }
}

void KBSRPCMonitor::slotReadyRead()
{
  if(m_status <= Idle) return;
  
  int len;
  char buffer[1024+1];
  
  while((len = m_socket->readBlock(buffer, 1024)) > 0) {
    buffer[len] = '\0';
    m_output.append(buffer);
  }
  
  if(len < 0) {
    slotError(len);
    return;
  }
  
  if(!m_output.contains('\003')) return;
  
  m_output.remove('\003');
  m_output.remove(QRegExp("<\\?xml[^>]+\\?>"));
   
  const QString tag = m_command.firstChild().toElement().nodeName();
  
  QDomDocument doc;
  if(doc.setContent(m_output))
  {
    QDomNodeList list;
    
    if(Authenticating == m_status
       && (list = doc.elementsByTagName("nonce")).count() > 0)
      m_nonce = list.item(0).toElement().text();
    else if(tag == "get_run_mode"
            && (list = doc.elementsByTagName("run_mode")).count() > 0)
    {
      const QDomElement node = list.item(0).toElement();
      KBSBOINCRunMode runMode = RunAuto;
      
      if(node.elementsByTagName("always").count() > 0)
        runMode = RunAlways;
      else if(node.elementsByTagName("never").count() > 0)
        runMode = RunNever;
      
      if(m_runMode != runMode) {
        m_runMode = runMode;
        emit runModeUpdated();
      }
    }
    else if(tag == "get_network_mode"
            && (list = doc.elementsByTagName("network_mode")).count() > 0)
    {
      const QDomElement node = list.item(0).toElement();
      KBSBOINCNetworkMode networkMode = ConnectAlways;
      
      if(node.elementsByTagName("never").count() > 0)
        networkMode = ConnectNever;
      
      if(m_networkMode != networkMode) {
        m_networkMode = networkMode;
        emit networkModeUpdated();
      }
    }
    else if(tag == "get_messages"
            && (list = doc.elementsByTagName("msg")).count() > 0)
    {
      const unsigned count = m_msgs.msg.count();
      
      for(unsigned i = 0; i < list.count(); ++i)
      {
        KBSBOINCMsg msg;
        if(msg.parse(list.item(i).toElement())) {
          if(int(msg.seqno) > m_seqno) m_seqno = msg.seqno;
          if(msg.project.isEmpty()) msg.project = "BOINC";
          m_msgs.msg << msg;
        }
      }
      
      if(m_msgs.msg.count() > count)
      {
        qHeapSort(m_msgs.msg);
        if(0 == count) emit updated();
        emit messagesUpdated();
      }
      
      getMessages();
    }
    else if(tag == "get_file_transfers"
            && (list = doc.elementsByTagName("file_transfers")).count() > 0)
    {
      KBSBOINCFileTransfers fileTransfers;
      
      if(fileTransfers.parse(list.item(0).toElement())
         && !(m_fileTransfers.file_transfer.isEmpty()
              && fileTransfers.file_transfer.isEmpty()))
      {
        massageFileTransfers(fileTransfers);
        m_fileTransfers = fileTransfers;
        emit fileTransfersUpdated();
      }
    }
    else if(tag.endsWith("_poll")
            && (list = doc.elementsByTagName("error_num")).count() > 0)
    {
      const int num = list.item(0).toElement().text().toInt();
      if(num != -204) // -204 means RPC in progress, keep polling
      {
        m_polling.remove(tag);
        
        QStringList messages;
        list = doc.elementsByTagName("message");
        for(unsigned i = 0; i < list.count(); ++i)
          messages << list.item(i).toElement().text();
        
        emit error(tag, num, messages);
      }
    }
    else if(tag.endsWith("_account_poll")
            && (list = doc.elementsByTagName("authenticator")).count() > 0)
    {
      m_polling.remove(tag);
      
      const QDomElement node = list.item(0).toElement();
      const QString account_key = node.text();
      
      emit output(tag, account_key);
    }
    else if(tag.endsWith("_config_poll")
            && (list = doc.elementsByTagName("project_config")).count() > 0)
    {
      m_polling.remove(tag);
      
      const QDomElement node = list.item(0).toElement();
      KBSBOINCProjectConfig config;
      
      if(config.parse(node)) emit output(config);
    }
    else if((list = doc.elementsByTagName("error")).count() > 0)
    {
      m_polling.remove(tag);
      const QDomElement node = list.item(0).toElement();
      emit error(tag, -1, node.text());
    }
    else if(doc.elementsByTagName("success").count() > 0)
      emit error(tag, 0);
    else if(doc.elementsByTagName("unrecognized").count() > 0)
    {
      m_polling.remove(tag);
      
      if(Authenticating == m_status)
        // set dummy nonce if the client doesn't support authentication
        m_nonce = "0"; 
      else
        emit error(tag, -1, "Unrecognized RPC command.");
    }
    else if(doc.elementsByTagName("unauthorized").count() > 0)
      // authorization has changed; get a new nonce
      m_nonce = QString::null;
  }
  
  m_output = QString::null;
  
  if(Authenticating == m_status && !m_nonce.isEmpty())
    return sendAuth2();
  
  m_status = Idle;
  sendQueued();
}

void KBSRPCMonitor::slotError(int)
{
  if(m_status > Disconnected) resetConnection();
  
  if(!m_msgs.msg.isEmpty())
  {
    m_msgs.msg.clear();
    m_seqno = -1;
    getMessages();
    
    emit updated();
    emit messagesUpdated();
  }
}

#include "kbsrpcmonitor.moc"
