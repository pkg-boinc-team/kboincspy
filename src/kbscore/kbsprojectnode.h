/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPROJECTNODE_H
#define KBSPROJECTNODE_H

#include <qstringlist.h>

#include <kurl.h>

#include <kbstree.h>

class KBSProjectNode : public KBSTreeNode
{
  Q_OBJECT
  public:
    KBSProjectNode(const QString &project, KBSTreeNode *parent, const char *name=0);
    
    virtual unsigned type() const;
    virtual QString name() const;
    virtual QStringList icons() const;
    
    virtual QString project() const;
    virtual KURL projectURL() const;
    virtual bool isSuspended() const;
    virtual bool isExtinguished() const;
    
    static QString name(const QString &project, KBSBOINCMonitor *monitor);
 
  protected:
    virtual KBSBOINCMonitor *monitor();
    
  private slots:
    void update();
    
    void addWorkunits(const QStringList &workunits);
    void removeWorkunits(const QStringList &workunits);
    void activateWorkunit(unsigned task, const QString &workunit, bool activated);
      
  private:
    void setupMonitor();
    void addPlugins();
    
    bool insertWorkunit(const QString &workunit);
    bool deleteWorkunit(const QString &workunit);
  
  protected:
    bool m_suspended, m_extinguished;
  
  private:
    QString m_project;
    KURL m_projectURL;
    KBSBOINCMonitor *m_monitor;
    
    enum Set {Input, Output, Task, Sets};
    QStringList m_workunit[Sets];
};

#endif
