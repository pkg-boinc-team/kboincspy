/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSTASKNODE_H
#define KBSTASKNODE_H

#include <kbsworkunitnode.h>

class KBSBOINCMonitor;

class KBSTaskNode : public KBSWorkunitNode
{
  Q_OBJECT
  public:
    KBSTaskNode(unsigned num, const QString &workunit, KBSTreeNode *parent, const char *name=0);

    virtual QString name() const;
  
    virtual int task() const;
    virtual QString result() const;
    
    static QString name(unsigned num, KBSBOINCMonitor *monitor);

  private:
    void addPlugins();
    
  protected:
    unsigned m_num;
    
  private:
    int m_task;
    QString m_result;
};

#endif
