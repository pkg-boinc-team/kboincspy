/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <limits.h>

#include <qdict.h>

#include <klibloader.h>
#include <klocale.h>
#include <ktrader.h>

#include <kbsboincmonitor.h>
#include <kbshostnode.h>
#include <kbspanelnode.h>
#include <kbsworkunitnode.h>

#include "kbscachenode.h"

KBSCacheNode::KBSCacheNode(const QString &project, KBSTreeNode *parent, const char *name)
            : KBSTreeNode(parent, name), m_project(project)
{
  setupMonitor();

  const KBSBOINCClientState *state = (NULL != m_monitor) ? m_monitor->state() : NULL;
  if(NULL != state) addWorkunits(state->workunit.keys());
  
  addPlugins();
}

unsigned KBSCacheNode::type() const
{
  return(INT_MAX - 1);
}

QString KBSCacheNode::name() const
{
  return i18n("Cache");
}

QStringList KBSCacheNode::icons() const
{
  return QStringList("cache");
}

QString KBSCacheNode::project() const
{
  return m_project;
}

KBSBOINCMonitor *KBSCacheNode::monitor()
{
  return m_monitor;
}

void KBSCacheNode::addWorkunits(const QStringList &workunits)
{
  if(NULL == m_monitor) return;
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    if(m_workunits.find(*workunit) != NULL) continue;
    if(m_monitor->project(state->workunit[*workunit]) != m_project) continue;
    
    KBSWorkunitNode *node = new KBSWorkunitNode(*workunit, this);
    insertChild(node);
    
    m_workunits.insert(*workunit, node);
  }
}

void KBSCacheNode::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    KBSWorkunitNode *node = m_workunits.find(*workunit);
    if(NULL == node) continue;
    
    m_workunits.remove(*workunit);
    removeChild(node);
  }
}

void KBSCacheNode::setupMonitor()
{
  KBSHostNode *host = static_cast<KBSHostNode*>(findAncestor("KBSHostNode"));
  m_monitor = (NULL != host) ? host->monitor() : NULL;
  if(NULL == m_monitor) return;
  
  connect(m_monitor, SIGNAL(workunitsAdded(const QStringList &)),
          this, SLOT(addWorkunits(const QStringList &)));
  connect(m_monitor, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
}

void KBSCacheNode::addPlugins()
{
  QString constraints;
  KTrader::OfferList offers;
  QDict<KBSPanelNode> plugins;
  
  // first load plugins specific to this project...
  constraints = "([X-KDE-Target] == 'Cache') and ('%1' in [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints.arg(m_project));
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_project);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
  
  // ... and then generic ones valid for all projects
  constraints = "([X-KDE-Target] == 'Cache') and (not exist [X-KDE-Projects])";
  offers = KTrader::self()->query("KBSPanelNode", constraints);
  for(KTrader::OfferListIterator offer = offers.begin(); offer != offers.end(); ++offer)
  {
    const QString name = (*offer)->property("X-KDE-Name").toString();
    if(name.isEmpty() || plugins.find(name) != NULL) continue;
    
    QStringList args = (*offer)->property("X-KDE-Arguments").toStringList();
    args.prepend(m_project);
    
    KLibFactory *factory = KLibLoader::self()->factory((*offer)->library());
    if(NULL == factory) continue;
    
    KBSPanelNode *node = static_cast<KBSPanelNode*>(factory->create(this, name,
                                                                    "KBSPanelNode", args));
    
    insertChild(node);
    plugins.insert(name, node);
  }
}

#include "kbscachenode.moc"

