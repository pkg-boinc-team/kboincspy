/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLOGMONITOR_H
#define KBSLOGMONITOR_H

#include <qdatetime.h>
#include <qiodevice.h>
#include <qmap.h>
#include <qvaluelist.h>
#include <qvariant.h>

#include <kbsdatamonitor.h>

typedef QMap<QString,QVariant> KBSLogDatum;
typedef QValueList<KBSLogDatum> KBSLogData;
typedef QMap<QString,QString> KBSKeyMap;

class KBSProjectMonitor;

class KBSLogMonitor : public KBSDataMonitor
{
  Q_OBJECT
  public:
    virtual QStringList keys() const;
    virtual bool hasResults() const;
    
    virtual KBSLogData workunits() const;
    virtual KBSLogData results(const QString &workunit) const;
  
    virtual void logWorkunit(KBSProjectMonitor *monitor, const QString &workunit);
  
  signals:
    void workunitsUpdated();
    void resultsUpdated();
  
  protected:
    KBSLogMonitor(const KURL &url, QObject *parent, const char *name=0);
    
    virtual void addLogFile(const QString &fileName);
    
    virtual QMap<QString,KBSLogData> formatWorkunit(KBSProjectMonitor *monitor, const QString &workunit) const;
    
    virtual void appendHeader(const KBSFileInfo *info, QIODevice *io);
    virtual void appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum);
    
  protected:
    static QDateTime parseSETIClassicDate(const QString &string);
    static QString formatSETIClassicDate(const QDateTime &date);
    static QString formatSETIClassicDate(double date);

    static unsigned parseMajorVersion(const QString &string);
    static unsigned parseMinorVersion(const QString &string);
    static QString formatVersion(unsigned major, unsigned minor);

    static unsigned parseVersion (const QString &string);
    static QString formatVersion(unsigned version);

    static QValueList<QVariant> parsePotData(const QString &string);
    static QString formatPotData(const QValueList<unsigned> &list);

    static QDateTime parseLogEntryDate(const QString &string);
    static QString formatLogEntryDate(const QDateTime &date);

    static QStringList parseCSVKeys(const QString &line, const QChar &sep=',');
    static QString formatCSVKeys(const QStringList &keys, const QChar &sep=',');

    static KBSLogDatum parseCSVDatum(const QString &line, const QStringList &keys, const QChar &sep=',');
    static QString formatCSVDatum(const KBSLogDatum &datum, const QStringList &keys, const QChar &sep=',');

    static QStringList remapKeys(const QStringList &keys, const KBSKeyMap &map);
    static KBSLogDatum remapCSVDatum(const KBSLogDatum &datum, const KBSKeyMap &map);

  private:
    void commenceLogReadJob(const QString &fileName);
    void commenceLogWriteJob(const QString &fileName);
  
  private slots:
    void readResult(KIO::Job *);
    void writeResult(KIO::Job *);
    
  protected:
    KBSLogData m_workunits;
    QMap<QString,KBSLogData> m_results;
  
  private:
    QStringList m_files;
    QMap<QString,KBSLogData> m_queue;
    KTempFile *m_tmp;    
    KIO::FileCopyJob *m_job;
};

#endif
