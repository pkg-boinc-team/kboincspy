/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdlib.h>

#include <dcopclient.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kuniqueapplication.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>

#include "kboincspy.h"

static const char description[] =
    I18N_NOOP("A BOINC monitoring tool for KDE");

static const char version[] = VERSION;

static KCmdLineOptions options[] =
{
    { "+[URL]", I18N_NOOP("Location to add"), 0 },
    KCmdLineLastOption
};

int main(int argc, char **argv)
{
  KAboutData about("kboincspy", I18N_NOOP("KBoincSpy"), version, description,
                   KAboutData::License_GPL, "(C) 2004-2005 Roberto A. Virga", 
                   0, "http://kboincspy.sf.net/", "rvirga@users.sf.net");
  about.addAuthor("Roberto A. Virga",
                  I18N_NOOP("Primary author and maintainer"),
                  "rvirga@users.sf.net");
  
  KCmdLineArgs::init(argc, argv, &about);
  KCmdLineArgs::addCmdLineOptions(options);
  
#ifndef TESTING
  KUniqueApplication::addCmdLineOptions();
  if(!KUniqueApplication::start()) exit(0);
  KUniqueApplication app;
#else
  KApplication app;
#endif

  KBoincSpy *kboincspy = new KBoincSpy();
  app.connect(&app, SIGNAL(shutDown()), kboincspy, SLOT(quit()));
  app.setMainWidget(kboincspy);
        
  KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
  if(args->count() > 0)
  {
    for(int i = 0; i < args->count(); i++)
    {
      KBSLocation location;

      location.url = args->url(i);
      if(!location.url.isValid()) continue;
      location.url.adjustPath(+1);
      
      location.host = location.defaultHost();
      location.port = location.defaultPort;
      
      kboincspy->document()->connectTo(location);
    }
    args->clear();
  }

  return app.exec();
}
