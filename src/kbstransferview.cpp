/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlayout.h>
#include <qpopupmenu.h>
#include <qprogressbar.h>
#include <qptrlist.h>
#include <qstringlist.h>
#include <qstyle.h>

#include <kapplication.h>
#include <kconfig.h>
#include <kglobal.h>
#include <klocale.h>
#include <kmenubar.h>
#include <ktoolbar.h>

#include <kbsboincmonitor.h>
#include <kbsrpcmonitor.h>

#include "kbstransferview.h"

using namespace KBSBOINC;

KBSTransferView::KBSTransferView(KBSBOINCMonitor *monitor, QWidget *parent, const char *name, WFlags f)
               : KMainWindow(parent, name, f),
                 m_view(new KListView(this)), m_monitor(monitor)
{
  KBSRPCMonitor *rpcMonitor = monitor->rpcMonitor();
  
  connect(rpcMonitor, SIGNAL(fileTransfersUpdated()), this, SLOT(updateFileTransfers()));
  connect(rpcMonitor, SIGNAL(updated()), this, SLOT(updateState()));
  
  const QString host = rpcMonitor->host();
  
  setCaption(i18n("File Transfers - %1").arg(host));
  
  setCentralWidget(m_view);
  setupView();
  
  setAutoSaveGeometry(QString("%1 - File Transfers").arg(host));
  
  setupActions();
  
  updateFileTransfers();
}

KBSTransferView::~KBSTransferView()
{
  const QString group = autoSaveGroup();
  if(!group.isEmpty()) {
    writeGeometry(group);
    m_view->saveLayout(kapp->config(), group);
  }
}

KBSBOINCMonitor *KBSTransferView::monitor()
{
  return m_monitor;
}

void KBSTransferView::setAutoSaveGeometry(const QString &group)
{
  setAutoSaveSettings(group, false);
  if(!group.isEmpty()) {
    readGeometry(group);
    m_view->restoreLayout(kapp->config(), group);
  }
}

void KBSTransferView::updateFileTransfers()
{
  const QMap<QString,KBSBOINCFileTransfer> transfers =
    m_monitor->rpcMonitor()->fileTransfers()->file_transfer;
  
  QPtrList<QListViewItem> deleted;
  QStringList added = transfers.keys();
  for(QListViewItem *item = m_view->firstChild(); NULL != item; item = item->nextSibling())
  {
    const QString name = item->text(1);
    
    if(added.contains(name)) {
      static_cast<Item*>(item)->setTransfer(transfers[name]);
      added.remove(name);
    } else
      deleted.append(item);
  }
  for(QStringList::const_iterator name = added.begin(); name != added.end(); ++name)
    (new Item(m_view))->setTransfer(transfers[*name]);
  for(QListViewItem *item = deleted.first(); item != NULL; item = deleted.next())
    m_view->takeItem(item);

  updateState();
}

void KBSTransferView::updateState()
{
  KBSRPCMonitor *rpcMonitor = m_monitor->rpcMonitor();
  const bool suspended = (rpcMonitor->runMode() == RunNever)
                         || (rpcMonitor->networkMode() == ConnectNever);

  for(QListViewItem *item = m_view->firstChild(); NULL != item; item = item->nextSibling())
    static_cast<Item*>(item)->setSuspended(suspended);
}

void KBSTransferView::transferRetry()
{
  Item *transfer = static_cast<Item*>(m_view->selectedItem());
  if(NULL == transfer || !transfer->canRetry()) return;
  
  m_monitor->rpcMonitor()->retryFileTransfer(transfer->transfer().project_url,
                                             transfer->transfer().name);
}

void KBSTransferView::transferAbort()
{
  Item *transfer = static_cast<Item*>(m_view->selectedItem());
  if(NULL == transfer || !transfer->canAbort()) return;
  
  m_monitor->rpcMonitor()->abortFileTransfer(transfer->transfer().project_url,
                                             transfer->transfer().name);
}

void KBSTransferView::slotContextMenu(QListViewItem *item, const QPoint &pos, int)
{
  if(NULL != item) m_view->setSelected(item, true);
  
  Item *transfer = static_cast<Item*>(m_view->selectedItem());
  transfer_retry->setEnabled(NULL != transfer && transfer->canRetry());
  transfer_abort->setEnabled(NULL != transfer && transfer->canAbort());
  
  QPopupMenu *context = static_cast<QPopupMenu*>(guiFactory()->container("context", this));
  context->popup(pos);
}

void KBSTransferView::setupView()
{
  m_view->addColumn(i18n("Project"));
  m_view->setColumnAlignment(0, AlignLeft);
  m_view->addColumn(i18n("File"));  
  m_view->setColumnAlignment(1, AlignLeft);
  m_view->addColumn(i18n("Progress"));
  m_view->setColumnAlignment(2, AlignCenter);
  m_view->addColumn(i18n("Size"));
  m_view->setColumnAlignment(3, AlignLeft);
  m_view->addColumn(i18n("Time"));
  m_view->setColumnAlignment(4, AlignLeft);
  m_view->addColumn(i18n("Speed"));
  m_view->setColumnAlignment(5, AlignLeft);
  m_view->addColumn(i18n("Status"));
  m_view->setColumnAlignment(6, AlignLeft);
  
  m_view->setSelectionMode(QListView::Single);
  m_view->setAllColumnsShowFocus(true);
  
  m_view->setShowSortIndicator(true);
  m_view->setSorting(1);
  m_view->sort();
  
  connect(m_view, SIGNAL(contextMenuRequested(QListViewItem *, const QPoint &, int)),
          this, SLOT(slotContextMenu(QListViewItem *, const QPoint &, int))); 
}

void KBSTransferView::setupActions()
{
  setStandardToolBarMenuEnabled(false);
  
  KStdAction::close(this, SLOT(close()), actionCollection())->setText(i18n("Close &Window"));
  
  transfer_retry = new KAction(i18n("&Retry File Transfer"), 0, this, SLOT(transferRetry()),
                                    actionCollection(), "transfer_retry");
  transfer_retry->setEnabled(false);
  
  transfer_abort = new KAction(i18n("&Abort File Transfer"), 0, this, SLOT(transferAbort()),
                                    actionCollection(), "transfer_abort");
  transfer_abort->setEnabled(false);
  
  createGUI("kbstransferui.rc", false);
  
  delete menuBar();
  delete toolBar();
}

void KBSTransferView::readGeometry(const QString &group)
{
  KConfig *config = kapp->config();
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
  
  QRect geometry;
  
  geometry.setTop(config->readNumEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), -1));
  if(geometry.top() < 0) return;
  
  geometry.setLeft(config->readNumEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), -1));
  if(geometry.left() < 0) return;
  
  geometry.setHeight(config->readNumEntry(QString("Height %1").arg(desk.height()), 0));
  if(geometry.height() <= 0) return;
  
  geometry.setWidth(config->readNumEntry(QString("Width %1").arg(desk.width()), -1));
  if(geometry.width() < 0) return;
  
  setGeometry(geometry);
}

void KBSTransferView::writeGeometry(const QString &group)
{
  KConfig *config = kapp->config();
  config->setGroup(group);
  
  const int screen = kapp->desktop()->screenNumber(parentWidget());
  const QRect desk = kapp->desktop()->screenGeometry(screen);
  
  config->writeEntry(QString("Top %1 %2").arg(desk.top()).arg(desk.height()), geometry().top());
  config->writeEntry(QString("Left %1 %2").arg(desk.left()).arg(desk.width()), geometry().left());
  config->writeEntry(QString("Height %1").arg(desk.height()), geometry().height());
  config->writeEntry(QString("Width %1").arg(desk.width()), geometry().width());
}

KBSTransferView::Item::Item(QListView *parent)
                     : KListViewItem(parent), m_suspended(false)
{
}

const KBSBOINCFileTransfer &KBSTransferView::Item::transfer() const
{
  return m_transfer;
}

void KBSTransferView::Item::setTransfer(const KBSBOINCFileTransfer &transfer)
{
  m_transfer = transfer;
  
  if(text(0).isNull()) setText(0, transfer.project_name);
  
  if(text(1).isNull()) setText(1, transfer.name);
  
  const double ratio =
    (m_transfer.nbytes > 0) ?  m_transfer.file_xfer.bytes_xferred / m_transfer.nbytes : 0;
  
  KLocale *locale = KGlobal::locale();
  
  setText(2, locale->formatNumber(1e2 * ratio, 0));
  
  setText(3, sizeRatio());
  
  setText(4, formatTime(transfer.persistent_file_xfer.time_so_far));
  
  setText(5, speed());
  
  setText(6, status());
}

bool KBSTransferView::Item::canRetry() const
{
  if(m_suspended) return false;
  
  const QDateTime currTime = QDateTime::currentDateTime(),
                  nextTime = m_transfer.persistent_file_xfer.next_request_time;
  
  return(nextTime > currTime);
}

bool KBSTransferView::Item::canAbort() const
{
  return(!m_transfer.marked_for_delete);
}

bool KBSTransferView::Item::isSuspended() const
{
  return m_suspended;
}

void KBSTransferView::Item::setSuspended(bool suspended)
{
  m_suspended = suspended;

  setText(6, status());
}

QString KBSTransferView::Item::sizeRatio() const
{
  double scale = 1;
  QString schema = i18n("%1/%2");
  unsigned precision = 0;

  if(m_transfer.nbytes / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1/%2 KB");
    precision = 2;
  }
  if(m_transfer.nbytes / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1/%2 MB");
  }
  if(m_transfer.nbytes / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1/%2 GB");
  }
  if(m_transfer.nbytes / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1/%2 TB");
  }
  
  KLocale *locale = KGlobal::locale();
  
  return schema.arg(locale->formatNumber(m_transfer.file_xfer.bytes_xferred / scale, precision))
               .arg(locale->formatNumber(m_transfer.nbytes / scale, precision));
}

QString KBSTransferView::Item::speed() const
{
  double scale = 1;
  QString schema = i18n("%1 Bps");
  unsigned precision = 0;

  if(m_transfer.file_xfer.xfer_speed / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1 KBps");
    precision = 2;
  }
  if(m_transfer.file_xfer.xfer_speed / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1 MBps");
  }
  if(m_transfer.file_xfer.xfer_speed / scale > 1024) {
    scale *= 1024;
    schema = i18n("%1 GBps");
  }
  
  KLocale *locale = KGlobal::locale();
  
  return schema.arg(locale->formatNumber(m_transfer.file_xfer.xfer_speed / scale, precision));
}

QString KBSTransferView::Item::status() const
{
  if(m_suspended) return i18n("Suspended");
  
  const QDateTime currTime = QDateTime::currentDateTime(),
                  nextTime = m_transfer.persistent_file_xfer.next_request_time;
  
  if(nextTime > currTime)
    return i18n("Retry in %1").arg(formatTime(currTime.secsTo(nextTime)));
  
  switch(m_transfer.status) {
    case 0:
      break;
    case 114:
    case -114:
      return i18n("Download failed");
    case 115:
    case -115:
      return i18n("Upload failed");
    default:
      return i18n("Error");
  }
  
  if(m_transfer.generated_locally)
    return i18n("Uploading");
  else
    return i18n("Downloading");
}

void KBSTransferView::Item::paintCell(QPainter *p, const QColorGroup &cg,
                                      int column, int width, int align)
{
  if(column != 2) return KListViewItem::paintCell(p, cg, column, width, align);
  
  QStyle::SFlags sflags = QStyle::Style_Default;
  if(listView()->isEnabled()) sflags |= QStyle::Style_Enabled;
  if(listView()->hasFocus()) sflags |= QStyle::Style_HasFocus;

  QRect r(1, 1, width - 2, height() - 2);
  
  QProgressBar progress;
  progress.setProgress(text(2).toInt(0, 10));
  progress.setGeometry(r);
  
  listView()->style().drawControl(QStyle::CE_ProgressBarContents, p, &progress, r, cg, sflags);
  listView()->style().drawControl(QStyle::CE_ProgressBarLabel, p, &progress, r, cg, sflags);
}

QString KBSTransferView::Item::key(int column, bool ascending) const
{
  switch(column) {
    case 0:
      return m_transfer.project_name;
    case 1:
      return m_transfer.name;
    case 2:
      {
        double ratio = 0.0;
        if(m_transfer.nbytes > 0)
          ratio = m_transfer.file_xfer.bytes_xferred / m_transfer.nbytes;
        
        return QString().sprintf("%05.lf", 1e5 * ratio);
      }
    case 3:
      return QString().sprintf("%012.0lf", m_transfer.file_xfer.bytes_xferred);
    case 4:
      return QString().sprintf("%09.lf", 1e2 * m_transfer.persistent_file_xfer.time_so_far);
    case 5:
      return QString().sprintf("%012.0lf", 1e2 * m_transfer.file_xfer.xfer_speed);
    default:
      return QListViewItem::key(column, ascending);
  }
}

#include "kbstransferview.moc"

