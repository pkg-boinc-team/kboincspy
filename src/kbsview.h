/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSVIEW_H
#define KBSVIEW_H

#include <qsplitter.h>
#include <qwidget.h>
#include <qwidgetstack.h>

#include <kconfig.h>

class KBSPanelNode;
class KBSPanelView;
class KBSTreeNode;
class KBSTreeView;

class KBSView : public QWidget
{
  Q_OBJECT
  public:
    enum TreeView {NestedView, FlattenedView};
    enum PanelView {SimpleView, TabbedView};
    
    KBSView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);
    
    virtual KBSTreeView *treeView(TreeView view) const;
    
    virtual KBSTreeView *currentTreeView() const;
    virtual void setCurrentTreeView(TreeView view);
    virtual void setCurrentTreeView(KBSTreeView *view);
    
    virtual KBSPanelView *panelView(PanelView view) const;
    
    virtual KBSPanelView *currentPanelView() const;
    virtual void setCurrentPanelView(PanelView view);
    virtual void setCurrentPanelView(KBSPanelView *view);
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  signals:
    void currentTreeViewChanged();
    
  protected:
    QWidgetStack *m_lhs, *m_rhs;
    
  private:
    QSplitter *m_split;
};

#endif
