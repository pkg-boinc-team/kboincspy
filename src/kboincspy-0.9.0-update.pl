#!/usr/bin/perl

my $current_group = "";

while ( <> ) {
  chomp;
  if ( /^\[/ ) {
    $current_group = $_;
    next;
  }

  my ($key, $value) = split /=/;

  if ($current_group eq "[General - Polling]" and $key eq "rpc") {
    if ($value < 100) {
      my $new_value = $value*1000;
      
      print "$current_group\n";
      print "rpc=$new_value\n";
    }
  }

  if ($current_group eq "[KBSDocument]" and $key eq "Locations") {

    # If $value is a number, we assume that
    # kboincspyrc is already up to date.
    if ($value =~ m/^\d+?$/) { next; }

    my $host = "localhost";
    my @locations = split(/,/, $value);
    
    for ($i = 0; $i <= $#locations; $i++) {
      if ($locations[$i] =~ m/\/\/(.*?)\//) {
        $host = $1;
        $host =~ s/.*@//;
      }
      print "$current_group\n";
      print "Location $i URL=$locations[$i]\n";
      print "Location $i host=$host\n";
      print "Location $i port=0\n";
    }
    
    print "Locations=$i\n";
  }
}

print "# DELETE [General - Polling]port\n";
