/***************************************************************************
 *   Copyright (C) 2006 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef KBSPROJECTWIZARD_H
#define KBSPROJECTWIZARD_H

#include <qmap.h>

#include <kdialog.h>

#include <kbsprojectwizardcontent.h>

class KBSBOINCMonitor;
class KBSBOINCProjectConfig;

class KBSProjectWizard : public KDialog
{
  Q_OBJECT
  public:
    KBSProjectWizard(KBSBOINCMonitor *monitor, QWidget *parent=0, const char *name=0);
    virtual ~KBSProjectWizard();
    
    virtual KBSBOINCMonitor *monitor() const;
  
  protected:
    enum { NoPage=-1, Welcome, ProjectURL, ConnectHost, ConnectNetwork, Account, AccountKey,
           Success, ConnectError, NetworkError, AuthError, DuplicateError, GenericError };
    
    virtual int previous(int page) const;
    virtual int next(int page) const;
    
    virtual bool isTerminal(int page) const;
  
  protected slots:
    virtual void reject();
  
  private:
    void setupMonitor();
    void setupView();
    
    void show(int page);
    bool isValid(int page) const;
    
    QString project(const QString &projectName) const;
  
  private slots:
    void input(const KBSBOINCProjectConfig &config);
    void input(const QString &tag, const QString &account_key);
    void error(const QString &tag, int num, QStringList messages);
    
    void help();
    void previous();
    void next();
    
    void validateText(const QString &);
  
  protected:
    bool m_legacy;
    KBSBOINCProjectConfig *m_config; 
    
    KBSProjectWizardContent *m_view;
    int m_page;
  
  private:
    KBSBOINCMonitor *m_monitor;
    QMap<int,bool> m_valid;
};

#endif
