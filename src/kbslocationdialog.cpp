#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbslocationdialog.ui'
**
** Created: Mon Feb 6 18:28:48 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbslocationdialog.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <kurlrequester.h>
#include <klineedit.h>
#include <kcombobox.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "./kbslocationdialog.ui.h"

/*
 *  Constructs a KBSLocationDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
KBSLocationDialog::KBSLocationDialog( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "KBSLocationDialog" );
    KBSLocationDialogLayout = new QVBoxLayout( this, 11, 6, "KBSLocationDialogLayout"); 

    layout_url = new QHBoxLayout( 0, 0, 6, "layout_url"); 

    label_url = new QLabel( this, "label_url" );
    layout_url->addWidget( label_url );

    url = new KURLRequester( this, "url" );
    url->setMode( 9 );
    layout_url->addWidget( url );
    KBSLocationDialogLayout->addLayout( layout_url );

    layout_host = new QHBoxLayout( 0, 0, 6, "layout_host"); 

    label_host = new QLabel( this, "label_host" );
    layout_host->addWidget( label_host );

    host = new KLineEdit( this, "host" );
    layout_host->addWidget( host );

    label_port = new QLabel( this, "label_port" );
    layout_host->addWidget( label_port );

    port = new KComboBox( FALSE, this, "port" );
    layout_host->addWidget( port );
    KBSLocationDialogLayout->addLayout( layout_host );
    spacer = new QSpacerItem( 1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSLocationDialogLayout->addItem( spacer );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 
    spacer_buttons = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_buttons );

    ok = new KPushButton( this, "ok" );
    ok->setEnabled( FALSE );
    layout_buttons->addWidget( ok );

    cancel = new KPushButton( this, "cancel" );
    layout_buttons->addWidget( cancel );
    KBSLocationDialogLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(400, 121).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( ok, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( cancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( url, SIGNAL( textChanged(const QString&) ), this, SLOT( updateURL(const QString&) ) );
    connect( url, SIGNAL( urlSelected(const QString&) ), this, SLOT( updateURL(const QString&) ) );
    connect( host, SIGNAL( textChanged(const QString&) ), this, SLOT( updateHost(const QString&) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSLocationDialog::~KBSLocationDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSLocationDialog::languageChange()
{
    setCaption( tr2i18n( "Add Location" ) );
    label_url->setText( tr2i18n( "URL:" ) );
    url->setFilter( tr2i18n( "client_state.xml|BOINC Client State File" ) );
    label_host->setText( tr2i18n( "Host:" ) );
    label_port->setText( tr2i18n( "Port:" ) );
    port->clear();
    port->insertItem( tr2i18n( "auto" ) );
    port->insertItem( tr2i18n( "1043" ) );
    port->insertItem( tr2i18n( "31416" ) );
    ok->setText( tr2i18n( "&OK" ) );
    ok->setAccel( QKeySequence( tr2i18n( "Alt+O" ) ) );
    cancel->setText( tr2i18n( "&Cancel" ) );
    cancel->setAccel( QKeySequence( tr2i18n( "Alt+C" ) ) );
}

#include "kbslocationdialog.moc"
