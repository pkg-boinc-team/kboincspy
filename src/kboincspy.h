/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBOINCSPY_H
#define KBOINCSPY_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <qptrdict.h>

#include <kactionclasses.h>
#include <kmainwindow.h>
#include <ksystemtray.h>

class KBSDocument;
class KBSHostNode;
class KBSMessageView;
class KBSPanel;
class KBSPreferences;
class KBSProjectNode;
class KBSSystemTray;
class KBSTransferView;
class KBSTreeNode;
class KBSView;
class KBSWebToolBar;
class KBSWorkunitNode;

extern KMainWindow *kmain;

class KBoincSpy : public KMainWindow
{
  Q_OBJECT
  public:
    KBoincSpy();
    virtual ~KBoincSpy();
    
    KBSDocument *document() const;
    
    virtual QSize sizeHint() const;
  
  public slots:
    virtual void applyPreferences();
    
    virtual void quit();
  
  protected:
    virtual void readConfig();
    virtual void writeConfig();
    
    virtual bool queryClose();
  
  private:
    void setupActions();
    
    void readGeometry(KConfig *config, const QString &group);
    void writeGeometry(KConfig *config, const QString &group);
  
  private slots:
    void locationAdd();
    void locationRemove();
    
    void editCopy();
    
    void activateTabbedView();
    
    void activateIconView();
    void activateTreeView();
    
    void activateSmallIcons();
    void activateMediumIcons();
    void activateLargeIcons();
    void activateHugeIcons();
    
    void activateRunAuto();
    void activateRunAlways();
    void activateRunNever();
    
    void clientMessages();
    void fileTransfers();
    
    void activateNetworkConnect();
    
    void runBenchmarks();
    
    void networkProxy();
    
    void projectAttach();
    void projectDetach();
    void projectReset();
    void projectUpdate();
    
    void activateProjectSuspend();
    void activateProjectExtinguish();
    
    void showGraphics();
    
    void resultAbort();
    
    void activateResultSuspend();
    
    void optionsShowMenubar();
    void optionsConfigureNotifications();
    void optionsPreferences();
  
    void removeViews(KBSTreeNode *host);
    
    void updateMenuBarStatus();
    
    void updateTreeViewSelection();
    void updatePanelVisible();
    
    void updateRunMode();
    void updateNetworkMode();
    void updateProjectState(KBSTreeNode *node);
    void updateResultState(KBSTreeNode *node);
  
  protected:
    KBSDocument *m_document;
    KBSView *m_view;
    KBSSystemTray *m_tray;
    KBSWebToolBar *m_toolbar;
    QPtrDict<KBSMessageView> m_messages;
    QPtrDict<KBSTransferView> m_transfers;
  
  private:
    bool m_show, m_close;
    KBSHostNode *m_host;
    KBSProjectNode *m_project;
    KBSWorkunitNode *m_workunit;
    KBSPanel *m_panel;
    KToggleAction *tabbed_view;
    KToggleAction *tree_view, *icon_view;
    KToggleAction *icon_small, *icon_medium, *icon_large, *icon_huge;
    KToggleAction *run_auto, *run_always, *run_never;
    KToggleAction *network_connect;
    KAction *project_detach, *project_reset, *project_update, *project_suspend;
    KToggleAction *project_extinguish;
    KAction *show_graphics, *result_suspend, *result_abort;
    KToggleAction *show_menubar;
    QPtrList<KAction> project_actions, result_actions;
};

#endif
