/****************************************************************************
** Form interface generated from reading ui file './kbsproxydialog.ui'
**
** Created: Mon Feb 6 18:28:47 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSPROXYDIALOG_H
#define KBSPROXYDIALOG_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QTabWidget;
class QWidget;
class QCheckBox;
class QFrame;
class KLineEdit;
class QGroupBox;
class KPasswordEdit;
class QPushButton;

class KBSProxyDialog : public QDialog
{
    Q_OBJECT

public:
    KBSProxyDialog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~KBSProxyDialog();

    QLabel* help;
    QTabWidget* tab_proxy;
    QWidget* tab;
    QCheckBox* socks;
    QFrame* frame_socks_server;
    QLabel* label_socks_server;
    KLineEdit* socks_server;
    QLabel* label_socks_port;
    KLineEdit* socks_port;
    QGroupBox* socks_auth;
    QFrame* frame_socks_auth;
    QLabel* label_socks_user;
    KLineEdit* socks_user;
    QLabel* label_socks_password;
    KPasswordEdit* socks_password;
    QWidget* tab_2;
    QCheckBox* http;
    QFrame* frame_http_server;
    QLabel* label_http_server;
    KLineEdit* http_server;
    QLabel* label_http_port;
    KLineEdit* http_port;
    QGroupBox* http_auth;
    QFrame* frame_http_auth;
    QLabel* label_http_user;
    KLineEdit* http_user;
    QLabel* label_http_password;
    KPasswordEdit* http_password;
    QPushButton* button_ok;
    QPushButton* button_cancel;

protected:
    QVBoxLayout* KBSProxyDialogLayout;
    QSpacerItem* spacer_middle;
    QVBoxLayout* tabLayout;
    QHBoxLayout* frame_socks_serverLayout;
    QVBoxLayout* socks_authLayout;
    QHBoxLayout* frame_socks_authLayout;
    QVBoxLayout* tabLayout_2;
    QHBoxLayout* frame_http_serverLayout;
    QVBoxLayout* http_authLayout;
    QHBoxLayout* frame_http_authLayout;
    QHBoxLayout* button_layout;
    QSpacerItem* spacer_button;

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // KBSPROXYDIALOG_H
