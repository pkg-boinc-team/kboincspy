/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kiconloader.h>
#include <krun.h>

#include <kbsboincmonitor.h>
#include <kbswebtoolbar.h>

#include "kbswebmenu.h"

KBSWebMenu::KBSWebMenu(const QString &project, KBSWebToolBar *parent)
          : KPopupMenu(parent->toolBar(), project),
            m_project(project), m_monitor(parent->monitor())
{
  connect(this, SIGNAL(activated(int)), this, SLOT(slotActivated(int)));
  
  update();
}

QString KBSWebMenu::project() const
{
  return m_project;
}

KBSBOINCMonitor *KBSWebMenu::monitor() const
{
  return m_monitor;
}

void KBSWebMenu::update()
{
  const KBSBOINCAccount *account = m_monitor->account(m_project);
  if(NULL == account) return;
  
  clear();
  for(unsigned i = 0; i < account->gui_urls.gui_url.count(); ++i)
    insertItem(SmallIcon("bookmark"), account->gui_urls.gui_url[i].name, i, i);
}

void KBSWebMenu::slotActivated(int id)
{
  const KBSBOINCAccount *account = m_monitor->account(m_project);
  if(NULL == account) return;
  
  if(id >= int(account->gui_urls.gui_url.count())) return;
  
  const KURL url = account->gui_urls.gui_url[id].url;
  KRun::runURL(url, "text/html", false, false);
}

#include "kbswebmenu.moc"
