#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsastropulseconfigpage.ui'
**
** Created: Mon Feb 6 18:28:55 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsastropulseconfigpage.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <kurlrequester.h>
#include <qcheckbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSAstroPulseConfigPage as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSAstroPulseConfigPage::KBSAstroPulseConfigPage( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSAstroPulseConfigPage" );
    KBSAstroPulseConfigPageLayout = new QVBoxLayout( this, 11, 6, "KBSAstroPulseConfigPageLayout"); 

    log_group = new QGroupBox( this, "log_group" );
    log_group->setColumnLayout(0, Qt::Vertical );
    log_group->layout()->setSpacing( 6 );
    log_group->layout()->setMargin( 11 );
    log_groupLayout = new QVBoxLayout( log_group->layout() );
    log_groupLayout->setAlignment( Qt::AlignTop );

    location_layout = new QHBoxLayout( 0, 0, 6, "location_layout"); 

    location_label = new QLabel( log_group, "location_label" );
    location_layout->addWidget( location_label );

    kcfg_location = new KURLRequester( log_group, "kcfg_location" );
    kcfg_location->setMode( 10 );
    location_layout->addWidget( kcfg_location );
    log_groupLayout->addLayout( location_layout );

    kcfg_write = new QCheckBox( log_group, "kcfg_write" );
    log_groupLayout->addWidget( kcfg_write );
    KBSAstroPulseConfigPageLayout->addWidget( log_group );
    vspacer = new QSpacerItem( 21, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSAstroPulseConfigPageLayout->addItem( vspacer );
    languageChange();
    resize( QSize(250, 115).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSAstroPulseConfigPage::~KBSAstroPulseConfigPage()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSAstroPulseConfigPage::languageChange()
{
    setCaption( tr2i18n( "General preferences for AstroPulse" ) );
    setIconText( tr2i18n( "AstroPulse" ) );
    log_group->setTitle( tr2i18n( "Log" ) );
    location_label->setText( tr2i18n( "Location:" ) );
    kcfg_write->setText( tr2i18n( "&Write to log" ) );
    kcfg_write->setAccel( QKeySequence( tr2i18n( "Alt+W" ) ) );
}

#include "kbsastropulseconfigpage.moc"
