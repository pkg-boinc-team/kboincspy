/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsastropulselogx.h>

#include "kbsastropulselogmanager.h"

KBSAstroPulseLogManager *KBSAstroPulseLogManager::s_self = NULL;

KBSAstroPulseLogManager *KBSAstroPulseLogManager::self()
{
  if(NULL == s_self)
    s_self = new KBSAstroPulseLogManager();

  return s_self;
}

KBSAstroPulseLogManager::KBSAstroPulseLogManager(QObject *parent, const char *name)
                       : KBSLogManager(parent, name)
{
}

unsigned KBSAstroPulseLogManager::formats() const
{
  return 1;
}

KBSLogMonitor *KBSAstroPulseLogManager:: createLogMonitor(unsigned format, const KURL &url, QObject *parent)
{
  return (0 == format) ? new KBSAstroPulseLogX(url, parent) : NULL;
}

#include "kbsastropulselogmanager.moc"
