/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSASTROPULSEMONITOR_H
#define KBSASTROPULSEMONITOR_H

#include <qdict.h>
#include <qdom.h>
#include <qstringlist.h>

#include <kbsprojectmonitor.h>

#include <kbsastropulsedata.h>

class KBSAstroPulseMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSAstroPulseMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    ~KBSAstroPulseMonitor();
    
    virtual const KBSAstroPulseResult *result(const QString &workunit) const;
    
    virtual KBSLogManager *logManager() const;
    
  protected:
    virtual bool parseable(const QString &openName) const;
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    virtual KBSAstroPulseResult *mkResult(const QString &workunt);

  private:
    bool parseWorkunitDocument(const QDomDocument &document, KBSAstroPulseHeader &header);
    bool parseResultDocument(const QDomDocument &document, KBSAstroPulseOutput &output);
    
    void setHeader(const KBSAstroPulseHeader &header, const QStringList &workunits);
    void setOutput(const KBSAstroPulseOutput &output, const QStringList &workunits);
    
  private slots:
    void removeWorkunits(const QStringList &workunits);
    void updateFile(const QString &fileName);
  
  protected:
    QDict<KBSAstroPulseResult> m_results;
};

#endif
