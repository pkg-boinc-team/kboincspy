/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSASTROPULSEDATA_H
#define KBSASTROPULSEDATA_H

#include <qdatetime.h>
#include <qdom.h>
#include <qstring.h>
#include <qvaluelist.h>

struct KBSAstroPulseCoords
{
  double ra,
        dec;
  QDateTime jd;
  
  bool parse(const QDomElement &node);
};

struct KBSAstroPulseThresholds
{
  QValueList<double> threshold;
  
  bool parse(const QDomElement &node);
};

struct KBSAstroPulseHeader
{
  unsigned datasize;
  KBSAstroPulseCoords start_coords, end_coords;
  KBSAstroPulseThresholds thresholds;
  unsigned fft_len;
  struct {
    unsigned low,
             hi,
             chunk;
  } dm;
  unsigned max_coadd;
  
  bool parse(const QDomElement &node);
};

struct KBSAstroPulsePulse
{
  unsigned index;
  double peak_power;
  unsigned scale,
           dm;
  double period;
  QString time_series;
  
  bool parse(const QDomElement &node);
};

struct KBSAstroPulsePulses
{
  QValueList<KBSAstroPulsePulse> pulse;
  
  bool parse(const QDomElement &node);
  
  int best(double *peak_power=NULL) const;
};

struct KBSAstroPulseOutput
{
  unsigned protocol;
  KBSAstroPulseThresholds thresholds;
  KBSAstroPulsePulses pulses, best_pulses;
  
  bool parse(const QDomElement &node);
};

struct KBSAstroPulseResult
{
  KBSAstroPulseHeader header;
  KBSAstroPulseOutput astropulse_output;
};

#endif
