/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>

#include <kbsastropulselogmanager.h>

#include "kbsastropulsemonitor.h"

const QString AstroPulseWorkunitOpenName = "in.dat";
const QString AstroPulseResultOpenName = "pulse.out";

KBSAstroPulseMonitor::KBSAstroPulseMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name)
                    : KBSProjectMonitor(project, parent, name)
{
  connect(parent, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

KBSAstroPulseMonitor::~KBSAstroPulseMonitor()
{
  for(QDictIterator<KBSAstroPulseResult> it(m_results); it.current() != NULL; ++it)
    delete it.current();
  m_results.clear();
}

const KBSAstroPulseResult *KBSAstroPulseMonitor::result(const QString &workunit) const
{
  return validWorkunit(workunit) ? m_results.find(workunit) : NULL;
}

KBSLogManager *KBSAstroPulseMonitor::logManager() const
{
  return KBSAstroPulseLogManager::self();
}

bool KBSAstroPulseMonitor::parseable(const QString &openName) const
{
  return(openName == AstroPulseWorkunitOpenName || openName == AstroPulseResultOpenName);
}

bool KBSAstroPulseMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{  
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  if(!m_meta.contains(file->fileName)) return false;
  const KBSFileMetaInfo meta = m_meta[file->fileName];
  
  QDomDocument document(file->fileName);
  if(AstroPulseWorkunitOpenName == meta.open_name)
  {
    // avoid reading the "garbage" that comes after the header
    QStringList lines;    
    if(!readFile(fileName, lines, "</header>")) return false;
    lines << "</header>";
    
    if(!document.setContent(lines.join("\n"))) return false;
    
    KBSAstroPulseHeader header;
    if(!parseWorkunitDocument(document, header)) return false;
    
    setHeader(header, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(AstroPulseResultOpenName == meta.open_name)
  {
    if(!readFile(fileName, document)) return false;
    
    KBSAstroPulseOutput output;
    if(!parseResultDocument(document, output)) return false;
    
    setOutput(output, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else
    return false;
}

KBSAstroPulseResult *KBSAstroPulseMonitor::mkResult(const QString &workunit)
{
  KBSAstroPulseResult *result = m_results.find(workunit);
  
  if(NULL == result) {
    result = new KBSAstroPulseResult();
    m_results.insert(workunit, result);
  }
  
  return result;
}

bool KBSAstroPulseMonitor::parseWorkunitDocument(const QDomDocument &document, KBSAstroPulseHeader &header)
{
  for(QDomNode child = document.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "header") {
        if(!header.parse(element)) return false;
      }
    }
  
  return true;
}

bool KBSAstroPulseMonitor::parseResultDocument(const QDomDocument &document, KBSAstroPulseOutput &output)
{
  for(QDomNode child = document.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "astropulse_output") {
        output.protocol = element.attribute("protocol", "2").toUInt(0, 10);
        if(!output.parse(element)) return false;
      }
    }
  
  return true;
}

void KBSAstroPulseMonitor::setHeader(const KBSAstroPulseHeader &header, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    mkResult(*workunit)->header = header;
}

void KBSAstroPulseMonitor::setOutput(const KBSAstroPulseOutput &output, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    mkResult(*workunit)->astropulse_output = output;
}

void KBSAstroPulseMonitor::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    KBSAstroPulseResult *result = m_results.take(*workunit);
    if(NULL != result) delete result;
  }
}

void KBSAstroPulseMonitor::updateFile(const QString &fileName)
{
  if(!m_meta.contains(fileName)) return;
  
  QStringList workunits = m_meta[fileName].workunits;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    emit updatedResult(*workunit);
}

#include "kbsastropulsemonitor.moc"
