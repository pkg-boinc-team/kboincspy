/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincdata.h>

#include "kbsastropulsedata.h"

using namespace KBSBOINC;

bool KBSAstroPulseHeader::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "datasize")
        datasize = element.text().toUInt(0, 10);
      else if(elementName == "start_coords") {
        if(!start_coords.parse(element)) return false;
      } else if(elementName == "end_coords") {
        if(!end_coords.parse(element)) return false;
      } else if(elementName == "thresholds") {
        if(!thresholds.parse(element)) return false;
      } else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "dm_low")
        dm.low = element.text().toUInt(0, 10);
      else if(elementName == "dm_hi")
        dm.hi = element.text().toUInt(0, 10);
      else if(elementName == "dm_chunk")
        dm.chunk = element.text().toUInt(0, 10);
      else if(elementName == "max_coadd")
        max_coadd = element.text().toUInt(0, 10);
      else if(element.nodeName().startsWith("thresh_")) {
        const unsigned coadd = element.nodeName().right(7).toUInt(0, 10);
        thresholds.threshold[coadd] = element.text().toDouble();
      }
    }
  
  return true;
}

bool KBSAstroPulseCoords::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "dec")
        dec = element.text().toDouble();
      else if(elementName == "jd")
        jd = parseJulianDate(element.text());
    }
  
  return true;
}

bool KBSAstroPulseThresholds::parse(const QDomElement &node)
{
  threshold.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "threshold") {
        const unsigned coadd = element.attribute("coadd", "0").toUInt(0, 10);
        threshold[coadd] = element.text().toDouble();
      }
    }
  
  return true;
}

bool KBSAstroPulsePulse::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "index")
        index = element.text().toUInt(0, 10);
      else if(elementName == "peak_power")
        peak_power = element.text().toDouble();
      else if(elementName == "scale")
        scale = element.text().toUInt(0, 10);
      else if(elementName == "dm")
        dm = element.text().toUInt(0, 10);
      else if(elementName == "period")
        period = element.text().toDouble();
      else if(elementName == "time_series")
        time_series = element.text();
    }
  
  return true;
}

bool KBSAstroPulsePulses::parse(const QDomElement &node)
{
  pulse.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "pulse") {
        KBSAstroPulsePulse item;
        
        if(item.parse(element)) pulse << item;
        else return false;
      }
    }
  
  return true;
}

int KBSAstroPulsePulses::best(double *peak_power) const
{
  const unsigned count = pulse.count();
  if(count == 0) return -1;
  
  unsigned best = 0;
  double best_peak_power = pulse.first().peak_power;
  for(unsigned i = 1; i < count; ++i) {
    const double peak_power = pulse[i].peak_power;
    if(peak_power > best_peak_power) {
      best = i;
      best_peak_power = peak_power;
    }
  }
  
  if(NULL != peak_power) *peak_power = best_peak_power;
  return best;
}

bool KBSAstroPulseOutput::parse(const QDomElement &node)
{
  thresholds.threshold.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "thresholds") {
        if(!thresholds.parse(element)) return false;
      } else if(elementName == "pulses") {
        if(!pulses.parse(element)) return false;
      } else if(elementName == "best_pulses") {
        if(!best_pulses.parse(element)) return false;
      } else if(elementName == "results") {
        if(!pulses.parse(element)) return false;
      } else if(elementName == "protocol_version")
        protocol = element.text().toUInt(0, 10);
      else if(element.nodeName().startsWith("thresh_")) {
        const unsigned coadd = element.nodeName().right(7).toUInt(0, 10);
        thresholds.threshold[coadd] = element.text().toDouble();
      }
    }
  
  return true;
}
