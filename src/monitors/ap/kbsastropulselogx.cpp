/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qtextstream.h>

#include <krfcdate.h>

#include <kbsastropulsemonitor.h>
#include <kbsboincmonitor.h>

#include <kbsastropulsedata.h>
#include <kbsboincdata.h>

#include "kbsastropulselogx.h"

using namespace KBSBOINC;

const QString KBSAstroPulseLogX::s_version = "0.10";
const QString KBSAstroPulseLogX::s_filename = "ap_boinc.csv";

KBSAstroPulseLogX::KBSAstroPulseLogX(const KURL &url, QObject *parent, const char *name)
                 : KBSLogMonitor(url, parent, name)
{
  initKeys();
  
  addLogFile(s_filename);

  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

QStringList KBSAstroPulseLogX::keys() const
{
  return m_keys;
}

bool KBSAstroPulseLogX::hasResults() const
{
  return true;
}

bool KBSAstroPulseLogX::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(s_filename == file->fileName)
    return parseLogDocument(lines);
  else
    return false;
}

KBSLogDatum KBSAstroPulseLogX::formatWorkunitDatum(KBSAstroPulseMonitor *projectMonitor,
                                                   const QString &workunit) const
{
  const QString project = projectMonitor->project();
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogDatum();
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSAstroPulseResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogDatum();
  
  double best_peak_power;
  int best_pulse = datum->astropulse_output.pulses.best(&best_peak_power);
    
  KBSLogDatum out;
  
  out["date"] = formatLogEntryDate(QDateTime::currentDateTime());
  out["project_name"] = state->project[project].project_name;
  out["app_name"] = state->workunit[workunit].app_name;
  out["domain_name"] = state->host_info.domain_name;
  out["p_ncpus"] = state->host_info.p.ncpus;
  out["p_vendor"] = state->host_info.p.vendor;
  out["p_model"] = state->host_info.p.model;
  out["p_fpops"] = state->host_info.p.fpops;
  out["p_iops"] = state->host_info.p.iops;
  out["p_membw"] = state->host_info.p.membw;
  out["p_calculated"] = state->host_info.p.calculated;
  out["os_name"] = state->host_info.os.name;
  out["os_version"] = state->host_info.os.version;
  out["m_nbytes"] = state->host_info.m.nbytes;
  out["m_cache"] = state->host_info.m.cache;
  out["m_swap"] = state->host_info.m.swap;
  out["d_total"] = state->host_info.d.total;
  out["d_free"] = state->host_info.d.free;
  out["on_frac"] = state->time_stats.on_frac;
  out["connected_frac"] = state->time_stats.connected_frac;
  out["active_frac"] = state->time_stats.active_frac;
  out["last_update"] = formatUNIXDate(state->time_stats.last_update);
  out["bwup"] = state->net_stats.bwup;
  out["bwdown"] = state->net_stats.bwdown;
  out["user_name"] = state->project[project].user_name;
  out["team_name"] = state->project[project].team_name;
  out["user_total_credit"] = state->project[project].user.total_credit;
  out["user_expavg_credit"] = state->project[project].user.expavg_credit;
  out["user_create_time"] = formatJulianDate(state->project[project].user.create_time);
  out["rpc_seqno"] = state->project[project].rpc_seqno;
  out["hostid"] = state->project[project].hostid;
  out["host_total_credit"] = state->project[project].host.total_credit;
  out["host_expavg_credit"] = state->project[project].host.expavg_credit;
  out["host_create_time"] = formatJulianDate(state->project[project].host.create_time);
  out["exp_avg_cpu"] = state->project[project].exp_avg.cpu;
  out["exp_avg_mod_time"] = state->project[project].exp_avg.mod_time;
  out["host_venue"] = state->host_venue;
  out["boinc_version"] = formatVersion(state->core_client.major_version, state->core_client.minor_version);
  out["logX_version"] = s_version;
  out["ap_version"] = formatVersion(state->workunit[workunit].version_num);
  out["wu_name"] = workunit;
  if(state->result[result].file_ref.count() > 0)
    out["result_name"] = state->result[result].file_ref.first().file_name;
  else
    out["result_name"] = result;
  out["start_ra"] = datum->header.start_coords.ra;
  out["start_dec"] = datum->header.start_coords.dec;
  out["end_ra"] = datum->header.start_coords.ra;
  out["end_dec"] = datum->header.end_coords.ra;
  out["datasize"] = datum->header.datasize;
  out["time_recorded"] = formatJulianDate(datum->header.start_coords.jd);
  out["fft_len"] = datum->header.fft_len;
  out["dm_low"] = datum->header.dm.low;
  out["dm_hi"] = datum->header.dm.hi;
  out["max_coadd"] = datum->header.max_coadd;
  out["dm_chunk"] = datum->header.dm.chunk;
  out["thresh_0"] = datum->header.thresholds.threshold[0];
  out["cpu"] = state->result[result].final_cpu_time;
  out["fpops_est"] = state->workunit[workunit].rsc.fpops_est;
  out["error"] = "false";
  if(best_pulse >= 0)
  {
    out["peak_power"] = best_peak_power;
    out["peak_index"] = datum->astropulse_output.pulses.pulse[best_pulse].index;
    out["peak_scale"] = datum->astropulse_output.pulses.pulse[best_pulse].scale;
    out["peak_dm"] = datum->astropulse_output.pulses.pulse[best_pulse].dm;
    out["peak_period"] = datum->astropulse_output.pulses.pulse[best_pulse].period;
  }
  out["peak_count"] = unsigned(datum->astropulse_output.pulses.pulse.count());
  
  return out;
}

QMap<QString,KBSLogData> KBSAstroPulseLogX::formatWorkunit(KBSProjectMonitor *monitor,
                                                           const QString &workunit) const
{
  KBSAstroPulseMonitor *projectMonitor = static_cast<KBSAstroPulseMonitor*>(monitor);
  QMap<QString,KBSLogData> out;
   
  out[s_filename] << formatWorkunitDatum(projectMonitor, workunit);
  
  return out;
}

void KBSAstroPulseLogX::appendHeader(const KBSFileInfo *info, QIODevice *io)
{
  QTextStream text(io);
  
  if(info->fileName == s_filename)
    text << formatCSVKeys(m_keys) << "\r\n";
}

void KBSAstroPulseLogX::appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum)
{
  QTextStream text(io);
  
  if(info->fileName == s_filename)
    text << formatCSVDatum(datum, m_keys) << "\r\n";
}

void KBSAstroPulseLogX::initKeys()
{
  m_keys.clear(); m_keys
    << "date" << "domain_name" << "p_ncpus" << "p_vendor" << "p_model" << "p_fpops"
    << "p_iops" << "p_membw" << "p_calculated" << "os_name" << "os_version"
    << "m_nbytes" << "m_cache" << "m_swap" << "d_total" << "d_free" << "on_frac"
    << "connected_frac" << "active_frac" << "last_update" << "bwup" << "bwdown"
    << "user_name" << "team_name" << "user_total_credit"
    << "user_expavg_credit" << "user_create_time" << "rpc_seqno" << "hostid"
    << "host_total_credit" << "host_expavg_credit" << "host_create_time"
    << "exp_avg_cpu" << "exp_avg_mod_time" << "host_venue" << "boinc_version"
    << "logX_version" << "ap_version" << "wu_name" << "result_name" << "start_ra"
    << "start_dec" << "end_ra" << "end_dec" << "datasize" << "time_recorded"
    << "fft_len" << "dm_low" << "dm_hi" << "max_coadd" << "dm_chunk" << "thresh_0"
    << "cpu" << "error" << "fpops_est" << "peak_power" << "peak_index"
    << "peak_scale" << "peak_dm" << "peak_period" << "peak_count";
}

bool KBSAstroPulseLogX::parseLogDocument(const QStringList &lines)
{
  QStringList::const_iterator line = lines.begin();
  
  if(lines.end() == line) return true;
  const unsigned keys = m_keys.count();
  m_keys = parseCSVKeys(*line);
  if(keys > m_keys.count()) return false;
  ++line;
  
  for(unsigned i = 0; i < m_workunits.count(); ++i)
    if(lines.end() != line) ++line;
    else return true;
    
  while(lines.end() != line) {
    KBSLogDatum datum = parseCSVDatum(*line, m_keys);
    ++line;
    
    datum["date"] = parseLogEntryDate(datum["date"].toString());
    datum["last_update"] = parseUNIXDate(datum["last_update"].toDouble());
    datum["user_create_time"] = parseUNIXDate(datum["user_create_time"].toDouble());
    datum["host_create_time"] = parseUNIXDate(datum["host_create_time"].toDouble());
    datum["date_recorded"] = parseJulianDate(datum["date_recorded"].toDouble());
    
    m_workunits << datum;
  }
  
  qDebug("... parse OK");
  
  return true;
}

void KBSAstroPulseLogX::updateFile(const QString &)
{
  emit workunitsUpdated();
}

#include "kbsastropulselogx.moc"
