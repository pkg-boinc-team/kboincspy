/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbsprojectmonitor.h>

#include <kbsastropulseconfigpage.h>
#include <kbsastropulselogmanager.h>
#include <kbsastropulsemonitor.h>

#include "kbsastropulseplugin.h"

class KBSAstroPulsePluginFactory : KGenericFactory<KBSAstroPulsePlugin,KBSDocument>
{
  public:
    KBSAstroPulsePluginFactory() : KGenericFactory<KBSAstroPulsePlugin,KBSDocument>("kbsastropulsemonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbsastropulsemonitor, KBSAstroPulsePluginFactory());

KBSAstroPulsePlugin::KBSAstroPulsePlugin(KBSDocument *parent, const char *name, const QStringList &)
                   : KBSProjectPlugin(parent, name)
{
  KBSAstroPulseLogManager *log = KBSAstroPulseLogManager::self();
  
  log->setCurrentFormat(0);
  log->setInterval(parent->interval());
  connect(parent, SIGNAL(intervalChanged(int)), log, SLOT(setInterval(int)));
}

QPtrList<QWidget> KBSAstroPulsePlugin::createConfigPages()
{
  QPtrList<QWidget> out;
  out.append(new KBSAstroPulseConfigPage(0, "astropulse"));
   
  return out;
}

KBSProjectMonitor *KBSAstroPulsePlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSAstroPulseMonitor(project, parent);
}

KBSTaskMonitor *KBSAstroPulsePlugin::createTaskMonitor(unsigned, KBSBOINCMonitor *)
{
  return NULL;
}

KConfigSkeleton *KBSAstroPulsePlugin::preferences()
{
  return &m_preferences;
}

void KBSAstroPulsePlugin::applyPreferences()
{
  KBSAstroPulseLogManager *log = KBSAstroPulseLogManager::self();
  
  log->setURL(m_preferences.location());
  log->setWriteMask(m_preferences.write() ? 1 : 0);
}

void KBSAstroPulsePlugin::readConfig(KConfig *)
{
  m_preferences.readConfig();
  applyPreferences();
}

void KBSAstroPulsePlugin::writeConfig(KConfig *)
{
  m_preferences.writeConfig();
}

#include "kbsastropulseplugin.moc"
