/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSCPDNMONITOR_H
#define KBSCPDNMONITOR_H

#include <qdict.h>
#include <qdom.h>
#include <qstringlist.h>

#include <kprocess.h>
#include <kurl.h>

#include <kbsprojectmonitor.h>

#include <kbscpdndata.h>

class KBSCPDNMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSCPDNMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    virtual ~KBSCPDNMonitor();
    
    virtual const KBSCPDNUMID *result(const QString &workunit) const;
    
    virtual KBSLogManager *logManager() const;
    
    virtual KURL graphicsURL() const;
    virtual bool canShowGraphics(const QString &workunit=QString::null) const;
    virtual void showGraphics(const QString &workunit);
    
  protected:
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    static QString parseFileName(const QString &fileName);
    static QString formatFileName(const QString &workunit);

  private slots:
    void addWorkunits(const QStringList &workunits);
    void removeWorkunits(const QStringList &workunits);
    void activateWorkunit(unsigned task, const QString &workunit, bool active);
  
  private:
    bool parseResultDocument(const QDomDocument &document, KBSCPDNUMID &umid);
    
  private slots:
    void updateFile(const QString &fileName);
    
    void slotProcessExited(KProcess *process);
      
  protected:
    QDict<KBSCPDNUMID> m_results;
    QDict<KProcess> m_processes;
};

#endif
