/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSCPDNDATA_H
#define KBSCPDNDATA_H

#include <qdom.h>
#include <qstringlist.h>

const unsigned KBSCPDNPhaseTS = 259248;
const unsigned KBSCPDNPhaseTrickles = 24;
const unsigned KBSCPDNTrickleTS = KBSCPDNPhaseTS / KBSCPDNPhaseTrickles;

struct KBSCPDNUMID
{
  unsigned v,
           md;
  QString n;
  unsigned ph,
           ts,
           day,
           mth,
           yr,
           hr,
           min,
           sec,
           csf,
           tr,
           st,
           rs,
           rsc,
           rsdt,
           rsmt,
           rsyt;
  QStringList rsd,
              rsm,
              rsy,
              cs;
  
  bool parse(const QDomElement &node);
};

#endif
