/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbsprojectmonitor.h>

#include <kbscpdnmonitor.h>

#include "kbscpdnplugin.h"

class KBSCPDNPluginFactory : KGenericFactory<KBSCPDNPlugin,KBSDocument>
{
  public:
    KBSCPDNPluginFactory() : KGenericFactory<KBSCPDNPlugin,KBSDocument>("kbscpdnmonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbscpdnmonitor, KBSCPDNPluginFactory());

KBSCPDNPlugin::KBSCPDNPlugin(KBSDocument *parent, const char *name, const QStringList &)
             : KBSProjectPlugin(parent, name)
{
}

KBSProjectMonitor *KBSCPDNPlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSCPDNMonitor(project, parent);
}

KBSTaskMonitor *KBSCPDNPlugin::createTaskMonitor(unsigned, KBSBOINCMonitor *)
{
  return NULL;
}

void KBSCPDNPlugin::readConfig(KConfig *)
{
}

void KBSCPDNPlugin::writeConfig(KConfig *)
{
}

#include "kbscpdnplugin.moc"
