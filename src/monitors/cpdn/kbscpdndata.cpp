/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "kbscpdndata.h"

bool KBSCPDNUMID::parse(const QDomElement &node)
{
  rsd.clear();
  rsm.clear();
  rsy.clear();
  cs.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower();
      
      if(elementName == "v")
        v = element.text().toUInt(0, 10);
      else if(elementName == "md")
        md = element.text().toUInt(0, 10);
      else if(elementName == "n")
        n = element.text();
      else if(elementName == "ph")
        ph = element.text().toUInt(0, 10);
      else if(elementName == "ts")
        ts = element.text().toUInt(0, 10);
      else if(elementName == "day")
        day = element.text().toUInt(0, 10);
      else if(elementName == "mth")
        mth = element.text().toUInt(0, 10);
      else if(elementName == "yr")
        yr = element.text().toUInt(0, 10);
      else if(elementName == "hr")
        hr = element.text().toUInt(0, 10);
      else if(elementName == "min")
        min = element.text().toUInt(0, 10);
      else if(elementName == "sec")
        sec = element.text().toUInt(0, 10);
      else if(elementName == "csf")
        csf = element.text().toUInt(0, 10);
      else if(elementName == "tr")
        tr = element.text().toUInt(0, 10);
      else if(elementName == "st")
        st = element.text().toUInt(0, 10);
      else if(elementName == "rs")
        rs = element.text().toUInt(0, 10);
      else if(elementName == "rsc")
        rsc = element.text().toUInt(0, 10);
      else if(elementName == "rsdt")
        rsdt = element.text().toUInt(0, 10);
      else if(elementName == "rsmt")
        rsmt = element.text().toUInt(0, 10);
      else if(elementName == "rsyt")
        rsyt = element.text().toUInt(0, 10);
      else if(elementName == "rsd")
        rsd << element.text();
      else if(elementName == "rsm")
        rsm << element.text();
      else if(elementName == "rsy")
        rsy << element.text();
      else if(elementName == "cs") {
        const QString item = element.text();
        
        if("BLANK" != item) cs << item;
      }
    }
  
  return true;
}
