/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCTASKMONITOR_H
#define KBSLHCTASKMONITOR_H

#include <qdatastream.h>
#include <qintdict.h>

#include <kbstaskmonitor.h>

#include <kbslhcdata.h>

class KBSLHCInterpolator;

class KBSLHCTaskMonitor : public KBSTaskMonitor
{
  Q_OBJECT
  public:
    KBSLHCTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name=0);
    
    virtual const KBSLHCState *state() const;
    virtual KBSLHCInterpolator *interpolator(unsigned set, unsigned position);
  
  signals:
    void updatedSet(unsigned set);
  
  protected:
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    static int parseFileName(const QString &fileName);
    static QString formatFileName(unsigned set);
  
  private:
    KBSLHCInterpolator *mkInterpolator(unsigned set, unsigned position);
  
  private slots:
    void updateFile(const QString &fileName);
    
  protected:
    KBSLHCState m_state;
    QIntDict<KBSLHCInterpolator> m_interpolators;
};

#endif
