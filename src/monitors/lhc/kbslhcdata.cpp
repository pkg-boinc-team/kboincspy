/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>

#include "kbslhcdata.h"

bool KBSLHCUnit3::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
#define getNextLine() \
  { \
    ++line; \
    while(lines.constEnd() != line && (*line).startsWith("/")) \
      ++line; \
    if(lines.constEnd() == line) return false; \
  }
  
  while(lines.constEnd() != line)
    if((*line).startsWith("/"))
      ++line;
    else if((*line).startsWith("ENDE"))
      break;
    else if((*line).startsWith("FREE") || (*line).startsWith("GEOM"))
    {
      geom.keyword = (*line).mid(4, 10).stripWhiteSpace();
      geom.title = (*line).mid(14).stripWhiteSpace();
      
      ++line;
    }
    else if((*line).startsWith("TRAC"))
    {
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%u %u %u %lf %lf %u %u",
             &trac.numl,
             &trac.numlr,
             &trac.napx,
             &trac.amp[0],
             &trac.amp[1],
             &trac.ird,
             &trac.imc);
      
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%u %u %u %u %u",
             &trac.idy[0],
             &trac.idy[1],
             &trac.idfor,
             &trac.irew,
             &trac.iclo6);
      
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%u %u %u %u %u %u %u %u",
             &trac.nde[0],
             &trac.nde[1],
             &trac.nwr[0],
             &trac.nwr[1],
             &trac.nwr[2],
             &trac.nwr[3],
             &trac.ntwin,
             &trac.ibidu);
      
      getNextLine();
      
      if("NEXT" != *line) return false;
      
      ++line;
    }
    else if((*line).startsWith("INIT"))
    {
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%u %lf %lf %lf %u",
             &init.itra,
             &init.chi0,
             &init.chid,
             &init.rat,
             &init.iver);
      
      getNextLine();
      
      init.coord[0].x.mm = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].x.mrad = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].y.mm = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].y.mrad = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].plen = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].rmdev = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].x.mm = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].x.mrad = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].y.mm = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].y.mrad = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].plen = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].rmdev = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.regy = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[0].egy = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      init.coord[1].egy = (*line).stripWhiteSpace().toDouble();
      
      getNextLine();
      
      if("NEXT" != *line) return false;
      
      ++line;
    }
    else if((*line).startsWith("SYNC"))
    {
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%d %lf %lf %lf %lf %lf %d %lf",
             &sync.harm,
             &sync.alc,
             &sync.u0,
             &sync.phag,
             &sync.tlen,
             &sync.pma,
             &sync.ition,
             &sync.dppoff);
      
      getNextLine();
      
      sscanf((*line).stripWhiteSpace(), "%lf %lf",
             &sync.dpscor,
             &sync.sigcor);
      
      getNextLine();
      
      if("NEXT" != *line) return false;
      
      ++line;
    }
    else
    {
      getNextLine();
      
      while("NEXT" != *line)
        getNextLine();
        
      ++line;
    }
   
  return true;

#undef getNextLine
}

bool KBSLHCHeader::parse(QDataStream &stream)
{
  unsigned check[2], bytes = 0;
  
#define readDouble(__var) { stream >> __var; bytes += 8; }
#define readUInt(__var) { stream >> (Q_UINT32 &) __var; bytes += 4;}
#define readString(__var, __len) { char __s[__len+1]; stream.readRawBytes(__s, __len); \
                                   __s[__len] = '\0'; __var = QString(__s).stripWhiteSpace(); \
                                   bytes += __len; }
#define skip(__bytes) { char __s[__bytes]; stream.readRawBytes(__s, __bytes); bytes += __bytes; }

  readUInt(check[0]);
  bytes = 0;
  
  readString(title[0], 80);
  if(title[0].isEmpty()) return false;
  readString(title[1], 80);
  
  {
    QString date_string;
    readString(date_string, 8);
    const unsigned day = date_string.mid(0, 2).toUInt(0, 10);
    const unsigned mth = date_string.mid(2, 2).toUInt(0, 10);
    const unsigned yr = 2000 + date_string.mid(4, 2).toUInt(0, 10);
    
    QString time_string;
    readString(time_string, 8);
    const unsigned hr = time_string.mid(0, 2).toUInt(0, 10);
    const unsigned min = time_string.mid(2, 2).toUInt(0, 10);
    const unsigned sec = (time_string.length() > 4) ? time_string.mid(4, 2).toUInt(0, 10) : 0;
    
    time = QDateTime(QDate(yr, mth, day), QTime(hr, min, sec));
  }
  
  readString(prog, 8);
  
  readUInt(part.fst);
  readUInt(part.last);
  readUInt(part.count);
  
  readUInt(phdim);
  readUInt(expl);
  
  readDouble(tune.hor);
  readDouble(tune.ver);
  readDouble(tune.lng);
  
  for(unsigned i = 0; i < 6; ++i)
    readDouble(clorb[i]);
  
  for(unsigned i = 0; i < 6; ++i)
    readDouble(disp[i]);
    
  for(unsigned i = 0; i < 6; ++i)
    for(unsigned j = 0; j < 6; ++j)
      readDouble(tfer[i][j]);
  
  readDouble(smax);
  readDouble(seed);
  readDouble(sstart);
  
  readDouble(numlr);
  
  readDouble(lyap[0]);
  readDouble(lyap[1]);
  
  readDouble(ripro);
  
  skip(43 * 8);
  
  if(check[0] != bytes) return false;
  readUInt(check[1]);
  if(check[0] != check[1]) return false;

  return true;

#undef skip  
#undef readUInt
#undef readDouble
#undef readString
}

bool KBSLHCDatum::parse(QDataStream &stream, unsigned &bytes)
{
#define readUInt(__var) { stream >> (Q_UINT32 &) __var; bytes += 4; }
#define readDouble(__var) { stream >> __var; bytes += 8; }

  readUInt(part);
      
  readDouble(phad);
      
  readDouble(coord.x.mm);
  readDouble(coord.x.mrad);
      
  readDouble(coord.y.mm);
  readDouble(coord.y.mrad);
      
  readDouble(coord.plen);
  readDouble(coord.rmdev);
  readDouble(coord.egy);
  
  return true;

#undef readDouble
#undef readUInt
}

bool KBSLHCOutput::parse(QDataStream &stream)
{
  if(stream.atEnd()) return false;
  
  if(!header.parse(stream)) return false;
  
  unsigned count = header.part.last - header.part.fst + 1;
  if(count > 2) count = 2;
  
  for(unsigned i = 0; i < 2; ++i)
    data[i].clear();
  
  unsigned check[2], bytes = 0;
    
#define readUInt(__var) { stream >> (Q_UINT32 &) __var; bytes += 4; }
  
  while(!stream.atEnd())
  {
    readUInt(check[0]);
    bytes = 0;
    
    unsigned turn;
    readUInt(turn);
    
    for(unsigned i = 0; i < count; ++i)
    {
      KBSLHCDatum datum;
      if(!datum.parse(stream, bytes)) return false;
      
      data[i].insert(turn, datum);
    }
    
    if(check[0] != bytes) return false;
    readUInt(check[1]);
    if(check[0] != check[1]) return false;
  }
  
  return true;

#undef readUInt
}

