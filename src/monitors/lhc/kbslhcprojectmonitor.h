/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCPROJECTMONITOR_H
#define KBSLHCPROJECTMONITOR_H

#include <qdict.h>
#include <qstringlist.h>

#include <kbsprojectmonitor.h>

#include <kbslhcdata.h>

class KBSLHCProjectMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSLHCProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    virtual ~KBSLHCProjectMonitor();
    
    virtual const KBSLHCResult *result(const QString &workunit);

    virtual KBSLogManager *logManager() const;
    
  protected:
    virtual bool parseable(const QString &openName) const;
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    virtual KBSLHCResult *mkResult(const QString &workunit);

  private slots:
    void removeWorkunits(const QStringList &workunits);
  
  private:
    void setUnit3(const KBSLHCUnit3 &unit3, const QStringList &workunits);
  
  private slots:
    void updateFile(const QString &fileName);
    
  protected:
    QDict<KBSLHCResult> m_results;
};

#endif
