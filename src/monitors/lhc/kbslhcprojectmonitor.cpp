/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qfile.h>

#include <ktempdir.h>
#include <kzip.h>

#include <kbsboincmonitor.h>

#include "kbslhcprojectmonitor.h"

const QString KBSLHCWorkunitOpenName = "fort.zip";

const QString KBSLHCUnit3FileName = "fort.3";

KBSLHCProjectMonitor::KBSLHCProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name)
                     : KBSProjectMonitor(project, parent, name)
{
  connect(parent, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

KBSLHCProjectMonitor::~KBSLHCProjectMonitor()
{
  for(QDictIterator<KBSLHCResult> it(m_results); it.current() != NULL; ++it)
    delete it.current();
  m_results.clear();
}

const KBSLHCResult *KBSLHCProjectMonitor::result(const QString &workunit)
{
  return validWorkunit(workunit) ? m_results.find(workunit) : NULL;
}

KBSLogManager *KBSLHCProjectMonitor::logManager() const
{
  return NULL;
}
    
bool KBSLHCProjectMonitor::parseable(const QString &openName) const
{
  return(openName == KBSLHCWorkunitOpenName);
}

bool KBSLHCProjectMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  if(!m_meta.contains(file->fileName)) return false;
  const KBSFileMetaInfo meta = m_meta[file->fileName];
  
  if(KBSLHCWorkunitOpenName == meta.open_name)
  {
    KZip zip(fileName);
    if(!zip.open(IO_ReadOnly)) return false;
    
    const KArchiveEntry *entry = zip.directory()->entry(KBSLHCUnit3FileName);
    if(NULL == entry || !entry->isFile()) return false;
    
    KTempDir dir;
    if(dir.name().isNull()) return false;
    
    static_cast<KArchiveFile const *>(entry)->copyTo(dir.name());
    const QString unzippedFileName = dir.name() + KBSLHCUnit3FileName;
    
    QStringList lines;
    const bool isOK = readFile(unzippedFileName, lines);
    
    (void) QFile::remove(unzippedFileName);
    dir.unlink();
    
    if(!isOK) return false;
    
    KBSLHCUnit3 unit3;
    if(!unit3.parse(lines)) return false;
    
    setUnit3(unit3, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else
    return false;
}

KBSLHCResult *KBSLHCProjectMonitor::mkResult(const QString &workunit)
{
  KBSLHCResult *result = m_results.find(workunit);
  if(NULL == result) {
    result = new KBSLHCResult();
    m_results.insert(workunit, result);
  }
  
  return result;
}

void KBSLHCProjectMonitor::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    KBSLHCResult *result = m_results.take(*workunit);
    if(NULL != result) delete result;
  }
}

void KBSLHCProjectMonitor::setUnit3(const KBSLHCUnit3 &unit3, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    mkResult(*workunit)->unit3 = unit3;
}

void KBSLHCProjectMonitor::updateFile(const QString &fileName)
{
  if(!m_meta.contains(fileName)) return;
  
  QStringList workunits = m_meta[fileName].workunits;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    emit updatedResult(*workunit);
}

#include "kbslhcprojectmonitor.moc"
