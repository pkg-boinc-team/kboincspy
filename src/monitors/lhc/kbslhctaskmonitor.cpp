/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qfile.h>

#include <kbsboincmonitor.h>

#include <kbslhcinterpolator.h>
#include <kbslhcprojectmonitor.h>

#include "kbslhctaskmonitor.h"

const QString LHCUnitPrefix = "fort.";

KBSLHCTaskMonitor::KBSLHCTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name)
                 : KBSTaskMonitor(task, parent, name)
{
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
  
  for(unsigned set = 0; set < 32; ++set)
    addFile(formatFileName(set));
}

const KBSLHCState *KBSLHCTaskMonitor::state() const
{
  return &m_state;
}

KBSLHCInterpolator *KBSLHCTaskMonitor::interpolator(unsigned set, unsigned position)
{
  return file(formatFileName(set))->ok ? mkInterpolator(set, position) : NULL;
}

bool KBSLHCTaskMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  const int set = parseFileName(file->fileName);
  if(set < 0 || set >= 32) return false;
  
  QFile f(fileName);
  if(!f.open(IO_ReadOnly)) return false;
  
  QDataStream data(&f);
  data.setByteOrder(QDataStream::LittleEndian);
  
  const bool isOK = m_state.output[set].parse(data);
  if(!isOK) m_state.output.remove(set);
  
  f.close();
  
  if(isOK) qDebug("... parse OK");
  
  return isOK;
}

int KBSLHCTaskMonitor::parseFileName(const QString &fileName)
{
  if(!fileName.startsWith(LHCUnitPrefix)) return -1;
  
  return(90 - QString(fileName).remove(LHCUnitPrefix).toInt(0, 10));
}

QString KBSLHCTaskMonitor::formatFileName(unsigned set)
{
  return(LHCUnitPrefix + QString::number(90 - set));
}

KBSLHCInterpolator *KBSLHCTaskMonitor::mkInterpolator(unsigned set, unsigned position)
{
  const unsigned particle = 2 * set + position;
  KBSLHCInterpolator *interpolator = m_interpolators.find(particle);
  if(NULL == interpolator) {
    interpolator = new KBSLHCInterpolator(set, position, this);
    m_interpolators.insert(particle, interpolator);
  }
  
  return interpolator;
}

void KBSLHCTaskMonitor::updateFile(const QString &fileName)
{
  const int set = parseFileName(fileName);
  if(set >= 0) emit updatedSet(set);
  
  emit updatedState();
}

#include "kbslhctaskmonitor.moc"
