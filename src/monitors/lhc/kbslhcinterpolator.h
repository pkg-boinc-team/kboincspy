/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSLHCINTERPOLATOR_H
#define KBSLHCINTERPOLATOR_H

#include <qobject.h>
#include <qvaluelist.h>

#include <kbslhcdata.h>

class KBSLHCTaskMonitor;

class KBSLHCInterpolator : public QObject
{
  Q_OBJECT
  public:
    KBSLHCInterpolator(unsigned set, unsigned position,
                       KBSLHCTaskMonitor *parent, const char *name=0);
    
    unsigned set() const;
    unsigned position() const;
    
    double interpolateXCoord(double turn);
    double interpolateYCoord(double turn);
    double interpolateEnergy(double turn);
  
  protected:
    KBSLHCTaskMonitor *taskMonitor();
    
  private:
    void resetIndices();
    void computeIndices(double turn);
    void computeCoefficients(double turn);
  
  private slots:
    void update();
    void update(unsigned set);
  
  protected:
    QMap<unsigned,KBSLHCDatum> m_data;
    QValueList<unsigned> m_indices;
    
  private:
    unsigned m_set, m_position;
    enum Index {PrevInf, Inf, Sup, SuccSup, MaxDegree};
    QValueList<unsigned>::const_iterator m_index[MaxDegree];
    
    double m_turn;
    double m_numerator[MaxDegree], m_denominator[MaxDegree];
    int m_start, m_end;
};

#endif
