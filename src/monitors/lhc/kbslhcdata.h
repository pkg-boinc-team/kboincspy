/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSKBSLHCDATA_H
#define KBSKBSLHCDATA_H

#include <qdatetime.h>
#include <qmap.h>
#include <qstringlist.h>

/*
   References:
     F. Schmidt, "SixTrack Version 3.1 User's Reference Manual", CERN/SL/94-56 (AP), March 2000
     http://frs.home.cern.ch/frs/report/six.ps.gz
*/

struct KBSLHCGeom {
  QString keyword,
          title;
};

struct KBSLHCTrac {
  unsigned numl,          // Number of turns in the forward direction
           numlr,         // Number of turns in the backward direction
           napx;          // Number of amplitude variations
  double amp[2];          // Start and end amplitude in the horizontal phase space plane
  unsigned ird,           // Type of amplitude variation
           imc,           // Number of variations of the relative momentum deviation
           idy[2],        // Coupling switches
           idfor,         // Prolonged run switch
           irew,          // Tracking data output rewound switch
           iclo6,         // Use algebra package switch
           nde[2],        // Number of turns at flat bottom and flat top
           nwr[4],        // Data output interval 
           ntwin,         // Computation accuracy switch
           ibidu;         // Create/read full binary dump on unit 32 switch
};

struct KBSLHCCoord {
  struct {
    double mm,
           mrad;
  } x, y;
  double plen,            // Path length (mm)
         rmdev,           // Relative momentum deviation
         egy;             // Energy (MeV)
};

struct KBSLHCInit {
  unsigned itra;          // Number of particles
  double chi0,            // Starting phase of the initial coordinate
         chid,            // Phase difference between the two particles
         rat;             // Emittance ratio
  unsigned iver;          // Vertical coordinates adjust switch
  KBSLHCCoord coord[2];   // Initial coordinates of the 2 particles
  double regy;            // Energy of the reference particle (MeV)
};

struct KBSLHCSync {
  int harm;               // Harmonic number
  double alc,             // Momentum compaction factor
         u0,              // Circumference voltage (MV)
         phag,            // Acceleration phase (deg)
         tlen,            // Length of the accelerator (m)
         pma;             // Rest mass of the particle (MeV/c^2)
  int ition;              // Transition energy switch
  double dppoff,          // Offset relative momentum deviation
         dpscor,          // Scaling factor for relative momentum deviation
         sigcor;          // Path length difference
};

struct KBSLHCUnit3 {
  KBSLHCGeom geom;
  KBSLHCTrac trac;
  KBSLHCInit init;
  KBSLHCSync sync;
  
  bool parse(const QStringList &lines);
};

struct KBSLHCResult {
  KBSLHCUnit3 unit3;
};

struct KBSLHCHeader {
  QString title[2];       // General and additional title
  QDateTime time;         // Date and time
  QString prog;           // Program name
  struct {
    unsigned fst,         // First particle in the file
             last,        // Last particle in the file
             count;       // Total number of particles
  } part;
  unsigned phdim,         // Code for dimensionality of phase space
           expl;          // Projected number of turns
  struct {
    double hor,           // Horizontal tune
           ver,           // Vertical tune
           lng;           // Longitudinal tune
  } tune;
  double clorb[6];        // Closed orbit vector
  double disp[6];         // Dispersion vector
  double tfer[6][6];      // Six-dimensional transfer map
  double smax,            // Maximum number of different seeds
         seed,            // Actual seed number
         sstart,          // Starting value of the seed
         numlr;           // Number of turns in the reverse direction
  double lyap[2];         // Correction-factor for the Lyapunov
  double ripro;           // Start turn number for ripple prolongation
  
  bool parse(QDataStream &stream);
};

struct KBSLHCDatum {
  unsigned part;          // Particle number
  double phad;            // Angular distance in phase space
  KBSLHCCoord coord;
  
  bool parse(QDataStream &stream, unsigned &bytes);
};

struct KBSLHCOutput {
  KBSLHCHeader header;
  QMap<unsigned,KBSLHCDatum> data[2];
  
  bool parse(QDataStream &stream);
};

struct KBSLHCState {
  QMap<unsigned,KBSLHCOutput> output;
};

#endif
