/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbsprojectmonitor.h>

#include "kbslhcplugin.h"

class KBSLHCPluginFactory : KGenericFactory<KBSLHCPlugin,KBSDocument>
{
  public:
    KBSLHCPluginFactory() : KGenericFactory<KBSLHCPlugin,KBSDocument>("kbslhcmonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbslhcmonitor, KBSLHCPluginFactory());

KBSLHCPlugin::KBSLHCPlugin(KBSDocument *parent, const char *name, const QStringList &)
            : KBSProjectPlugin(parent, name)
{
}

KBSProjectMonitor *KBSLHCPlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSLHCProjectMonitor(project, parent);
}

KBSTaskMonitor *KBSLHCPlugin::createTaskMonitor(unsigned task, KBSBOINCMonitor *parent)
{
  return new KBSLHCTaskMonitor(task, parent);
}

void KBSLHCPlugin::readConfig(KConfig *)
{
}

void KBSLHCPlugin::writeConfig(KConfig *)
{
}

#include "kbslhcplugin.moc"
