#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsseticonfigpage.ui'
**
** Created: Mon Feb 6 18:28:58 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsseticonfigpage.h"

#include <qvariant.h>
#include <qimage.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qcheckbox.h>
#include <qlabel.h>
#include <kurlrequester.h>
#include <kcombobox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include "./kbsseticonfigpage.ui.h"

/*
 *  Constructs a KBSSETIConfigPage as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETIConfigPage::KBSSETIConfigPage( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETIConfigPage" );
    KBSSETIConfigPageLayout = new QVBoxLayout( this, 11, 6, "KBSSETIConfigPageLayout"); 

    log_group = new QGroupBox( this, "log_group" );
    log_group->setColumnLayout(0, Qt::Vertical );
    log_group->layout()->setSpacing( 6 );
    log_group->layout()->setMargin( 11 );
    log_groupLayout = new QVBoxLayout( log_group->layout() );
    log_groupLayout->setAlignment( Qt::AlignTop );

    read_group = new QButtonGroup( log_group, "read_group" );
    read_group->setFrameShape( QButtonGroup::NoFrame );
    read_group->setLineWidth( 1 );
    read_group->setColumnLayout(0, Qt::Vertical );
    read_group->layout()->setSpacing( 6 );
    read_group->layout()->setMargin( 11 );
    read_groupLayout = new QHBoxLayout( read_group->layout() );
    read_groupLayout->setAlignment( Qt::AlignTop );

    kcfg_read_0 = new QRadioButton( read_group, "kcfg_read_0" );
    kcfg_read_0->setChecked( TRUE );
    read_groupLayout->addWidget( kcfg_read_0 );

    kcfg_read_1 = new QRadioButton( read_group, "kcfg_read_1" );
    read_groupLayout->addWidget( kcfg_read_1 );

    kcfg_read_2 = new QRadioButton( read_group, "kcfg_read_2" );
    read_groupLayout->addWidget( kcfg_read_2 );

    kcfg_read_3 = new QRadioButton( read_group, "kcfg_read_3" );
    read_groupLayout->addWidget( kcfg_read_3 );

    kcfg_read_4 = new QRadioButton( read_group, "kcfg_read_4" );
    read_groupLayout->addWidget( kcfg_read_4 );
    log_groupLayout->addWidget( read_group );

    write_group = new QGroupBox( log_group, "write_group" );
    write_group->setFrameShape( QGroupBox::NoFrame );
    write_group->setColumnLayout(0, Qt::Vertical );
    write_group->layout()->setSpacing( 6 );
    write_group->layout()->setMargin( 11 );
    write_groupLayout = new QHBoxLayout( write_group->layout() );
    write_groupLayout->setAlignment( Qt::AlignTop );

    kcfg_write_0 = new QCheckBox( write_group, "kcfg_write_0" );
    kcfg_write_0->setChecked( TRUE );
    write_groupLayout->addWidget( kcfg_write_0 );

    kcfg_write_1 = new QCheckBox( write_group, "kcfg_write_1" );
    kcfg_write_1->setChecked( FALSE );
    write_groupLayout->addWidget( kcfg_write_1 );

    kcfg_write_2 = new QCheckBox( write_group, "kcfg_write_2" );
    kcfg_write_2->setChecked( FALSE );
    write_groupLayout->addWidget( kcfg_write_2 );

    kcfg_write_3 = new QCheckBox( write_group, "kcfg_write_3" );
    kcfg_write_3->setChecked( FALSE );
    write_groupLayout->addWidget( kcfg_write_3 );

    kcfg_write_4 = new QCheckBox( write_group, "kcfg_write_4" );
    kcfg_write_4->setChecked( FALSE );
    write_groupLayout->addWidget( kcfg_write_4 );
    log_groupLayout->addWidget( write_group );

    location_layout = new QHBoxLayout( 0, 0, 6, "location_layout"); 

    label_location = new QLabel( log_group, "label_location" );
    location_layout->addWidget( label_location );

    kcfg_location = new KURLRequester( log_group, "kcfg_location" );
    kcfg_location->setMode( 10 );
    location_layout->addWidget( kcfg_location );
    log_groupLayout->addLayout( location_layout );
    KBSSETIConfigPageLayout->addWidget( log_group );

    group_gaussian_0 = new QGroupBox( this, "group_gaussian_0" );
    group_gaussian_0->setColumnLayout(0, Qt::Vertical );
    group_gaussian_0->layout()->setSpacing( 6 );
    group_gaussian_0->layout()->setMargin( 11 );
    group_gaussian_0Layout = new QVBoxLayout( group_gaussian_0->layout() );
    group_gaussian_0Layout->setAlignment( Qt::AlignTop );

    layout_gaussian_format_0 = new QHBoxLayout( 0, 0, 6, "layout_gaussian_format_0"); 

    label_gaussian_filter_0 = new QLabel( group_gaussian_0, "label_gaussian_filter_0" );
    layout_gaussian_format_0->addWidget( label_gaussian_filter_0 );

    kcfg_gaussian_filter_0 = new KComboBox( FALSE, group_gaussian_0, "kcfg_gaussian_filter_0" );
    layout_gaussian_format_0->addWidget( kcfg_gaussian_filter_0 );

    label_gaussian_format_0 = new QLabel( group_gaussian_0, "label_gaussian_format_0" );
    layout_gaussian_format_0->addWidget( label_gaussian_format_0 );

    kcfg_gaussian_format_0 = new KComboBox( FALSE, group_gaussian_0, "kcfg_gaussian_format_0" );
    layout_gaussian_format_0->addWidget( kcfg_gaussian_format_0 );

    label_gaussian_size_0 = new QLabel( group_gaussian_0, "label_gaussian_size_0" );
    layout_gaussian_format_0->addWidget( label_gaussian_size_0 );

    kcfg_gaussian_size_0 = new KComboBox( FALSE, group_gaussian_0, "kcfg_gaussian_size_0" );
    layout_gaussian_format_0->addWidget( kcfg_gaussian_size_0 );
    spacer_gaussian_format_0 = new QSpacerItem( 16, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_gaussian_format_0->addItem( spacer_gaussian_format_0 );
    group_gaussian_0Layout->addLayout( layout_gaussian_format_0 );

    layout_gaussian_location_0 = new QHBoxLayout( 0, 0, 6, "layout_gaussian_location_0"); 

    label_gaussian_location_0 = new QLabel( group_gaussian_0, "label_gaussian_location_0" );
    layout_gaussian_location_0->addWidget( label_gaussian_location_0 );

    kcfg_gaussian_location_0 = new KURLRequester( group_gaussian_0, "kcfg_gaussian_location_0" );
    kcfg_gaussian_location_0->setMode( 10 );
    layout_gaussian_location_0->addWidget( kcfg_gaussian_location_0 );
    group_gaussian_0Layout->addLayout( layout_gaussian_location_0 );
    KBSSETIConfigPageLayout->addWidget( group_gaussian_0 );

    group_gaussian_1 = new QGroupBox( this, "group_gaussian_1" );
    group_gaussian_1->setColumnLayout(0, Qt::Vertical );
    group_gaussian_1->layout()->setSpacing( 6 );
    group_gaussian_1->layout()->setMargin( 11 );
    group_gaussian_1Layout = new QVBoxLayout( group_gaussian_1->layout() );
    group_gaussian_1Layout->setAlignment( Qt::AlignTop );

    layout_gaussian_format_1 = new QHBoxLayout( 0, 0, 6, "layout_gaussian_format_1"); 

    label_gaussian_filter_1 = new QLabel( group_gaussian_1, "label_gaussian_filter_1" );
    layout_gaussian_format_1->addWidget( label_gaussian_filter_1 );

    kcfg_gaussian_filter_1 = new KComboBox( FALSE, group_gaussian_1, "kcfg_gaussian_filter_1" );
    layout_gaussian_format_1->addWidget( kcfg_gaussian_filter_1 );

    label_gaussian_format_1 = new QLabel( group_gaussian_1, "label_gaussian_format_1" );
    layout_gaussian_format_1->addWidget( label_gaussian_format_1 );

    kcfg_gaussian_format_1 = new KComboBox( FALSE, group_gaussian_1, "kcfg_gaussian_format_1" );
    layout_gaussian_format_1->addWidget( kcfg_gaussian_format_1 );

    label_gaussian_size_1 = new QLabel( group_gaussian_1, "label_gaussian_size_1" );
    layout_gaussian_format_1->addWidget( label_gaussian_size_1 );

    kcfg_gaussian_size_1 = new KComboBox( FALSE, group_gaussian_1, "kcfg_gaussian_size_1" );
    layout_gaussian_format_1->addWidget( kcfg_gaussian_size_1 );
    spacer_gaussian_format_1 = new QSpacerItem( 16, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_gaussian_format_1->addItem( spacer_gaussian_format_1 );
    group_gaussian_1Layout->addLayout( layout_gaussian_format_1 );

    layout_gaussian_location_1 = new QHBoxLayout( 0, 0, 6, "layout_gaussian_location_1"); 

    label_gaussian_location = new QLabel( group_gaussian_1, "label_gaussian_location" );
    layout_gaussian_location_1->addWidget( label_gaussian_location );

    kcfg_gaussian_location_1 = new KURLRequester( group_gaussian_1, "kcfg_gaussian_location_1" );
    kcfg_gaussian_location_1->setMode( 10 );
    layout_gaussian_location_1->addWidget( kcfg_gaussian_location_1 );
    group_gaussian_1Layout->addLayout( layout_gaussian_location_1 );
    KBSSETIConfigPageLayout->addWidget( group_gaussian_1 );
    spacer = new QSpacerItem( 31, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETIConfigPageLayout->addItem( spacer );
    languageChange();
    resize( QSize(605, 400).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETIConfigPage::~KBSSETIConfigPage()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETIConfigPage::languageChange()
{
    setCaption( tr2i18n( "General preferences for SETI@home" ) );
    setIconText( tr2i18n( "SETI@home" ) );
    log_group->setTitle( tr2i18n( "Log" ) );
    read_group->setTitle( tr2i18n( "Read from:" ) );
    kcfg_read_0->setText( tr2i18n( "BOINCLog&X" ) );
    kcfg_read_0->setAccel( QKeySequence( tr2i18n( "Alt+X" ) ) );
    kcfg_read_1->setText( tr2i18n( "SETILog&9x" ) );
    kcfg_read_1->setAccel( QKeySequence( tr2i18n( "Alt+9" ) ) );
    kcfg_read_2->setText( tr2i18n( "SETI&Log" ) );
    kcfg_read_2->setAccel( QKeySequence( tr2i18n( "Alt+L" ) ) );
    kcfg_read_3->setText( tr2i18n( "SETI &Spy" ) );
    kcfg_read_3->setAccel( QKeySequence( tr2i18n( "Alt+S" ) ) );
    kcfg_read_4->setText( tr2i18n( "Star&Map" ) );
    kcfg_read_4->setAccel( QKeySequence( tr2i18n( "Alt+M" ) ) );
    write_group->setTitle( tr2i18n( "Write to:" ) );
    kcfg_write_0->setText( tr2i18n( "BOINCLogX" ) );
    kcfg_write_0->setAccel( QKeySequence( QString::null ) );
    kcfg_write_1->setText( tr2i18n( "SETILog9x" ) );
    kcfg_write_1->setAccel( QKeySequence( QString::null ) );
    kcfg_write_2->setText( tr2i18n( "SETILog" ) );
    kcfg_write_2->setAccel( QKeySequence( QString::null ) );
    kcfg_write_3->setText( tr2i18n( "SETI Spy" ) );
    kcfg_write_3->setAccel( QKeySequence( QString::null ) );
    kcfg_write_4->setText( tr2i18n( "StarMap" ) );
    kcfg_write_4->setAccel( QKeySequence( QString::null ) );
    label_location->setText( tr2i18n( "Location:" ) );
    group_gaussian_0->setTitle( tr2i18n( "Best Gaussians" ) );
    label_gaussian_filter_0->setText( tr2i18n( "Save best score gaussian:" ) );
    kcfg_gaussian_filter_0->clear();
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "none" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "all" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "interesting" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 2.0" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 2.5" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 3.0" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 3.5" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 4.0" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 4.5" ) );
    kcfg_gaussian_filter_0->insertItem( tr2i18n( "S.R. > 5.0" ) );
    label_gaussian_format_0->setText( tr2i18n( "Format:" ) );
    label_gaussian_size_0->setText( tr2i18n( "Size:" ) );
    kcfg_gaussian_size_0->clear();
    kcfg_gaussian_size_0->insertItem( tr2i18n( "default" ) );
    kcfg_gaussian_size_0->insertItem( tr2i18n( "SETI@home" ) );
    label_gaussian_location_0->setText( tr2i18n( "Location:" ) );
    group_gaussian_1->setTitle( tr2i18n( "Returned Gaussians" ) );
    label_gaussian_filter_1->setText( tr2i18n( "Save returned gaussians:" ) );
    kcfg_gaussian_filter_1->clear();
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "none" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "all" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "interesting" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "S.R. > 3.5" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "S.R. > 4.0" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "S.R. > 4.5" ) );
    kcfg_gaussian_filter_1->insertItem( tr2i18n( "S.R. > 5.0" ) );
    label_gaussian_format_1->setText( tr2i18n( "Format:" ) );
    label_gaussian_size_1->setText( tr2i18n( "Size:" ) );
    kcfg_gaussian_size_1->clear();
    kcfg_gaussian_size_1->insertItem( tr2i18n( "default" ) );
    kcfg_gaussian_size_1->insertItem( tr2i18n( "SETI@home" ) );
    label_gaussian_location->setText( tr2i18n( "Location:" ) );
}

#include "kbsseticonfigpage.moc"
