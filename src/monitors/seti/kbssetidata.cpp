/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <klocale.h>

#include <kbsboincdata.h>

#include "kbssetidata.h"

using namespace KBSBOINC;

QString KBSSETI::formatRA(double ra)
{
  const int h = int(ra) % 24;
  ra -= int(ra); ra *= 60;
  const int m = int(ra);
  ra -= m; ra *= 60;
  const int s = int(ra);

  return(i18n("%1 h %2' %3\"").arg(h).arg(m).arg(s));
}

QString KBSSETI::formatDec(double dec, bool sign)
{
  const QString p = (dec < 0.0) ? QString("-") : sign ? QString("+") : QString::null;
  dec = (dec < 0.0) ? -dec : dec;
  const int d = int(dec);
  dec -= d; dec *= 60;
  const int m = int(dec);
  dec -= m; dec *= 60;
  const int s = int(dec);

  return(QString("%1%2\xb0 %4' %5\"").arg(p).arg(d).arg(m).arg(s));
}

static double signalResolution(unsigned fft_len)
{
  const double WorkunitBandwidth = 9765.625;

  return (fft_len > 0) ? WorkunitBandwidth / fft_len : 0.0;
}

static QValueList<uint> parseUIntList(const QString &text)
{
  QStringList list = QStringList::split(",", text);
  QValueList<uint> out;
  
  for(QStringList::const_iterator it = list.constBegin(); list.constEnd() != it; ++it)
    out << (*it).toUInt(0, 10);
    
  return out;
}

static QValueList<double> parseDoubleList(const QString &text)
{
  QStringList list = QStringList::split(",", text);
  QValueList<double> out;
  
  for(QStringList::const_iterator it = list.constBegin(); list.constEnd() != it; ++it)
    out << (*it).toDouble();
    
  return out;
}

bool KBSSETITapeInfo::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "start_time") {
        start_time_jd = element.text().toDouble();
        start_time = parseJulianDate(start_time_jd);
      } else if(elementName == "last_block_time") {
        last_block_time_jd = element.text().toDouble();
        last_block_time = parseJulianDate(last_block_time_jd);
      } else if(elementName == "last_block_done")
        last_block_done = element.text().toUInt(0, 10);
      else if(elementName == "missed")
        missed = element.text().toUInt(0, 10);
      else if(elementName == "tape_quality")
        tape_quality = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSSETICoordinateT::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "time")
        time = parseJulianDate(element.text());
      else if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "dec")
        dec = element.text().toDouble();
    }
  
  return true;
}

bool KBSSETICoords::parse(const QDomElement &node)
{
  coordinate_t.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "coordinate_t") {
        KBSSETICoordinateT item;
        
        if(item.parse(element)) coordinate_t << item;
        else return false;
      }
    }
  
  return true;
}

bool KBSSETIDataDesc::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "start_ra")
        start.ra = element.text().toDouble();
      else if(elementName == "start_dec")
        start.dec = element.text().toDouble();
      else if(elementName == "end_ra")
        end.ra = element.text().toDouble();
      else if(elementName == "end_dec")
        end.dec = element.text().toDouble();
      else if(elementName == "true_angle_range")
        true_angle_range = element.text().toDouble();
      else if(elementName == "time_recorded_jd") {
        time_recorded_jd = element.text().toDouble();
        time_recorded = parseJulianDate(time_recorded_jd);
      } else if(elementName == "nsamples")
        nsamples = element.text().toUInt(0, 10);
      else if(elementName == "coords") {
        if(!coords.parse(element)) return false;
      }
    }
  
  return true;
}

double KBSSETIDataDesc::teraFLOPs(double angle_range)
{
  return (angle_range < 0.2255) ? 3.54 * exp(angle_range * 0.0327)
       : (angle_range > 1.1274) ? 3.37 * pow(angle_range, -0.0065)
                                : 3.74 * pow(angle_range, -0.1075);
}

double KBSSETIDataDesc::teraFLOPs() const
{
  return teraFLOPs(true_angle_range);
}

bool KBSSETIReceiverCfg::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "s4_id")
        s4_id = element.text().toUInt(0, 10);
      else if(elementName == "name")
        name = element.text();
      else if(elementName == "beam_width")
        beam_width = element.text().toDouble();
      else if(elementName == "center_freq")
        center_freq = element.text().toDouble();
      else if(elementName == "latitude")
        latitude = element.text().toDouble();
      else if(elementName == "longitude")
        longitude = element.text().toDouble();
      else if(elementName == "elevation")
        elevation = element.text().toDouble();
      else if(elementName == "diameter")
        diameter = element.text().toDouble();
      else if(elementName == "az_orientation")
        az.orientation = element.text().toDouble();
      else if(elementName == "az_corr_coeff")
        az.corr_coeff = parseDoubleList(element.text());
      else if(elementName == "zen_corr_coeff")
        zen_corr_coeff = parseDoubleList(element.text());
    }
  
  return true;
}

bool KBSSETIRecorderCfg::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "bits_per_sample")
        bits_per_sample = element.text().toUInt(0, 10);
      else if(elementName == "sample_rate")
        sample_rate = element.text().toUInt(0, 10);
      else if(elementName == "beams")
        beams = element.text().toUInt(0, 10);
      else if(elementName == "version")
        version = element.text();
    }
  
  return true;
}

bool KBSSETISplitterCfg::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "version")
        version = element.text();
      else if(elementName == "data_type")
        data_type = element.text();
      else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "ifft_len")
        ifft_len = element.text().toUInt(0, 10);
      else if(elementName == "filter")
        filter = element.text();
      else if(elementName == "window")
        window = element.text();
    }
  
  return true;
}

bool KBSSETIChirpParameterT::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "chirp_limit")
        chirp_limit = element.text().toUInt(0, 10);
      else if(elementName == "fft_len_flags")
        fft_len_flags = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSSETIChirps::parse(const QDomElement &node)
{
  chirp_parameter_t.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "chirp_parameter_t") {
        KBSSETIChirpParameterT item;
        
        if(item.parse(element)) chirp_parameter_t << item;
        else return false;
      }
    }
  
  return true;
}

bool KBSSETIAnalysisCfg::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "spike_thresh")
        spike_thresh = element.text().toDouble();
      else if(elementName == "spikes_per_spectrum")
        spikes_per_spectrum = element.text().toUInt(0, 10);
      else if(elementName == "gauss_null_chi_sq_thresh")
        gauss.null_chi_sq_thresh = element.text().toDouble();
      else if(elementName == "gauss_chi_sq_thresh")
        gauss.chi_sq_thresh = element.text().toDouble();
      else if(elementName == "gauss_power_thresh")
        gauss.power_thresh = element.text().toDouble();
      else if(elementName == "gauss_peak_power_thresh")
        gauss.peak_power_thresh = element.text().toDouble();
      else if(elementName == "gauss_pot_length")
        gauss.pot_length = element.text().toUInt(0, 10);
      else if(elementName == "pulse_thresh")
        pulse.thresh = element.text().toDouble();
      else if(elementName == "pulse_display_thresh")
        pulse.display_thresh = element.text().toDouble();
      else if(elementName == "pulse_max")
        pulse.max = element.text().toUInt(0, 10);
      else if(elementName == "pulse_min")
        pulse.min = element.text().toUInt(0, 10);
      else if(elementName == "pulse_fft_max")
        pulse.fft_max = element.text().toUInt(0, 10);
      else if(elementName == "pulse_pot_length")
        pulse.pot_length = element.text().toUInt(0, 10);
      else if(elementName == "triplet_thresh")
        triplet.thresh = element.text().toDouble();
      else if(elementName == "triplet_max")
        triplet.max = element.text().toUInt(0, 10);
      else if(elementName == "triplet_min")
        triplet.min = element.text().toUInt(0, 10);
      else if(elementName == "triplet_pot_length")
        triplet.pot_length = element.text().toUInt(0, 10);
      else if(elementName == "pot_overlap_factor")
        pot.overlap_factor = element.text().toDouble();
      else if(elementName == "pot_t_offset")
        pot.t_offset = element.text().toUInt(0, 10);
      else if(elementName == "pot_min_slew")
        pot.min_slew = element.text().toDouble();
      else if(elementName == "pot_max_slew")
        pot.max_slew = element.text().toDouble();
      else if(elementName == "chirp_resolution")
        chirp_resolution = element.text().toDouble();
      else if(elementName == "analysis_fft_lengths")
        analysis_fft_lengths = element.text().toUInt(0, 10);
      else if(elementName == "bsmooth_boxcar_length")
        bsmooth.boxcar_length = element.text().toUInt(0, 10);
      else if(elementName == "bsmooth_chunk_size")
        bsmooth.chunk_size = element.text().toUInt(0, 10);
      else if(elementName == "chirps") {
        if(!chirps.parse(element)) return false;
      } else if(elementName == "pulse_beams")
        pulse_beams = element.text().toUInt(0, 10);
      else if(elementName == "max_signals")
        max_signals = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSSETIGroupInfo::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "tape_info") {
        if(!tape_info.parse(element)) return false;
      } else if(elementName == "name")
        name = element.text();
      else if(elementName == "data_desc") {
        if(!data_desc.parse(element)) return false;
      } else if(elementName == "receiver_cfg") {
        if(!receiver_cfg.parse(element)) return false;
      } else if(elementName == "recorder_cfg") {
        if(!recorder_cfg.parse(element)) return false;
      } else if(elementName == "splitter_cfg") {
        if(!splitter_cfg.parse(element)) return false;
      } else if(elementName == "analysis_cfg") {
        if(!analysis_cfg.parse(element)) return false;
      }
    }
  
  return true;
}

bool KBSSETISubbandDesc::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "number")
        number = element.text().toUInt(0, 10);
      else if(elementName == "center")
        center = element.text().toDouble();
      else if(elementName == "base")
        base = element.text().toDouble();
      else if(elementName == "sample_rate")
        sample_rate = element.text().toDouble();
    }
  
  return true;
}

bool KBSSETIWorkunitHeader::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "name")
        name = element.text();
      else if(elementName == "group_info") {
        if(!group_info.parse(element)) return false;
      } else if(elementName == "subband_desc") {
        if(!subband_desc.parse(element)) return false;
      }     
    }
  
  return true;
}

const QString KBSSETISpike::type = "spike";

bool KBSSETISpike::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "peak_power")
        peak_power = element.text().toDouble();
      else if(elementName == "mean_power")
        mean_power = element.text().toDouble();
      else if(elementName == "time") {
        time_jd = element.text().toDouble();
        time = parseJulianDate(time_jd);
      } else if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "decl")
        decl = element.text().toDouble();
      else if(elementName == "q_pix")
        q_pix = element.text().toUInt(0, 10);
      else if(elementName == "freq")
        freq = element.text().toDouble();
      else if(elementName == "detection_freq")
        detection_freq = element.text().toDouble();
      else if(elementName == "barycentric_freq")
        barycentric_freq = element.text().toDouble();
      else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "chirp_rate")
        chirp_rate = element.text().toDouble();
      else if(elementName == "rfi_checked")
        rfi.checked = element.text().toUInt(0, 10);
      else if(elementName == "rfi_found")
        rfi.found = element.text().toUInt(0, 10);
      else if(elementName == "reserved")
        reserved = element.text().toUInt(0, 10);
    }
  
  return true;
}

double KBSSETISpike::score() const
{
  return (peak_power > 0.0) ? log10(0.025 * peak_power) : 0.0;
}

double KBSSETISpike::resolution() const
{
  return signalResolution(fft_len);
}

double KBSSETISpike::signal_ratio() const
{
  return(peak_power / mean_power);
}

const QString KBSSETIGaussian::type = "gaussian";

bool KBSSETIGaussian::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "peak_power")
        peak_power = element.text().toDouble();
      else if(elementName == "mean_power")
        mean_power = element.text().toDouble();
      else if(elementName == "time") {
        time_jd = element.text().toDouble();
        time = parseJulianDate(time_jd);
      } else if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "decl")
        decl = element.text().toDouble();
      else if(elementName == "q_pix")
        q_pix = element.text().toUInt(0, 10);
      else if(elementName == "freq")
        freq = element.text().toDouble();
      else if(elementName == "detection_freq")
        detection_freq = element.text().toDouble();
      else if(elementName == "barycentric_freq")
        barycentric_freq = element.text().toDouble();
      else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "chirp_rate")
        chirp_rate = element.text().toDouble();
      else if(elementName == "rfi_checked")
        rfi.checked = element.text().toUInt(0, 10);
      else if(elementName == "rfi_found")
        rfi.found = element.text().toUInt(0, 10);
      else if(elementName == "reserved")
        reserved = element.text().toUInt(0, 10);
      else if(elementName == "sigma")
        sigma = element.text().toDouble();
      else if(elementName == "chisqr")
        chisqr = element.text().toDouble();
      else if(elementName == "null_chisqr")
        null_chisqr = element.text().toDouble();
      else if(elementName == "max_power")
        max_power = element.text().toDouble();
      else if(elementName == "pot")
        pot = parseUIntList(element.text());
    }
  
  return true;
}

double KBSSETIGaussian::score() const
{
  return (chisqr > 0.0) ? peak_power/chisqr : 0.0;
}

bool KBSSETIGaussian::interesting() const
{
  return(peak_power > 0.0
         && chisqr > 0.0 && chisqr < 10.0
         && chisqr < 2.0 + 1.6 * peak_power);
}

double KBSSETIGaussian::resolution() const
{
  return signalResolution(fft_len);
}

double KBSSETIGaussian::signal_ratio() const
{
  return(peak_power / mean_power);
}

const QString KBSSETIPulse::type = "pulse";

bool KBSSETIPulse::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "peak_power")
        peak_power = element.text().toDouble();
      else if(elementName == "mean_power")
        mean_power = element.text().toDouble();
      else if(elementName == "time") {
        time_jd = element.text().toDouble();
        time = parseJulianDate(time_jd);
      } else if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "decl")
        decl = element.text().toDouble();
      else if(elementName == "q_pix")
        q_pix = element.text().toUInt(0, 10);
      else if(elementName == "freq")
        freq = element.text().toDouble();
      else if(elementName == "detection_freq")
        detection_freq = element.text().toDouble();
      else if(elementName == "barycentric_freq")
        barycentric_freq = element.text().toDouble();
      else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "chirp_rate")
        chirp_rate = element.text().toDouble();
      else if(elementName == "rfi_checked")
        rfi.checked = element.text().toUInt(0, 10);
      else if(elementName == "rfi_found")
        rfi.found = element.text().toUInt(0, 10);
      else if(elementName == "reserved")
        reserved = element.text().toUInt(0, 10);
      else if(elementName == "period")
        period = element.text().toDouble();
      else if(elementName == "snr")
        snr = element.text().toDouble();
      else if(elementName == "thresh")
        thresh = element.text().toDouble();
      else if(elementName == "len_prof")
        len_prof = element.text().toUInt(0, 10);
      else if(elementName == "pot")
        pot = parseUIntList(element.text());
    }
  
  return true;
}

double KBSSETIPulse::score() const
{
  return (thresh > 0.0) ? snr/thresh : 0.0;
}

double KBSSETIPulse::resolution() const
{
  return signalResolution(fft_len);
}

double KBSSETIPulse::signal_ratio() const
{
  return(peak_power / mean_power);
}

const QString KBSSETITriplet::type = "triplet";

bool KBSSETITriplet::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "peak_power")
        peak_power = element.text().toDouble();
      else if(elementName == "mean_power")
        mean_power = element.text().toDouble();
      else if(elementName == "time") {
        time_jd = element.text().toDouble();
        time = parseJulianDate(time_jd);
      } else if(elementName == "ra")
        ra = element.text().toDouble();
      else if(elementName == "decl")
        decl = element.text().toDouble();
      else if(elementName == "q_pix")
        q_pix = element.text().toUInt(0, 10);
      else if(elementName == "freq")
        freq = element.text().toDouble();
      else if(elementName == "detection_freq")
        detection_freq = element.text().toDouble();
      else if(elementName == "barycentric_freq")
        barycentric_freq = element.text().toDouble();
      else if(elementName == "fft_len")
        fft_len = element.text().toUInt(0, 10);
      else if(elementName == "chirp_rate")
        chirp_rate = element.text().toDouble();
      else if(elementName == "rfi_checked")
        rfi.checked = element.text().toUInt(0, 10);
      else if(elementName == "rfi_found")
        rfi.found = element.text().toUInt(0, 10);
      else if(elementName == "reserved")
        reserved = element.text().toUInt(0, 10);
      else if(elementName == "period")
        period = element.text().toDouble();
    }
  
  return true;
}

double KBSSETITriplet::score() const
{
  return peak_power;
}

double KBSSETITriplet::resolution() const
{
  return signalResolution(fft_len);
}

double KBSSETITriplet::signal_ratio() const
{
  return(peak_power / mean_power);
}

bool KBSSETIBestSpike::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "spike") {
        if(!spike.parse(element)) return false;
      } else if(elementName == "bs_score")
        bs.score = element.text().toDouble();
      else if(elementName == "bs_bin")
        bs.bin = element.text().toUInt(0, 10);
      else if(elementName == "bs_fft_ind")
        bs.fft_ind = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSSETIBestGaussian::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "gaussian") {
        if(!gaussian.parse(element)) return false;
      } else if(elementName == "bg_score")
        bg.score = element.text().toDouble();
      else if(elementName == "bg_display_power_thresh")
        bg.display_power_thresh = element.text().toDouble();
      else if(elementName == "bg_bin")
        bg.bin = element.text().toUInt(0, 10);
      else if(elementName == "bg_fft_ind")
        bg.fft_ind = element.text().toUInt(0, 10);
    }
  
  return true;
}

bool KBSSETIBestPulse::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "pulse") {
        if(!pulse.parse(element)) return false;
      } else if(elementName == "bp_score")
        bp.score = element.text().toDouble();
      else if(elementName == "bp_freq_bin")
        bp.freq_bin = element.text().toUInt(0, 10);
      else if(elementName == "bp_time_bin")
        bp.time_bin = element.text().toDouble();
    }
  
  return true;
}

bool KBSSETIBestTriplet::parse(const QDomElement &node)
{
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "triplet") {
        if(!triplet.parse(element)) return false;
      } else if(elementName == "bt_score")
        bt.score = element.text().toDouble();
      else if(elementName == "bt_bperiod")
        bt.bperiod = element.text().toDouble();
      else if(elementName == "bt_tpotind0_0")
        bt.tpotind[0][0] = element.text().toUInt(0, 10);
      else if(elementName == "bt_tpotind0_1")
        bt.tpotind[0][1] = element.text().toUInt(0, 10);
      else if(elementName == "bt_tpotind1_0")
        bt.tpotind[1][0] = element.text().toUInt(0, 10);
      else if(elementName == "bt_tpotind1_1")
        bt.tpotind[1][1] = element.text().toUInt(0, 10);
      else if(elementName == "bt_tpotind2_0")
        bt.tpotind[2][0] = element.text().toUInt(0, 10);
      else if(elementName == "bt_tpotind2_1")
        bt.tpotind[2][1] = element.text().toUInt(0, 10);
      else if(elementName == "bt_freq_bin")
        bt.freq_bin = element.text().toUInt(0, 10);
      else if(elementName == "bt_time_bin")
        bt.time_bin = element.text().toDouble();
      else if(elementName == "bt_scale")
        bt.scale = element.text().toDouble();
    }
  
  return true;
}

bool KBSSETIState::parse(const QDomElement &node)
{
  best_spike.spike.time_jd = 0.0;
  best_gaussian.gaussian.time_jd = 0.0;
  best_pulse.pulse.time_jd = 0.0;
  best_triplet.triplet.time_jd = 0.0;
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "ncfft")
        ncfft = element.text().toUInt(0, 10);
      else if(elementName == "cr")
        cr = element.text().toDouble();
      else if(elementName == "fl")
        fl = element.text().toUInt(0, 10);
      else if(elementName == "prog")
        prog = element.text().toDouble();
      else if(elementName == "potfreq")
        potfreq = element.text().toInt(0, 10);
      else if(elementName == "potactivity")
        potactivity = element.text().toUInt(0, 10);
      else if(elementName == "signal_count")
        signal_count = element.text().toUInt(0, 10);
      else if(elementName == "best_spike") {
        if(!best_spike.parse(element)) return false;
      } else if(elementName == "best_gaussian") {
        if(!best_gaussian.parse(element)) return false;
      } else if(elementName == "best_pulse") {
        if(!best_pulse.parse(element)) return false;
      } else if(elementName == "best_triplet") {
        if(!best_triplet.parse(element)) return false;
      }
    }
  
  return true;
}

bool KBSSETIResult::parse(const QDomElement &node)
{
  spike.clear();
  gaussian.clear();
  pulse.clear();
  triplet.clear();
  
  for(QDomNode child = node.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "workunit_header") {
        if(!workunit_header.parse(element)) return false;
      } else if(elementName == "spike") {
        KBSSETISpike item;
        
        if(item.parse(element)) spike << item;
        else return false;
     } else if(elementName == "gaussian") {
        KBSSETIGaussian item;
        
        if(item.parse(element)) gaussian << item;
        else return false;
     } else if(elementName == "pulse") {
        KBSSETIPulse item;
        
        if(item.parse(element)) pulse << item;
        else return false;
     } else if(elementName == "triplet") {
        KBSSETITriplet item;
        
        if(item.parse(element)) triplet << item;
        else return false;
      }     
    }
  
  return true;
}

int KBSSETIResult::bestSpike(double *score) const
{
  const unsigned count = spike.count();
  if(count == 0) return -1;
  
  unsigned best = 0;
  double best_score = spike.first().score();
  for(unsigned i = 1; i < count; ++i) {
    const double score = spike[i].score();
    if(score > best_score) {
      best = i;
      best_score = score;
    }
  }
  
  if(NULL != score) *score = best_score;
  return best;
}

int KBSSETIResult::bestGaussian(double *score) const
{
  const unsigned count = gaussian.count();
  if(count == 0) return -1;
  
  unsigned best = 0;
  double best_score = gaussian.first().score();
  for(unsigned i = 1; i < count; ++i) {
    const double score = gaussian[i].score();
    if(score > best_score) {
      best = i;
      best_score = score;
    }
  }
  
  if(NULL != score) *score = best_score;
  return best;
}

int KBSSETIResult::bestPulse(double *score) const
{
  const unsigned count = pulse.count();
  if(count == 0) return -1;
  
  unsigned best = 0;
  double best_score = pulse.first().score();
  for(unsigned i = 1; i < count; ++i) {
    const double score = pulse[i].score();
    if(score > best_score) {
      best = i;
      best_score = score;
    }
  }
  
  if(NULL != score) *score = best_score;
  return best;
}

int KBSSETIResult::bestTriplet(double *score) const
{
  const unsigned count = triplet.count();
  if(count == 0) return -1;
  
  unsigned best = 0;
  double best_score = triplet.first().score();
  for(unsigned i = 1; i < count; ++i) {
    const double score = triplet[i].score();
    if(score > best_score) {
      best = i;
      best_score = score;
    }
  }
  
  if(NULL != score) *score = best_score;
  return best;
}
