/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>
#include <kbsprojectmonitor.h>
#include <kbssetiprojectmonitor.h>
#include <kbssetitaskmonitor.h>

#include <kbsseticalibrationpage.h>
#include <kbsseticonfigpage.h>
#include <kbssetilogmanager.h>

#include "kbssetiplugin.h"

class KBSSETIPluginFactory : KGenericFactory<KBSSETIPlugin,KBSDocument>
{
  public:
    KBSSETIPluginFactory() : KGenericFactory<KBSSETIPlugin,KBSDocument>("kbssetimonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbssetimonitor, KBSSETIPluginFactory());

KBSSETIPlugin::KBSSETIPlugin(KBSDocument *parent, const char *name, const QStringList &)
             : KBSProjectPlugin(parent, name), m_preferences()
{
  KBSSETILogManager *log = KBSSETILogManager::self();
  
  log->setInterval(parent->interval());
  connect(parent, SIGNAL(intervalChanged(int)), log, SLOT(setInterval(int)));
}

QPtrList<QWidget> KBSSETIPlugin::createConfigPages()
{
  QPtrList<QWidget> out;
  out.append(new KBSSETIConfigPage(0, "seti"));
  out.append(new KBSSETICalibrationPage(0, "calibration"));
   
  return out;
}

KBSProjectMonitor *KBSSETIPlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSSETIProjectMonitor(project, parent);
}

KBSTaskMonitor *KBSSETIPlugin::createTaskMonitor(unsigned slot, KBSBOINCMonitor *parent)
{
  return new KBSSETITaskMonitor(slot, parent);
}

KConfigSkeleton *KBSSETIPlugin::preferences()
{
  return &m_preferences;
}

void KBSSETIPlugin::applyPreferences()
{
  KBSSETILogManager *log = KBSSETILogManager::self();
  
  log->setCurrentFormat(m_preferences.format());
  log->setURL(m_preferences.location);  
  log->setWriteMask(m_preferences.writeMask());
  
  KBSSETIGaussianLog *gaussianLog = KBSSETIGaussianLog::self();
  
  gaussianLog->setPreferences(KBSSETIGaussianLog::Best,
                              m_preferences.gaussianLogPreferences(KBSSETIGaussianLog::Best));
  gaussianLog->setPreferences(KBSSETIGaussianLog::Returned,
                              m_preferences.gaussianLogPreferences(KBSSETIGaussianLog::Returned));
  
  KBSSETICalibrator *calibrator = KBSSETICalibrator::self();
  
  calibrator->setAuto(m_preferences.calibrate_auto);
  if(!m_preferences.calibrate_auto)
    calibrator->setCalibration(m_preferences.calibration());
}

void KBSSETIPlugin::readConfig(KConfig *config)
{
  KBSSETICalibrator::self()->readConfig(config);
  
  m_preferences.readConfig();
  applyPreferences();
}

void KBSSETIPlugin::writeConfig(KConfig *config)
{
  KBSSETICalibrator::self()->writeConfig(config);
  
  m_preferences.writeConfig();
}

#include "kbssetiplugin.moc"

