/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETILOG_H
#define KBSSETILOG_H

#include <kbslogmonitor.h>

class KBSSETIProjectMonitor;

class KBSSETILog : public KBSLogMonitor
{
  Q_OBJECT
  public:
    KBSSETILog(const KURL &url, QObject *parent=0, const char *name=0);
  
    virtual QStringList keys() const;
    virtual bool hasResults() const;
  
  protected:
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
  
    virtual KBSLogDatum formatWorkunitDatum(KBSSETIProjectMonitor *projectMonitor, const QString &workunit) const;
    virtual KBSLogData formatSpikeData(KBSSETIProjectMonitor *monitor, const QString &workunit) const;
    virtual KBSLogData formatGaussianData(KBSSETIProjectMonitor *monitor, const QString &workunit) const;
    virtual KBSLogData formatPulseData(KBSSETIProjectMonitor *monitor, const QString &workunit) const;
    virtual KBSLogData formatTripletData(KBSSETIProjectMonitor *monitor, const QString &workunit) const;
    virtual QMap<QString,KBSLogData> formatWorkunit(KBSProjectMonitor *monitor, const QString &workunit) const;
    
    virtual void appendHeader(const KBSFileInfo *info, QIODevice *io);
    virtual void appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum);
    
    virtual void appendResult(QIODevice *io, const KBSLogDatum &datum);

  protected:
    static KBSLogDatum parseKVPSequence(const QString &string);
      
  private:
    void initKeys();
    
    bool parseWorkunitLogDocument(const QStringList &lines);
    bool parseResultsLogDocument(const QStringList &lines);
    
  private slots:
    void updateFile(const QString &fileName);
    
  protected:
    enum FileType {WorkunitFile, ResultsFile};
    static const QString s_filename[ResultsFile+1];
    QStringList m_keys;
    
  private:
    enum ResultType {Spike, Gaussian, Pulse, Triplet};
    QString m_workunit;
    unsigned m_count;
    KBSKeyMap m_map;
};

#endif
