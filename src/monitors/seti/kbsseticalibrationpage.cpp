#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsseticalibrationpage.ui'
**
** Created: Mon Feb 6 18:28:58 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsseticalibrationpage.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <knuminput.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSSETICalibrationPage as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSSETICalibrationPage::KBSSETICalibrationPage( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSSETICalibrationPage" );
    KBSSETICalibrationPageLayout = new QVBoxLayout( this, 0, 6, "KBSSETICalibrationPageLayout"); 

    calibrate_group = new QButtonGroup( this, "calibrate_group" );
    calibrate_group->setFrameShape( QButtonGroup::NoFrame );
    calibrate_group->setColumnLayout(0, Qt::Vertical );
    calibrate_group->layout()->setSpacing( 6 );
    calibrate_group->layout()->setMargin( 11 );
    calibrate_groupLayout = new QVBoxLayout( calibrate_group->layout() );
    calibrate_groupLayout->setAlignment( Qt::AlignTop );

    kcfg_calibrate_auto = new QRadioButton( calibrate_group, "kcfg_calibrate_auto" );
    kcfg_calibrate_auto->setChecked( TRUE );
    calibrate_groupLayout->addWidget( kcfg_calibrate_auto );

    calibrate_manual = new QRadioButton( calibrate_group, "calibrate_manual" );
    calibrate_manual->setChecked( FALSE );
    calibrate_groupLayout->addWidget( calibrate_manual );

    layout_ar = new QHBoxLayout( 0, 0, 6, "layout_ar"); 

    group_low_ar = new QGroupBox( calibrate_group, "group_low_ar" );
    group_low_ar->setEnabled( FALSE );
    group_low_ar->setColumnLayout(0, Qt::Vertical );
    group_low_ar->layout()->setSpacing( 6 );
    group_low_ar->layout()->setMargin( 11 );
    group_low_arLayout = new QGridLayout( group_low_ar->layout() );
    group_low_arLayout->setAlignment( Qt::AlignTop );

    label_reported_low = new QLabel( group_low_ar, "label_reported_low" );
    label_reported_low->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, label_reported_low->sizePolicy().hasHeightForWidth() ) );

    group_low_arLayout->addWidget( label_reported_low, 0, 0 );

    kcfg_reported_0_0 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_0" );
    kcfg_reported_0_0->setMaxValue( 100 );
    kcfg_reported_0_0->setMinValue( 0 );
    kcfg_reported_0_0->setLineStep( 0.1 );
    kcfg_reported_0_0->setPrecision( 1 );

    group_low_arLayout->addWidget( kcfg_reported_0_0, 1, 0 );

    kcfg_reported_0_1 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_1" );
    kcfg_reported_0_1->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_1, 2, 0 );

    kcfg_reported_0_2 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_2" );
    kcfg_reported_0_2->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_2, 3, 0 );

    kcfg_reported_0_3 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_3" );
    kcfg_reported_0_3->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_3, 4, 0 );

    kcfg_reported_0_4 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_4" );
    kcfg_reported_0_4->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_4, 5, 0 );

    kcfg_reported_0_5 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_5" );
    kcfg_reported_0_5->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_5, 6, 0 );

    kcfg_reported_0_6 = new KDoubleSpinBox( group_low_ar, "kcfg_reported_0_6" );
    kcfg_reported_0_6->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_reported_0_6, 7, 0 );

    label_effective_low = new QLabel( group_low_ar, "label_effective_low" );
    label_effective_low->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, label_effective_low->sizePolicy().hasHeightForWidth() ) );

    group_low_arLayout->addWidget( label_effective_low, 0, 1 );

    kcfg_effective_0_0 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_0" );
    kcfg_effective_0_0->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_0, 1, 1 );

    kcfg_effective_0_1 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_1" );
    kcfg_effective_0_1->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_1, 2, 1 );

    kcfg_effective_0_2 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_2" );
    kcfg_effective_0_2->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_2, 3, 1 );

    kcfg_effective_0_3 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_3" );
    kcfg_effective_0_3->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_3, 4, 1 );

    kcfg_effective_0_4 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_4" );
    kcfg_effective_0_4->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_4, 5, 1 );

    kcfg_effective_0_5 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_5" );
    kcfg_effective_0_5->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_5, 6, 1 );

    kcfg_effective_0_6 = new KDoubleSpinBox( group_low_ar, "kcfg_effective_0_6" );
    kcfg_effective_0_6->setMaxValue( 100 );

    group_low_arLayout->addWidget( kcfg_effective_0_6, 7, 1 );
    layout_ar->addWidget( group_low_ar );

    group_medium_ar = new QGroupBox( calibrate_group, "group_medium_ar" );
    group_medium_ar->setEnabled( FALSE );
    group_medium_ar->setColumnLayout(0, Qt::Vertical );
    group_medium_ar->layout()->setSpacing( 6 );
    group_medium_ar->layout()->setMargin( 11 );
    group_medium_arLayout = new QGridLayout( group_medium_ar->layout() );
    group_medium_arLayout->setAlignment( Qt::AlignTop );

    label_reported_medium = new QLabel( group_medium_ar, "label_reported_medium" );
    label_reported_medium->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_reported_medium->sizePolicy().hasHeightForWidth() ) );

    group_medium_arLayout->addWidget( label_reported_medium, 0, 0 );

    kcfg_reported_1_0 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_0" );
    kcfg_reported_1_0->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_0, 1, 0 );

    kcfg_reported_1_1 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_1" );
    kcfg_reported_1_1->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_1, 2, 0 );

    kcfg_reported_1_2 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_2" );
    kcfg_reported_1_2->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_2, 3, 0 );

    kcfg_reported_1_3 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_3" );
    kcfg_reported_1_3->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_3, 4, 0 );

    kcfg_reported_1_4 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_4" );
    kcfg_reported_1_4->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_4, 5, 0 );

    kcfg_reported_1_5 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_5" );
    kcfg_reported_1_5->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_5, 6, 0 );

    kcfg_reported_1_6 = new KDoubleSpinBox( group_medium_ar, "kcfg_reported_1_6" );
    kcfg_reported_1_6->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_reported_1_6, 7, 0 );

    label_effective_medium = new QLabel( group_medium_ar, "label_effective_medium" );
    label_effective_medium->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_effective_medium->sizePolicy().hasHeightForWidth() ) );

    group_medium_arLayout->addWidget( label_effective_medium, 0, 1 );

    kcfg_effective_1_0 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_0" );
    kcfg_effective_1_0->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_0, 1, 1 );

    kcfg_effective_1_1 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_1" );
    kcfg_effective_1_1->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_1, 2, 1 );

    kcfg_effective_1_2 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_2" );
    kcfg_effective_1_2->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_2, 3, 1 );

    kcfg_effective_1_3 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_3" );
    kcfg_effective_1_3->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_3, 4, 1 );

    kcfg_effective_1_4 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_4" );
    kcfg_effective_1_4->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_4, 5, 1 );

    kcfg_effective_1_5 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_5" );
    kcfg_effective_1_5->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_5, 6, 1 );

    kcfg_effective_1_6 = new KDoubleSpinBox( group_medium_ar, "kcfg_effective_1_6" );
    kcfg_effective_1_6->setMaxValue( 100 );

    group_medium_arLayout->addWidget( kcfg_effective_1_6, 7, 1 );
    layout_ar->addWidget( group_medium_ar );

    group_high_ar = new QButtonGroup( calibrate_group, "group_high_ar" );
    group_high_ar->setEnabled( FALSE );
    group_high_ar->setColumnLayout(0, Qt::Vertical );
    group_high_ar->layout()->setSpacing( 6 );
    group_high_ar->layout()->setMargin( 11 );
    group_high_arLayout = new QGridLayout( group_high_ar->layout() );
    group_high_arLayout->setAlignment( Qt::AlignTop );

    label_reported_high = new QLabel( group_high_ar, "label_reported_high" );
    label_reported_high->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_reported_high->sizePolicy().hasHeightForWidth() ) );

    group_high_arLayout->addWidget( label_reported_high, 0, 0 );

    kcfg_reported_2_0 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_0" );
    kcfg_reported_2_0->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_0, 1, 0 );

    kcfg_reported_2_1 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_1" );
    kcfg_reported_2_1->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_1, 2, 0 );

    kcfg_reported_2_2 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_2" );
    kcfg_reported_2_2->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_2, 3, 0 );

    kcfg_reported_2_3 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_3" );
    kcfg_reported_2_3->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_3, 4, 0 );

    kcfg_reported_2_4 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_4" );
    kcfg_reported_2_4->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_4, 5, 0 );

    kcfg_reported_2_5 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_5" );
    kcfg_reported_2_5->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_5, 6, 0 );

    kcfg_reported_2_6 = new KDoubleSpinBox( group_high_ar, "kcfg_reported_2_6" );
    kcfg_reported_2_6->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_reported_2_6, 7, 0 );

    label_effective_high = new QLabel( group_high_ar, "label_effective_high" );
    label_effective_high->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_effective_high->sizePolicy().hasHeightForWidth() ) );

    group_high_arLayout->addWidget( label_effective_high, 0, 1 );

    kcfg_effective_2_0 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_0" );
    kcfg_effective_2_0->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_0, 1, 1 );

    kcfg_effective_2_1 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_1" );
    kcfg_effective_2_1->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_1, 2, 1 );

    kcfg_effective_2_2 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_2" );
    kcfg_effective_2_2->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_2, 3, 1 );

    kcfg_effective_2_3 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_3" );
    kcfg_effective_2_3->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_3, 4, 1 );

    kcfg_effective_2_4 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_4" );
    kcfg_effective_2_4->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_4, 5, 1 );

    kcfg_effective_2_5 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_5" );
    kcfg_effective_2_5->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_5, 6, 1 );

    kcfg_effective_2_6 = new KDoubleSpinBox( group_high_ar, "kcfg_effective_2_6" );
    kcfg_effective_2_6->setMaxValue( 100 );

    group_high_arLayout->addWidget( kcfg_effective_2_6, 7, 1 );
    layout_ar->addWidget( group_high_ar );
    calibrate_groupLayout->addLayout( layout_ar );
    KBSSETICalibrationPageLayout->addWidget( calibrate_group );
    spacer = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSSETICalibrationPageLayout->addItem( spacer );
    languageChange();
    resize( QSize(469, 339).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( calibrate_manual, SIGNAL( toggled(bool) ), group_low_ar, SLOT( setEnabled(bool) ) );
    connect( calibrate_manual, SIGNAL( toggled(bool) ), group_medium_ar, SLOT( setEnabled(bool) ) );
    connect( calibrate_manual, SIGNAL( toggled(bool) ), group_high_ar, SLOT( setEnabled(bool) ) );

    // tab order
    setTabOrder( kcfg_calibrate_auto, calibrate_manual );
    setTabOrder( calibrate_manual, kcfg_reported_0_0 );
    setTabOrder( kcfg_reported_0_0, kcfg_effective_0_0 );
    setTabOrder( kcfg_effective_0_0, kcfg_reported_0_1 );
    setTabOrder( kcfg_reported_0_1, kcfg_effective_0_1 );
    setTabOrder( kcfg_effective_0_1, kcfg_reported_0_2 );
    setTabOrder( kcfg_reported_0_2, kcfg_effective_0_2 );
    setTabOrder( kcfg_effective_0_2, kcfg_reported_0_3 );
    setTabOrder( kcfg_reported_0_3, kcfg_effective_0_3 );
    setTabOrder( kcfg_effective_0_3, kcfg_reported_0_4 );
    setTabOrder( kcfg_reported_0_4, kcfg_effective_0_4 );
    setTabOrder( kcfg_effective_0_4, kcfg_reported_0_5 );
    setTabOrder( kcfg_reported_0_5, kcfg_effective_0_5 );
    setTabOrder( kcfg_effective_0_5, kcfg_reported_0_6 );
    setTabOrder( kcfg_reported_0_6, kcfg_effective_0_6 );
    setTabOrder( kcfg_effective_0_6, kcfg_reported_1_0 );
    setTabOrder( kcfg_reported_1_0, kcfg_effective_1_0 );
    setTabOrder( kcfg_effective_1_0, kcfg_reported_1_1 );
    setTabOrder( kcfg_reported_1_1, kcfg_effective_1_1 );
    setTabOrder( kcfg_effective_1_1, kcfg_reported_1_2 );
    setTabOrder( kcfg_reported_1_2, kcfg_effective_1_2 );
    setTabOrder( kcfg_effective_1_2, kcfg_reported_1_3 );
    setTabOrder( kcfg_reported_1_3, kcfg_effective_1_3 );
    setTabOrder( kcfg_effective_1_3, kcfg_reported_1_4 );
    setTabOrder( kcfg_reported_1_4, kcfg_effective_1_4 );
    setTabOrder( kcfg_effective_1_4, kcfg_reported_1_5 );
    setTabOrder( kcfg_reported_1_5, kcfg_effective_1_5 );
    setTabOrder( kcfg_effective_1_5, kcfg_reported_1_6 );
    setTabOrder( kcfg_reported_1_6, kcfg_effective_1_6 );
    setTabOrder( kcfg_effective_1_6, kcfg_reported_2_0 );
    setTabOrder( kcfg_reported_2_0, kcfg_effective_2_0 );
    setTabOrder( kcfg_effective_2_0, kcfg_reported_2_1 );
    setTabOrder( kcfg_reported_2_1, kcfg_effective_2_1 );
    setTabOrder( kcfg_effective_2_1, kcfg_reported_2_2 );
    setTabOrder( kcfg_reported_2_2, kcfg_effective_2_2 );
    setTabOrder( kcfg_effective_2_2, kcfg_reported_2_3 );
    setTabOrder( kcfg_reported_2_3, kcfg_effective_2_3 );
    setTabOrder( kcfg_effective_2_3, kcfg_reported_2_4 );
    setTabOrder( kcfg_reported_2_4, kcfg_effective_2_4 );
    setTabOrder( kcfg_effective_2_4, kcfg_reported_2_5 );
    setTabOrder( kcfg_reported_2_5, kcfg_effective_2_5 );
    setTabOrder( kcfg_effective_2_5, kcfg_reported_2_6 );
    setTabOrder( kcfg_reported_2_6, kcfg_effective_2_6 );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSSETICalibrationPage::~KBSSETICalibrationPage()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSSETICalibrationPage::languageChange()
{
    setCaption( tr2i18n( "Calibrate SETI@home reported progress" ) );
    setIconText( tr2i18n( "Calibration" ) );
    calibrate_group->setTitle( QString::null );
    kcfg_calibrate_auto->setText( tr2i18n( "&Automatic (adaptive)" ) );
    kcfg_calibrate_auto->setAccel( QKeySequence( tr2i18n( "Alt+A" ) ) );
    calibrate_manual->setText( tr2i18n( "&Manual" ) );
    calibrate_manual->setAccel( QKeySequence( tr2i18n( "Alt+M" ) ) );
    group_low_ar->setTitle( tr2i18n( "Low AR" ) );
    label_reported_low->setText( tr2i18n( "Reported:" ) );
    label_effective_low->setText( tr2i18n( "Effective:" ) );
    group_medium_ar->setTitle( tr2i18n( "Medium AR" ) );
    label_reported_medium->setText( tr2i18n( "Reported:" ) );
    label_effective_medium->setText( tr2i18n( "Effective:" ) );
    group_high_ar->setTitle( tr2i18n( "High AR" ) );
    label_reported_high->setText( tr2i18n( "Reported:" ) );
    label_effective_high->setText( tr2i18n( "Effective:" ) );
}

void KBSSETICalibrationPage::init()
{
}

#include "kbsseticalibrationpage.moc"
