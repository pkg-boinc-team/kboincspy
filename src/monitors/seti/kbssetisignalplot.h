/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETISIGNALPLOT_H
#define KBSSETISIGNALPLOT_H

#include <qpixmap.h>
#include <qwidget.h>

#include <kbssetidata.h>

class KBSSETISignalPlot : public QWidget
{
  Q_OBJECT
  public:
    KBSSETISignalPlot(QWidget *parent=0, const char *name=0);
    
    virtual void clearData();
    virtual void setData(const KBSSETIWorkunitHeader &workunit, const KBSSETIGaussian &gaussian);
    virtual void setData(const KBSSETIWorkunitHeader &workunit, const KBSSETIPulse &pulse);
    virtual void setData(const KBSSETIWorkunitHeader &workunit);
    
    virtual QPixmap pixmap();
    
    static const QSize defaultSize, setiSize;
  
  protected:
    virtual void paintEvent(QPaintEvent *event);
  
  private:
    static void computeBoundaries(const QValueList<double> &list, double &min, double &max);
    static void arrow(QPainter &painter, int x1, int y1, int x2, int y2);
  
  protected:
    double m_start;
    
    enum {NoData, Gaussian, Pulse, TelescopePath} type;
    KBSSETIGaussian m_gaussian;
    KBSSETIPulse m_pulse;
    KBSSETIDataDesc m_data_desc;
    
    static const unsigned s_margin;
};

#endif
