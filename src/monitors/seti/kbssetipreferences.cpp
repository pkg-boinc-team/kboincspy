/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qimage.h>

#include <kbssetisignalplot.h>

#include "kbssetipreferences.h"

KBSSETIPreferences::KBSSETIPreferences() : KConfigSkeleton()
{
  setCurrentGroup("SETI@home - Log");

  addItemString("location", location);  
  for(unsigned i = 0; i < 5; i++)
  {  
    addItemBool(QString("read_%1").arg(i), read[i], 0 == i);  
    addItemBool(QString("write_%1").arg(i), write[i], 0 == i);
  }
  
  setCurrentGroup("SETI@home - Gaussian Log");
  
  int default_format = QImageIO::outputFormats().find("PNG");
  if(default_format < 0) default_format = 0;
  
  for(unsigned i = 0; i < 2; ++i)
  {
    addItemInt(QString("gaussian_filter_%1").arg(i), gaussian_filter[i], KBSSETIGaussianLogPreferences::None);
    addItemInt(QString("gaussian_format_%1").arg(i), gaussian_format[i], default_format);
    addItemInt(QString("gaussian_size_%1").arg(i), gaussian_size[i], 0);
    addItemString(QString("gaussian_location_%1").arg(i), gaussian_location[i]);
  }
  
  setCurrentGroup("SETI@home - Calibration");
 
  addItemBool(QString("calibrate_auto"), calibrate_auto, true);
  KBSSETICalibration standardCalibration = KBSSETICalibrator::self()->standardCalibration();
  for(unsigned group = LowAR; group <= HighAR; ++group)
  {
    QMap<double,double> map = standardCalibration.map[group];
    QValueList<double> keys = map.keys();
    qHeapSort(keys);
    
    for(unsigned i = 0; i < 7; ++i) {
      double value = (i < keys.count()) ? keys[i] : 1.0;
      addItemDouble(QString("reported_%1_%2").arg(group).arg(i), reported[group][i], value * 1e2);
      value = (i < keys.count()) ? map[value] : 1.0;
      addItemDouble(QString("effective_%1_%2").arg(group).arg(i), effective[group][i], value * 1e2);
    }
  }  
}

unsigned KBSSETIPreferences::format() const
{
  unsigned i = 0;
  while(i < 5)
    if(read[i]) break; else i++;
    
  return i;
}

unsigned KBSSETIPreferences::writeMask() const
{
  unsigned mask = 0;
  for(unsigned i = 0; i < 5; ++i)
    if(write[i]) mask += (1 << i);
    
  return mask;
}

KBSSETIGaussianLogPreferences KBSSETIPreferences::gaussianLogPreferences(KBSSETIGaussianLog::Type type)
{
  KBSSETIGaussianLogPreferences out;
  unsigned index = unsigned(type);
  
  switch(gaussian_filter[index]) {
    case 0:
      out.filter = KBSSETIGaussianLogPreferences::None;
      break;
    case 1:
      out.filter = KBSSETIGaussianLogPreferences::All;
      break;
    case 2:
      out.filter = KBSSETIGaussianLogPreferences::Interesting;
    default:
      out.filter = KBSSETIGaussianLogPreferences::SNRAbove;
      if(KBSSETIGaussianLog::Best == type)
        out.threshold = 2.0 + 0.5 * (gaussian_filter[index] - 3);
      else
        out.threshold = 3.5 + 0.5 * (gaussian_filter[index] - 3);      
  }
  
  out.format = (gaussian_format[index] >= 0) ? QImageIO::outputFormats().at(gaussian_format[index]) : "PNG";
  
  out.size = (0 == gaussian_size[index]) ? KBSSETISignalPlot::defaultSize : KBSSETISignalPlot::setiSize;
  
  out.url = KURL(gaussian_location[index]);
  out.url.adjustPath(+1);
  
  return out;
}

KBSSETICalibration KBSSETIPreferences::calibration() const
{
  KBSSETICalibration out;
  
  for(unsigned group = LowAR; group <= HighAR; ++group)
    for(unsigned i = 0; i < 7; ++i)
      out.map[group][reported[group][i] / 1e2] = effective[group][i] / 1e2;
  
  return out;
}
