/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIPREFERENCES_H
#define KBSSETIPREFERENCES_H

#include <kconfigskeleton.h>

#include <kbssetigaussianlog.h>
#include <kbsseticalibrator.h>

class KBSSETIPreferences : public KConfigSkeleton
{
  public:
    KBSSETIPreferences();
    
    virtual unsigned format() const;
    virtual unsigned writeMask() const; 
    
    virtual KBSSETIGaussianLogPreferences gaussianLogPreferences(KBSSETIGaussianLog::Type type);
    virtual KBSSETICalibration calibration() const;
    
  public:
    QString location;
    bool read[5], write[5];
    
    int gaussian_filter[2],
        gaussian_format[2],
        gaussian_size[2];
    QString gaussian_location[2];
    
    bool calibrate_auto;
    double reported[HighAR+1][7];
    double effective[HighAR+1][7];
};

#endif
