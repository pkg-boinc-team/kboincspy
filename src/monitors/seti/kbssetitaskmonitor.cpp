/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qregexp.h>

#include <kbsboincmonitor.h>

#include <kbsseticalibrator.h>
#include <kbssetiprojectmonitor.h>

#include "kbssetitaskmonitor.h"

const QString SETIStateFile = "state.sah";

KBSSETITaskMonitor::KBSSETITaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name)
                  : KBSTaskMonitor(task, parent, name)
{
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));  
  
  KBSSETICalibrator::self()->startLog(this);
  logCalibrationPair();
  
  addFile(SETIStateFile);
}

KBSSETITaskMonitor::~KBSSETITaskMonitor()
{
  KBSSETIProjectMonitor *monitor =
    static_cast<KBSSETIProjectMonitor*>(boincMonitor()->projectMonitor(project()));
  
  const KBSSETIResult *result = (NULL != monitor) ? monitor->result(workunit()) : NULL;
  const double ar = (NULL != result) ? result->workunit_header.group_info.data_desc.true_angle_range : -1.0;
  
  KBSSETICalibrator::self()->endLog(this, ar);
}

const KBSSETIState *KBSSETITaskMonitor::state() const
{
  return file(SETIStateFile)->ok ? &m_state : NULL;
}

bool KBSSETITaskMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QString text;
  if(!readFile(fileName, text)) return false;    
    
  // some massaging to convert it to valid XML
  text.replace(QRegExp("length=[0-9]+"), "");
  text.replace("ind\n", "ind>\n");
  text.prepend("<state>");
  text.append("</state>");
    
  QString message;
  int line, column;
  QDomDocument document(file->fileName);
  if(!document.setContent(text, false, &message, &line, &column))
  {
    qDebug("Error at line %d, column %d: %s", line, column, message.latin1());
    return false;
  }
    
  return parseStateDocument(document);
}

bool KBSSETITaskMonitor::parseStateDocument(const QDomDocument &document)
{
  for(QDomNode child = document.firstChild(); !child.isNull(); child = child.nextSibling())
    if(child.isElement()) {
      QDomElement element = child.toElement();
      const QString elementName = element.nodeName().lower(); 
      
      if(elementName == "state") {
        if(!m_state.parse(element)) return false;
      }
    }
  
  qDebug("... parse OK");
  
  return true;
}

void KBSSETITaskMonitor::logCalibrationPair()
{
  const KBSBOINCClientState *state = boincMonitor()->state();
  if(NULL != state) {
    const KBSBOINCActiveTask &task = state->active_task_set.active_task[this->task()];
    KBSSETICalibrator::self()->logPair(this, task.fraction_done, task.current_cpu_time);
  }
}

void KBSSETITaskMonitor::updateFile(const QString &)
{
  logCalibrationPair();
  
  KBSSETIProjectMonitor *monitor =
    static_cast<KBSSETIProjectMonitor*>(boincMonitor()->projectMonitor(project()));
  if(NULL != monitor) monitor->setState(workunit(), m_state);
  
  emit updatedState();
}

#include "kbssetitaskmonitor.moc"
