/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIPLUGIN_H
#define KBSSETIPLUGIN_H

#include <qptrlist.h>
#include <qstringlist.h>

#include <kbsprojectplugin.h>

#include <kbssetipreferences.h>

class KBSSETIPlugin : public KBSProjectPlugin
{
  Q_OBJECT
  public:
    KBSSETIPlugin(KBSDocument *parent, const char *name, const QStringList &args);
    
    virtual QPtrList<QWidget> createConfigPages();
    virtual KBSProjectMonitor *createProjectMonitor(const QString &project, KBSBOINCMonitor *parent);
    virtual KBSTaskMonitor *createTaskMonitor(unsigned slot, KBSBOINCMonitor *parent);
    
    virtual KConfigSkeleton *preferences();
    virtual void applyPreferences();
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  protected:
    KBSSETIPreferences m_preferences;
};

#endif
