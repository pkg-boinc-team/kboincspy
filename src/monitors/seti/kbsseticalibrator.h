/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETICALIBRATOR_H
#define KBSSETICALIBRATOR_H

#include <qmap.h>
#include <qobject.h>
#include <qptrdict.h>

#include <kconfig.h>
#include <kurl.h>

enum KBSSETICalibrationGroup {LowAR, MediumAR, HighAR};

struct KBSSETICalibration
{
  QMap<double,double> map[HighAR+1];
};

typedef QMap<double,double> KBSSETICalibrationLog;

class KBSSETITaskMonitor;

class KBSSETICalibrator : public QObject
{
  Q_OBJECT
  public:
    static KBSSETICalibrator *self();
    virtual ~KBSSETICalibrator();
    
    virtual bool isAuto() const;
    virtual void setAuto(bool set);
    
    virtual void setCalibration(const KBSSETICalibration &calibration);
    virtual const KBSSETICalibration &standardCalibration() const;
    
    virtual void resetCalibration(const KURL &url);
    virtual double calibrate(const KURL &url, double ar, double prog);
  
    virtual double count(const KURL &url, unsigned group);
    virtual const KBSSETICalibration &calibration(const KURL &url);
    
    virtual void startLog(KBSSETITaskMonitor *monitor);
    virtual void logPair(KBSSETITaskMonitor *monitor, double prog, double cpu);
    virtual void endLog(KBSSETITaskMonitor *monitor, double ar);
  
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
    
  signals:
    void calibrationUpdated();
    
  protected:
    KBSSETICalibrator(QObject *parent=0, const char *name=0);
    
    virtual QString id(const KURL &url) const;
    virtual unsigned computeGroup(double ar) const;
    virtual const KBSSETICalibration &autoCalibration(const QString &id);
  
  private:
    void setupStandardCalibration();
    
  protected:
    bool m_auto;
    KBSSETICalibration m_entered, m_standard;
    QMap<QString,KBSSETICalibration> m_computed;
    QMap<QString,double> m_count[HighAR+1];
    QPtrDict<KBSSETICalibrationLog> m_logs;
    
  private:
    static KBSSETICalibrator *s_self;
};

#endif
