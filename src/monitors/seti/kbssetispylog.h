/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETISPYLOG_H
#define KBSSETISPYLOG_H

#include <kbslogmonitor.h>

class KBSSETIProjectMonitor;

class KBSSETISpyLog : public KBSLogMonitor
{
  Q_OBJECT
  public:
    KBSSETISpyLog(const KURL &url, QObject *parent=0, const char *name=0);
    
    virtual QStringList keys() const;
  
  protected:
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
  
    virtual KBSLogDatum formatWorkunitDatum(KBSSETIProjectMonitor *monitor, const QString &workunit) const;
    virtual QMap<QString,KBSLogData> formatWorkunit(KBSProjectMonitor *monitor, const QString &workunit) const;
    
    virtual void appendHeader(const KBSFileInfo *info, QIODevice *io);
    virtual void appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum);
    
  private:
    void initKeys();
    
    bool parseSETISpyLogDocument(const QStringList &lines);
    
  private slots:
    void updateFile(const QString &fileName);
    
  protected:
    static const QString s_filename;
    QStringList m_keys;
    static const QString s_separator, s_header[2];
    
  private:
    unsigned m_count;
};

#endif
