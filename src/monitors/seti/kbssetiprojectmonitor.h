/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIPROJECTMONITOR_H
#define KBSSETIPROJECTMONITOR_H

#include <qdict.h>
#include <qdom.h>

#include <kbsprojectmonitor.h>

#include <kbssetidata.h>

class KBSSETITaskMonitor;

class KBSSETIProjectMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSSETIProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    virtual ~KBSSETIProjectMonitor();
    
    virtual const KBSSETIResult *result(const QString &workunit) const;
    
    virtual void setState(const QString &workunit, const KBSSETIState &state);
    
    virtual KBSLogManager *logManager() const;
  
  protected:
    virtual bool parseable(const QString &openName) const;
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    virtual KBSSETIResult *mkResult(const QString &workunit);
  
  private:
    bool parseWorkunitDocument(const QDomDocument &document, KBSSETIWorkunitHeader &header);
    bool parseResultDocument(const QDomDocument &document, KBSSETIResult &result);
    
    void setHeader(const KBSSETIWorkunitHeader &header, const QStringList &workunits);
    void setResult(const KBSSETIResult &result, const QStringList &workunits);
  
  private slots:
    void removeWorkunits(const QStringList &workunits);
    void logResults(const QStringList &results);
    void updateFile(const QString &fileName);
  
  protected:
    QDict<KBSSETIResult> m_results;
};

#endif
