/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qpixmap.h>

#include <kapplication.h>
#include <kio/netaccess.h>
#include <ktempfile.h>

#include <kbssetiprojectmonitor.h>
#include <kbssetisignalplot.h>

#include "kbssetigaussianlog.h"

KBSSETIGaussianLog *KBSSETIGaussianLog::s_self = NULL;

KBSSETIGaussianLog *KBSSETIGaussianLog::self()
{
  if(NULL == s_self) s_self = new KBSSETIGaussianLog();
  
  return s_self;
}

KBSSETIGaussianLog::KBSSETIGaussianLog(QObject *parent, const char *name)
                  : QObject(parent, name)
{
}

const KBSSETIGaussianLogPreferences &KBSSETIGaussianLog::preferences(Type type) const
{
  return m_preferences[unsigned(type)];
}

void KBSSETIGaussianLog::setPreferences(Type type, const KBSSETIGaussianLogPreferences &preferences)
{
  m_preferences[unsigned(type)] = preferences;
}

void KBSSETIGaussianLog::logGaussian(Type type,
                                     const KBSSETIWorkunitHeader &workunit,
                                     const KBSSETIGaussian &gaussian)
{
  const double snr = gaussian.mean_power > 0.0 ? gaussian.peak_power / gaussian.mean_power : 0.0;
  KBSSETIGaussianLogPreferences preferences = this->preferences(type);
  
  switch(preferences.filter) {
    case KBSSETIGaussianLogPreferences::None:
      return;
    case KBSSETIGaussianLogPreferences::Interesting:
      if(!gaussian.interesting()) return;
      break;
    case KBSSETIGaussianLogPreferences::SNRAbove:
      if(snr <= preferences.threshold) return;
      break;
    default:
      break;
  }
  
  const QString schema = this->schema(type, workunit) + "." + preferences.format.lower();
  KURL fileURL = uniqueURL(preferences.url, schema);
  
  KBSSETISignalPlot signal;
  signal.setData(workunit, gaussian);
  signal.resize(preferences.size);
  
  if(fileURL.isLocalFile())
    signal.pixmap().save(fileURL.path(-1), preferences.format);
  else
  {
    KTempFile fileTemp;
    fileTemp.setAutoDelete(true);
    
    signal.pixmap().save(fileTemp.name(), preferences.format);
    KIO::NetAccess::upload(fileTemp.name(), fileURL, kapp->mainWidget());
  }
}

QString KBSSETIGaussianLog::schema(Type type, const KBSSETIWorkunitHeader &workunit)
{
  return (Best == type) ? (workunit.name + ".best%1") : (workunit.name + ".returned%1");
}

KURL KBSSETIGaussianLog::uniqueURL(const KURL &url, const QString &schema)
{
  unsigned index = 0;
  while(true) {
    KURL fileURL(url, schema.arg(QString().sprintf("%02u", index++)));
    
    if(!KIO::NetAccess::exists(fileURL, false, kapp->mainWidget()))
      return fileURL;
  }
}

#include "kbssetigaussianlog.moc"
