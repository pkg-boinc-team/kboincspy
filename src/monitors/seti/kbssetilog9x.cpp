/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>
#include <kbssetiprojectmonitor.h>

#include "kbssetilog9x.h"

using namespace KBSBOINC;

const QString KBSSETILog9x::s_filename[] =
  {"log9x.csv", "spikelog.csv", "gaussianlog.csv", "pulselog.csv", "tripletlog.csv"};

KBSSETILog9x::KBSSETILog9x(const KURL &url, QObject *parent, const char *name)
            : KBSLogMonitor(url, parent, name)
{
  initKeys();
  
  for(unsigned i = Spike; i <= Triplet; ++i)
    m_count[i] = 0;
  
  for(unsigned i = WorkunitFile; i <= TripletFile; ++i)
    addLogFile(s_filename[i]);

  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

QStringList KBSSETILog9x::keys() const
{
  return remapKeys(m_keys[WorkunitFile], m_map);
}

bool KBSSETILog9x::hasResults() const
{
  return true;
}

bool KBSSETILog9x::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(s_filename[WorkunitFile] == file->fileName)
    return parseWorkunitLogDocument(lines);
  else
    for(unsigned i = SpikeFile; i <= TripletFile; ++i)
      if(s_filename[i] == file->fileName)
        return parseResultsLogDocument(i - SpikeFile, lines);
        
  return false;
}

KBSLogDatum KBSSETILog9x::formatWorkunitDatum(KBSSETIProjectMonitor *projectMonitor,
                                              const QString &workunit) const
{
  const QString project = projectMonitor->project();
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogDatum();
  
  QString app_filename;
  const QString app_name = state->workunit[workunit].app_name;
  const unsigned version_num = state->workunit[workunit].version_num;
  for(KBSBOINCAppVersionList::const_iterator it = state->app_version[app_name].begin();
      it != state->app_version[app_name].end(); ++it)
    if((*it).version_num == version_num) {
      if(!(*it).file_ref.isEmpty())
        app_filename = (*it).file_ref.first().file_name;
      break;
  }
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogDatum();

  KBSLogDatum out;
  
  out["date"] = formatLogEntryDate(QDateTime::currentDateTime());
  // out["id"] = ...
  // out["key"] = ...
  // out["email_addr"] = ...
  out["user_name"] = state->project[project].user_name;
  // out["url"] = ...
  // out["country"] = ...
  out["register_time"] = formatSETIClassicDate(state->project[project].user.create_time);
  out["last_wu_time"] = formatSETIClassicDate(state->time_stats.last_update);
  out["last_result_time"] = formatSETIClassicDate(state->time_stats.last_update);
  // out["nwus"] = ...
  // out["nresults"] = ...
  out["client_version"] = formatVersion(version_num);
  out["platform"] = app_filename;
  out["cpu_type"] = state->host_info.p.model;
  out["system"] = state->host_info.domain_name;
  // out["total_cpu"] = ...
  // out["params_index"] = ...
  out["task"] = "seti";
  out["version"] = formatVersion(version_num);
  out["name"] = workunit;
  out["data_type"] = datum->workunit_header.group_info.splitter_cfg.data_type;
  out["data_class"] = 0;
  out["splitter_version"] = datum->workunit_header.group_info.splitter_cfg.version;
  out["start_ra"] = datum->workunit_header.group_info.data_desc.start.ra;
  out["start_dec"] = datum->workunit_header.group_info.data_desc.start.dec;
  out["end_ra"] = datum->workunit_header.group_info.data_desc.end.ra;
  out["end_dec"] = datum->workunit_header.group_info.data_desc.end.dec;
  out["angle_range"] = datum->workunit_header.group_info.data_desc.true_angle_range;
  out["time_recorded"] = formatSETIClassicDate(datum->workunit_header.group_info.data_desc.time_recorded_jd);
  out["subband_center"] = datum->workunit_header.subband_desc.center;
  out["subband_base"] = datum->workunit_header.subband_desc.base;
  out["subband_sample_rate"] = datum->workunit_header.subband_desc.sample_rate;
  out["fft_len"] = datum->workunit_header.group_info.splitter_cfg.fft_len;
  out["ifft_len"] = datum->workunit_header.group_info.splitter_cfg.ifft_len;
  out["subband_number"] = datum->workunit_header.subband_desc.number;
  out["receiver"] = datum->workunit_header.group_info.receiver_cfg.name;
  out["nsamples"] = datum->workunit_header.group_info.data_desc.nsamples;
  out["tape_version"] = datum->workunit_header.group_info.recorder_cfg.version;
  out["ncfft"] = datum->state.ncfft;
  out["cr"] = datum->state.cr;
  out["fl"] = datum->state.fl;
  out["cpu"] = state->result[result].final_cpu_time;
  out["prog"] = 1.0;
  if(datum->state.best_spike.spike.time_jd > 0) {
    out["bs_score"] = datum->state.best_spike.bs.score;
    out["bs_power"] = datum->state.best_spike.spike.peak_power;
    out["bs_bin"] = datum->state.best_spike.bs.bin;
    out["bs_fft_ind"] = datum->state.best_spike.bs.fft_ind;
    out["bs_chirp_rate"] = datum->state.best_spike.spike.chirp_rate;
    out["bs_fft_len"] = datum->state.best_spike.spike.fft_len;
  }
  if(datum->state.best_gaussian.gaussian.time_jd > 0) {
    out["bg_score"] = datum->state.best_gaussian.bg.score;
    out["bg_power"] = datum->state.best_gaussian.gaussian.peak_power;
    out["bg_chisq"] = datum->state.best_gaussian.gaussian.chisqr;
    out["bg_bin"] = datum->state.best_gaussian.bg.bin;
    out["bg_fft_ind"] = datum->state.best_gaussian.bg.fft_ind;
    out["bg_chirp_rate"] = datum->state.best_gaussian.gaussian.chirp_rate;
    out["bg_fft_len"] = datum->state.best_gaussian.gaussian.fft_len;
    out["bg_sigma"] = datum->state.best_gaussian.gaussian.sigma;
    out["bg_true_mean"] = datum->state.best_gaussian.gaussian.mean_power;
  }
  if(datum->state.best_pulse.pulse.time_jd > 0) {
    out["bp_score"] = datum->state.best_pulse.bp.score;
    out["bp_power"] = datum->state.best_pulse.pulse.peak_power;
    out["bp_mean"] = datum->state.best_pulse.pulse.mean_power;
    out["bp_period"] = datum->state.best_pulse.pulse.period;
    out["bp_chirp_rate"] = datum->state.best_pulse.pulse.chirp_rate;
    out["bp_fft_len"] = datum->state.best_pulse.pulse.fft_len;
  }
  if(datum->state.best_triplet.triplet.time_jd > 0) {
    out["bt_score"] = datum->state.best_triplet.bt.score;
    out["bt_power"] = datum->state.best_triplet.triplet.peak_power;
    out["bt_mean"] = datum->state.best_triplet.triplet.mean_power;
    out["bt_period"] = datum->state.best_triplet.triplet.period;
    out["bt_bperiod"] = datum->state.best_triplet.bt.bperiod;
    out["bt_chirp_rate"] = datum->state.best_triplet.triplet.chirp_rate;
    out["bt_scale"] = datum->state.best_triplet.bt.scale;
    out["bt_fft_len"] = datum->state.best_triplet.triplet.fft_len;
  }
  
  return out;
}

KBSLogData KBSSETILog9x::formatSpikeData(KBSSETIProjectMonitor *projectMonitor,
                                         const QString &workunit) const
{
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogData();
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogData();
  
  KBSLogData out;
  for(QValueList<KBSSETISpike>::const_iterator it = datum->spike.constBegin();
      it != datum->spike.constEnd(); ++it)
  {
    KBSLogDatum spike;
    
    spike["wu_name"] = workunit;
    spike["power"]  = (*it).peak_power;
    spike["ra"] = (*it).ra;
    spike["dec"] = (*it).decl;
    spike["time"] = formatJulianDate((*it).time);
    spike["freq"] = (*it).freq;
    spike["fft_len"] = (*it).fft_len;
    spike["chirp_rate"] = (*it).chirp_rate;
    
    out << spike;
  }
  
  return out;
}

KBSLogData KBSSETILog9x::formatGaussianData(KBSSETIProjectMonitor *projectMonitor,
                                            const QString &workunit) const
{
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogData();
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogData();
  
  KBSLogData out;
  for(QValueList<KBSSETIGaussian>::const_iterator it = datum->gaussian.constBegin();
      it != datum->gaussian.constEnd(); ++it)
  {
    KBSLogDatum gaussian;
    
    gaussian["wu_name"] = workunit;
    gaussian["score"]  = (*it).score();
    gaussian["peak"] = (*it).peak_power;
    gaussian["chisqr"] = (*it).chisqr;
    gaussian["mean"] = (*it).mean_power;
    gaussian["ra"] = (*it).ra;
    gaussian["dec"] = (*it).decl;
    gaussian["time"] = formatJulianDate((*it).time);
    gaussian["freq"] = (*it).freq;
    gaussian["sigma"] = (*it).sigma;
    gaussian["fft_len"] = (*it).fft_len;
    gaussian["chirp_rate"] = (*it).chirp_rate;
    gaussian["maxpow"] = (*it).max_power;
    gaussian["pot"] = formatPotData((*it).pot);
    
    out << gaussian;
  }
  
  return out;
}

KBSLogData KBSSETILog9x::formatPulseData(KBSSETIProjectMonitor *projectMonitor,
                                         const QString &workunit) const
{
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogData();
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogData();
  
  KBSLogData out;
  for(QValueList<KBSSETIPulse>::const_iterator it = datum->pulse.constBegin();
      it != datum->pulse.constEnd(); ++it)
  {
    KBSLogDatum pulse;
    
    pulse["wu_name"] = workunit;
    pulse["score"]  = (*it).score();
    pulse["power"]  = (*it).peak_power;
    pulse["mean"]  = (*it).mean_power;
    pulse["period"]  = (*it).period;
    pulse["ra"] = (*it).ra;
    pulse["dec"] = (*it).decl;
    pulse["time"] = formatJulianDate((*it).time);
    pulse["freq"] = (*it).freq;
    pulse["fft_len"] = (*it).fft_len;
    pulse["chirp_rate"] = (*it).chirp_rate;
    pulse["snr"] = (*it).snr;
    pulse["thresh"] = (*it).thresh;
    pulse["len_prof"] = unsigned((*it).pot.count());
    pulse["prof"] = formatPotData((*it).pot);
    
    out << pulse;
  }
  
  return out;
}

KBSLogData KBSSETILog9x::formatTripletData(KBSSETIProjectMonitor *projectMonitor,
                                           const QString &workunit) const
{
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogData();
  
  const QString result = state->workunit[workunit].result_name;
  
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogData();
  
  KBSLogData out;
  for(QValueList<KBSSETITriplet>::const_iterator it = datum->triplet.constBegin();
      it != datum->triplet.constEnd(); ++it)
  {
    KBSLogDatum triplet;
    
    triplet["wu_name"] = workunit;
    triplet["power"]  = (*it).peak_power;
    triplet["mean"]  = (*it).mean_power;
    triplet["period"]  = (*it).period;
    triplet["ra"] = (*it).ra;
    triplet["dec"] = (*it).decl;
    triplet["time"] = formatJulianDate((*it).time);
    triplet["freq"] = (*it).freq;
    triplet["fft_len"] = (*it).fft_len;
    triplet["chirp_rate"] = (*it).chirp_rate;
    
    out << triplet;
  }
  
  return out;
}
  
QMap<QString,KBSLogData> KBSSETILog9x::formatWorkunit(KBSProjectMonitor *monitor,
                                                      const QString &workunit) const
{
  KBSSETIProjectMonitor *projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor);
  QMap<QString,KBSLogData> out;
  
  out[s_filename[WorkunitFile]] << formatWorkunitDatum(projectMonitor, workunit);
    
  out[s_filename[SpikeFile]] = formatSpikeData(projectMonitor, workunit);
  out[s_filename[GaussianFile]] = formatGaussianData(projectMonitor, workunit);
  out[s_filename[PulseFile]] = formatPulseData(projectMonitor, workunit);
  out[s_filename[TripletFile]] = formatTripletData(projectMonitor, workunit);
  
  return out;
}

void KBSSETILog9x::appendHeader(const KBSFileInfo *info, QIODevice *io)
{
  QTextStream text(io);
  
  for(unsigned i = WorkunitFile; i <= TripletFile; ++i)
    if(s_filename[i] == info->fileName) {
      text << formatCSVKeys(m_keys[i]) << "\r\n";
      break;
    }
}

void KBSSETILog9x::appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum)
{
  QTextStream text(io);
  
  if(s_filename[WorkunitFile] == info->fileName)
    text << formatCSVDatum(datum, m_keys[WorkunitFile]) << "\r\n";
  else if(s_filename[SpikeFile] == info->fileName)
    text << formatCSVDatum(datum, m_keys[SpikeFile]) << "\r\n";
  else if(s_filename[GaussianFile] == info->fileName)
    text << formatCSVDatum(datum, m_keys[GaussianFile]) << "\r\n";
  else if(s_filename[PulseFile] == info->fileName)
    text << formatCSVDatum(datum, m_keys[PulseFile]) << "\r\n";
  else if(s_filename[TripletFile] == info->fileName)
    text << formatCSVDatum(datum, m_keys[TripletFile]) << "\r\n";
}

void KBSSETILog9x::initKeys()
{
  m_keys[WorkunitFile].clear(); m_keys[WorkunitFile]
    << "date" << "id" << "key" << "email_addr" << "user_name" << "url" << "country"
    << "register_time" << "last_wu_time" << "last_result_time" << "nwus"
    << "nresults" << "client_version" << "platform" << "cpu_type" << "system"
    << "total_cpu" << "params_index" << "task" << "version" << "name"
    << "data_type" << "data_class" << "splitter_version" << "start_ra"
    << "start_dec" << "end_ra" << "end_dec" << "angle_range" << "time_recorded"
    << "subband_center" << "subband_base" << "subband_sample_rate" << "fft_len"
    << "ifft_len" << "subband_number" << "receiver" << "nsamples"
    << "tape_version" << "ncfft" << "cr" << "fl" << "cpu" << "prog" << "bs_power"
    << "bs_score" << "bs_bin" << "bs_fft_ind" << "bs_chirp_rate" << "bs_fft_len"
    << "bg_score" << "bg_power" << "bg_chisq" << "bg_bin" << "bg_fft_ind"
    << "bg_chirp_rate" << "bg_fft_len" << "bg_sigma" << "bg_true_mean"
    << "bp_score" << "bp_power" << "bp_mean" << "bp_period" << "bp_chirp_rate"
    << "bp_fft_len" << "bt_score" << "bt_power" << "bt_mean" << "bt_period"
    << "bt_bperiod" << "bt_chirp_rate" << "bt_scale" << "bt_fft_len";
  
  m_map.clear();
  m_map["register_time"] = "user_create_time";
  m_map["last_wu_time"] = "last_update";
  m_map["client_version"] = "sah_version";
  m_map["cpu_type"] = "p_model";
  m_map["system"] = "domain_name";
  m_map["name"] = "wu_name";
    
  m_keys[SpikeFile].clear(); m_keys[SpikeFile]
    << "wu_name" << "power" << "ra" << "dec" << "time" << "freq" << "fft_len"
    << "chirp_rate";
  
  m_keys[GaussianFile].clear(); m_keys[GaussianFile]
    << "wu_name" << "score" << "peak" << "chisqr" << "mean" << "ra" << "dec"
    << "time" << "freq" << "sigma" << "fft_len" << "chirp_rate" << "maxpow" << "pot";
  
  m_keys[PulseFile].clear(); m_keys[PulseFile]
    << "wu_name" << "score" << "power" << "mean" << "period" << "ra" << "dec"
    << "time" << "freq" << "fft_len" << "chirp_rate" << "snr" << "thresh"
    << "len_prof" << "prof";
  
  m_keys[TripletFile].clear(); m_keys[TripletFile]
    << "wu_name" << "power" << "mean" << "period" << "ra" << "dec" << "time"
    << "freq" << "fft_len" << "chirp_rate";
}

bool KBSSETILog9x::parseWorkunitLogDocument(const QStringList &lines)
{
  if(lines.isEmpty()) return true;
  
  QStringList::const_iterator line = lines.begin();
  
  if(lines.end() == line) return true;
  const unsigned keys = m_keys[WorkunitFile].count();
  m_keys[WorkunitFile] = parseCSVKeys(*line);
  if(keys > m_keys[WorkunitFile].count()) return false;
  ++line;
  
  for(unsigned i = 0; i < m_workunits.count(); ++i)
    if(lines.end() != line) ++line;
    else return true;
    
  while(lines.end() != line) {
    KBSLogDatum datum = parseCSVDatum(*line, m_keys[WorkunitFile]);
    line++;
    
    datum["date"] = parseLogEntryDate(datum["date"].toString());
    datum["register_time"] = parseSETIClassicDate(datum["register_time"].toString());
    datum["last_wu_time"] = parseSETIClassicDate(datum["last_wu_time"].toString());
    datum["last_result_time"] = parseSETIClassicDate(datum["last_result_time"].toString());
    datum["time_recorded"] = parseSETIClassicDate(datum["time_recorded"].toString());
    
    m_workunits << remapCSVDatum(datum, m_map);
  }
  
  qDebug("... parse OK");
  
  return true;
}

bool KBSSETILog9x::parseResultsLogDocument(unsigned type, const QStringList &lines)
{
  if(lines.isEmpty()) return true;
  
  QStringList::const_iterator line = lines.begin();
  
  if(lines.end() == line) return true;
  const unsigned keys = m_keys[SpikeFile+type].count();
  m_keys[SpikeFile+type] = parseCSVKeys(*line);
  if(keys > m_keys[SpikeFile+type].count()) return false;
  ++line;
  
  for(unsigned i = 0; i < m_count[type]; ++i)
    if(lines.end() != line) ++line;
    else return true;
    
  while(lines.end() != line) {
    KBSLogDatum datum = parseCSVDatum(*line, m_keys[SpikeFile+type]);
    line++;
    
    QString workunit = datum["wu_name"].toString();
    if(!workunit.isEmpty()) continue;
    
    datum["time"] = parseJulianDate(datum["time"].toDouble());
    switch(type) {
      case Spike:
        datum["type"] = KBSSETISpike::type;
        break;
      case Gaussian:
        datum["type"] = KBSSETIGaussian::type;
        datum["pot"] = parsePotData(datum["pot"].toString());
        break;
      case Pulse:
        datum["type"] = KBSSETIPulse::type;
        datum["prof"] = parsePotData(datum["prof"].toString());
        break;
      case Triplet:
        datum["type"] = KBSSETITriplet::type;
    }
    
    m_results[workunit] << datum;
    ++m_count[type];
  }
  
  qDebug("... parse OK");
  
  return true;
}

void KBSSETILog9x::updateFile(const QString &fileName)
{
  if(s_filename[WorkunitFile] == fileName)
    emit workunitsUpdated();
  else if(s_filename[SpikeFile] == fileName)
    emit resultsUpdated();
  else if(s_filename[GaussianFile] == fileName)
    emit resultsUpdated();
  else if(s_filename[PulseFile] == fileName)
    emit resultsUpdated();
  else if(s_filename[TripletFile] == fileName)
    emit resultsUpdated();
}

#include "kbssetilog9x.moc"
