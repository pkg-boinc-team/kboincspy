/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbssetilog.h>
#include <kbssetilog9x.h>
#include <kbssetilogx.h>
#include <kbssetispylog.h>
#include <kbssetistarmaplog.h>

#include "kbssetilogmanager.h"

KBSSETILogManager *KBSSETILogManager::s_self = NULL;

KBSSETILogManager *KBSSETILogManager::self()
{
  if(NULL == s_self)
    s_self = new KBSSETILogManager();
  
  return s_self;
}

KBSSETILogManager::KBSSETILogManager(QObject *parent, const char *name)
                 : KBSLogManager(parent, name)
{
}

unsigned KBSSETILogManager::formats() const
{
  return 5;
}

KBSLogMonitor *KBSSETILogManager::createLogMonitor(unsigned format, const KURL &url, QObject *parent)
{
  switch(format) {
    case 0:
      return new KBSSETILogX(url, parent);
    case 1:
      return new KBSSETILog9x(url, parent);
    case 2:
      return new KBSSETILog(url, parent);
    case 3:
      return new KBSSETISpyLog(url, parent);
    case 4:
      return new KBSSETIStarMapLog(url, parent);
    default:
      return NULL;
  }
}

#include "kbssetilogmanager.moc"
