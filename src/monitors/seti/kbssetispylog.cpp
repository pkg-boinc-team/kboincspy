/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>
#include <kbssetiprojectmonitor.h>

#include "kbssetispylog.h"

const QString KBSSETISpyLog::s_filename = "setispy.log";

const QString KBSSETISpyLog::s_separator =
  QString("----- Caution: New Log File format from here! -----");

const QString KBSSETISpyLog::s_header[] =
{
  QString("    Date       Time              Work Unit Name              Start   Start  Angle  Tera-  Process") +
  QString("  Percent  Returned   Best  Returned   Best  Returned   Best  Returned   Best"),
  QString("    Done       Done                                           RA      Dec   Range  FLOPs  Time-hr") +
  QString("    Done    Spikes   Spike  Gauss'ns Gauss'n  Pulses   Pulse  Triplets Triplet")
};


KBSSETISpyLog::KBSSETISpyLog(const KURL &url, QObject *parent, const char *name)
             : KBSLogMonitor(url, parent, name), m_count(0)
{
  initKeys();
  
  addLogFile(s_filename);

  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

QStringList KBSSETISpyLog::keys() const
{
  return m_keys;
}

bool KBSSETISpyLog::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(s_filename == file->fileName)
    return parseSETISpyLogDocument(lines);
  else
    return false;
}

KBSLogDatum KBSSETISpyLog::formatWorkunitDatum(KBSSETIProjectMonitor *projectMonitor,
                                               const QString &workunit) const
{
  const QString project = projectMonitor->project();
  const KBSBOINCMonitor *boincMonitor =  projectMonitor->boincMonitor();
  const KBSBOINCClientState *state = boincMonitor->state();
  if(NULL == state) return KBSLogDatum();
  
  QString app;
  for(QMap<QString,KBSBOINCApp>::const_iterator it = state->app.begin(); it != state->app.end(); ++it)
    if(boincMonitor->project(it.data()) == project) {
      app = it.key();
      break;
  }
  const QString result = state->workunit[workunit].result_name;
  const KBSSETIResult *datum = projectMonitor->result(workunit);
  if(NULL == datum) return KBSLogDatum();  
  
  KBSLogDatum out;
  
  out["date"] = QDateTime::currentDateTime();
  out["wu_name"] = workunit;
  out["start_ra"] = datum->workunit_header.group_info.data_desc.start.ra;
  out["start_dec"] = datum->workunit_header.group_info.data_desc.start.dec;
  out["angle_range"] = datum->workunit_header.group_info.data_desc.true_angle_range;
  out["cpu"] = state->result[result].final_cpu_time;
  out["prog"] = 1.0;
  out["bs_score"] = datum->state.best_spike.bs.score;
  out["spike_count"] = unsigned(datum->spike.count());
  out["bg_score"] = datum->state.best_gaussian.bg.score;
  out["gaussian_count"] = unsigned(datum->gaussian.count());
  out["bp_score"] = datum->state.best_pulse.bp.score;
  out["pulse_count"] = unsigned(datum->pulse.count());
  out["bt_score"] = datum->state.best_triplet.bt.score;
  out["triplet_count"] = unsigned(datum->triplet.count());
  
  return out;
}

QMap<QString,KBSLogData> KBSSETISpyLog::formatWorkunit(KBSProjectMonitor *monitor,
                                                       const QString &workunit) const
{
  KBSSETIProjectMonitor *projectMonitor = static_cast<KBSSETIProjectMonitor*>(monitor);
  QMap<QString,KBSLogData> out;
  
  out[s_filename] << formatWorkunitDatum(projectMonitor, workunit);
  
  return out;
}

void KBSSETISpyLog::appendHeader(const KBSFileInfo *info, QIODevice *io)
{
  if(info->fileName != s_filename) return;
  
  QTextStream text(io);
    
  if(info->exists)
    text << s_separator << "\r\n";
    
  for(unsigned i = 0; i < 2; ++i)
    text << s_header[i] << "\r\n";
}

void KBSSETISpyLog::appendWorkunit(const KBSFileInfo *info, QIODevice *io, const KBSLogDatum &datum)
{
  if(info->fileName != s_filename) return;
  
  QTextStream text(io);
  
  QString field;
  
  QDateTime now = datum["date"].toDateTime();
  
  field.sprintf("%d/%d/%d", now.date().month(), now.date().day(), now.date().year());
  text << field.rightJustify(10);

  text << "  ";
  if(now.time().hour() == 0)
    field.sprintf("12:%.2d:%.2d AM", now.time().minute(), now.time().second());
  else if(now.time().hour() == 12)
    field.sprintf("12:%.2d:%.2d PM", now.time().minute(), now.time().second());
  else if(now.time().hour() < 12)
    field.sprintf("%d:%.2d:%.2d AM", now.time().hour(), now.time().minute(), now.time().second());
  else
    field.sprintf("%d:%.2d:%.2d PM", now.time().hour()-12, now.time().minute(), now.time().second());
  text << field.rightJustify(11);

  text << "  ";
  field = datum["wu_name"].toString();
  text << field.leftJustify(33);

  text << "  ";
  field = QString::number(datum["start_ra"].toDouble(), 'f', 3);
  text << field.rightJustify(6);

  text << "  ";
  field = QString::number(datum["start_dec"].toDouble(), 'f', 3);
  text << field.rightJustify(6);

  text << "  ";
  field = QString::number(datum["angle_range"].toDouble(), 'f', 3);
  text << field.rightJustify(5);

  text << "  ";
  field = QString::number(KBSSETIDataDesc::teraFLOPs(datum["angle_range"].toDouble()), 'f', 3);
  text << field.rightJustify(5);

  text << "  ";
  field = QString::number(datum["cpu"].toDouble() / 3600, 'f', 3);
  text << field.rightJustify(7);

  text << "  ";
  field = QString::number(datum["prog"].toDouble() * 1e2, 'f', 2) + "%";
  text << field.rightJustify(7);

  text << "  ";
  field = QString::number(datum["spike_count"].toUInt());
  text << " " << field.rightJustify((7 + field.length())/2).leftJustify(7) << " ";

  text << " ";
  field = QString::number(datum["bs_score"].toDouble(), 'f', 3);
  text << field.rightJustify((6 + field.length())/2).leftJustify(6);

  text << " ";
  field = QString::number(datum["gaussian_count"].toUInt());
  text << " " << field.rightJustify((7 + field.length())/2).leftJustify(7) << " ";

  text << " ";
  field = QString::number(datum["bg_score"].toDouble(), 'f', 3);
  text << field.rightJustify((6 + field.length())/2).leftJustify(6);

  text << " ";
  field = QString::number(datum["pulse_count"].toUInt());
  text << " " << field.rightJustify((7 + field.length())/2).leftJustify(7) << " ";

  text << " ";
  field = QString::number(datum["bp_score"].toDouble(), 'f', 3);
  text << field.rightJustify((6 + field.length())/2).leftJustify(6);

  text << " ";
  field = QString::number(datum["triplet_count"].toUInt());
  text << " " << field.rightJustify((7 + field.length())/2).leftJustify(7) << " ";

  text << " ";
  field = QString::number(datum["bt_score"].toDouble(), 'f', 3);
  text << field.rightJustify((6 + field.length())/2).leftJustify(6);

  text << "\r\n";
}

void KBSSETISpyLog::initKeys()
{
  m_keys.clear(); m_keys 
    << "date" << "wu_name" << "start_ra" << "start_dec" << "angle_range"
    << "teraflops" << "cpu" << "prog" << "spike_count" << "bs_score"
    << "gaussian_count" << "bg_score" << "pulse_count" << "bp_score"
    << "triplet_count" << "bt_score";
}

bool KBSSETISpyLog::parseSETISpyLogDocument(const QStringList &lines)
{
  if(lines.isEmpty()) return true;
  
  QStringList::const_iterator line = lines.begin();
    
  for(unsigned i = 0; i < m_count; ++i)
    if(lines.end() != line) ++line;
    else return true;
    
  for( ; lines.end() != line; ++line)
  {
    QStringList items = QStringList::split("  ", *line);
    if(items.count() != 17) continue;
    
    KBSLogDatum datum;
    QStringList::iterator item = items.begin();
    
    QDateTime date;
    QStringList aux;
    
    aux = QStringList::split("/", (*item).stripWhiteSpace());
    if(aux.count() != 3) continue;
    date.setDate(QDate(aux[2].toUInt(0, 10), aux[0].toUInt(0, 10), aux[1].toUInt(0, 10)));
    ++item;
    
    aux = QStringList::split(" ", (*item).stripWhiteSpace());
    if(aux.count() != 2) continue;
    date.setTime(QTime::fromString(aux[0]));
    if("PM" == aux[1]) date.addSecs(12 * 60 * 60);
    
    datum["date"] = date;
    ++item;
    
    datum["wu_name"] = (*item).stripWhiteSpace();
    ++item;
    
    datum["start_ra"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["start_dec"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["angle_range"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["teraflops"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["cpu"] = (*item).stripWhiteSpace().toDouble() * 3600;
    ++item;
    
    datum["prog"] = (*item).stripWhiteSpace().remove('%').toDouble() / 100;
    ++item;
    
    datum["spike_count"] = (*item).stripWhiteSpace().toUInt(0, 10);
    ++item;
    
    datum["bs_score"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["gaussian_count"] = (*item).stripWhiteSpace().toUInt(0, 10);
    ++item;
    
    datum["bg_score"] = (*item).stripWhiteSpace().remove('*').toDouble();
    ++item;
    
    datum["pulse_count"] = (*item).stripWhiteSpace().toUInt(0, 10);
    ++item;
    
    datum["bs_score"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    datum["triplet_count"] = (*item).stripWhiteSpace().toUInt(0, 10);
    ++item;
    
    datum["bt_score"] = (*item).stripWhiteSpace().toDouble();
    ++item;
    
    m_workunits << datum;
  }
  
  m_count = lines.count();
  
  qDebug("... parse OK");
    
  return true;
}

void KBSSETISpyLog::updateFile(const QString &)
{
  emit workunitsUpdated();
}

#include "kbssetispylog.moc"
