/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSSETIGAUSSIANLOG_H
#define KBSSETIGAUSSIANLOG_H

#include <qobject.h>
#include <qsize.h>

#include <kurl.h>

#include <kbssetidata.h>

struct KBSSETIGaussianLogPreferences {
  enum Filter {None, All, Interesting, SNRAbove} filter;
  double threshold;
  QString format;
  QSize size;
  KURL url;
};

class KBSSETIProjectMonitor;

class KBSSETIGaussianLog : public QObject
{
  Q_OBJECT
  public:
    static KBSSETIGaussianLog *self();
    
    enum Type {Best, Returned};
    virtual const KBSSETIGaussianLogPreferences &preferences(Type type) const;
    virtual void setPreferences(Type type, const KBSSETIGaussianLogPreferences &preferences); 
    virtual void logGaussian(Type type,
                             const KBSSETIWorkunitHeader &workunit,
                             const KBSSETIGaussian &gaussian);
  
  protected:
    KBSSETIGaussianLog(QObject *parent=0, const char *name=0);
    
    static QString schema(Type type, const KBSSETIWorkunitHeader &workunit);
    
    virtual KURL uniqueURL(const KURL &url, const QString &schema);
  
  protected:
    KBSSETIGaussianLogPreferences m_preferences[Returned+1];
  
  private:
    static KBSSETIGaussianLog *s_self;
};

#endif
