/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>

#include <kbspredictorprojectmonitor.h>

#include "kbspredictortaskmonitor.h"

const QString KBSPredictorMonssterRestartFile = "monsster.restart";

KBSPredictorTaskMonitor::KBSPredictorTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name)
                       : KBSTaskMonitor(task, parent, name)
{
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
  
  addFile(KBSPredictorMonssterRestartFile);
}

const KBSPredictorState *KBSPredictorTaskMonitor::state() const
{
  return file(KBSPredictorMonssterRestartFile)->ok ? &m_state : NULL;
}

bool KBSPredictorTaskMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(KBSPredictorMonssterRestartFile == file->fileName)
  {
    m_state.app_type = MFOLD;
    return m_state.monsster_restart.parse(lines);
  }
  else
    return false;
}

void KBSPredictorTaskMonitor::updateFile(const QString &)
{
  KBSPredictorProjectMonitor *monitor =
    static_cast<KBSPredictorProjectMonitor*>(boincMonitor()->projectMonitor(project()));
  if(NULL != monitor) monitor->setState(workunit(), m_state);
  
  emit updatedState();
}

#include "kbspredictortaskmonitor.moc"
