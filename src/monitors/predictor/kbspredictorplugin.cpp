/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>

#include <kbspredictorprojectmonitor.h>
#include <kbspredictortaskmonitor.h>

#include <kbspredictorconfigpage.h>

#include "kbspredictorplugin.h"

class KBSPredictorPluginFactory : KGenericFactory<KBSPredictorPlugin,KBSDocument>
{
  public:
    KBSPredictorPluginFactory() : KGenericFactory<KBSPredictorPlugin,KBSDocument>("kbspredictormonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbspredictormonitor, KBSPredictorPluginFactory());

KBSPredictorPlugin::KBSPredictorPlugin(KBSDocument *parent, const char *name, const QStringList &)
                   : KBSProjectPlugin(parent, name), m_preferences()
{
}

QPtrList<QWidget> KBSPredictorPlugin::createConfigPages()
{
  QPtrList<QWidget> out;
  out.append(new KBSPredictorConfigPage(0, "predictor"));
  
  return out;
}

KBSProjectMonitor *KBSPredictorPlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSPredictorProjectMonitor(project, parent);
}

KBSTaskMonitor *KBSPredictorPlugin::createTaskMonitor(unsigned task, KBSBOINCMonitor *parent)
{
  return new KBSPredictorTaskMonitor(task, parent);
}

KConfigSkeleton *KBSPredictorPlugin::preferences()
{
  return &m_preferences;
}

void KBSPredictorPlugin::applyPreferences()
{
  KBSPredictorMoleculeLog *moleculeLog = KBSPredictorMoleculeLog::self();
  
  moleculeLog->setPreferences(MFOLD, m_preferences.moleculeLogPreferences(MFOLD));
  moleculeLog->setPreferences(CHARMM, m_preferences.moleculeLogPreferences(CHARMM));
}

void KBSPredictorPlugin::readConfig(KConfig *)
{
  m_preferences.readConfig();
  applyPreferences();
}

void KBSPredictorPlugin::writeConfig(KConfig *)
{
  m_preferences.writeConfig();
}

#include "kbspredictorplugin.moc"
