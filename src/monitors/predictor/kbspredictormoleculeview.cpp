/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qdom.h>
#include <qfile.h>
#include <qimage.h>
#include <qmap.h>
#include <qtextstream.h>

#include <kaboutdata.h>
#include <kfilterdev.h>
#include <kglobal.h>
#include <kinstance.h>

#include "kbspredictormoleculeview.h"

QColor progressColor(double progress)
{
  double r, g, b;
  if(progress <= 0.25) {
    r = 0.0;
    g = progress / 0.25;
    b = 1.0;
  } else if(progress <= 0.50) {
    r = 0.0;
    g = 1.0;
    b = 1.0 - (progress - 0.25) / 0.25;
  } else if(progress <= 0.75) {
    r = (progress - 0.50) / 0.25;
    g = 1.0;
    b = 0.0;
  } else {
    r = 1.0;
    g = 1.0 - (progress - 0.75) / 0.25;
    b = 0.0;
  }
  
  return QColor(int(r * 255), int(g * 255), int(b * 255));
}

const unsigned KBSPredictorAminoAcidColor[AminoAcids+1] =
  {0xEBEBEB, 0xC8C8C8, 0xFA9600, 0xE6E600, 0x0F820F, 0xFA9600, 0x0F820F,
   0xDC9682, 0xE6E600, 0xE60A0A, 0x00DCDC, 0x0F820F, 0x145AFF, 0xE60A0A,
   0x00DCDC, 0x145AFF, 0x8282D2, 0x3232AA, 0x3232AA, 0xB45AB4, 0xBEA06E};

QColor shapelyColor(KBSPredictorAminoAcid aa)
{
  return QColor(KBSPredictorAminoAcidColor[aa]);
}

const unsigned KBSPredictorElementColor[Elements+1] =
  {0xFFFFFF, 0xFFC0CB, 0xB22222, 0x00FF00, 0xC8C8C8, 0x8F8FFF, 0xF00000,
   0xDAA520, 0xFF1493, 0x0000FF, 0x228B22, 0x808090, 0xDAA520, 0xFFA500,
   0xFFC832, 0x00FF00, 0xFF1493, 0xFF1493, 0x808090, 0x808090, 0x808090,
   0x808090, 0xFFA500, 0xA52A2A, 0xA52A2A, 0xA52A2A, 0xFF1493, 0xFF1493,
   0xFF1493, 0xA52A2A, 0xFF1493, 0xFF1493, 0x808090, 0xFF1493, 0xFF1493,
   0xFF1493, 0xFF1493, 0xA020F0, 0xFF1493, 0xFFA500, 0xFF1493, 0xDAA520,
   0xFF1493, 0xFF1493, 0xFF1493, 0xFF1493, 0xFF1493};

QColor cpkColor(KBSPredictorElement element)
{
  return QColor(KBSPredictorElementColor[element]);
}

const unsigned KBSPredictorFeatureColor[Features+1] =
  {0xF00080, 0xFFFF00, 0x6080FF, 0xFFFFFF};

QColor structureColor(KBSPredictorFeature feature)
{
  return QColor(KBSPredictorFeatureColor[feature]);
}

QColor monochromeColor()
{
  return Qt::lightGray;
}

const double KBSPredictorElementRadius[Elements+1] =
  {1.20, 1.40, 1.82, 2.00, 1.70, 1.55, 1.52, 1.47, 1.54, 2.27, 1.73, 2.00,
   2.10, 1.80, 1.80, 1.75, 1.88, 2.75, 2.00, 2.00, 2.00, 2.00, 2.00, 1.63,
   1.40, 1.39, 1.87, 1.85, 1.90, 1.85, 2.02, 1.63, 1.72, 1.58, 1.93, 2.17,
   2.06, 1.98, 2.16, 1.72, 1.66, 1.55, 1.96, 2.02, 1.86, 2.00};

double atomRadius(KBSPredictorElement element) 
{
  return KBSPredictorElementRadius[element];
}

double dotProduct(GLfloat *v1, GLfloat *v2)
{
  return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

void crossProduct(GLfloat *v1, GLfloat *v2, GLfloat *v3)
{
  v3[0] = v1[1] * v2[2] - v1[2] * v2[1];
  v3[1] = v1[2] * v2[0] - v1[0] * v2[2];
  v3[2] = v1[0] * v2[1] - v2[1] * v1[0];
}

double norm(GLfloat *v)
{
  return sqrt(dotProduct(v, v));
}

void normalize(GLfloat *v)
{
  GLfloat n = norm(v);
  v[0] /= n; v[1] /= n; v[2] /= n;
}

double distance(GLfloat *v1, GLfloat *v2)
{
  GLfloat v[] = {
                  v1[0] - v2[0],
                  v1[1] - v2[1],
                  v1[2] - v2[2]
                };
  
  return norm(v);
}

KBSPredictorVRMLRenderer::KBSPredictorVRMLRenderer(const QString &fileName)
                        : KBSPredictorMoleculeRenderer(),
                          m_color("0 0 0"), m_open(false)
{
  QIODevice *device = KFilterDev::deviceForFile(fileName, "application/x-gzip", true);
  
  if(device->open(IO_WriteOnly))
  {
    m_text.setDevice(device);
    
    const KAboutData *aboutData = KGlobal::instance()->aboutData();
    QString comment = i18n("Generated by %1 %2").arg(aboutData->programName())
                                                .arg(aboutData->version());
    m_text <<  QString("#VRML V2.0 utf8 %1\n").arg(comment);
  
    m_text << "NavigationInfo { type \"EXAMINE\" }\n";
    m_text << "Group { children [\n";
    
    m_open = true;
  }
  else
    delete device;
}

KBSPredictorVRMLRenderer::~ KBSPredictorVRMLRenderer()
{
  close();
}

bool KBSPredictorVRMLRenderer::isOpen() const
{
  return m_open;
}

void KBSPredictorVRMLRenderer::close()
{
  if(!m_open) return;
  
  if(!m_lineCoords.isEmpty())
  {
    m_text << "Shape {\n";
    m_text << "geometry IndexedLineSet {\n";
    
    QString indices;
    for(unsigned i = 0; i < m_lineCoords.count(); i+=2)
      indices += QString("%1 %2 -1 ").arg(i).arg(i+1);
    
    m_text << "coord Coordinate { point [ " << m_lineCoords.join(", ") << " ] }\n";
    m_text << "coordIndex [ " << indices << "]\n";
    m_text << "color Color { color [ " << m_lineColors.join(", ") << " ] }\n";
    m_text << "colorIndex [ " << indices << "]\n";
    
    m_text << "}\n"; // IndexedLineSet
    m_text << "}\n"; // Shape
  }
  
  if(!m_faceCoords.isEmpty())
  {
    m_text << "Shape {\n";
    m_text << "geometry IndexedFaceSet {";
    
    QString indices;
    for(unsigned i = 0; i < m_lineCoords.count(); i+=4)
      indices += QString("%1 %2 %3 -1 %4 %5 %6 -1 ").arg(i).arg(i+1).arg(i+2)
                                                    .arg(i+1).arg(i+2).arg(i+3);
    
    m_text << "coord Coordinate { point [ " << m_lineCoords.join(", ") << " ] }\n";
    m_text << "color Color { color [ " << m_lineColors.join(", ") << " ] }\n";
    m_text << "coordIndex [ " << indices << "]\n";
    m_text << "colorIndex [ " << indices << "]\n";
    
    m_text << "}\n"; // IndexedFaceSet
    m_text << "}\n"; // Shape
  }
  
  m_text << "]\n"; // children
  m_text << "}\n"; // Group
  
  QIODevice *device = m_text.device();
  device->close();
  delete device;
  
  m_open = false;
}

void KBSPredictorVRMLRenderer::setColor(QColor color)
{
  m_color = QString("%1 %2 %3").arg(double(color.red()) / 255)
                               .arg(double(color.green()) / 255)
                               .arg(double(color.blue()) / 255);
}

void KBSPredictorVRMLRenderer::drawBall(GLfloat *v, GLfloat radius)
{
  if(!m_open || radius <= 0) return;
  
  m_text << "Transform {\n";
  
  m_text << QString("translation %1 %2 %3\n").arg(v[0]).arg(v[1]).arg(v[2]);
  
  m_text << "children ";
  
  m_text << "Shape {\n";
  
  m_text << QString("geometry Sphere { radius %1 }\n").arg(radius * 0.09);
  
  m_text << "appearance Appearance {\n";
  
  m_text << "material Material { diffuseColor " << m_color << " }\n";
  
  m_text << "}\n"; // Appearance
  
  m_text << "}\n"; // Shape
  
  m_text << "}\n"; // Transform
}

void KBSPredictorVRMLRenderer::drawLine(GLfloat *v1, GLfloat *v2, GLfloat thickness)
{
  if(!m_open) return;
  
  if(thickness > 0)
  {
    const double radius = thickness * 0.03;
    
    {
      m_text << "Transform {\n";
      
      m_text << QString("translation %1 %2 %3\n").arg(v1[0]).arg(v1[1]).arg(v1[2]);
      
      m_text << "children Shape {\n";
      
      m_text << QString("geometry Sphere { radius %1 }\n").arg(radius);
        
      m_text << "appearance Appearance {\n";
      
      m_text << "material Material { diffuseColor " << m_color << " }\n";
      
      m_text << "}\n"; // Appearance
        
      m_text << "}\n"; // Shape
      
      m_text << "}\n"; // Transform
    }
    
    {
      m_text << "Transform {\n";
      
      m_text << QString("translation %1 %2 %3\n").arg(v2[0]).arg(v2[1]).arg(v2[2]);
      
      m_text << "children Shape {\n";
      
      m_text << QString("geometry Sphere { radius %1 }\n").arg(radius);
        
      m_text << "appearance Appearance {\n";
      
      m_text << "material Material { diffuseColor " << m_color << " }\n";
      
      m_text << "}\n"; // Appearance
        
      m_text << "}\n"; // Shape
      
      m_text << "}\n"; // Transform
    }
    
    {
      GLfloat v[] = {
                      (v1[0] + v2[0]) / 2,
                      (v1[1] + v2[1]) / 2,
                      (v1[2] + v2[2]) / 2
                    };
      
      const double dx = v1[0] - v2[0],
                  dy = v1[1] - v2[1],
                  dz = v1[2] - v2[2];
      
      const double height = sqrt(dx * dx + dy * dy + dz * dz),
                  alpha = acos(dy / height);
      
      m_text << "Transform {\n";
      
      m_text << QString("translation %1 %2 %3\n").arg(v[0]).arg(v[1]).arg(v[2]);
      
      m_text << QString("rotation %1 0 %2 %3").arg(dz).arg(-dx).arg(alpha);
      
      m_text << "children Shape {\n";
      
      m_text << "geometry Cylinder {\n";
      
      m_text << QString("radius %1 height %2\n").arg(radius).arg(height);
      
      m_text << "}\n"; // Cylinder
        
      m_text << "appearance Appearance {\n";
      
      m_text << "material Material { diffuseColor " << m_color << " }\n";
      
      m_text << "}\n"; // Appearance
        
      m_text << "}\n"; // Shape
      
      m_text << "}\n"; // Transform
    }
  }
  else
  {
    m_lineCoords << QString("%1 %2 %3").arg(v1[0]).arg(v1[1]).arg(v1[2]);
    m_lineCoords << QString("%1 %2 %3").arg(v2[0]).arg(v2[1]).arg(v2[2]);
    m_lineColors << m_color << m_color;
  }
}

void KBSPredictorVRMLRenderer::drawQuadrangle(GLfloat *v1, GLfloat *v2,
                                              GLfloat *v3, GLfloat *v4)
{
  if(!m_open) return;
  
  m_faceCoords << QString("%1 %2 %3").arg(v1[0]).arg(v1[1]).arg(v1[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v2[0]).arg(v2[1]).arg(v2[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v3[0]).arg(v3[1]).arg(v3[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v4[0]).arg(v4[1]).arg(v4[2]);
  m_faceColors << m_color << m_color << m_color << m_color;
}

KBSPredictorX3DRenderer::KBSPredictorX3DRenderer(const QString &fileName)
                       : KBSPredictorMoleculeRenderer(),
                         m_color("0 0 0"), m_open(false)
{
  QIODevice *device = KFilterDev::deviceForFile(fileName, "application/x-gzip", true);
  
  if(device->open(IO_WriteOnly))
  {
    m_text.setDevice(device);
    
    m_text << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    
    QDomDocumentType docType = QDomImplementation().createDocumentType(
                                 "X3D",
                                 "ISO//Web3D//DTD X3D 3.0//EN", 
                                 "http://www.web3d.org/specifications/x3d-3.0.dtd"
                               );
    
    m_doc = QDomDocument(docType);
    
    QDomElement x3d = m_doc.createElement("X3D");
    x3d.setAttribute("profile", "Immersive");
    m_doc.appendChild(x3d);
  
    QDomElement scene = m_doc.createElement("Scene");
    x3d.appendChild(scene);
    
    QDomElement navigationInfo = m_doc.createElement("NavigationInfo");
    navigationInfo.setAttribute("type", "EXAMINE");
    scene.appendChild(navigationInfo);
    
    m_group = m_doc.createElement("Group");
    scene.appendChild(m_group);
    
    m_open = true;
  }
  else
    delete device;
}

KBSPredictorX3DRenderer::~ KBSPredictorX3DRenderer()
{
  close();
}

bool KBSPredictorX3DRenderer::isOpen() const
{
  return m_open;
}

void KBSPredictorX3DRenderer::close()
{
  if(!m_open) return;
  
  if(!m_lineCoords.isEmpty())
  {
    QDomElement shape = m_doc.createElement("Shape");
    m_group.appendChild(shape);
    
    QString indices;
    for(unsigned i = 0; i < m_lineCoords.count(); i+=2)
      indices += QString("%1 %2 -1 ").arg(i).arg(i+1);
    
    QDomElement indexedLineSet = m_doc.createElement("IndexedLineSet");
    indexedLineSet.setAttribute("coordIndex", indices);
    indexedLineSet.setAttribute("colorIndex", indices);
    shape.appendChild(indexedLineSet);
    
    QDomElement coordinate = m_doc.createElement("Coordinate");
    coordinate.setAttribute("point", m_lineCoords.join(", "));
    indexedLineSet.appendChild(coordinate);
    
    QDomElement color = m_doc.createElement("Color");
    color.setAttribute("color", m_lineColors.join(", "));
    indexedLineSet.appendChild(color);
  }
  
  if(!m_faceCoords.isEmpty())
  {
    QDomElement shape = m_doc.createElement("Shape");
    m_group.appendChild(shape);
    
    QString indices;
    for(unsigned i = 0; i < m_faceCoords.count(); i+=4)
      indices += QString("%1 %2 %3 -1 %4 %5 %6 -1 ").arg(i).arg(i+1).arg(i+2)
                                                    .arg(i+1).arg(i+2).arg(i+3);
    
    QDomElement indexedFaceSet = m_doc.createElement("IndexedFaceSet");
    indexedFaceSet.setAttribute("coordIndex", indices);
    indexedFaceSet.setAttribute("colorIndex", indices);
    shape.appendChild(indexedFaceSet);
    
    QDomElement coordinate = m_doc.createElement("Coordinate");
    coordinate.setAttribute("point", m_faceCoords.join(", "));
    indexedFaceSet.appendChild(coordinate);
    
    QDomElement color = m_doc.createElement("Color");
    color.setAttribute("color", m_faceColors.join(", "));
    indexedFaceSet.appendChild(color);
  }
  
  m_text << m_doc.toString();
  
  QIODevice *device = m_text.device();
  device->close();
  delete device;
  
  m_open = false;
}

void KBSPredictorX3DRenderer::setColor(QColor color)
{
  m_color = QString("%1 %2 %3").arg(double(color.red()) / 255)
                               .arg(double(color.green()) / 255)
                               .arg(double(color.blue()) / 255);
}

void KBSPredictorX3DRenderer::drawBall(GLfloat *v, GLfloat radius)
{
  if(!m_open || radius <= 0) return;
  
  const QString coord = QString("%1 %2 %3").arg(v[0]).arg(v[1]).arg(v[2]);
  
  QDomElement transform = m_doc.createElement("Transform");
  transform.setAttribute("translation", coord);
  m_group.appendChild(transform);
  
  QDomElement shape = m_doc.createElement("Shape");
  transform.appendChild(shape);
  
  QDomElement sphere = m_doc.createElement("Sphere");
  sphere.setAttribute("radius", radius * 0.1);
  shape.appendChild(sphere);
  
  QDomElement appearance = m_doc.createElement("Appearance");
  shape.appendChild(appearance);
  
  QDomElement material = m_doc.createElement("Material");
  material.setAttribute("diffuseColor", m_color);
  appearance.appendChild(material);
}

void KBSPredictorX3DRenderer::drawLine(GLfloat *v1, GLfloat *v2, GLfloat thickness)
{
  if(!m_open) return;
  
  if(thickness > 0)
  {
    const double radius = thickness * 0.03;
    
    {
      const QString coord = QString("%1 %2 %3").arg(v1[0]).arg(v1[1]).arg(v1[2]);
      
      QDomElement transform = m_doc.createElement("Transform");
      transform.setAttribute("translation", coord);
      m_group.appendChild(transform);
      
      QDomElement shape = m_doc.createElement("Shape");
      transform.appendChild(shape);
      
      QDomElement sphere = m_doc.createElement("Sphere");
      sphere.setAttribute("radius", radius);
      shape.appendChild(sphere);
      
      QDomElement appearance = m_doc.createElement("Appearance");
      shape.appendChild(appearance);
      
      QDomElement material = m_doc.createElement("Material");
      material.setAttribute("diffuseColor", m_color);
      appearance.appendChild(material);
    }
    
    {
      const QString coord = QString("%1 %2 %3").arg(v2[0]).arg(v2[1]).arg(v2[2]);
      
      QDomElement transform = m_doc.createElement("Transform");
      transform.setAttribute("translation", coord);
      m_group.appendChild(transform);
      
      QDomElement shape = m_doc.createElement("Shape");
      transform.appendChild(shape);
      
      QDomElement sphere = m_doc.createElement("Sphere");
      sphere.setAttribute("radius", radius);
      shape.appendChild(sphere);
      
      QDomElement appearance = m_doc.createElement("Appearance");
      shape.appendChild(appearance);
      
      QDomElement material = m_doc.createElement("Material");
      material.setAttribute("diffuseColor", m_color);
      appearance.appendChild(material);
    }
    
    {
      GLfloat v[] = {
                      (v1[0] + v2[0]) / 2,
                      (v1[1] + v2[1]) / 2,
                      (v1[2] + v2[2]) / 2
                    };
      
      const double dx = v1[0] - v2[0],
                   dy = v1[1] - v2[1],
                   dz = v1[2] - v2[2];
      
      const double height = sqrt(dx * dx + dy * dy + dz * dz),
                   alpha = acos(dy / height);
      
      const QString coord = QString("%1 %2 %3").arg(v[0]).arg(v[1]).arg(v[2]),
                    rot = QString("%1 0 %2 %3").arg(dz).arg(-dx).arg(alpha);
      
      QDomElement transform = m_doc.createElement("Transform");
      transform.setAttribute("translation", coord);
      transform.setAttribute("rotation", rot);
      m_group.appendChild(transform);
      
      QDomElement shape = m_doc.createElement("Shape");
      transform.appendChild(shape);
      
      QDomElement cylinder = m_doc.createElement("Cylinder");
      cylinder.setAttribute("radius", radius);
      cylinder.setAttribute("height", height);
      shape.appendChild(cylinder);
      
      QDomElement appearance = m_doc.createElement("Appearance");
      shape.appendChild(appearance);
      
      QDomElement material = m_doc.createElement("Material");
      material.setAttribute("diffuseColor", m_color);
      appearance.appendChild(material);
    }
  }
  else
  {
    m_lineCoords << QString("%1 %2 %3").arg(v1[0]).arg(v1[1]).arg(v1[2]);
    m_lineCoords << QString("%1 %2 %3").arg(v2[0]).arg(v2[1]).arg(v2[2]);
    m_lineColors << m_color << m_color;
  }
}

void KBSPredictorX3DRenderer::drawQuadrangle(GLfloat *v1, GLfloat *v2,
                                             GLfloat *v3, GLfloat *v4)
{
  if(!m_open) return;
  
  m_faceCoords << QString("%1 %2 %3").arg(v1[0]).arg(v1[1]).arg(v1[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v2[0]).arg(v2[1]).arg(v2[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v3[0]).arg(v3[1]).arg(v3[2]);
  m_faceCoords << QString("%1 %2 %3").arg(v4[0]).arg(v4[1]).arg(v4[2]);
  m_faceColors << m_color << m_color << m_color << m_color;
}

const unsigned KBSPredictorMoleculeModel::s_divisions = 10;

KBSPredictorMoleculeModel::KBSPredictorMoleculeModel(QObject *parent, char *name)
                         : QObject(parent, name),
                           m_groups(0), m_backbone(NULL), m_atom(NULL),
                           m_style(Backbone), m_coloring(Group)
{
}

KBSPredictorMoleculeModel::~KBSPredictorMoleculeModel()
{
  if(NULL != m_backbone) delete m_backbone;
  if(NULL != m_atom) delete m_atom;
}

KBSPredictorMoleculeModel::Style KBSPredictorMoleculeModel::style() const
{
  return m_style;
}

void KBSPredictorMoleculeModel::setStyle(Style style)
{
  if(m_style == style || !isSupportedStyle(style)) return;
  m_style = style;
  
  if(!isSupportedColoring(m_coloring)) m_coloring = Monochrome;
  
  emit styleChanged();
}

bool KBSPredictorMoleculeModel::isSupportedStyle(Style style) const
{
  return(style < Wireframe || !m_atoms.isEmpty());
}

KBSPredictorMoleculeModel::Coloring KBSPredictorMoleculeModel::coloring() const
{
  return m_coloring;
}

void KBSPredictorMoleculeModel::setColoring(Coloring coloring)
{
  if(m_coloring == coloring || !isSupportedColoring(coloring)) return;
  m_coloring = coloring;
  
  emit coloringChanged();
}

bool KBSPredictorMoleculeModel::isSupportedColoring(Coloring coloring) const
{
  switch(coloring) {
    case CPK:
      return(m_style >= Wireframe && !m_atoms.isEmpty()); 
    case Shapely:
      return(!m_seq.isEmpty());
    default:
      return true;
  }
}

void KBSPredictorMoleculeModel::setChain(const QValueList<KBSPredictorMonssterAtom> &chain)
{
  m_groups = chain.count() >= 2 ? chain.count() - 2 : 0;
  if(m_seq.count() != m_groups) m_seq.clear();
  m_atoms.clear();
  
  if(NULL != m_atom) delete m_atom;
  m_atom = NULL;
  
  if(NULL != m_backbone) delete m_backbone;
  m_backbone = NULL;

  if(0 == m_groups) return;
  
  const unsigned points = m_groups + s_divisions * (m_groups - 1);
  m_backbone = new GLfloat[3 * points];
  
  unsigned g;
  QValueList<KBSPredictorMonssterAtom>::const_iterator atom;
  for(g = 0, atom = chain.at(1); g < m_groups; ++g, ++atom)
  {
    const unsigned p = g * (s_divisions + 1);
    
    m_backbone[3*p + 0] = 0.1 * ((*atom).x - 50.0);
    m_backbone[3*p + 1] = 0.1 * ((*atom).y - 50.0);
    m_backbone[3*p + 2] = 0.1 * ((*atom).z - 50.0);
  }
  
  interpolateBackbone();
  
  emit dataChanged();
}

void KBSPredictorMoleculeModel::setSeq(const KBSPredictorMonssterSeq &seq)
{
  m_seq = seq.groups;
  
  emit dataChanged();
}

void KBSPredictorMoleculeModel::setPDB(const KBSPredictorProteinPDB &pdb)
{
  m_groups = pdb.groups;
  m_seq.clear();
  m_atoms = pdb.atom;
  
  const unsigned atoms = m_atoms.count();
  
  if(NULL != m_atom) delete m_atom;
  m_atom = new GLfloat[3 * atoms];
  
  if(NULL != m_backbone) delete m_backbone;
  const unsigned points = m_groups + s_divisions * (m_groups - 1);
  m_backbone = new GLfloat[3 * points];
  
  unsigned a, g;
  QValueList<KBSPredictorAtomPDB>::const_iterator atom;
  for(a = 0, g = 0, atom = m_atoms.begin(); atom != m_atoms.end(); ++a, ++atom)
  {
    m_atom[3*a + 0] = 0.1 * (*atom).x;
    m_atom[3*a + 1] = 0.1 * (*atom).y;
    m_atom[3*a + 2] = 0.1 * (*atom).z;
    
    if((*atom).name.iupac == "CA")
    {
      const unsigned p = g++ * (s_divisions + 1);
      
      m_backbone[3*p + 0] = 0.1 * (*atom).x;
      m_backbone[3*p + 1] = 0.1 * (*atom).y;
      m_backbone[3*p + 2] = 0.1 * (*atom).z;
      
      KBSPredictorMonssterResidue seq;
      seq.resSeq = (*atom).resSeq;
      seq.resName = (*atom).resName;
      seq.count[0] = seq.count[1] = 1;
      m_seq << seq;
    }
  }
  
  interpolateBackbone();
  
  emit dataChanged();
}

void KBSPredictorMoleculeModel::rotateData(int dx, int dy)
{
  const double alpha = 1e-2 * dx, beta = 1e-2 * dy;
  
  const double sinAlpha = sin(alpha), cosAlpha = cos(alpha),
               sinBeta = sin(beta), cosBeta = cos(beta);
  
  if(NULL != m_backbone)
  {
    const unsigned points = m_groups + s_divisions * (m_groups - 1);
    
    for(unsigned p = 0; p < points; ++p)
    {
      const double x = m_backbone[3*p + 0],
                   y = m_backbone[3*p + 1],
                   z = m_backbone[3*p + 2];
      
      const double tx = x * cosAlpha + z * sinAlpha,
                   ty = x * sinAlpha * sinBeta + y * cosBeta - z * cosAlpha * sinBeta,
                   tz = -x * sinAlpha * cosBeta + y * sinBeta + z * cosAlpha * cosBeta;
      
      m_backbone[3*p + 0] = tx;
      m_backbone[3*p + 1] = ty;
      m_backbone[3*p + 2] = tz;
    }
  }
  
  if(NULL != m_atom)
  {
    const unsigned atoms = m_atoms.count();
    
    for(unsigned a = 0; a < atoms; ++a)
    {
      const double x = m_atom[3*a + 0],
                   y = m_atom[3*a + 1],
                   z = m_atom[3*a + 2];
      
      const double tx = x * cosAlpha + z * sinAlpha,
                   ty = x * sinAlpha * sinBeta + y * cosBeta - z * cosAlpha * sinBeta,
                   tz = -x * sinAlpha * cosBeta + y * sinBeta + z * cosAlpha * cosBeta;
      
      m_atom[3*a + 0] = tx;
      m_atom[3*a + 1] = ty;
      m_atom[3*a + 2] = tz;
    }
  }

  emit dataChanged();
}

bool KBSPredictorMoleculeModel::exportVRML(const QString &fileName) const
{
  if(0 == m_groups) return false;
  
  KBSPredictorVRMLRenderer renderer(fileName);
  if(!renderer.isOpen()) return false;
  
  render(&renderer);
  renderer.close();

  return true;
}

bool KBSPredictorMoleculeModel::exportX3D(const QString &fileName) const
{
  if(0 == m_groups) return false;
  
  KBSPredictorX3DRenderer renderer(fileName);
  if(!renderer.isOpen()) return false;
  
  render(&renderer);
  renderer.close();

  return true;
}

void KBSPredictorMoleculeModel::render(KBSPredictorMoleculeRenderer *renderer) const
{
  if(Backbone == m_style)
  {
    for(unsigned g1 = 0; g1 < m_groups-1; ++g1)
    {
      QColor c1;
      switch(m_coloring) {
        case Group:
          c1 = progressColor(double(g1) / (m_groups - 1));
          break;
        case Shapely:
          if(g1 < m_seq.count()) {
            c1 = shapelyColor(m_seq[g1].resName);
            break;
          }
        default:
          c1 = monochromeColor();
          break;
      }
      
      const unsigned g2 = g1 + 1;
      
      QColor c2;
      switch(m_coloring) {
        case Group:
          c2 = progressColor(double(g2) / (m_groups - 1));
          break;
        case Shapely:
          if(g2 < m_seq.count()) {
            c2 = shapelyColor(m_seq[g2].resName);
            break;
          }
        default:
          c2 = monochromeColor();
          break;
      }
      
      const unsigned p1 = g1 * (s_divisions + 1),
                      p2 = p1 + (s_divisions + 1);          
      
      if(c1 != c2)
      {
        GLfloat v[] = {
                        (m_backbone[3*p1 + 0] + m_backbone[3*p2 + 0]) / 2,
                        (m_backbone[3*p1 + 1] + m_backbone[3*p2 + 1]) / 2,
                        (m_backbone[3*p1 + 2] + m_backbone[3*p2 + 2]) / 2
                      };
        
        renderer->setColor(c1);
        renderer->drawLine(m_backbone + 3*p1, v, 1);
        renderer->setColor(c2);
        renderer->drawLine(v, m_backbone + 3*p2, 1);
      }
      else
      {
        renderer->setColor(c1);
        renderer->drawLine(m_backbone + 3*p1, m_backbone + 3*p2, 1);
      }
    }
  }
  else if(Spline >= m_style)
  {
    const unsigned points = m_groups + s_divisions * (m_groups - 1);
    
    for(unsigned p1 = 0; p1 < points-1; ++p1)
    {
      const unsigned g = unsigned(double(p1) / (s_divisions + 1) + 0.5);
      
      switch(m_coloring) {
        case Group:
          renderer->setColor(progressColor(double(g) / (m_groups - 1)));
          break;
        case Shapely:
          if(g < m_seq.count()) {
            renderer->setColor(shapelyColor(m_seq[g].resName));
            break;
          }
        default:
          renderer->setColor(monochromeColor());
          break;
      }
      
      const unsigned p2 = p1 + 1;
      
      renderer->drawLine(m_backbone + 3*p1, m_backbone + 3*p2);
    }  
  }
  else
  {
    const double radius = (BallAndStick == m_style) ? 0.3 
                        : (Spacefill == m_style) ? 1.0
                        : 0.0;
    
    const double thickness = (BallAndStick == m_style) ? 0.4
                           : (Sticks == m_style) ? 1.0
                           : 0.0;
    
    for(QValueList<KBSPredictorAtomPDB>::const_iterator atom = m_atoms.begin();
        atom != m_atoms.end(); ++atom)
    {
      const unsigned ga = (*atom).resSeq - 1,
                     pa = (*atom).serial - 1;
      
      QColor ca;
      switch(m_coloring) {
        case Group:
          ca = progressColor(double(ga) / (m_groups - 1));
          break;
        case Shapely:
          ca = shapelyColor(m_seq[ga].resName);
          break;
        case CPK:
          ca = cpkColor((*atom).element);
          break;
        default:
          ca = monochromeColor();
          break;
      }
      
      renderer->setColor(ca);
      
      if(Spacefill == m_style || BallAndStick == m_style)
        renderer->drawBall(m_atom + 3*pa, radius * atomRadius((*atom).element));
      
      if(Spacefill == m_style) continue;
      
      for(QValueList<KBSPredictorAtomPDB>::const_iterator parent = m_atoms.begin();
          parent != atom; ++parent)
      {
        if(!(*atom).covalentBond(*parent)) continue;
        
        const unsigned gp = (*parent).resSeq - 1,
                       pp = (*parent).serial - 1;
        
        QColor cp;
        switch(m_coloring) {
          case Group:
            cp = progressColor(double(gp) / (m_groups - 1));
            break;
          case Shapely:
            cp = shapelyColor(m_seq[gp].resName);
            break;
          case CPK:
            cp = cpkColor((*parent).element);
            break;
          default:
            cp = monochromeColor();
            break;
        }
        
        if(cp != ca)
        {
          GLfloat v[] = {
                          (m_atom[3*pp + 0] + m_atom[3*pa + 0]) / 2,
                          (m_atom[3*pp + 1] + m_atom[3*pa + 1]) / 2,
                          (m_atom[3*pp + 2] + m_atom[3*pa + 2]) / 2
                        };
          
          renderer->drawLine(v, m_atom + 3*pa, thickness);
          renderer->setColor(cp);
          renderer->drawLine(m_atom + 3*pp, v, thickness);
        }
        else
          renderer->drawLine(m_atom + 3*pp, m_atom + 3*pa, thickness);
      }
    }
  }
}

void KBSPredictorMoleculeModel::interpolateBackbone()
{
  for(unsigned g = 0; g < m_groups - 1; ++g)
  {
    const unsigned p1 = g * (s_divisions + 1),
                   p2 = p1 + (s_divisions + 1);
    
    const double d12 = distance(m_backbone + 3*p1, m_backbone + 3*p2);
    
    GLfloat a1[3], a2[3];
    for(unsigned i = 0; i < 3; ++i) {
      if(g > 0) {
        const unsigned p0 = p1 - (s_divisions + 1);
        
        a1[i] = (m_backbone[3*p2 + i] - m_backbone[3*p0 + i]);
        a1[i] *= d12 / distance(m_backbone + 3*p2, m_backbone + 3*p0);
      } else
        a1[i] = m_backbone[3*p2 + i] - m_backbone[3*p1 + i];
     
      a1[i] *= 0.4;
      a1[i] += m_backbone[3*p1 + i];
      
      if(g < m_groups-2) {
        const unsigned p3 = p2 + (s_divisions + 1);
        
        a2[i] = (m_backbone[3*p3 + i] - m_backbone[3*p1 + i]);
        a2[i] *= d12 / distance(m_backbone + 3*p3, m_backbone + 3*p1);
      }
      else
        a2[i] = m_backbone[3*p2 + i] - m_backbone[3*p1 + i];
      
      a2[i] *= -0.4;
      a2[i] += m_backbone[3*p2 + i];
    }
    
    for(unsigned p = p1 + 1; p < p2; ++p)
    {
      const double t = double(p - p1) / (s_divisions + 1),
                   t1 = 1 - t;
      
      double cp1 = t1 * t1 * t1,
             ca1 = 3 * cp1 * t / t1,
             ca2 = ca1 * t / t1,
             cp2 = (ca2 / 3) * t / t1;
      
      for(unsigned i = 0; i < 3; ++i)
        m_backbone[3*p + i] = cp1 * m_backbone[3*p1 + i]
                            + ca1 * a1[i] + ca2 * a2[i]
                            + cp2 * m_backbone[3*p2 + i];
    }
  }
}

KBSPredictorMoleculeView::KBSPredictorMoleculeView(QWidget *parent, char *name)
                        : QGLWidget(parent, name),
                          KBSPredictorMoleculeRenderer(), m_scale(1),
                          m_model(new KBSPredictorMoleculeModel(this)),
                          m_tracking(false), m_quadric(NULL), m_base(0)
{
  setFocusPolicy(StrongFocus);

  connect(m_model, SIGNAL(styleChanged()), this, SLOT(updateGL()));
  connect(m_model, SIGNAL(coloringChanged()), this, SLOT(updateGL()));
  connect(m_model, SIGNAL(dataChanged()), this, SLOT(updateGL()));
}

KBSPredictorMoleculeView::~KBSPredictorMoleculeView()
{
  makeCurrent();
  if(0 != m_base) glDeleteLists(m_base, Shapes);
  if(NULL != m_quadric) gluDeleteQuadric(m_quadric);
}

void KBSPredictorMoleculeView::setColor(QColor color)
{
  qglColor(color);
}

void KBSPredictorMoleculeView::drawBall(GLfloat *v, GLfloat radius)
{
  if(radius <= 0) return;
  
  glEnable(GL_LIGHTING);
  
  glPushMatrix();          
  glTranslatef(v[0], v[1], v[2]);
  glScalef(radius, radius, radius);
  glCallList(m_base + Sphere);
  glPopMatrix();
  
  glDisable(GL_LIGHTING);
}

void KBSPredictorMoleculeView::drawLine(GLfloat *v1, GLfloat *v2, GLfloat thickness)
{
  if(thickness > 0)
  {
    glEnable(GL_LIGHTING);
    
    glPushMatrix();          
    glTranslatef(v1[0], v1[1], v1[2]);
    glScalef(thickness * 0.3, thickness * 0.3, thickness * 0.3);
    glCallList(m_base + Sphere);
    glPopMatrix();
    
    glPushMatrix();          
    glTranslatef(v2[0], v2[1], v2[2]);
    glScalef(thickness * 0.3, thickness * 0.3, thickness * 0.3);
    glCallList(m_base + Sphere);
    glPopMatrix();
    
    const double dx = v1[0] - v2[0],
                 dy = v1[1] - v2[1],
                 dz = v1[2] - v2[2];
    
    const double height = sqrt(dx * dx + dy * dy + dz * dz),
                 alpha = 180 * acos(dz / height) / M_PI;
    
    glPushMatrix();          
    glTranslatef(v2[0], v2[1], v2[2]);
    glRotated(-alpha, dy, -dx, 0);
    glScaled(thickness, thickness, height);
    glCallList(m_base + Cylinder);
    glPopMatrix();
    
    glDisable(GL_LIGHTING);
  }
  else
  {
    glBegin(GL_LINES);
    glVertex3fv(v1);
    glVertex3fv(v2);
    glEnd();
  }
}

void KBSPredictorMoleculeView::drawQuadrangle(GLfloat *v1, GLfloat *v2,
                                              GLfloat *v3, GLfloat *v4)
{
  glBegin(GL_TRIANGLE_STRIP);
  glVertex3fv(v1);
  glVertex3fv(v2);
  glVertex3fv(v3);
  glVertex3fv(v4);
  glEnd();
}

KBSPredictorMoleculeModel *KBSPredictorMoleculeView::model() const
{
  return m_model;
}

QPixmap KBSPredictorMoleculeView::pixmap()
{
  updateGL();

  QImage image = grabFrameBuffer();

  QPixmap out;
  out.convertFromImage(image, AvoidDither);

  return out;
}

void KBSPredictorMoleculeView::initializeGL()
{
  qglClearColor(black);
  glShadeModel(GL_SMOOTH);
  
  glEnable(GL_DEPTH_TEST);
  
  GLfloat lightPosition[] = {0.5, 0.5, 0.5, 0.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
  glEnable(GL_LIGHT0);
  
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_NORMALIZE);
  
  m_quadric = gluNewQuadric();
  gluQuadricDrawStyle(m_quadric, GLU_FILL);
  
  m_base = glGenLists(Shapes);
  
  glNewList(m_base + Sphere, GL_COMPILE);
    gluSphere(m_quadric, 0.09, 15, 10);
  glEndList();
  
  glNewList(m_base + Cylinder, GL_COMPILE);
    gluCylinder(m_quadric, 0.03, 0.03, 1, 10, 10);
  glEndList();
}

void KBSPredictorMoleculeView::resizeGL(int width, int height)
{
  if(0 == height) height++;
  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, GLfloat(width) / GLfloat(height), 1, 200);
  glMatrixMode(GL_MODELVIEW);
}

void KBSPredictorMoleculeView::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  if(0 == m_model->m_groups) return;
  
  glPushMatrix();  
  gluLookAt(0, 0, 10, 0, 0, 0, 0, 1, 0);
  glScaled(m_scale, m_scale, m_scale);
  m_model->render(this);
  glPopMatrix();
  
  glFlush();
}

void KBSPredictorMoleculeView::keyPressEvent(QKeyEvent *e)
{
  switch(e->key()) {
    case Key_Up:
      m_model->rotateData(0, -10);
      break;
    case Key_Down:
      m_model->rotateData(0, 10);
      break;
    case Key_Left:
      m_model->rotateData(-10, 0);
      break;
    case Key_Right:
      m_model->rotateData(10, 0);
      break;
    case Key_Plus:
      m_scale *= 1.05;
      updateGL();
      break;
    case Key_Minus:
      m_scale /= 1.05;
      updateGL();
      break;
    default:
      e->ignore();
      break;
  }
}

void KBSPredictorMoleculeView::mousePressEvent(QMouseEvent *e)
{
  if(e->button() == LeftButton) {
    m_tracking = true;
    m_last = e->pos();
  } else
    e->ignore();
}

void KBSPredictorMoleculeView::mouseReleaseEvent(QMouseEvent *e)
{
  if(m_tracking)
  {
    if(e->state() & ControlButton) {
      const int dy = (e->pos().y() - m_last.y());
      if(dy > 0) m_scale /= (1 + 1e-3 * dy); else m_scale *= (1 - 1e-3 * dy);
      updateGL();
    } else
      m_model->rotateData(e->pos().x() - m_last.x(), e->pos().y() - m_last.y());
    
    m_tracking = false;
  }
  else
    e->ignore();
}

void KBSPredictorMoleculeView::mouseMoveEvent(QMouseEvent *e)
{
  if(m_tracking)
  {
    if(e->state() & ControlButton) {
      const int dy = (e->pos().y() - m_last.y());
      if(dy > 0) m_scale /= (1 + 1e-3 * dy); else m_scale *= (1 - 1e-3 * dy);
      updateGL();
    } else
      m_model->rotateData(e->pos().x() - m_last.x(), e->pos().y() - m_last.y());
    
    m_last = e->pos();
  }
  else
    e->ignore();
}

#include "kbspredictormoleculeview.moc"
