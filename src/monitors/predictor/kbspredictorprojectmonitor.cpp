/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>

#include <kbsboincmonitor.h>

#include <kbspredictormoleculelog.h>

#include "kbspredictorprojectmonitor.h"

const QString KBSPredictorBurialsOpenName = "burials";
const QString KBSPredictorECovers24OpenName = "ecovers_24";
const QString KBSPredictorProfile3OpenName = "profile3";
const QString KBSPredictorQuasi3OpenName = "quasi3";
const QString KBSPredictorScale3BOpenName = "scale3b";
const QString KBSPredictorS1234OpenName = "s1234";
const QString KBSPredictorS1234HOpenName = "s1234h";
const QString KBSPredictorS1234EOpenName = "s1234e";
const QString KBSPredictorMonssterInitChainOpenName = "monsster.init.chain";
const QString KBSPredictorMonssterInputOpenName = "monsster.input";
const QString KBSPredictorMonssterSeqOpenName = "monsster.seq";
const QString KBSPredictorMonssterRestraintsOpenName = "monsster.restraints";
const QString KBSPredictorMonssterFinalChainOpenName = "monsster.final.chain";
const QString KBSPredictorMonssterFinalPDBOpenName = "monsster.final.pdb";
const QString KBSPredictorMonssterFinalNOEOpenName = "monsster.final.noe";

const QString KBSPredictorParam19InpOpenName = "param19.inp";
const QString KBSPredictorTopH19InpOpenName = "toph19.inp";
const QString KBSPredictorCharmmInpOpenName = "charmm.inp";
const QString KBSPredictorProteinPDBOpenName = "protein.pdb";
const QString KBSPredictorProteinNOEOpenName = "protein.noe";
const QString KBSPredictorSeedStreamOpenName = "seed.stream";
const QString KBSPredictorProteinFinalPDBOpenName = "proteinfinal.pdb";

KBSPredictorProjectMonitor::KBSPredictorProjectMonitor(const QString &project, KBSBOINCMonitor *parent,
                                                       const char *name)
                          : KBSProjectMonitor(project, parent, name)
{
  const KBSBOINCClientState *state = parent->state();
  if(NULL != state) m_start = state->workunit.keys();
  
  connect(parent, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  
  connect(parent, SIGNAL(resultsCompleted(const QStringList &)),
          this, SLOT(logResults(const QStringList &)));
  
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

KBSPredictorProjectMonitor::~KBSPredictorProjectMonitor()
{
  for(QDictIterator<KBSPredictorResult> it(m_results); it.current() != NULL; ++it)
    delete it.current();
  m_results.clear();
}

const KBSPredictorResult *KBSPredictorProjectMonitor::result(const QString &workunit)
{
  return validWorkunit(workunit) ? m_results.find(workunit) : NULL;
}

void KBSPredictorProjectMonitor::setState(const QString &workunit, const KBSPredictorState &state)
{
  KBSPredictorResult *result = mkResult(workunit);
  if(NULL == result) {
    result = mkResult(workunit);
    result->app_type = state.app_type;
  }
  
  if(result->app_type != state.app_type) return;
  
  if(MFOLD == result->app_type)
    result->mfold.monsster.final.chain = state.monsster_restart.chain;
  
  emit updatedResult(workunit);
}

KBSLogManager *KBSPredictorProjectMonitor::logManager() const
{
  return NULL;
}
    
bool KBSPredictorProjectMonitor::parseable(const QString &openName) const
{
  return(// openName == KBSPredictorBurialsOpenName
   // || openName == KBSPredictorECovers24OpenName
   // || openName == KBSPredictorProfile3OpenName
   // || openName == KBSPredictorQuasi3OpenName
   // || openName == KBSPredictorScale3BOpenName
   // || openName == KBSPredictorS1234OpenName
   // || openName == KBSPredictorS1234HOpenName
   // || openName == KBSPredictorS1234EOpenName
         openName == KBSPredictorMonssterInitChainOpenName
      || openName == KBSPredictorMonssterInputOpenName
      || openName == KBSPredictorMonssterSeqOpenName
      || openName == KBSPredictorMonssterRestraintsOpenName
      || openName == KBSPredictorMonssterFinalChainOpenName
      || openName == KBSPredictorMonssterFinalPDBOpenName
   // || openName == KBSPredictorMonssterFinalNOEOpenName
      || openName == KBSPredictorCharmmInpOpenName
      || openName == KBSPredictorProteinPDBOpenName
      || openName == KBSPredictorProteinNOEOpenName
      || openName == KBSPredictorSeedStreamOpenName
      || openName == KBSPredictorProteinFinalPDBOpenName);
}

bool KBSPredictorProjectMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  if(!m_meta.contains(file->fileName)) return false;
  const KBSFileMetaInfo meta = m_meta[file->fileName];
  
  if(KBSPredictorBurialsOpenName == meta.open_name)
  {
    KBSPredictorBurials burials;
    if(!burials.parse(lines)) return false;
    
    setBurials(burials, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorECovers24OpenName == meta.open_name)
  {
    KBSPredictorECovers24 ecovers_24;
    if(!ecovers_24.parse(lines)) return false;
    
    setECovers24(ecovers_24, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorProfile3OpenName == meta.open_name)
  {
    KBSPredictorProfile3 profile3;
    if(!profile3.parse(lines)) return false;
    
    setProfile3(profile3, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorQuasi3OpenName == meta.open_name)
  {
    KBSPredictorQuasi3 quasi3;
    if(!quasi3.parse(lines)) return false;
    
    setQuasi3(quasi3, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorScale3BOpenName == meta.open_name)
  {
    QValueList<KBSPredictorScale3B> scale3b;
    if(!parseScale3B(lines, scale3b)) return false;
    
    setScale3B(scale3b, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorS1234OpenName == meta.open_name)
  {
    KBSPredictorS1234 s1234;
    if(!s1234.parse(lines)) return false;
    
    setS1234(s1234, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorS1234HOpenName == meta.open_name)
  {
    KBSPredictorS1234 s1234h;
    if(!s1234h.parse(lines)) return false;
    
    setS1234H(s1234h, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorS1234EOpenName == meta.open_name)
  {
    KBSPredictorS1234 s1234e;
    if(!s1234e.parse(lines)) return false;
    
    setS1234E(s1234e, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterInitChainOpenName == meta.open_name)
  {
    QValueList<KBSPredictorMonssterAtom> init_chain;
    if(!parseMonssterChain(lines, init_chain)) return false;
    
    setMonssterInitChain(init_chain, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterInputOpenName == meta.open_name)
  {
    KBSPredictorMonssterInput input;
    if(!input.parse(lines)) return false;
    
    setMonssterInput(input, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterSeqOpenName == meta.open_name)
  {
    KBSPredictorMonssterSeq seq;
    if(!seq.parse(lines)) return false;
    
    setMonssterSeq(seq, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterRestraintsOpenName == meta.open_name)
  {
    QValueList<KBSPredictorMonssterRestraint> restraints;
    if(!parseMonssterRestraints(lines, restraints)) return false;
    
    setMonssterRestraints(restraints, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterFinalChainOpenName == meta.open_name)
  {
    QValueList<KBSPredictorMonssterAtom> final_chain;
    if(!parseMonssterChain(lines, final_chain)) return false;
    
    setMonssterFinalChain(final_chain, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterFinalPDBOpenName == meta.open_name)
  {
    KBSPredictorProteinPDB final_pdb;
    if(!final_pdb.parse(lines)) return false;
    
    setMonssterFinalPDB(final_pdb, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorMonssterFinalNOEOpenName == meta.open_name)
  {
    QValueList<KBSPredictorProteinNOE> final_noe;
    if(!parseProteinNOE(lines, final_noe)) return false;
    
    setMonssterFinalNOE(final_noe, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorParam19InpOpenName == meta.open_name)
  {
    setAppType(CHARMM, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorTopH19InpOpenName == meta.open_name)
  {
    setAppType(CHARMM, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorCharmmInpOpenName == meta.open_name)
  {
    KBSPredictorCharmmInp inp;
    if(!inp.parse(lines)) return false;
    
    setCharmmInp(inp, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorProteinPDBOpenName == meta.open_name)
  {
    KBSPredictorProteinPDB pdb;
    if(!pdb.parse(lines)) return false;
    
    setProteinPDB(pdb, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorProteinNOEOpenName == meta.open_name)
  {
    QValueList<KBSPredictorProteinNOE> noe;
    if(!parseProteinNOE(lines, noe)) return false;
    
    setProteinNOE(noe, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorSeedStreamOpenName == meta.open_name)
  {
    unsigned seed;
    if(!parseSeedStream(lines, seed)) return false;
    
    setSeedStream(seed, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSPredictorProteinFinalPDBOpenName == meta.open_name)
  {
    KBSPredictorProteinPDB final_pdb;
    if(!final_pdb.parse(lines)) return false;
    
    setProteinFinalPDB(final_pdb, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else
    return false;
}

KBSPredictorResult *KBSPredictorProjectMonitor::mkResult(const QString &workunit)
{
  KBSPredictorResult *result = m_results.find(workunit);
  
  if(NULL == result) {
    result = new KBSPredictorResult();
    m_results.insert(workunit, result);
  }
  
  return result;
}

bool KBSPredictorProjectMonitor::parseScale3B(const QStringList &lines,
                                              QValueList<KBSPredictorScale3B> &scale3b)
{
  scale3b.clear();
  QStringList::const_iterator line = lines.constBegin();

  if(lines.constEnd() == line) return false;
  const unsigned n = (*line).toUInt(0, 10);
  ++line;
  
  for(unsigned i = 0; i < n; i++)
  {
    if(lines.constEnd() == line) return false;
    
    KBSPredictorScale3B item;
    if(!item.parse(*line)) return false;

    scale3b << item;
    line++;
  }

  return true;
}

bool KBSPredictorProjectMonitor::parseMonssterChain(const QStringList &lines,
                                                    QValueList<KBSPredictorMonssterAtom> &chain)
{
  chain.clear();

  QStringList::const_iterator line = lines.constBegin();
  if(lines.constEnd() == line) return false;

  QStringList fields = QStringList::split(" ", *line);
  if(fields.isEmpty()) return false;
  
  const unsigned n = fields[0].toUInt(0, 10);
  ++line;
  
  for(unsigned i = 0; i < n; i++)
  {
    KBSPredictorMonssterAtom item;
    
    if(lines.constEnd() == line || !item.parse(*line)) return false;
    ++line;

    chain << item;
  }

  return true;
}

bool KBSPredictorProjectMonitor::parseMonssterRestraints(const QStringList &lines,
                                                         QValueList<KBSPredictorMonssterRestraint> &restraints)
{
  restraints.clear();
  QStringList::const_iterator line = lines.constBegin();

  if(lines.constEnd() == line) return false;
  const unsigned n = (*line).toUInt(0, 10);
  ++line;
  
  for(unsigned i = 0; i < n; i++)
  {
    if(lines.constEnd() == line) return false;
    
    KBSPredictorMonssterRestraint item;
    if(!item.parse(*line)) return false;
    
    restraints << item;
    ++line;
  }

  return true;
}

bool KBSPredictorProjectMonitor::parseProteinNOE(const QStringList &lines,
                                                 QValueList<KBSPredictorProteinNOE> &noe)
{
  noe.clear();
  QStringList::const_iterator line = lines.constBegin();
  
  // skip header
  if(lines.constEnd() == line) return false;
  ++line;
  
  while(line != lines.constEnd())
  {
    if((*line).startsWith("END")) break;
    
    KBSPredictorProteinNOE item;
    if(!item.parse(*line)) return false;
    
    noe << item;
    ++line;
  }
    
  return true;
}

bool KBSPredictorProjectMonitor::parseSeedStream(const QStringList &lines, unsigned &seed)
{
  QStringList::const_iterator line = lines.constBegin();
  if(lines.constEnd() == line) return false;
  
  sscanf(*line, "set seed = %u", &seed);
   
  return true;
}

void KBSPredictorProjectMonitor::setAppType(KBSPredictorAppType app_type, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    mkResult(*workunit)->app_type = app_type;
}

void KBSPredictorProjectMonitor::setBurials(const KBSPredictorBurials &burials, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.burials = burials;
  }
}

void KBSPredictorProjectMonitor::setECovers24(const KBSPredictorECovers24 &ecovers_24,
                                              const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.ecovers_24 = ecovers_24;
  }
}

void KBSPredictorProjectMonitor::setProfile3(const KBSPredictorProfile3 &profile3,
                                             const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.profile3 = profile3;
  }
}

void KBSPredictorProjectMonitor::setQuasi3(const KBSPredictorQuasi3 &quasi3, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.quasi3 = quasi3;
  }
}

void KBSPredictorProjectMonitor::setScale3B(const QValueList<KBSPredictorScale3B> &scale3b,
                                            const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.scale3b = scale3b;
  }
}

void KBSPredictorProjectMonitor::setS1234(const KBSPredictorS1234 &s1234, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.s1234 = s1234;
  }
}

void KBSPredictorProjectMonitor::setS1234H(const KBSPredictorS1234 &s1234h, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.s1234h = s1234h;
  }
}

void KBSPredictorProjectMonitor::setS1234E(const KBSPredictorS1234 &s1234e, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.s1234e = s1234e;
  }
}

void KBSPredictorProjectMonitor::setMonssterInitChain(const QValueList<KBSPredictorMonssterAtom> &init_chain,
                                                      const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.init_chain = init_chain;
    
    if(!m_start.contains(*workunit) && result->mfold.monsster.seq.groups.count() > 0)
      KBSPredictorMoleculeLog::self()->logWorkunit(*workunit, result);
  }
}

void KBSPredictorProjectMonitor::setMonssterInput(const KBSPredictorMonssterInput &input,
                                                  const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.input = input;
  }
}

void KBSPredictorProjectMonitor::setMonssterSeq(const KBSPredictorMonssterSeq &seq,
                                                const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.seq = seq;
    
    if(!m_start.contains(*workunit) && result->mfold.monsster.init_chain.count() > 0)
      KBSPredictorMoleculeLog::self()->logWorkunit(*workunit, result);
  }
}

void KBSPredictorProjectMonitor::setMonssterRestraints(const QValueList<KBSPredictorMonssterRestraint> &restraints,
                                                       const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.restraints = restraints;
  }
}

void KBSPredictorProjectMonitor::setMonssterFinalChain(const QValueList<KBSPredictorMonssterAtom> &final_chain,
                                                       const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.final.chain = final_chain;
  }
}

void KBSPredictorProjectMonitor::setMonssterFinalPDB(const KBSPredictorProteinPDB &final_pdb,
                                                       const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.final.pdb = final_pdb;
  }
}

void KBSPredictorProjectMonitor::setMonssterFinalNOE(const QValueList<KBSPredictorProteinNOE> &final_noe,
                                                       const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = MFOLD;
    result->mfold.monsster.final.noe = final_noe;
  }
}

void KBSPredictorProjectMonitor::setCharmmInp(const KBSPredictorCharmmInp &inp,
                                              const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = CHARMM;
    result->charmm.inp = inp;
    
    if(!m_start.contains(*workunit))
      KBSPredictorMoleculeLog::self()->logWorkunit(*workunit, result);
  }
}

void KBSPredictorProjectMonitor::setProteinPDB(const KBSPredictorProteinPDB &pdb,
                                               const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = CHARMM;
    result->charmm.protein.pdb = pdb;
    
    if(!m_start.contains(*workunit))
      KBSPredictorMoleculeLog::self()->logWorkunit(*workunit, result);
  }
}

void KBSPredictorProjectMonitor::setProteinNOE(const QValueList<KBSPredictorProteinNOE> &noe,
                                               const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = CHARMM;
    result->charmm.protein.noe = noe;
  }
}

void KBSPredictorProjectMonitor::setSeedStream(unsigned seed, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = CHARMM;
    result->charmm.seed.stream = seed;
  }
}

void KBSPredictorProjectMonitor::setProteinFinalPDB(const KBSPredictorProteinPDB &final_pdb,
                                                    const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
  {
    KBSPredictorResult *result = mkResult(*workunit);
    
    result->app_type = CHARMM;
    result->charmm.protein.final_pdb = final_pdb;
  }
}

void KBSPredictorProjectMonitor::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    KBSPredictorResult *result = m_results.take(*workunit);
    if(NULL != result) delete result;
  }
}

void KBSPredictorProjectMonitor::logResults(const QStringList &results)
{
  const KBSBOINCClientState *state = boincMonitor()->state();
  if(NULL == state) return;
  
  for(QStringList::const_iterator result = results.constBegin(); result != results.constEnd(); ++result)
  {
    if(boincMonitor()->project(state->result[*result]) != project()) continue;
    
    KBSPredictorResult *predictorResult = m_results.find(state->result[*result].wu_name);
    if(NULL == predictorResult) continue;
    
    KBSPredictorMoleculeLog::self()->logResult(*result, predictorResult);
  }
}

void KBSPredictorProjectMonitor::updateFile(const QString &fileName)
{
  if(!m_meta.contains(fileName)) return;
  
  QStringList workunits = m_meta[fileName].workunits;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    emit updatedResult(*workunit);
}

#include "kbspredictorprojectmonitor.moc"
