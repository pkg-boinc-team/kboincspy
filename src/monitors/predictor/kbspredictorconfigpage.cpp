#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbspredictorconfigpage.ui'
**
** Created: Mon Feb 6 18:28:57 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbspredictorconfigpage.h"

#include <qvariant.h>
#include <qimage.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <kcombobox.h>
#include <kurlrequester.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSPredictorConfigPage as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSPredictorConfigPage::KBSPredictorConfigPage( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSPredictorConfigPage" );
    KBSPredictorConfigPageLayout = new QVBoxLayout( this, 11, 6, "KBSPredictorConfigPageLayout"); 

    group_0 = new QGroupBox( this, "group_0" );
    group_0->setColumnLayout(0, Qt::Vertical );
    group_0->layout()->setSpacing( 6 );
    group_0->layout()->setMargin( 11 );
    group_0Layout = new QVBoxLayout( group_0->layout() );
    group_0Layout->setAlignment( Qt::AlignTop );

    layout_filter_0 = new QHBoxLayout( 0, 0, 6, "layout_filter_0"); 

    label_filter_0 = new QLabel( group_0, "label_filter_0" );
    layout_filter_0->addWidget( label_filter_0 );

    kcfg_filter_0 = new KComboBox( FALSE, group_0, "kcfg_filter_0" );
    layout_filter_0->addWidget( kcfg_filter_0 );

    label_format_0 = new QLabel( group_0, "label_format_0" );
    layout_filter_0->addWidget( label_format_0 );

    kcfg_format_0 = new KComboBox( FALSE, group_0, "kcfg_format_0" );
    layout_filter_0->addWidget( kcfg_format_0 );

    label_style_0 = new QLabel( group_0, "label_style_0" );
    layout_filter_0->addWidget( label_style_0 );

    kcfg_style_0 = new KComboBox( FALSE, group_0, "kcfg_style_0" );
    layout_filter_0->addWidget( kcfg_style_0 );

    label_coloring_0 = new QLabel( group_0, "label_coloring_0" );
    layout_filter_0->addWidget( label_coloring_0 );

    kcfg_coloring_0 = new KComboBox( FALSE, group_0, "kcfg_coloring_0" );
    layout_filter_0->addWidget( kcfg_coloring_0 );
    spacer_picture_0 = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_filter_0->addItem( spacer_picture_0 );
    group_0Layout->addLayout( layout_filter_0 );

    layout_location_0 = new QHBoxLayout( 0, 0, 6, "layout_location_0"); 

    label_location_0 = new QLabel( group_0, "label_location_0" );
    layout_location_0->addWidget( label_location_0 );

    kcfg_location_0 = new KURLRequester( group_0, "kcfg_location_0" );
    kcfg_location_0->setMode( 10 );
    layout_location_0->addWidget( kcfg_location_0 );
    group_0Layout->addLayout( layout_location_0 );
    KBSPredictorConfigPageLayout->addWidget( group_0 );

    group_1 = new QGroupBox( this, "group_1" );
    group_1->setColumnLayout(0, Qt::Vertical );
    group_1->layout()->setSpacing( 6 );
    group_1->layout()->setMargin( 11 );
    group_1Layout = new QVBoxLayout( group_1->layout() );
    group_1Layout->setAlignment( Qt::AlignTop );

    layout_filter_1 = new QHBoxLayout( 0, 0, 6, "layout_filter_1"); 

    label_filter_1 = new QLabel( group_1, "label_filter_1" );
    layout_filter_1->addWidget( label_filter_1 );

    kcfg_filter_1 = new KComboBox( FALSE, group_1, "kcfg_filter_1" );
    layout_filter_1->addWidget( kcfg_filter_1 );

    label_format_1 = new QLabel( group_1, "label_format_1" );
    layout_filter_1->addWidget( label_format_1 );

    kcfg_format_1 = new KComboBox( FALSE, group_1, "kcfg_format_1" );
    layout_filter_1->addWidget( kcfg_format_1 );

    label_style_1 = new QLabel( group_1, "label_style_1" );
    layout_filter_1->addWidget( label_style_1 );

    kcfg_style_1 = new KComboBox( FALSE, group_1, "kcfg_style_1" );
    layout_filter_1->addWidget( kcfg_style_1 );

    label_coloring_1 = new QLabel( group_1, "label_coloring_1" );
    layout_filter_1->addWidget( label_coloring_1 );

    kcfg_coloring_1 = new KComboBox( FALSE, group_1, "kcfg_coloring_1" );
    layout_filter_1->addWidget( kcfg_coloring_1 );
    spacer_1 = new QSpacerItem( 1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_filter_1->addItem( spacer_1 );
    group_1Layout->addLayout( layout_filter_1 );

    layout_location_1 = new QHBoxLayout( 0, 0, 6, "layout_location_1"); 

    label_location_1 = new QLabel( group_1, "label_location_1" );
    layout_location_1->addWidget( label_location_1 );

    kcfg_location_1 = new KURLRequester( group_1, "kcfg_location_1" );
    kcfg_location_1->setMode( 10 );
    layout_location_1->addWidget( kcfg_location_1 );
    group_1Layout->addLayout( layout_location_1 );
    KBSPredictorConfigPageLayout->addWidget( group_1 );
    spacer = new QSpacerItem( 1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSPredictorConfigPageLayout->addItem( spacer );
    languageChange();
    resize( QSize(606, 215).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSPredictorConfigPage::~KBSPredictorConfigPage()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSPredictorConfigPage::languageChange()
{
    setCaption( tr2i18n( "General preferences for ProteinPredictorAtHome" ) );
    setIconText( tr2i18n( "ProteinPredictor" ) );
    group_0->setTitle( tr2i18n( "Protein Prediction (MONSSTER)" ) );
    label_filter_0->setText( tr2i18n( "Save:" ) );
    kcfg_filter_0->clear();
    kcfg_filter_0->insertItem( tr2i18n( "none" ) );
    kcfg_filter_0->insertItem( tr2i18n( "work unit" ) );
    kcfg_filter_0->insertItem( tr2i18n( "result" ) );
    kcfg_filter_0->insertItem( tr2i18n( "both" ) );
    label_format_0->setText( tr2i18n( "Format:" ) );
    kcfg_format_0->clear();
    kcfg_format_0->insertItem( tr2i18n( "VRML" ) );
    kcfg_format_0->insertItem( tr2i18n( "X3D" ) );
    label_style_0->setText( tr2i18n( "Style:" ) );
    kcfg_style_0->clear();
    kcfg_style_0->insertItem( tr2i18n( "backbone" ) );
    kcfg_style_0->insertItem( tr2i18n( "spline" ) );
    label_coloring_0->setText( tr2i18n( "Coloring:" ) );
    kcfg_coloring_0->clear();
    kcfg_coloring_0->insertItem( tr2i18n( "monochrome" ) );
    kcfg_coloring_0->insertItem( tr2i18n( "group" ) );
    kcfg_coloring_0->insertItem( tr2i18n( "shapely" ) );
    label_location_0->setText( tr2i18n( "Location:" ) );
    group_1->setTitle( tr2i18n( "Protein Refinement (CHARMM)" ) );
    label_filter_1->setText( tr2i18n( "Save:" ) );
    kcfg_filter_1->clear();
    kcfg_filter_1->insertItem( tr2i18n( "none" ) );
    kcfg_filter_1->insertItem( tr2i18n( "work unit" ) );
    kcfg_filter_1->insertItem( tr2i18n( "result" ) );
    kcfg_filter_1->insertItem( tr2i18n( "both" ) );
    label_format_1->setText( tr2i18n( "Format:" ) );
    kcfg_format_1->clear();
    kcfg_format_1->insertItem( tr2i18n( "VRML" ) );
    kcfg_format_1->insertItem( tr2i18n( "X3D" ) );
    label_style_1->setText( tr2i18n( "Style:" ) );
    kcfg_style_1->clear();
    kcfg_style_1->insertItem( tr2i18n( "backbone" ) );
    kcfg_style_1->insertItem( tr2i18n( "spline" ) );
    kcfg_style_1->insertItem( tr2i18n( "wireframe" ) );
    kcfg_style_1->insertItem( tr2i18n( "sticks" ) );
    kcfg_style_1->insertItem( tr2i18n( "spacefill" ) );
    kcfg_style_1->insertItem( tr2i18n( "ball & stick" ) );
    label_coloring_1->setText( tr2i18n( "Coloring:" ) );
    kcfg_coloring_1->clear();
    kcfg_coloring_1->insertItem( tr2i18n( "monochrome" ) );
    kcfg_coloring_1->insertItem( tr2i18n( "group" ) );
    kcfg_coloring_1->insertItem( tr2i18n( "shapely" ) );
    kcfg_coloring_1->insertItem( tr2i18n( "CPK" ) );
    label_location_1->setText( tr2i18n( "Location:" ) );
}

void KBSPredictorConfigPage::init()
{
}

#include "kbspredictorconfigpage.moc"
