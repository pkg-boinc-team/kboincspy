/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qgl.h>
#include <qimage.h>

#include <kapplication.h>
#include <kio/netaccess.h>
#include <ktempfile.h>

#include "kbspredictormoleculelog.h"

KBSPredictorMoleculeLog *KBSPredictorMoleculeLog::s_self = NULL;

KBSPredictorMoleculeLog *KBSPredictorMoleculeLog::self()
{
  if(NULL == s_self) s_self = new KBSPredictorMoleculeLog();
  
  return s_self;
}

KBSPredictorMoleculeLog::KBSPredictorMoleculeLog(QObject *parent, const char *name)
                       : QObject(parent, name)
{
}

const KBSPredictorMoleculeLogPreferences &KBSPredictorMoleculeLog::preferences(KBSPredictorAppType type) const
{
  return m_preferences[type];
}

void KBSPredictorMoleculeLog::setPreferences(KBSPredictorAppType type,
                                             const KBSPredictorMoleculeLogPreferences &preferences)
{
  m_preferences[type] = preferences;
}

void KBSPredictorMoleculeLog::logWorkunit(const QString &name, const KBSPredictorResult *data)
{
  if(!QGLFormat::hasOpenGL()) return;
  
  KBSPredictorAppType app_type = data->app_type;
  KBSPredictorMoleculeLogPreferences preferences = this->preferences(app_type);
  
  if(!preferences.url.isValid()) return;
  
  KBSPredictorMoleculeModel *model = new KBSPredictorMoleculeModel();
  
  if(app_type == MFOLD) {
    model->setChain(data->mfold.monsster.init_chain);
    model->setSeq(data->mfold.monsster.seq);
  } else
    model->setPDB(data->charmm.protein.pdb);
  model->setStyle(preferences.style);
  model->setColoring(preferences.coloring);
  
  const QString fileName = name + "." + preferences.format.lower() + ".gz";
  KURL url(preferences.url, fileName);
  
  if(preferences.filter & KBSPredictorMoleculeLogPreferences::Workunit
     && url.isValid() && !KIO::NetAccess::exists(url, false, kapp->mainWidget()))
    if(url.isLocalFile())
      if("WRL" == preferences.format)
        model->exportVRML(url.path(-1));
      else
        model->exportX3D(url.path(-1));
    else
    {
      KTempFile fileTemp;
      fileTemp.setAutoDelete(true);
      
      if("WRL" == preferences.format)
        model->exportVRML(fileTemp.name());
      else
        model->exportX3D(fileTemp.name());
      KIO::NetAccess::upload(fileTemp.name(), url, kapp->mainWidget());
    } 
  
  delete model;
}

void KBSPredictorMoleculeLog::logResult(const QString &name, const KBSPredictorResult *data)
{
  if(!QGLFormat::hasOpenGL()) return;
  
  KBSPredictorAppType app_type = data->app_type;
  KBSPredictorMoleculeLogPreferences preferences = this->preferences(app_type);
  
  if(!preferences.url.isValid()) return;
  
  KBSPredictorMoleculeModel *model = new KBSPredictorMoleculeModel();
  if(app_type == MFOLD) {
    model->setChain(data->mfold.monsster.final.chain);
    model->setSeq(data->mfold.monsster.seq);
  } else
    model->setPDB(data->charmm.protein.final_pdb);
  model->setStyle(preferences.style);
  model->setColoring(preferences.coloring);
  
  const QString fileName = name + "." + preferences.format.lower();
  KURL url(preferences.url, fileName);
  
  if(preferences.filter & KBSPredictorMoleculeLogPreferences::Result
     && url.isValid() && !KIO::NetAccess::exists(url, false, kapp->mainWidget()))
    if(url.isLocalFile())
      if("WRL" == preferences.format)
        model->exportVRML(url.path(-1));
      else
        model->exportX3D(url.path(-1));
    else
    {
      KTempFile fileTemp;
      fileTemp.setAutoDelete(true);
    
      if("WRL" == preferences.format)
        model->exportVRML(fileTemp.name());
      else
        model->exportX3D(fileTemp.name());
      KIO::NetAccess::upload(fileTemp.name(), url, kapp->mainWidget());
    }
    
    delete model; 
}

#include "kbspredictormoleculelog.moc"
