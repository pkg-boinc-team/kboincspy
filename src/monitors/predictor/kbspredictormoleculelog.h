/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPREDICTORMOLECULELOG_H
#define KBSPREDICTORMOLECULELOG_H

#include <qobject.h>
#include <qsize.h>

#include <kurl.h>

#include <kbspredictordata.h>

#include <kbspredictormoleculeview.h>

struct KBSPredictorMoleculeLogPreferences {
  enum Filter {Workunit=1, Result=2};
  
  KBSPredictorMoleculeModel::Style style;
  KBSPredictorMoleculeModel::Coloring coloring;
  unsigned filter;
  QString format;
  KURL url;
};

class KBSPredictorProjectMonitor;

class KBSPredictorMoleculeLog : public QObject
{
  Q_OBJECT
  public:
    static KBSPredictorMoleculeLog *self();
    
    virtual const KBSPredictorMoleculeLogPreferences &preferences(KBSPredictorAppType type) const;
    virtual void setPreferences(KBSPredictorAppType type,
                                const KBSPredictorMoleculeLogPreferences &preferences);
    
    virtual void logWorkunit(const QString &name, const KBSPredictorResult *data);
    virtual void logResult(const QString &name, const KBSPredictorResult *data);
  
  protected:
    KBSPredictorMoleculeLog(QObject *parent=0, const char *name=0);
    
  protected:
    KBSPredictorMoleculeLogPreferences m_preferences[CHARMM+1];
    
  private:
    static KBSPredictorMoleculeLog *s_self;
};

#endif
