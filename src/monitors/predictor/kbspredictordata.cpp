/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <math.h>

#include <qregexp.h>

#include "kbspredictordata.h"

const QString KBSPredictorAminoAcidName[] =
  {"GLY", "ALA", "SER", "CYS", "VAL", "THR", "ILE", "PRO", "MET", "ASP",
   "ASN", "LEU", "LYS", "GLU", "GLN", "ARG", "HIS", "PHE", "TYR", "TRP",
   "???"};

const char KBSPredictorAminoAcidAbbrev[] =
  {'G', 'A', 'S', 'C', 'V', 'T', 'I', 'P', 'M', 'D',
   'N', 'L', 'K', 'E', 'Q', 'R', 'H', 'F', 'Y', 'W',
   '?'};

const unsigned KBSPredictorAminoAcidAtoms[] =
  {4, 5, 6, 6, 7, 7, 8, 7, 8, 8, 8, 8, 9, 9, 9, 11, 10, 11, 12, 14, 0};

/*
const double KBSPredictorAminoAcidMass[] =
  { 75.07, 89.09, 105.09, 121.16, 117.15, 119.12, 131.17, 115.13, 149.21, 133.10,
    132.12, 131.17, 146.19, 147.13, 146.15, 174.20, 155.16, 165.19, 181.19, 204.23,
    0.0};
*/

const QString KBSPredictorElementName[Elements+1] =
  {"H", "HE",
   "LI", "B", "C", "N", "O", "F", "NE",
   "NA", "MG", "AL", "SI", "P", "S", "CL", "AR",
   "K", "CA", "TI", "CR", "MN", "FE", "NI", "CU", "ZN", "GA", "AS", "SE", "BR", "KR",
   "PD", "AG", "CD", "IN", "SN", "TE", "I", "XE",
   "BA", "PT", "AU", "HG", "TL", "PB",
   "U",
   "?"};

bool parseAminoAcid(const QString &name, KBSPredictorAminoAcid &aa)
{
  for(unsigned i = 0; i < AminoAcids; ++i)
    if(KBSPredictorAminoAcidName[i] == name) {
      aa = KBSPredictorAminoAcid(i);
      return true;
    }

  return false;
}

bool parseElement(const QString &name, KBSPredictorElement &element)
{
  // special case for hydrogen
  if(name.startsWith("H")) {
    element = Hydrogen;
    return true;
  }

  for(unsigned i = 1; i < Elements; ++i)
    if(name == KBSPredictorElementName[i]) {
      element = KBSPredictorElement(i);
      return true;
    }

  return false;
}

QValueList<uint> parseUIntList(const QString &text)
{
  QStringList list = QStringList::split(" ", text);
  QValueList<uint> out;
  
  for(QStringList::const_iterator it = list.constBegin(); list.constEnd() != it; ++it)
    out << (*it).toUInt(0, 10);
    
  return out;
}

QValueList<double> parseDoubleList(const QString &text)
{
  QStringList list = QStringList::split(" ", text);
  QValueList<double> out;
  
  for(QStringList::const_iterator it = list.constBegin(); list.constEnd() != it; ++it)
    out << (*it).toDouble();
    
  return out;
}

bool KBSPredictorBurials::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  if(lines.constEnd() == line
  || !(*line).contains("Average percentage of burial")) return false;
  ++line;
  
  // skip header
  if(lines.constEnd() == line) return false;
  ++line;
  
  // parse table
  for(unsigned row = 0; row <= AminoAcids; ++row)
  {
    if(lines.constEnd() == line) return false;
    
    QValueList<double> values = parseDoubleList((*line).mid(4));
    if(values.count() != AminoAcids+1)  return false;
     
    for(unsigned column = 0; column <= AminoAcids; ++column)
      avg[row][column] = values[column];
                
    ++line;
  }
  
  if(lines.constEnd() == line 
  || !(*line).contains("Average standard deviation of burial")) return false;
  ++line;
  
  // skip header
  if(lines.constEnd() == line) return false;
  ++line;
  
  // parse table
  for(unsigned row = 0; row <= AminoAcids; ++row)
  {
    if(lines.constEnd() == line) return false;
    
    QValueList<double> values = parseDoubleList((*line).mid(4));
    if(values.count() != AminoAcids+1)  return false;
        
    for(unsigned column = 0; column <= AminoAcids; ++column)
      sdev[row][column] = values[column];
                
    ++line;
  }
    
  if(lines.constEnd() == line 
  || !(*line).contains("Number of pairs used")) return false;
  ++line; if(lines.constEnd() == line) return false;
  
  // skip header
  ++line; if(lines.constEnd() == line) return false;
  
  // parse table
  for(unsigned row = 0; row <= AminoAcids; ++row)
  {
    QValueList<unsigned> values = parseUIntList((*line).mid(4));
    if(values.count() != AminoAcids+1)  return false;
        
    for(unsigned column = 0; column <= AminoAcids; ++column)
      pairs[row][column] = values[column];
                
    ++line; if(lines.constEnd() == line) return false;
  }
  
  return true;
}

bool KBSPredictorECovers24::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  // skip header
  if(lines.constEnd() == line) return false;
  ++line;
  
  // parse table
  for(unsigned row = 0; row < AminoAcids; ++row)
  {
    if(lines.constEnd() == line) return false;
    
    QValueList<double> values = parseDoubleList((*line).mid(4));
    if(values.count() != 25)  return false;
     
    for(unsigned column = 0; column < 25; ++column)
      value[row][column] = values[column];
                
    ++line;
  }
  
  return true;
}
  
bool KBSPredictorProfile3::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  for(unsigned aa = 0; aa < AminoAcids; ++aa)
  {
    // skip header
    if(lines.constEnd() == line) return false;
    ++line;

    for(unsigned set = 0; set < 5; ++set)
      for(unsigned row = 0; row < 5; ++row)
      {
        if(lines.constEnd() == line) return false;
        sscanf(*line, "%lf %lf %lf %lf %lf",
               &value[aa][set][row][0],
               &value[aa][set][row][1],
               &value[aa][set][row][2],
               &value[aa][set][row][3],
               &value[aa][set][row][4]);
        ++line;
      }
  }
  
  return true;
}

bool KBSPredictorQuasi3::parse(const QStringList &lines)
{
  unsigned npar = 0, nmid = 0, nant = 0;
  QStringList::const_iterator line = lines.constBegin();
  
  while(lines.constEnd() != line)
  {
    if((*line).startsWith("PAR")) {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
      {
        if(lines.constEnd() == line) return false;
    
        QValueList<double> values = parseDoubleList((*line).mid(4));
        if(values.count() != AminoAcids)  return false;
     
        for(unsigned column = 0; column < AminoAcids; ++column)
          par[npar][row][column] = values[column];
                
        ++line; 
      }
      
      npar++;
    }
    else if((*line).startsWith("MID"))
    {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
      {
        if(lines.constEnd() == line) return false;
    
        QValueList<double> values = parseDoubleList((*line).mid(4));
        if(values.count() != AminoAcids)  return false;
     
        for(unsigned column = 0; column < AminoAcids; ++column)
          mid[nmid][row][column] = values[column];
                
        ++line; 
      }
      
      nmid++;
    }
    else if((*line).startsWith("ANT"))
    {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
      {
        if(lines.constEnd() == line) return false;
    
        QValueList<double> values = parseDoubleList((*line).mid(4));
        if(values.count() != AminoAcids)  return false;
     
        for(unsigned column = 0; column < AminoAcids; ++column)
          ant[nant][row][column] = values[column];
                
        ++line; 
      }
      
      nant++;
    }
    else
      return false;
  }
  
  return true;
}

bool KBSPredictorScale3B::parse(const QString &line)
{
  if(!parseAminoAcid(line.mid(0, 3), aa[0])) return false;
  if(!parseAminoAcid(line.mid(4, 3), aa[1])) return false;
  if(!parseAminoAcid(line.mid(8, 3), aa[2])) return false;
  sscanf(line.mid(12), "%u %u %u %lf",
         &count[0], &count[1], &count[2],
         &value);
  
  return true;
}

bool KBSPredictorS1234::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  while(lines.constEnd() != line)
  {
    if((*line).startsWith("##### R1.2")) {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
        for(unsigned column = 0; column < AminoAcids; ++column)
        {
          // skip header
          if(lines.constEnd() == line) return false;
          line++;
          
          if(lines.constEnd() == line) return false;
          sscanf(*line, "%lf %lf %lf",
                 &r1_2[row][column][0],
                 &r1_2[row][column][1],
                 &r1_2[row][column][2]);

          line++;
        }
    } else if((*line).startsWith("##### R1.3")) {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
        for(unsigned column = 0; column < AminoAcids; ++column)
        {
          // skip header
          if(lines.constEnd() == line) return false;
          line++;
          
          if(lines.constEnd() == line) return false;
          sscanf(*line, "%lf %lf %lf %lf",
                 &r1_3[row][column][0],
                 &r1_3[row][column][1],
                 &r1_3[row][column][2],
                 &r1_3[row][column][3]);

          line++;
        }
    } else if((*line).startsWith("##### R1.4")) {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
        for(unsigned column = 0; column < AminoAcids; ++column)
        {
          // skip header
          if(lines.constEnd() == line) return false;
          line++;
          
          if(lines.constEnd() == line) return false;
          sscanf(*line, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
                 &r1_4[row][column][0],
                 &r1_4[row][column][1],
                 &r1_4[row][column][2],
                 &r1_4[row][column][3],
                 &r1_4[row][column][4],
                 &r1_4[row][column][5],
                 &r1_4[row][column][6],
                 &r1_4[row][column][7],
                 &r1_4[row][column][8],
                 &r1_4[row][column][9],
                 &r1_4[row][column][10],
                 &r1_4[row][column][11],
                 &r1_4[row][column][12],
                 &r1_4[row][column][13]);

          line++;
        }
    } else if((*line).startsWith("##### R1.5")) {
      line++;
      
      for(unsigned row = 0; row < AminoAcids; ++row)
        for(unsigned column = 0; column < AminoAcids; ++column)
        {
          // skip header
          if(lines.constEnd() == line) return false;
          line++;
          
          if(lines.constEnd() == line) return false;
          sscanf(*line, "%lf %lf %lf %lf %lf %lf %lf",
                 &r1_5[row][column][0],
                 &r1_5[row][column][1],
                 &r1_5[row][column][2],
                 &r1_5[row][column][3],
                 &r1_5[row][column][4],
                 &r1_5[row][column][5],
                 &r1_5[row][column][6]);

          line++;
        }
    } else
      return false;
  }
  
  return true;
}

bool KBSPredictorMonssterAtom::parse(const QString &line)
{
  sscanf(line, "%u %u %u", &x, &y, &z);
  
  return true;
}

bool KBSPredictorMonssterInput::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();

  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u %u %u %u", &random, &ncycle, &icycle, &tsteps);
  ++line;
         
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u %u", &resmin, &resmax);
  ++line;
  
  if(lines.constEnd() == line) return false;
  ++line;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%lf %lf %lf %lf", &temp[0], &temp[1], &softcore, &central);
  ++line;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%lf %lf %lf %lf %lf", &stiff, &pair, &kdcore, &hbond, &shrt);
  ++line;
    
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%lf %lf %lf", &burial, &multibody, &threebody);
  ++line;
  
  return true;
}

bool KBSPredictorMonssterResidue::parse(const QString &line)
{
  resSeq = line.left(5).toUInt(0, 10);
  if(!parseAminoAcid(line.mid(8, 3), resName)) return false;
  sscanf(line.mid(12), "%u %u", &count[0], &count[1]);
  
  return true;
}

bool KBSPredictorMonssterSeq::parse(const QStringList &lines)
{
  atoms = 0;
  groups.clear();
  QStringList::const_iterator line = lines.constBegin();

  while(lines.constEnd() != line)
  {
    KBSPredictorMonssterResidue item;
    if(!item.parse(*line)) return false;
    
    atoms += KBSPredictorAminoAcidAtoms[item.resName];
    groups << item;
    ++line;
  }

  return true;
}

QString KBSPredictorMonssterSeq::toString() const
{
  QString out;

  unsigned column = 0;
  for(QValueList<KBSPredictorMonssterResidue>::const_iterator group = groups.begin();
      group != groups.end(); ++group)
  {
    if(column > 0 && (column % 60) == 0) out.append('\n');
    out.append(KBSPredictorAminoAcidAbbrev[(*group).resName]);
    ++column;
  }

  return out;
}

bool KBSPredictorMonssterRestraint::parse(const QString &line)
{
  sscanf(line, "%u %lf", &num, &value);
  
  return true;
}

bool KBSPredictorAtomPDB::parse(const QString &line)
{
  const unsigned len = line.length();
  
  if(len < 6 || line.left(6) != "ATOM  ") return false;
  
  serial = (len > 6) ? line.mid(6, 5).toUInt(0, 10) : 0;
  
  if(len > 12)
  {
    if(!parseElement(line.mid(12, 2).stripWhiteSpace().upper(), element))
      return false;
    
    name.remoteness = 0;
    const QString greek = " ABGDEZHT";
    while(line.at(14) != greek.at(name.remoteness))
      if(++name.remoteness >= greek.length())
        break;
    
    name.branch = (line.at(15) == ' ') ? 0 : (line.at(15) - '1');
    name.iupac = line.mid(12, 4).stripWhiteSpace();
  }
  else
  {
    name.iupac = QString::null;
    name.remoteness = name.branch = 0;
    element = Elements;
  }
  
  altLoc = (len > 16) ? line.at(16) : QChar(' ');
  
  if(len > 17) {
    if(!parseAminoAcid(line.mid(17, 3), resName)) return false;
  } else
    resName = AminoAcids;
  
  chainID = (len > 21) ? line.at(21) : QChar(' ');
  
  resSeq = (len > 22) ? line.mid(22, 4).toUInt(0, 10) : 0;
  
  iCode = (len > 26) ? line.at(26) : QChar(' ');
  
  x = (len > 30) ? line.mid(30, 8).toDouble() : 0.0;
  
  y = (len > 38) ? line.mid(38, 8).toDouble() : 0.0;
  
  z = (len > 46) ? line.mid(46, 8).toDouble() : 0.0;
  
  occupancy = (len > 54) ? line.mid(54, 6).toDouble() : 0.0;
  
  tempFactor = (len > 60) ? line.mid(60, 6).toDouble() : 0.0;
  
  segID = (len > 72) ? line.mid(72, 4).stripWhiteSpace() : QString::null;
  
  if(len > 76 && !parseElement(line.mid(76, 2).stripWhiteSpace().upper(), element))
    return false;
  
  charge = (len > 78) ? line.mid(78, 2).stripWhiteSpace() : QString::null;
  
  return true;
}

bool KBSPredictorAtomPDB::covalentBond(const KBSPredictorAtomPDB &atom) const
{
  const double dx = x - atom.x;
  if(dx < -1.9 || dx > 1.9) return false;

  const double dy = y - atom.y;
  if(dy < -1.9 || dy > 1.9) return false;

  double dz = z - atom.z;
  if(dz < -1.9 || dz > 1.9) return false;
  
  const double d = dx * dx + dy * dy + dz * dz;
  if(d < 0.16) return false;
  
  if(element == Hydrogen || atom.element == Hydrogen)
    return(d < 1.44);
  
  return(d < 3.61);
}

double distance(const KBSPredictorAtomPDB &a1, const KBSPredictorAtomPDB &a2)
{
  const double dx = a1.x - a2.x,
               dy = a1.y - a2.y,
               dz = a1.z - a2.z;
  
  return sqrt(dx * dx + dy * dy + dz * dz);
}

bool operator<(const KBSPredictorAtomPDB &a1, const KBSPredictorAtomPDB &a2)
{
  return(a1.serial < a2.serial);
}

bool KBSPredictorHelixPDB::parse(const QString &line)
{
  const unsigned len = line.length();
  
  if(len < 6 || line.left(6) != "HELIX ") return false;
  
  serNum = (len > 7) ? line.mid(7, 3).toUInt(0, 10) : 0;
  
  helixID = (len > 11) ? line.mid(11, 3).stripWhiteSpace() : QString::null;
  
  if(len > 15) {
    if(!parseAminoAcid(line.mid(15, 3), init.resName)) return false;
  } else
    init.resName = AminoAcids;
  
  init.chainID = (len > 19) ? line.at(19) : QChar(' ');
  
  init.seqNum = (len > 21) ? line.mid(21, 4).toUInt(0, 10) : 0;
  
  init.iCode = (len > 25) ? line.at(25) : QChar(' ');
  
  if(len > 27) {
    if(!parseAminoAcid(line.mid(27, 3), end.resName)) return false;
  } else
    end.resName = AminoAcids;
  
  end.chainID = (len > 31) ? line.at(31) : QChar(' ');
  
  end.seqNum = (len > 33) ? line.mid(33, 4).toUInt(0, 10) : 0;
  
  end.iCode = (len > 37) ? line.at(37) : QChar(' ');
  
  helixClass = (len > 38) ? KBSPredictorHelixClass(line.mid(38, 2).toUInt(0, 10))
                          : RightHandedApha;
  
  comment = (len > 40) ? line.mid(40, 30).stripWhiteSpace() : QString::null;
  
  length = (len > 71) ? line.mid(71, 5).toUInt(0, 10) : 0;
  
  return true;
}

bool operator<(const KBSPredictorHelixPDB &h1, const KBSPredictorHelixPDB &h2)
{
  return(h1.init.seqNum < h2.init.seqNum);
}

bool KBSPredictorSheetPDB::parse(const QString &line)
{
  const unsigned len = line.length();
  
  if(len < 6 || line.left(6) != "SHEET ") return false;
  
  strand = (len > 7) ? line.mid(7, 3).toUInt(0, 10) : 0;
  
  sheetID = (len > 11) ? line.mid(11, 3).stripWhiteSpace() : QString::null;
  
  numStrands = (len > 14) ? line.mid(14, 2).toUInt(0, 10) : 0;
  
  if(len > 17) {
    if(!parseAminoAcid(line.mid(17, 3), init.resName)) return false;
  } else
    init.resName = AminoAcids;
  
  init.chainID = (len > 21) ? line.at(21) : QChar(' ');
  
  init.seqNum = (len > 22) ? line.mid(22, 4).toUInt(0, 10) : 0;
  
  init.iCode = (len > 26) ? line.at(26) : QChar(' ');
  
  if(len > 28) {
    if(!parseAminoAcid(line.mid(28, 3), end.resName)) return false;
  } else
    end.resName = AminoAcids;
  
  end.chainID = (len > 32) ? line.at(32) : QChar(' ');
  
  end.seqNum = (len > 33) ? line.mid(33, 4).toUInt(0, 10) : 0;
  
  end.iCode = (len > 37) ? line.at(37) : QChar(' ');
  
  sense = (len > 38) ? line.mid(38, 2).toInt(0, 10) : 0;
  
  curr.atom = (len > 41) ? line.mid(41, 4).stripWhiteSpace() : QString::null;
  
  if(len > 45) {
    if(!parseAminoAcid(line.mid(45, 3), curr.resName)) return false;
  } else
    curr.resName = AminoAcids;
  
  curr.chainId = (len > 49) ? line.at(49) : QChar(' ');
  
  curr.resSeq = (len > 50) ? line.mid(50, 4).toUInt(0, 10) : 0;
  
  curr.iCode = (len > 54) ? line.at(54) : QChar(' ');
  
  prev.atom = (len > 56) ? line.mid(56, 4).stripWhiteSpace() : QString::null;
  
  if(len > 60) {
    if(!parseAminoAcid(line.mid(60, 3), prev.resName)) return false;
  } else
    prev.resName = AminoAcids;
  
  prev.chainId = (len > 64) ? line.at(64) : QChar(' ');
  
  prev.resSeq = (len > 65) ? line.mid(65, 4).toUInt(0, 10) : 0;
  
  prev.iCode = (len > 69) ? line.at(69) : QChar(' ');
  
  return true;
}

bool operator<(const KBSPredictorSheetPDB &s1, const KBSPredictorSheetPDB &s2)
{
  return(s1.init.seqNum < s2.init.seqNum);
}

bool KBSPredictorTurnPDB::parse(const QString &line)
{
  const unsigned len = line.length();
  
  if(len < 6 || line.left(6) != "TURN  ") return false;
  
  seq = (len > 7) ? line.mid(7, 3).toUInt(0, 10) : 0;
  
  turnID = (len > 11) ? line.mid(11, 3).stripWhiteSpace() : QString::null;
  
  if(len > 15) {
    if(!parseAminoAcid(line.mid(15, 3), init.resName)) return false;
  } else
    init.resName = AminoAcids;
  
  init.chainID = (len > 19) ? line.at(19) : QChar(' ');
  
  init.seqNum = (len > 20) ? line.mid(20, 4).toUInt(0, 10) : 0;
  
  init.iCode = (len > 24) ? line.at(24) : QChar(' ');
  
  if(len > 26) {
    if(!parseAminoAcid(line.mid(26, 3), end.resName)) return false;
  } else
    end.resName = AminoAcids;
  
  end.chainID = (len > 30) ? line.at(30) : QChar(' ');
  
  end.seqNum = (len > 31) ? line.mid(31, 4).toUInt(0, 10) : 0;
  
  end.iCode = (len > 35) ? line.at(35) : QChar(' ');
  
  comment = (len > 40) ? line.mid(40, 30) : QString::null;
  
  return true;
}

bool operator<(const KBSPredictorTurnPDB &t1, const KBSPredictorTurnPDB &t2)
{
  return(t1.init.seqNum < t2.init.seqNum);
}

KBSPredictorFeature KBSPredictorStructurePDB::feature(unsigned group, unsigned *index)
{
  unsigned i;
  
  i = 0;
  for(QValueList<KBSPredictorHelixPDB>::iterator it = helix.begin();
      it != helix.end(); ++it, ++i)
    if(group >= (*it).init.seqNum && group <= (*it).end.seqNum) {
      if(NULL != index) *index = i;
      return AlphaHelix;
    }
  
  i = 0;
  for(QValueList<KBSPredictorSheetPDB>::iterator it = sheet.begin();
      it != sheet.end(); ++it, ++i)
    if(group >= (*it).init.seqNum && group <= (*it).end.seqNum) {
      if(NULL != index) *index = i;
      return BetaSheet;
    }
  
  i = 0;
  for(QValueList<KBSPredictorTurnPDB>::iterator it = turn.begin();
      it != turn.end(); ++it, ++i)
    if(group >= (*it).init.seqNum && group <= (*it).end.seqNum) {
      if(NULL != index) *index = i;
      return Turn;
    }
  
  return Features;
}

bool KBSPredictorProteinPDB::parse(const QStringList &lines)
{
  groups = 0;
  atom.clear();
  structure.helix.clear();
  structure.sheet.clear();
  structure.turn.clear();

  bool hasStructure = false;
  for(QStringList::const_iterator line = lines.constBegin();
      line != lines.constEnd(); ++line)
    if((*line).startsWith("ATOM"))
    {
      KBSPredictorAtomPDB item;
      if(!item.parse(*line)) return false;
      
      atom << item;
      if(item.name.iupac == "CA") groups++;
    }
    else if((*line).startsWith("HELIX"))
    {
      hasStructure = true;
      KBSPredictorHelixPDB item;
      if(!item.parse(*line)) return false;
      
      structure.helix << item;
    }
    else if((*line).startsWith("SHEET"))
    {
      hasStructure = true;
      KBSPredictorSheetPDB item;
      if(!item.parse(*line)) return false;
      
      structure.sheet << item;
    }
    else if((*line).startsWith("TURN"))
    {
      hasStructure = true;
      KBSPredictorTurnPDB item;
      if(!item.parse(*line)) return false;
      
      structure.turn << item;
    }
    else if((*line).startsWith("END"))
      break;
  
  qHeapSort(atom);
  
  if(hasStructure) {
    qHeapSort(structure.helix);
    qHeapSort(structure.sheet);
    qHeapSort(structure.turn);
  }
  // else predictStructure();
  
  return true;
}

QString KBSPredictorProteinPDB::toString() const
{
  QString out;

  unsigned column = 0;
  for(QValueList<KBSPredictorAtomPDB>::const_iterator it = atom.begin();
      it != atom.end(); ++it)
  {
    if((*it).name.iupac != "CA") continue;
    if(column > 0 && (column % 60) == 0) out.append('\n');
    out.append(KBSPredictorAminoAcidAbbrev[(*it).resName]);
    ++column;
  }

  return out;
}

KBSPredictorAminoAcid KBSPredictorProteinPDB::resName(unsigned resSeq) const
{
  for(QValueList<KBSPredictorAtomPDB>::const_iterator it = atom.begin(); it != atom.end(); ++it)
    if((*it).resSeq == resSeq) return (*it).resName;

  return AminoAcids;
}

/*
    
    Kabsch & Sander's algorithm

    References: W. Kabsch, C. Sander, "Dictionary of Protein Secondary Structure: Pattern
                Recognition of Hydrogen-Bonded and Geometrical Features", Biopolymers,
                Vol. 22 pp.2577-2637, Wiley & Sons, 1983 
                
    
*/

bool KBSPredictorProteinPDB::hydrogenBond(unsigned i, unsigned j, double threshold) const
{
  const unsigned atoms = atom.count();
  if(i >= atoms || i >= atoms) return false;
  
  const QValueList<KBSPredictorAtomPDB>::const_iterator null = atom.end();
  QValueList<KBSPredictorAtomPDB>::const_iterator n[]  = {null, null},
                                               c[] = {null, null},
                                               o[] = {null, null};
  
  for(QValueList<KBSPredictorAtomPDB>::const_iterator it = atom.begin(); it != null; ++it)
  {
    const unsigned resSeq = (*it).resSeq;
    if(resSeq > i && resSeq > j) break;
    else if(resSeq != i && resSeq != j) continue;
    const unsigned k = (resSeq == i) ? 0 : 1;
    
    if((*it).name.iupac == "N") n[k] = it;
    else if((*it).name.iupac == "C") c[k] = it;
    else if((*it).name.iupac == "O") o[k] = it;
  }
  
  for(unsigned k = 0; k < 2; ++k)
    if(n[k] == null || c[k] == null || o[k] == null)
      return false;
      
  KBSPredictorAtomPDB h;
  
  h.x = (*n[1]).x + (*c[1]).x - (*o[1]).x;
  h.y = (*n[1]).y + (*c[1]).y - (*o[1]).y;
  h.z = (*n[1]).z + (*c[1]).z - (*o[1]).z;
  
  const double q1 = 0.42,
               q2 = 0.2,
               f = 332.0;
  
  const double on = distance(*o[0], *n[1]),
               ch = distance(*c[0], h),
               oh = distance(*o[0], h),
               cn = distance(*c[0], *n[1]);
               
  const double energy = 1e-3 *q1 * q2 * (1/on + 1/ch - 1/oh - 1/cn) * f;
  
  return(energy < threshold);
}


void KBSPredictorProteinPDB::predictStructure(double threshold)

#define turn(_i, _n)                                                          \
  (                                                                           \
    (_i) < groups + (_n)                                                      \
  &&                                                                          \
    hydrogenBond((_i), (_i) + (_n), threshold)                                \
  )

#define parallel_bridge(_i, _j)                                               \
  (                                                                           \
    (                                                                         \
      (_i) > 0 && (_i) < groups - 1                                           \
    &&                                                                        \
      hydrogenBond((_i) - 1, (_j), threshold)                                 \
    &&                                                                        \
      hydrogenBond((_j), (_i) + 1, threshold)                                 \
    )                                                                         \
  ||                                                                          \
    (                                                                         \
      (_j) > 0 && (_j) < groups - 1                                           \
    &&                                                                        \
      hydrogenBond((_j) - 1, (_i), threshold)                                 \
    &&                                                                        \
      hydrogenBond((_i), (_j) + 1, threshold)                                 \
    )                                                                         \
  )

#define antiparallel_bridge(_i, _j)                                           \
  (                                                                           \
    (                                                                         \
      hydrogenBond((_i), (_j), threshold)                                     \
    &&                                                                        \
      hydrogenBond((_j), (_i), threshold)                                     \
    )                                                                         \
  ||                                                                          \
    (                                                                         \
      (_i) > 0 && (_i) < groups - 1                                           \
    &&                                                                        \
      (_j) > 0 && (_j) < groups - 1                                           \
    &&                                                                        \
      hydrogenBond((_i) - 1, (_j) + 1, threshold)                             \
    &&                                                                        \
      hydrogenBond((_j) - 1, (_i) + 1, threshold)                             \
    )                                                                         \
  )

{
  structure.helix.clear();
  structure.sheet.clear();
  structure.turn.clear();
  unsigned helices = 0, sheets = 0, turns = 0;
  
  unsigned i = 0;
  while(i < groups)
  {
    unsigned n = 3;
    while(n <= 5)
      if(turn(i, n)) break;
      else n++;
    
    if(n <= 5)
    {
      unsigned k = 1;
      while(i + k < groups && turn(i + k, n))
        k++;

      if(k > 1)
      {
        KBSPredictorHelixPDB item;
        
        item.serNum = ++helices;
        item.helixID = QString("H%1").arg(helices);
        item.init.chainID = item.init.iCode = ' ';
        item.init.seqNum = i - n;
        item.init.resName = resName(item.init.seqNum);
        item.end.chainID = item.end.iCode = ' ';
        item.end.seqNum = i + (k - 1);
        item.end.resName = resName(item.end.seqNum);
        item.helixClass = RightHandedApha;
        item.comment = "";
        item.length = (k - 1) + n;
        
        structure.helix << item;
      }
      else
      {
        KBSPredictorTurnPDB item;
        
        item.seq = ++turns;
        item.turnID = QString("T%1").arg(item.seq);
        item.init.chainID = item.init.iCode = ' ';
        item.init.seqNum = i - n;
        item.init.resName = resName(item.init.seqNum);
        item.end.chainID = item.end.iCode = ' ';
        item.end.seqNum = i;
        item.end.resName = resName(item.end.seqNum);
        item.comment = "";
        
        structure.turn << item;
      }
      
      i += k + n;
      continue;
    }
    
    unsigned j;
    bool parallel = false;
    for(j = 0; j < i; ++j)
      if(parallel_bridge(i, j)) {
        parallel = true;
        break;
      } else if(antiparallel_bridge(i, j)) {
        parallel = false;
        break;
      }
    
    if(j == i) { i++; continue; }
    
    unsigned index;
    KBSPredictorFeature feature = structure.feature(i, &index);
    
    {
      unsigned k = 1;
      if(parallel)
        while(i + k < groups && j + k < i && parallel_bridge(i + k, j + k))
          k++;
      else
        while(i + k < groups && j + k < i && antiparallel_bridge(i + k, j + k))
          k++;

      if(BetaSheet == feature)
      {
        
        KBSPredictorSheetPDB prev = structure.sheet[index];
        for(QValueList<KBSPredictorSheetPDB>::iterator it = structure.sheet.begin();
            it != structure.sheet.end(); ++it)
          if((*it).sheetID == prev.sheetID) (*it).numStrands++;
        
        KBSPredictorSheetPDB item;
        
        item.strand = prev.numStrands;
        item.sheetID = prev.sheetID;
        item.numStrands = prev.numStrands;
        item.init.chainID = item.init.iCode = ' ';
        item.init.seqNum = i;
        item.init.resName = resName(item.init.seqNum);
        item.end.chainID = item.end.iCode = ' ';
        item.end.seqNum = i + (k - 1);
        item.end.resName = resName(item.end.seqNum);
        item.sense = parallel ? 1 : -1;
        item.curr.atom = "O";
        item.curr.resSeq = i;
        item.curr.resName = resName(item.curr.resSeq);
        item.curr.iCode = item.curr.chainId = ' ';
        item.prev.atom = "N";
        item.prev.resSeq = j;
        item.prev.resName = resName(item.prev.resSeq);
        item.prev.iCode = item.prev.chainId = ' ';
        
        structure.sheet << item;
      }
      else
      {
        KBSPredictorSheetPDB item;
        
        item.strand = 1;
        item.sheetID = QString("S%1").arg(++sheets);
        item.numStrands = 2;
        item.init.chainID = item.init.iCode = ' ';
        item.init.seqNum = j;
        item.init.resName = resName(item.init.seqNum);
        item.end.chainID = item.end.iCode = ' ';
        item.end.seqNum = j + (k - 1);
        item.end.resName = resName(item.end.seqNum);
        item.sense = 0;
        
        structure.sheet << item;
        
        item.strand = 2;
        item.init.seqNum = i;
        item.init.resName = resName(item.init.seqNum);
        item.end.seqNum = i + (k - 1);
        item.end.resName = resName(item.end.seqNum);
        item.sense = parallel ? 1 : -1;
        item.curr.atom = "O";
        item.curr.resSeq = i;
        item.curr.resName = resName(item.curr.resSeq);
        item.curr.iCode = item.curr.chainId = ' ';
        item.prev.atom = "N";
        item.prev.resSeq = j;
        item.prev.resName = resName(item.prev.resSeq);
        item.prev.iCode = item.prev.chainId = ' ';
        
        structure.sheet << item;
      }
      
      i += k;
    }
  }
}
#undef turn
#undef parallel_bridge
#undef antiparallel_bridge

bool KBSPredictorProteinNOE::parse(const QString &line)
{
  QStringList values = QStringList::split(" ", line);
  if(values.count() != 21) return false;
  
  select[0].index = values[4].toUInt(0, 10);
  select[0].name = values[5];
  
  select[1].index = values[10].toUInt(0, 10);
  select[1].name = values[11];
  
  kmin = values[14].toDouble();
  rmin = values[16].toDouble();
  kmax = values[18].toDouble();
  rmax = values[20].toDouble();
  
  return true;
}

bool KBSPredictorCharmmInp::parse(const QStringList &lines)
{
  ntemps = nsteps = t.low = t.high = 0;
  
  for(QStringList::const_iterator line = lines.begin(); lines.constEnd() != line; ++line)
  {
    if((*line).stripWhiteSpace().startsWith("!")) continue;
    
    int start = (*line).find(QRegExp("set \\w+ = "));
    if(start < 0) continue;
    start += 4;
    
    int end = (*line).find('=', start);
    if(end < 0) continue;
    
    const QString key = (*line).mid(start, end - start).stripWhiteSpace(),
                  value = (*line).mid(end+1).stripWhiteSpace();
    
    if(key == "ntemps")
      ntemps = value.toUInt(0, 10);
    else if(key == "nsteps")
      nsteps = value.toUInt(0, 10);
    else if(key == "thigh")
      t.high = value.toUInt(0, 10);
    else if(key == "tlow")
      t.low = value.toUInt(0, 10);
  }
  
  return true;
}

bool KBSPredictorMonssterRestart::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();

  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u %u %lf %lf %lf %lf %lf",
         &line1a[0], &line1a[1],
         &line1b[0], &line1b[1], &line1b[2], &line1b[3], &line1b[4]);
  ++line;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u %u %u", &line2[0], &line2[1], &line2[2]);
  ++line;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%lf %lf", &line3[0], &line3[1]);
  ++line;
  
  unsigned count = 0;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u", &count);
  ++line;
  
  chain.clear();
  for(unsigned i = 0; i < count; ++i)
  {
    KBSPredictorMonssterAtom item;
    
    if(lines.constEnd() == line || !item.parse(*line)) return false;
    ++line;
    
    chain << item;
  }
  
  qDebug("...parse OK");
  
  return true;
}
