/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qimage.h>

#include <kbspredictordata.h>

#include "kbspredictorpreferences.h"

KBSPredictorPreferences::KBSPredictorPreferences() : KConfigSkeleton()
{
  setCurrentGroup("ProteinPredictorAtHome - Molecule Log");
  
  for(unsigned i = MFOLD; i <= CHARMM; ++i)
  {
    addItemInt(QString("filter_%1").arg(i), filter[i], 0);
    addItemInt(QString("format_%1").arg(i), format[i], 0);
    addItemInt(QString("style_%1").arg(i), style[i], 0);
    addItemInt(QString("coloring_%1").arg(i), coloring[i], 0);
    addItemString(QString("location_%1").arg(i), location[i]);
  }
}

KBSPredictorMoleculeLogPreferences KBSPredictorPreferences::moleculeLogPreferences(KBSPredictorAppType type)
{
  KBSPredictorMoleculeLogPreferences out;
  
  out.filter = filter[type];
  out.format = (0 == format[type]) ? "WRL" : "X3D";
  
  out.style = KBSPredictorMoleculeModel::Style(style[type]);
  out.coloring = KBSPredictorMoleculeModel::Coloring(coloring[type]);
  
  out.url = KURL(location[type]);
  out.url.adjustPath(+1);
  
  return out;
}
