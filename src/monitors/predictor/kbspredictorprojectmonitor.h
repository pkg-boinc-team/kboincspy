/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPREDICTORPROJECTMONITOR_H
#define KBSPREDICTORPROJECTMONITOR_H

#include <qdict.h>
#include <qstringlist.h>

#include <kbsprojectmonitor.h>

#include <kbspredictordata.h>

class KBSPredictorProjectMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSPredictorProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    virtual ~KBSPredictorProjectMonitor();
    
    virtual const KBSPredictorResult *result(const QString &workunit);

    virtual void setState(const QString &workunit, const KBSPredictorState &state);
    
    virtual KBSLogManager *logManager() const;
    
  protected:
    virtual bool parseable(const QString &openName) const;
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    virtual KBSPredictorResult *mkResult(const QString &workunt);
  
  private:
    bool parseScale3B(const QStringList &lines, QValueList<KBSPredictorScale3B> &scale3b);
    bool parseMonssterChain(const QStringList &lines, QValueList<KBSPredictorMonssterAtom> &chain);
    bool parseMonssterRestraints(const QStringList &lines,
                                 QValueList<KBSPredictorMonssterRestraint> &restraints);
    bool parseProteinNOE(const QStringList &lines, QValueList<KBSPredictorProteinNOE> &noe);
    bool parseSeedStream(const QStringList &lines, unsigned &seed);
    
    void setAppType(KBSPredictorAppType app_type, const QStringList &workunits);
    void setBurials(const KBSPredictorBurials &burials, const QStringList &workunits);
    void setECovers24(const KBSPredictorECovers24 &ecovers_24, const QStringList &workunits);
    void setProfile3(const KBSPredictorProfile3 &profile3, const QStringList &workunits);
    void setQuasi3(const KBSPredictorQuasi3 &quasi3, const QStringList &workunits);
    void setScale3B(const QValueList<KBSPredictorScale3B> &scale3b, const QStringList &workunits);
    void setS1234(const KBSPredictorS1234 &s1234, const QStringList &workunits);
    void setS1234H(const KBSPredictorS1234 &s1234h, const QStringList &workunits);
    void setS1234E(const KBSPredictorS1234 &s1234e, const QStringList &workunits);
    void setMonssterInitChain(const QValueList<KBSPredictorMonssterAtom> &init_chain,
                              const QStringList &workunits);
    void setMonssterInput(const KBSPredictorMonssterInput &input, const QStringList &workunits);
    void setMonssterSeq(const KBSPredictorMonssterSeq &seq, const QStringList &workunits);
    void setMonssterRestraints(const QValueList<KBSPredictorMonssterRestraint> &restraints,
                               const QStringList &workunits);
    void setMonssterFinalChain(const QValueList<KBSPredictorMonssterAtom> &final_chain,
                               const QStringList &workunits);
    void setMonssterFinalPDB(const KBSPredictorProteinPDB &final_pdb, const QStringList &workunits);
    void setMonssterFinalNOE(const QValueList<KBSPredictorProteinNOE> &final_noe,
                             const QStringList &workunits);
    void setCharmmInp(const KBSPredictorCharmmInp &inp, const QStringList &workunits);
    void setProteinPDB(const KBSPredictorProteinPDB &pdb, const QStringList &workunits);
    void setProteinNOE(const QValueList<KBSPredictorProteinNOE> &noe, const QStringList &workunits);
    void setSeedStream(unsigned seed, const QStringList &workunits);
    void setProteinFinalPDB(const KBSPredictorProteinPDB &final_pdb, const QStringList &workunits);
    
    static unsigned countGroups(const QValueList<KBSPredictorAtomPDB> &pdb);
  
  private slots:
    void removeWorkunits(const QStringList &workunits);
    void logResults(const QStringList &results);
    void updateFile(const QString &fileName);
  
  protected:
    QDict<KBSPredictorResult> m_results;

  private:
    QStringList m_start;
};

#endif
