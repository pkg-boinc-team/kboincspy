/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qfile.h>

#include <ktempdir.h>
#include <kzip.h>

#include <kbsboincmonitor.h>

#include "kbseinsteinprojectmonitor.h"

const QString KBSEinsteinConfOpenName = "conf";
const QString KBSEinsteinEarthOpenName = "earth";
const QString KBSEinsteinSunOpenName = "sun";
const QString KBSEinsteinPolkaOutOpenName = "polka.out";

KBSEinsteinProjectMonitor::KBSEinsteinProjectMonitor(const QString &project, KBSBOINCMonitor *parent,
                                                     const char *name)
                          : KBSProjectMonitor(project, parent, name)
{
  connect(parent, SIGNAL(workunitsRemoved(const QStringList &)),
          this, SLOT(removeWorkunits(const QStringList &)));
  
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
}

KBSEinsteinProjectMonitor::~KBSEinsteinProjectMonitor()
{
  for(QDictIterator<KBSEinsteinResult> it(m_results); it.current() != NULL; ++it)
    delete it.current();
  m_results.clear();
}

const KBSEinsteinResult *KBSEinsteinProjectMonitor::result(const QString &workunit)
{
  return validWorkunit(workunit) ? m_results.find(workunit) : NULL;
}

KBSLogManager *KBSEinsteinProjectMonitor::logManager() const
{
  return NULL;
}

bool KBSEinsteinProjectMonitor::parseable(const QString &openName) const
{
  return(openName == KBSEinsteinConfOpenName
      // || openName == KBSEinsteinEarthOpenName
      // || openName == KBSEinsteinSunOpenName
      || openName == KBSEinsteinPolkaOutOpenName
  );
}

bool KBSEinsteinProjectMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  if(!m_meta.contains(file->fileName)) return false;
  const KBSFileMetaInfo meta = m_meta[file->fileName];
  
  bool isOK;
  QStringList lines;
  if(KBSEinsteinPolkaOutOpenName == meta.open_name)
  {
    KZip zip(fileName);
    if(!zip.open(IO_ReadOnly)) return false;
    
    const KArchiveEntry *entry = zip.directory()->entry(file->fileName);
    if(NULL == entry || !entry->isFile()) return false;
    
    KTempDir dir;
    if(dir.name().isNull()) return false;
    
    static_cast<KArchiveFile const *>(entry)->copyTo(dir.name());
    const QString unzippedFileName = dir.name() + file->fileName;
    
    isOK = readFile(unzippedFileName, lines);
    
    (void) QFile::remove(unzippedFileName);
    dir.unlink();    
  }
  else
    isOK = readFile(fileName, lines);
  if(!isOK) return false;
  
  if(KBSEinsteinConfOpenName == meta.open_name)
  {
    KBSEinsteinConf conf;
    if(!conf.parse(lines)) return false;
    
    setConf(conf, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSEinsteinEarthOpenName == meta.open_name)
  {
    KBSEinsteinMass earth;
    if(!earth.parse(lines)) return false;
    
    setEarth(earth, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSEinsteinSunOpenName == meta.open_name)
  {
    KBSEinsteinMass sun;
    if(!sun.parse(lines)) return false;
    
    setSun(sun, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else if(KBSEinsteinPolkaOutOpenName == meta.open_name)
  {
    KBSEinsteinPolkaOut polka_out;
    if(!polka_out.parse(lines)) return false;
    
    setPolkaOut(polka_out, meta.workunits);
    
    qDebug("... parse OK");
    return true;
  }
  else
    return false;
}

KBSEinsteinResult *KBSEinsteinProjectMonitor::mkResult(const QString &workunit)
{
  KBSEinsteinResult *result = m_results.find(workunit);
  if(NULL == result) {
    result = new KBSEinsteinResult();
    m_results.insert(workunit, result);
    
    const KBSBOINCClientState *state = boincMonitor()->state();
    if(NULL != state)
      result->args.parse(state->workunit[workunit].command_line);
  }
  
  return result;
}

void KBSEinsteinProjectMonitor::setConf(const KBSEinsteinConf &conf, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
    mkResult(*workunit)->conf = conf;
}

void KBSEinsteinProjectMonitor::setEarth(const KBSEinsteinMass &earth, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
    mkResult(*workunit)->earth = earth;
}

void KBSEinsteinProjectMonitor::setSun(const KBSEinsteinMass &sun, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
    mkResult(*workunit)->sun = sun;
}

void KBSEinsteinProjectMonitor::setPolkaOut(const KBSEinsteinPolkaOut &polka_out, const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
    mkResult(*workunit)->polka_out = polka_out;
}

void KBSEinsteinProjectMonitor::removeWorkunits(const QStringList &workunits)
{
  for(QStringList::const_iterator workunit = workunits.constBegin();
      workunit != workunits.constEnd(); ++workunit)
  {
    KBSEinsteinResult *result = m_results.take(*workunit);
    if(NULL != result) delete result;
  }
}

void KBSEinsteinProjectMonitor::updateFile(const QString &fileName)
{
  if(!m_meta.contains(fileName)) return;
  
  QStringList workunits = m_meta[fileName].workunits;
  for(QStringList::const_iterator workunit = workunits.begin();
      workunit != workunits.end(); ++ workunit)
    emit updatedResult(*workunit);
}

#include "kbseinsteinprojectmonitor.moc"
