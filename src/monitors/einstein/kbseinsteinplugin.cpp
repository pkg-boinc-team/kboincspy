/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kgenericfactory.h>

#include <kbsboincmonitor.h>
#include <kbsdocument.h>

#include <kbseinsteinprojectmonitor.h>
#include <kbseinsteintaskmonitor.h>

#include "kbseinsteinplugin.h"

class KBSEinsteinPluginFactory : KGenericFactory<KBSEinsteinPlugin,KBSDocument>
{
  public:
    KBSEinsteinPluginFactory() : KGenericFactory<KBSEinsteinPlugin,KBSDocument>("kbseinsteinmonitor") {};
};

K_EXPORT_COMPONENT_FACTORY(libkbseinsteinmonitor, KBSEinsteinPluginFactory());

KBSEinsteinPlugin::KBSEinsteinPlugin(KBSDocument *parent, const char *name, const QStringList &)
                 : KBSProjectPlugin(parent, name)
{
}

KBSProjectMonitor *KBSEinsteinPlugin::createProjectMonitor(const QString &project, KBSBOINCMonitor *parent)
{
  return new KBSEinsteinProjectMonitor(project, parent);
}

KBSTaskMonitor *KBSEinsteinPlugin::createTaskMonitor(unsigned task, KBSBOINCMonitor *parent)
{
  return new KBSEinsteinTaskMonitor(task, parent);
}

void KBSEinsteinPlugin::readConfig(KConfig *)
{
}

void KBSEinsteinPlugin::writeConfig(KConfig *)
{
}

#include "kbseinsteinplugin.moc"
