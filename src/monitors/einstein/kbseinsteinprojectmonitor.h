/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSEINSTEINPROJECTMONITOR_H
#define KBSEINSTEINPROJECTMONITOR_H

#include <qdict.h>
#include <qstringlist.h>

#include <kbsprojectmonitor.h>

#include <kbseinsteindata.h>

class KBSEinsteinProjectMonitor : public KBSProjectMonitor
{
  Q_OBJECT
  public:
    KBSEinsteinProjectMonitor(const QString &project, KBSBOINCMonitor *parent, const char *name=0);
    virtual ~KBSEinsteinProjectMonitor();
    
    virtual const KBSEinsteinResult *result(const QString &workunit);

    virtual KBSLogManager *logManager() const;
  
  protected:
    virtual bool parseable(const QString &openName) const;
    virtual bool parseFile(KBSFileInfo *file, const QString &fileName);
    
    virtual KBSEinsteinResult *mkResult(const QString &workunit);
  
  private:
    void setConf(const KBSEinsteinConf &conf, const QStringList &workunits);
    void setEarth(const KBSEinsteinMass &earth, const QStringList &workunits);
    void setSun(const KBSEinsteinMass &sun, const QStringList &workunits);
    void setPolkaOut(const KBSEinsteinPolkaOut &polka_out, const QStringList &workunits);
  
  private slots:
    void removeWorkunits(const QStringList &workunits);
    void updateFile(const QString &fileName);
  
  protected:
    QDict<KBSEinsteinResult> m_results;
};

#endif
