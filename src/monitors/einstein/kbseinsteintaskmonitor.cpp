/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kbsboincmonitor.h>

#include <kbsboincdata.h>

#include "kbseinsteintaskmonitor.h"

const QString KBSEinsteinFstatsPrefix = "Fstats";

KBSEinsteinTaskMonitor::KBSEinsteinTaskMonitor(unsigned task, KBSBOINCMonitor *parent, const char *name)
                      : KBSTaskMonitor(task, parent, name)
{
  connect(this, SIGNAL(fileUpdated(const QString &)), this, SLOT(updateFile(const QString &)));
  
  const KBSBOINCClientState *state = parent->state();
  if(NULL != state) m_args.parse(state->workunit[workunit()].command_line);
  
  for(unsigned i = 0; i < 2; ++i)
  {
    QString fileName = formatFileName(i);
    if(!fileName.isNull()) addFile(fileName);
  }
}

const KBSEinsteinState *KBSEinsteinTaskMonitor::state() const
{
  return &m_state;
}

bool KBSEinsteinTaskMonitor::parseFile(KBSFileInfo *file, const QString &fileName)
{
  qDebug("Parsing file %s...", file->fileName.latin1());
  
  int index = parseFileName(file->fileName);
  if(index < 0) return false;
  
  QStringList lines;
  if(!readFile(fileName, lines)) return false;
  
  return parseFstats(lines, m_state.fstats[index]);
}

bool KBSEinsteinTaskMonitor::parseFstats(const QStringList &lines,
                                         QValueList<KBSEinsteinFstat> &fstats)
{
  QStringList::const_iterator line = lines.constBegin();

  fstats.clear();  
  while(lines.constEnd() != line)
  {
    KBSEinsteinFstat item;
    if(!item.parse(*line)) return false;
    
    fstats << item;
    ++line;
  }
  
  qDebug("... parse OK");
  
  return true;
}

int KBSEinsteinTaskMonitor::parseFileName(const QString &fileName) const
{
  if(!fileName.startsWith(KBSEinsteinFstatsPrefix)) return -1;
  
  const QString suffix = QString(fileName).remove(KBSEinsteinFstatsPrefix);
  for(unsigned i = 0; i < 2; ++i)
    if(suffix == m_args.h[i]["o"])
      return i;
      
  return -1;
}

QString KBSEinsteinTaskMonitor::formatFileName(unsigned i) const
{
  if(i >= 2 || !m_args.h[i].contains("o")) return QString::null;
  else return(KBSEinsteinFstatsPrefix + m_args.h[i]["o"]);
}

void KBSEinsteinTaskMonitor::updateFile(const QString &)
{
  emit updatedState();
}

#include "kbseinsteintaskmonitor.moc"
