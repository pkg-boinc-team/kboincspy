/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSEINSTEINDATA_H
#define KBSEINSTEINDATA_H

#include <qdatetime.h>
#include <qmap.h>
#include <qstring.h>
#include <qvaluelist.h>

namespace KBSEinstein
{
  QDateTime parseGPSDate(int secs);
  QDateTime parseGPSDate(const QString &text);
  int formatGPSDate(const QDateTime &date);
  
  QString formatRA(double ra);
  QString formatDec(double dec, bool sign=true);
}

struct KBSEinsteinCmdLineArgs
{
  QMap<QString,QString> h[2], out;
  
  bool parse(const QString &input);
};

struct KBSEinsteinConf
{
  unsigned Fthreshold;
  double dFreq,
         FreqBand;
  QString ephemDir;
  double Alpha,
         AlphaBand,
         dAlpha,
         Delta,
         DeltaBand,
         dDelta;
  unsigned gridType;
  QString IFO,
          mergedSFTFile;
  
  bool parse(const QStringList &lines);
};

struct KBSEinsteinMass
{
  QValueList< QValueList<double> > data;
  unsigned count;
  double value;
  
  bool parse(const QStringList &lines);
};

struct KBSEinsteinCoord
{
  double freq,
         ra,
         dec;
};

struct KBSEinsteinH
{
  KBSEinsteinCoord coord;
  double power;
  
  bool parse(const QString &line);
};

struct KBSEinsteinCoincidence
{
  unsigned index[2];
  double coincidence;
  
  bool parse(const QString &line);
};

struct KBSEinsteinPolkaOut
{
  QValueList<KBSEinsteinH> h[2];
  QValueList<KBSEinsteinCoincidence> coincidences;
  
  bool parse(const QStringList &lines);
};

struct KBSEinsteinResult
{
  KBSEinsteinCmdLineArgs args;
  KBSEinsteinConf conf;
  KBSEinsteinMass earth, sun;
  KBSEinsteinPolkaOut polka_out;
};

struct KBSEinsteinFstat
{
  KBSEinsteinCoord coord;
  unsigned count;
  double value[2];
  double power;
  
  bool parse(const QString &line);
};

struct KBSEinsteinState
{
  QValueList<KBSEinsteinFstat> fstats[2];
};

#endif
