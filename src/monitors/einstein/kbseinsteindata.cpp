/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License as       *
 *   published by the Free Software Foundation; either version 2 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with this program; if not, write to the                 *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>

#include <qstringlist.h>

#include <klocale.h>
#include <krfcdate.h>

#include "kbseinsteindata.h"

QDateTime KBSEinstein::parseGPSDate(int secs)
{
  const QDateTime epoch(QDate(1980, 1, 6));
  
  return epoch.addSecs(secs + KRFCDate::localUTCOffset() * 60);
}

QDateTime KBSEinstein::parseGPSDate(const QString &text)
{
  return parseGPSDate(text.toInt(0, 10));
}

int KBSEinstein::formatGPSDate(const QDateTime &date)
{
  const QDateTime epoch(QDate(1980, 1, 6));
  
  return(epoch.secsTo(date) - KRFCDate::localUTCOffset() * 60);
}

QString KBSEinstein::formatRA(double ra)
{
  const int h = int(ra);
  ra -= int(ra); ra *= 60;
  const int m = int(ra);
  ra -= m; ra *= 60;
  const int s = int(ra);

  return(i18n("%1 h %2' %3\"").arg(h).arg(m).arg(s));
}

QString KBSEinstein::formatDec(double dec, bool sign)
{
  const QString p = (dec < 0.0) ? QString("-") : sign ? QString("+") : QString::null;
  dec = (dec < 0.0) ? -dec : dec;
  const int d = int(dec);
  dec -= d; dec *= 60;
  const int m = int(dec);
  dec -= m; dec *= 60;
  const int s = int(dec);
  
  return(QString("%1%2\xb0 %4' %5\"").arg(p).arg(d).arg(m).arg(s));
}

bool KBSEinsteinCmdLineArgs::parse(const QString &input)
{
  h[0].clear();
  h[1].clear();
  out.clear();
  
  unsigned nh = 0;
  
  QStringList lines = QStringList::split("//", input);
  for(QStringList::iterator line = lines.begin(); line != lines.end(); ++line)
  {
    QMap<QString,QString> *item;
    QStringList words = QStringList::split(" ", (*line).simplifyWhiteSpace());
    QStringList::iterator word = words.begin();
    
    if(words.end() == word) continue;
    if((*word) == "@conf") {
      if(nh > 1) continue;
      item = &h[nh++];
    } else
      item = &out;
    
    while(words.end() != word)
    {
      if(!(*word).startsWith("-")) {
        ++word;
        continue;
      }
      
      QString key, value;
      
      if((*word).startsWith("--")) {
        key = (*word).mid(2);
        value = "";
      } else {
        key = (*word).mid(1, 1);
        value = (*word).mid(2);
      }
      ++word;
      
      while(words.end() != word && !(*word).startsWith("-")) {
        value = value + " " + *word;
        ++word;
      }
      value = value.stripWhiteSpace();
      
      item->insert(key, value);
    }
  }
  
  return true;
}

bool KBSEinsteinConf::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  while(lines.constEnd() != line)
  {
    int index = (*line).find('=');
    
    if(index > 0)
    {
      QString lhs = (*line).left(index),
              rhs = (*line).mid(index+1);
              
      if("Fthreshold" == lhs)
        Fthreshold = rhs.toUInt(0, 10);
      else if("dFreq" == lhs)
        dFreq = rhs.toDouble();
      else if("FreqBand" == lhs)
        FreqBand = rhs.toDouble();
      else if("ephemDir" == lhs)
        ephemDir = rhs;
      else if("Alpha" == lhs)
        Alpha = rhs.toDouble();
      else if("AlphaBand" == lhs)
        AlphaBand = rhs.toDouble();
      else if("dAlpha" == lhs)
        dAlpha = rhs.toDouble();
      else if("Delta" == lhs)
        Delta = rhs.toDouble();
      else if("DeltaBand" == lhs)
        DeltaBand = rhs.toDouble();
      else if("dDelta" == lhs)
        dDelta = rhs.toDouble();
      else if("gridType" == lhs)
        gridType = rhs.toUInt(0, 10);
      else if("IFO" == lhs)
        IFO = rhs;
      else if("mergedSFTFile" == lhs)
        mergedSFTFile = rhs;
    }
    
    ++line;
  }
    
  return true;
}
  
bool KBSEinsteinMass::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  unsigned items;
  
  if(lines.constEnd() == line) return false;
  sscanf(*line, "%u %lf %u", &count, &value, &items);
  ++line;  
  
  for(unsigned i = 0; i < items; ++i)
  {
    QValueList<double> item;
    double value[3];
    
    if(lines.constEnd() == line) return false;
    sscanf(*line, "%lf %lf %lf", &value[0], &value[1], &value[2]);
    item << value[0] << value[1] << value[2];
    ++line;
  
    if(lines.constEnd() == line) return false;
    sscanf(*line, "%lf %lf %lf", &value[0], &value[1], &value[2]);
    item << value[0] << value[1] << value[2];
    ++line;
  
    if(lines.constEnd() == line) return false;
    sscanf(*line, "%lf %lf %lf", &value[0], &value[1], &value[2]);
    item << value[0] << value[1] << value[2];
    ++line;
  
    if(lines.constEnd() == line) return false;
    sscanf(*line, "%lf", &value[0]);
    item << value[0];
    ++line;
  }
  
  return true;
}

bool KBSEinsteinH::parse(const QString &line)
{
  sscanf(line, "%lf %lf %lf %lf", &coord.freq, &coord.ra, &coord.dec, &power);

  return true;
}

bool KBSEinsteinCoincidence::parse(const QString &line)
{
  sscanf(line, "%u %u %lf", &index[0], &index[1], &coincidence);
  
  return true;
}

bool KBSEinsteinPolkaOut::parse(const QStringList &lines)
{
  QStringList::const_iterator line = lines.constBegin();
  
  while(lines.constEnd() != line)
  {
    if((*line).startsWith("%1") || (*line).startsWith("%2"))
    {
      unsigned index = (*line).mid(1).toUInt(0, 10) - 1;
      
      ++line;
      
      while(lines.constEnd() != line && !(*line).startsWith("%"))
      {
        KBSEinsteinH item;
        if(!item.parse(*line)) return false;
                       
        h[index] << item;
        ++line;
      }
      
      if(lines.constEnd() == line) break;
    }
    else if((*line).startsWith("%coincidences"))
    {
      ++line;
      
      while(lines.constEnd() != line)
      {
        KBSEinsteinCoincidence item;
        if(!item.parse(*line)) return false;
                       
        coincidences << item;
        ++line;
      }
      
      if(lines.constEnd() == line) break;
    }
    
    ++line;
  }
  
  return true;
}

bool KBSEinsteinFstat::parse(const QString &line)
{
  sscanf(line, "%lf %lf %lf %u %lf %lf %lf",
         &coord.freq, &coord.ra, &coord.dec,
         &count, &value[0], &value[1],
         &power);
  
  return true;
}
