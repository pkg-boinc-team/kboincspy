/****************************************************************************
** Form interface generated from reading ui file './kbsconfigpage.ui'
**
** Created: Mon Feb 6 18:28:47 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSCONFIGPAGE_H
#define KBSCONFIGPAGE_H

#include <qvariant.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QGroupBox;
class QLabel;
class QSpinBox;
class QCheckBox;
class KURLRequester;
class KLineEdit;

class KBSConfigPage : public QWidget
{
    Q_OBJECT

public:
    KBSConfigPage( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSConfigPage();

    QGroupBox* group_polling;
    QLabel* label_fam;
    QLabel* label_rpc;
    QSpinBox* kcfg_fam;
    QSpinBox* kcfg_rpc;
    QGroupBox* group_tray;
    QCheckBox* kcfg_use_tray;
    QCheckBox* kcfg_startup_tray;
    QGroupBox* log_group;
    QLabel* location_label;
    KURLRequester* kcfg_location;
    QCheckBox* kcfg_write;
    QGroupBox* client_group;
    QLabel* client_label;
    KLineEdit* kcfg_client;
    QCheckBox* kcfg_client_exec;
    QCheckBox* kcfg_client_kill;

protected:
    QVBoxLayout* KBSConfigPageLayout;
    QSpacerItem* spacer;
    QHBoxLayout* polling_layout;
    QGridLayout* group_pollingLayout;
    QVBoxLayout* group_trayLayout;
    QHBoxLayout* log_layout;
    QVBoxLayout* log_groupLayout;
    QHBoxLayout* location_layout;
    QVBoxLayout* client_groupLayout;
    QHBoxLayout* client_layout;
    QHBoxLayout* client_options_layout;

protected slots:
    virtual void languageChange();

};

#endif // KBSCONFIGPAGE_H
