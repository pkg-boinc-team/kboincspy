/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSTREEVIEW_H
#define KBSTREEVIEW_H

#include <qptrdict.h>

#include <kconfig.h>
#include <klistbox.h>
#include <klistview.h>

class KBSTreeNode;

class KBSTreeView : public QWidget
{
  Q_OBJECT
  public:
    KBSTreeView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);
    
    virtual KBSTreeNode *selection() = 0;
    virtual void select(KBSTreeNode *node) = 0;
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  signals:
    void selectionChanged();
    void activated(KBSTreeNode *node);
  
  protected:
    virtual void rebuild() = 0;
  
  protected:
    KBSTreeNode *m_root;
};

class KBSNestedTreeView : public KBSTreeView
{
  Q_OBJECT
  public:
    KBSNestedTreeView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);

    virtual KBSTreeNode *selection(); 
    virtual void select(KBSTreeNode *node);
  
  protected:
    virtual void rebuild();
  
  protected slots:
    virtual void updateChange(KBSTreeNode *node);
    virtual void updateInsert(KBSTreeNode *node);
    virtual void updateRemove(KBSTreeNode *node);
  
  private:
    QListViewItem *viewItem(const KBSTreeNode *node) const;
    void buildTreeView(const KBSTreeNode *node, QListViewItem *item);
  
  private slots:
    void slotExecuted(QListViewItem *item);
    void slotContextMenu(KListView *list, QListViewItem *item, const QPoint &pos);
  
  protected:
    KListView *m_view;
    QPtrDict<KBSTreeNode> m_nodes;
};

class KBSFlattenedTreeView: public KBSTreeView
{
  Q_OBJECT
  public:
    KBSFlattenedTreeView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);
    
    KBSTreeNode *selection();
    virtual void select(KBSTreeNode *node);
    
    unsigned iconSize() const;
    virtual void setIconSize(unsigned size);
  
  protected:
    void rebuild();
  
  protected slots:
    virtual void updateChange(KBSTreeNode *node);
    virtual void updateInsert(KBSTreeNode *node);
    virtual void updateRemove(KBSTreeNode *node);
  
  private slots:
    virtual void slotExecuted(QListBoxItem *item);
    virtual void slotContextMenuRequested(QListBoxItem *item, const QPoint &pos);
  
  private:
    KBSTreeNode *treeNode(QListBoxItem *item) const;
    void setupConnections(KBSTreeNode *node, bool set);
    void setFocus(KBSTreeNode *node);
  
  protected:
    unsigned m_size;
    KListBox *m_view;
    KBSTreeNode *m_focus;
};

#endif
