#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsprojectwizardcontent.ui'
**
** Created: Mon Feb 6 18:28:48 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsprojectwizardcontent.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qwidgetstack.h>
#include <klineedit.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <kpassdlg.h>
#include <qtextbrowser.h>
#include <qframe.h>
#include <kpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSProjectWizardContent as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSProjectWizardContent::KBSProjectWizardContent( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSProjectWizardContent" );
    KBSProjectWizardContentLayout = new QVBoxLayout( this, 11, 8, "KBSProjectWizardContentLayout"); 

    layout_content = new QHBoxLayout( 0, 0, 6, "layout_content"); 

    layout_sidebar = new QVBoxLayout( 0, 0, 6, "layout_sidebar"); 

    sidebar = new QLabel( this, "sidebar" );
    sidebar->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, sidebar->sizePolicy().hasHeightForWidth() ) );
    sidebar->setFrameShape( QLabel::Panel );
    sidebar->setFrameShadow( QLabel::Sunken );
    sidebar->setLineWidth( 1 );
    layout_sidebar->addWidget( sidebar );
    sidebar_spacer = new QSpacerItem( 10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout_sidebar->addItem( sidebar_spacer );
    layout_content->addLayout( layout_sidebar );

    pages = new QWidgetStack( this, "pages" );
    pages->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)3, 0, 0, pages->sizePolicy().hasHeightForWidth() ) );

    welcome_page = new QWidget( pages, "welcome_page" );
    welcome_pageLayout = new QVBoxLayout( welcome_page, 11, 6, "welcome_pageLayout"); 

    welcome_text = new QLabel( welcome_page, "welcome_text" );
    welcome_text->setAlignment( int( QLabel::WordBreak | QLabel::AlignTop ) );
    welcome_pageLayout->addWidget( welcome_text );
    welcome_spacer = new QSpacerItem( 21, 51, QSizePolicy::Minimum, QSizePolicy::Expanding );
    welcome_pageLayout->addItem( welcome_spacer );
    pages->addWidget( welcome_page, 0 );

    project_url_page = new QWidget( pages, "project_url_page" );
    project_url_pageLayout = new QVBoxLayout( project_url_page, 11, 6, "project_url_pageLayout"); 

    project_url_text = new QLabel( project_url_page, "project_url_text" );
    project_url_pageLayout->addWidget( project_url_text );

    project_url_layout = new QHBoxLayout( 0, 0, 6, "project_url_layout"); 

    project_url_label = new QLabel( project_url_page, "project_url_label" );
    project_url_layout->addWidget( project_url_label );

    project_url = new KLineEdit( project_url_page, "project_url" );
    project_url_layout->addWidget( project_url );
    project_url_pageLayout->addLayout( project_url_layout );
    project_url_spacer = new QSpacerItem( 31, 90, QSizePolicy::Minimum, QSizePolicy::Expanding );
    project_url_pageLayout->addItem( project_url_spacer );
    pages->addWidget( project_url_page, 1 );

    connect_host_page = new QWidget( pages, "connect_host_page" );
    connect_host_pageLayout = new QVBoxLayout( connect_host_page, 11, 6, "connect_host_pageLayout"); 

    connect_host_text = new QLabel( connect_host_page, "connect_host_text" );
    connect_host_pageLayout->addWidget( connect_host_text );
    connect_host_spacer_hi = new QSpacerItem( 10, 10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding );
    connect_host_pageLayout->addItem( connect_host_spacer_hi );

    connect_host_movie = new QLabel( connect_host_page, "connect_host_movie" );
    connect_host_movie->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)1, 0, 0, connect_host_movie->sizePolicy().hasHeightForWidth() ) );
    connect_host_movie->setAlignment( int( QLabel::AlignCenter ) );
    connect_host_pageLayout->addWidget( connect_host_movie );
    connect_host_spacer_lo = new QSpacerItem( 10, 10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding );
    connect_host_pageLayout->addItem( connect_host_spacer_lo );
    pages->addWidget( connect_host_page, 2 );

    connect_network_page = new QWidget( pages, "connect_network_page" );
    connect_network_pageLayout = new QVBoxLayout( connect_network_page, 11, 6, "connect_network_pageLayout"); 

    connect_network_text = new QLabel( connect_network_page, "connect_network_text" );
    connect_network_pageLayout->addWidget( connect_network_text );
    connect_network_spacer_hi = new QSpacerItem( 10, 10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding );
    connect_network_pageLayout->addItem( connect_network_spacer_hi );

    connect_network_movie = new QLabel( connect_network_page, "connect_network_movie" );
    connect_network_movie->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)1, 0, 0, connect_network_movie->sizePolicy().hasHeightForWidth() ) );
    connect_network_movie->setAlignment( int( QLabel::AlignCenter ) );
    connect_network_pageLayout->addWidget( connect_network_movie );
    connect_network_spacer_lo = new QSpacerItem( 10, 10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding );
    connect_network_pageLayout->addItem( connect_network_spacer_lo );
    pages->addWidget( connect_network_page, 3 );

    account_page = new QWidget( pages, "account_page" );
    account_pageLayout = new QVBoxLayout( account_page, 11, 6, "account_pageLayout"); 

    account_text = new QLabel( account_page, "account_text" );
    account_pageLayout->addWidget( account_text );

    account_group = new QButtonGroup( account_page, "account_group" );
    account_group->setFrameShape( QButtonGroup::NoFrame );
    account_group->setColumnLayout(0, Qt::Vertical );
    account_group->layout()->setSpacing( 6 );
    account_group->layout()->setMargin( 11 );
    account_groupLayout = new QHBoxLayout( account_group->layout() );
    account_groupLayout->setAlignment( Qt::AlignTop );

    create_account = new QRadioButton( account_group, "create_account" );
    create_account->setChecked( TRUE );
    account_groupLayout->addWidget( create_account );

    use_account = new QRadioButton( account_group, "use_account" );
    account_groupLayout->addWidget( use_account );
    account_pageLayout->addWidget( account_group );

    login_text = new QLabel( account_page, "login_text" );
    account_pageLayout->addWidget( login_text );

    login_layout = new QGridLayout( 0, 1, 1, 0, 6, "login_layout"); 

    password_label = new QLabel( account_page, "password_label" );
    password_label->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    login_layout->addWidget( password_label, 1, 0 );

    confirm_password_label = new QLabel( account_page, "confirm_password_label" );
    confirm_password_label->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    login_layout->addWidget( confirm_password_label, 2, 0 );

    username_label = new QLabel( account_page, "username_label" );
    username_label->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    login_layout->addWidget( username_label, 0, 0 );

    username = new KLineEdit( account_page, "username" );

    login_layout->addWidget( username, 0, 1 );

    password = new KPasswordEdit( account_page, "password" );

    login_layout->addWidget( password, 1, 1 );

    confirm_password = new KPasswordEdit( account_page, "confirm_password" );

    login_layout->addWidget( confirm_password, 2, 1 );
    account_pageLayout->addLayout( login_layout );

    remark = new QLabel( account_page, "remark" );
    remark->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    remark->setAlignment( int( QLabel::AlignBottom ) );
    account_pageLayout->addWidget( remark );
    pages->addWidget( account_page, 4 );

    account_key_page = new QWidget( pages, "account_key_page" );
    account_key_pageLayout = new QVBoxLayout( account_key_page, 11, 6, "account_key_pageLayout"); 

    account_key_text = new QLabel( account_key_page, "account_key_text" );
    account_key_pageLayout->addWidget( account_key_text );

    account_key_layout = new QHBoxLayout( 0, 0, 6, "account_key_layout"); 

    account_key_label = new QLabel( account_key_page, "account_key_label" );
    account_key_layout->addWidget( account_key_label );

    account_key = new KLineEdit( account_key_page, "account_key" );
    account_key_layout->addWidget( account_key );
    account_key_pageLayout->addLayout( account_key_layout );
    account_key_spacer = new QSpacerItem( 21, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    account_key_pageLayout->addItem( account_key_spacer );
    pages->addWidget( account_key_page, 5 );

    success_page = new QWidget( pages, "success_page" );
    success_pageLayout = new QVBoxLayout( success_page, 11, 6, "success_pageLayout"); 

    success_text = new QLabel( success_page, "success_text" );
    success_pageLayout->addWidget( success_text );
    success_spacer = new QSpacerItem( 31, 158, QSizePolicy::Minimum, QSizePolicy::Expanding );
    success_pageLayout->addItem( success_spacer );
    pages->addWidget( success_page, 6 );

    connect_error_page = new QWidget( pages, "connect_error_page" );
    connect_error_pageLayout = new QVBoxLayout( connect_error_page, 11, 6, "connect_error_pageLayout"); 

    connect_error_text = new QLabel( connect_error_page, "connect_error_text" );
    connect_error_pageLayout->addWidget( connect_error_text );
    connect_error_spacer = new QSpacerItem( 20, 80, QSizePolicy::Minimum, QSizePolicy::Expanding );
    connect_error_pageLayout->addItem( connect_error_spacer );
    pages->addWidget( connect_error_page, 7 );

    network_error_page = new QWidget( pages, "network_error_page" );
    network_error_pageLayout = new QVBoxLayout( network_error_page, 11, 6, "network_error_pageLayout"); 

    network_error_text = new QLabel( network_error_page, "network_error_text" );
    network_error_pageLayout->addWidget( network_error_text );
    network_error_spacer = new QSpacerItem( 20, 111, QSizePolicy::Minimum, QSizePolicy::Expanding );
    network_error_pageLayout->addItem( network_error_spacer );
    pages->addWidget( network_error_page, 8 );

    auth_error_page = new QWidget( pages, "auth_error_page" );
    auth_error_pageLayout = new QVBoxLayout( auth_error_page, 11, 6, "auth_error_pageLayout"); 

    auth_error_text = new QLabel( auth_error_page, "auth_error_text" );
    auth_error_pageLayout->addWidget( auth_error_text );
    auth_error_spacer = new QSpacerItem( 31, 91, QSizePolicy::Minimum, QSizePolicy::Expanding );
    auth_error_pageLayout->addItem( auth_error_spacer );
    pages->addWidget( auth_error_page, 9 );

    duplicate_error_page = new QWidget( pages, "duplicate_error_page" );
    duplicate_error_pageLayout = new QVBoxLayout( duplicate_error_page, 11, 6, "duplicate_error_pageLayout"); 

    duplicate_error_text = new QLabel( duplicate_error_page, "duplicate_error_text" );
    duplicate_error_pageLayout->addWidget( duplicate_error_text );
    duplicate_error_spacer = new QSpacerItem( 51, 91, QSizePolicy::Minimum, QSizePolicy::Expanding );
    duplicate_error_pageLayout->addItem( duplicate_error_spacer );
    pages->addWidget( duplicate_error_page, 10 );

    generic_error_page = new QWidget( pages, "generic_error_page" );
    generic_error_pageLayout = new QVBoxLayout( generic_error_page, 11, 6, "generic_error_pageLayout"); 

    generic_error_text = new QLabel( generic_error_page, "generic_error_text" );
    generic_error_pageLayout->addWidget( generic_error_text );

    error_messages = new QTextBrowser( generic_error_page, "error_messages" );
    generic_error_pageLayout->addWidget( error_messages );
    pages->addWidget( generic_error_page, 11 );
    layout_content->addWidget( pages );
    KBSProjectWizardContentLayout->addLayout( layout_content );
    spacer12 = new QSpacerItem( 31, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSProjectWizardContentLayout->addItem( spacer12 );

    line = new QFrame( this, "line" );
    line->setFrameShape( QFrame::HLine );
    line->setFrameShadow( QFrame::Sunken );
    line->setMargin( 10 );
    line->setFrameShape( QFrame::HLine );
    KBSProjectWizardContentLayout->addWidget( line );

    layout_buttons = new QHBoxLayout( 0, 0, 6, "layout_buttons"); 

    help = new KPushButton( this, "help" );
    layout_buttons->addWidget( help );
    spacer_buttons = new QSpacerItem( 170, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout_buttons->addItem( spacer_buttons );

    previous = new KPushButton( this, "previous" );
    layout_buttons->addWidget( previous );

    next = new KPushButton( this, "next" );
    layout_buttons->addWidget( next );

    finish = new KPushButton( this, "finish" );
    layout_buttons->addWidget( finish );
    KBSProjectWizardContentLayout->addLayout( layout_buttons );
    languageChange();
    resize( QSize(480, 375).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( create_account, SIGNAL( toggled(bool) ), confirm_password, SLOT( setShown(bool) ) );
    connect( create_account, SIGNAL( toggled(bool) ), confirm_password_label, SLOT( setShown(bool) ) );

    // buddies
    project_url_label->setBuddy( project_url );
    password_label->setBuddy( password );
    confirm_password_label->setBuddy( confirm_password );
    username_label->setBuddy( username );
    account_key_label->setBuddy( account_key );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSProjectWizardContent::~KBSProjectWizardContent()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSProjectWizardContent::languageChange()
{
    setCaption( tr2i18n( "Attach to Project" ) );
    welcome_text->setText( tr2i18n( "<h2>Welcome!</h2>\n"
"<p>This wizard will guide you through the process of attaching to a project.<br>\n"
"Click <i>Next</i> to continue.</p>" ) );
    project_url_text->setText( tr2i18n( "<h2>Project URL</h2>\n"
"<p>Please enter the URL of the project's web site in the field below.<br>\n"
"You can copy and paste the URL from your browser's address bar.</p>" ) );
    project_url_label->setText( tr2i18n( "Project URL:" ) );
    connect_host_text->setText( tr2i18n( "<h2>Connecting</h2>\n"
"<p>Please wait while I contact the project's web site...</p>" ) );
    connect_host_movie->setText( QString::null );
    connect_network_text->setText( tr2i18n( "<h2>Connecting</h2>\n"
"<p>Please wait while I test your internet connection...</p>" ) );
    connect_network_movie->setText( QString::null );
    account_text->setText( tr2i18n( "<h2>Account</h2>\n"
"<p>Do you want to use an existing account or create a new one?</p>" ) );
    account_group->setTitle( QString::null );
    create_account->setText( tr2i18n( "Create a new account" ) );
    create_account->setAccel( QKeySequence( tr2i18n( "Alt+N" ) ) );
    use_account->setText( tr2i18n( "Use existing account" ) );
    use_account->setAccel( QKeySequence( tr2i18n( "Alt+U" ) ) );
    login_text->setText( tr2i18n( "Enter your login information below." ) );
    password_label->setText( tr2i18n( "Password:" ) );
    confirm_password_label->setText( tr2i18n( "Confirm Password:" ) );
    username_label->setText( QString::null );
    remark->setText( QString::null );
    account_key_text->setText( tr2i18n( "<h2>Account Key</h2>\n"
"                              <p>This project uses an \"account key\" to identify you.<br>\n"
"                              If you already have this key, please enter it in the field below.<br>\n"
"                              Otherwise, go to the project's web site and create an account; your\n"
"                              account key will be emailed to you.</p>" ) );
    account_key_label->setText( tr2i18n( "Account Key:" ) );
    success_text->setText( tr2i18n( "<h2>Done!</h2>\n"
"<p>You are now attached to this project. Click <i>Finish</i> to close this wizard.</p>" ) );
    connect_error_text->setText( tr2i18n( "<h2>Connection Error</h2>\n"
"<p>I can't establish a connection with the project's site. Please check that the URL you entered is correct.</p>" ) );
    network_error_text->setText( tr2i18n( "<h2>Connection Error</h2>\n"
"<p>You don't appear to be connected to the Internet. Please check your network connection and proxy settings.</p>" ) );
    auth_error_text->setText( tr2i18n( "<h2>Authentication Error</h2>\n"
"<p>Authentication with the project's site failed. Please check that the account information you entered is correct.</p>" ) );
    duplicate_error_text->setText( tr2i18n( "<h2>Error</h2>\n"
"<p>You are already attached to this project. Please go back and choose a different one, or click <i>Finish</i>. </p>" ) );
    generic_error_text->setText( tr2i18n( "<h2>Error</h2>\n"
"<p>An error has occurred while attaching to the project.<br>\n"
"These are the messages I received from the server:</p>" ) );
    help->setText( tr2i18n( "Help" ) );
    help->setAccel( QKeySequence( tr2i18n( "Alt+H" ) ) );
    previous->setText( tr2i18n( "Back" ) );
    previous->setAccel( QKeySequence( tr2i18n( "Alt+B" ) ) );
    next->setText( tr2i18n( "Next" ) );
    next->setAccel( QKeySequence( tr2i18n( "Alt+N" ) ) );
    finish->setText( QString::null );
    finish->setAccel( QKeySequence( QString::null ) );
}

#include "kbsprojectwizardcontent.moc"
