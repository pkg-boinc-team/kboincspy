/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qstringlist.h>

#include <klocale.h>

#include <kbsboincmonitor.h>

#include <kbswebmenu.h>

#include "kbswebtoolbar.h"

KBSWebToolBar::KBSWebToolBar(KMainWindow *parent, const char *name)
             : QObject(parent, name),
               m_toolbar(parent->toolBar(name)), m_monitor(NULL)
{
}

KToolBar *KBSWebToolBar::toolBar() const
{
  return m_toolbar;
}

KBSBOINCMonitor *KBSWebToolBar::monitor() const
{
  return m_monitor;
}

void KBSWebToolBar::setMonitor(KBSBOINCMonitor *monitor)
{
  if(m_monitor == monitor) return;

  if(NULL != m_monitor)
  {
    clear();
    
    disconnect(m_monitor, SIGNAL(projectsAdded(const QStringList &)),
               this, SLOT(addProjects(const QStringList &)));
    disconnect(m_monitor, SIGNAL(projectsRemoved(const QStringList &)),
               this, SLOT(removeProjects(const QStringList &)));
    disconnect(m_monitor, SIGNAL(accountUpdated(const QString &)),
               this, SLOT(updateAccount(const QString &)));
  }
  
  m_monitor = monitor;
  
  if(NULL != m_monitor)
  {
    connect(m_monitor, SIGNAL(projectsAdded(const QStringList &)),
            this, SLOT(addProjects(const QStringList &)));
    connect(m_monitor, SIGNAL(projectsRemoved(const QStringList &)),
            this, SLOT(removeProjects(const QStringList &)));
    connect(m_monitor, SIGNAL(accountUpdated(const QString &)),
            this, SLOT(updateAccount(const QString &)));
    
    const KBSBOINCClientState *state = monitor->state();
    if(NULL == state) return;
    
    addProjects(state->project.keys());
  }
}

void KBSWebToolBar::clear()
{
  removeProjects(m_projects.keys());
}

void KBSWebToolBar::addProjects(const QStringList &projects)
{
  if(NULL == m_toolbar) return;
  
  const KBSBOINCClientState *state = m_monitor->state();
  if(NULL == state) return;
  
  for(QStringList::const_iterator project = projects.begin();
      project != projects.end(); ++project)
  {
    const KBSBOINCAccount *account = m_monitor->account(*project);
    if(NULL == account || account->gui_urls.gui_url.isEmpty()) continue;
    
    if(m_projects.contains(*project)) continue;
    
    const int id = generateID();
    m_projects.insert(*project, id);
    
    KBSWebMenu *menu = new KBSWebMenu(*project, this);
    m_menus.insert(id, menu);
    
    QStringList names = m_projects.keys();
    names.sort();
    
    const int pos = names.findIndex(*project);
    const QString name = state->project[*project].project_name;
    
    m_toolbar->insertButton("bookmark_folder", id, menu, true, name, pos);
  }
}

void KBSWebToolBar::removeProjects(const QStringList &projects)
{
  if(NULL == m_toolbar) return;
  
  for(QStringList::const_iterator project = projects.begin();
      project != projects.end(); ++project)
  {
    if(!m_projects.contains(*project)) continue;
    const int id = m_projects[*project];
    
    KBSWebMenu *menu = m_menus.find(id);
    if(NULL == menu) continue;
    
    m_projects.remove(*project);
    m_menus.remove(id);
    
    m_toolbar->removeItemDelayed(id);
  }
  
  m_toolbar->update();
}

void KBSWebToolBar::updateAccount(const QString &project)
{
  if(NULL == m_toolbar || NULL == m_monitor) return;
  
  const KBSBOINCAccount *account = m_monitor->account(project);
  
  if(NULL == account || account->gui_urls.gui_url.isEmpty())
    removeProjects(project);
  else if(!m_projects.contains(project))
    addProjects(project);
  else {
    const int id = m_projects[project];
    KBSWebMenu *menu = m_menus.find(id);
    if(NULL != menu) menu->update();
  }
}

int KBSWebToolBar::generateID() const
{
  for(int id = 0; ; ++id)
    if(!m_menus.find(id)) return id;
}

#include "kbswebtoolbar.moc"
