/****************************************************************************
** Form interface generated from reading ui file './kbsprojectwizardcontent.ui'
**
** Created: Mon Feb 6 18:28:47 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSPROJECTWIZARDCONTENT_H
#define KBSPROJECTWIZARDCONTENT_H

#include <qvariant.h>
#include <qwidget.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QWidgetStack;
class KLineEdit;
class QButtonGroup;
class QRadioButton;
class KPasswordEdit;
class QTextBrowser;
class QFrame;
class KPushButton;

class KBSProjectWizardContent : public QWidget
{
    Q_OBJECT

public:
    KBSProjectWizardContent( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~KBSProjectWizardContent();

    QLabel* sidebar;
    QWidgetStack* pages;
    QWidget* welcome_page;
    QLabel* welcome_text;
    QWidget* project_url_page;
    QLabel* project_url_text;
    QLabel* project_url_label;
    KLineEdit* project_url;
    QWidget* connect_host_page;
    QLabel* connect_host_text;
    QLabel* connect_host_movie;
    QWidget* connect_network_page;
    QLabel* connect_network_text;
    QLabel* connect_network_movie;
    QWidget* account_page;
    QLabel* account_text;
    QButtonGroup* account_group;
    QRadioButton* create_account;
    QRadioButton* use_account;
    QLabel* login_text;
    QLabel* password_label;
    QLabel* confirm_password_label;
    QLabel* username_label;
    KLineEdit* username;
    KPasswordEdit* password;
    KPasswordEdit* confirm_password;
    QLabel* remark;
    QWidget* account_key_page;
    QLabel* account_key_text;
    QLabel* account_key_label;
    KLineEdit* account_key;
    QWidget* success_page;
    QLabel* success_text;
    QWidget* connect_error_page;
    QLabel* connect_error_text;
    QWidget* network_error_page;
    QLabel* network_error_text;
    QWidget* auth_error_page;
    QLabel* auth_error_text;
    QWidget* duplicate_error_page;
    QLabel* duplicate_error_text;
    QWidget* generic_error_page;
    QLabel* generic_error_text;
    QTextBrowser* error_messages;
    QFrame* line;
    KPushButton* help;
    KPushButton* previous;
    KPushButton* next;
    KPushButton* finish;

protected:
    QVBoxLayout* KBSProjectWizardContentLayout;
    QSpacerItem* spacer12;
    QHBoxLayout* layout_content;
    QVBoxLayout* layout_sidebar;
    QSpacerItem* sidebar_spacer;
    QVBoxLayout* welcome_pageLayout;
    QSpacerItem* welcome_spacer;
    QVBoxLayout* project_url_pageLayout;
    QSpacerItem* project_url_spacer;
    QHBoxLayout* project_url_layout;
    QVBoxLayout* connect_host_pageLayout;
    QSpacerItem* connect_host_spacer_hi;
    QSpacerItem* connect_host_spacer_lo;
    QVBoxLayout* connect_network_pageLayout;
    QSpacerItem* connect_network_spacer_hi;
    QSpacerItem* connect_network_spacer_lo;
    QVBoxLayout* account_pageLayout;
    QHBoxLayout* account_groupLayout;
    QGridLayout* login_layout;
    QVBoxLayout* account_key_pageLayout;
    QSpacerItem* account_key_spacer;
    QHBoxLayout* account_key_layout;
    QVBoxLayout* success_pageLayout;
    QSpacerItem* success_spacer;
    QVBoxLayout* connect_error_pageLayout;
    QSpacerItem* connect_error_spacer;
    QVBoxLayout* network_error_pageLayout;
    QSpacerItem* network_error_spacer;
    QVBoxLayout* auth_error_pageLayout;
    QSpacerItem* auth_error_spacer;
    QVBoxLayout* duplicate_error_pageLayout;
    QSpacerItem* duplicate_error_spacer;
    QVBoxLayout* generic_error_pageLayout;
    QHBoxLayout* layout_buttons;
    QSpacerItem* spacer_buttons;

protected slots:
    virtual void languageChange();

};

#endif // KBSPROJECTWIZARDCONTENT_H
