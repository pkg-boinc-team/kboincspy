/***************************************************************************
 *   Copyright (C) 2005 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSWEBTOOLBAR_H
#define KBSWEBTOOLBAR_H

#include <qintdict.h>
#include <qmap.h>
#include <qpopupmenu.h>

#include <kmainwindow.h>
#include <ktoolbar.h>

class KBSBOINCMonitor;
class KBSWebMenu;

class KBSWebToolBar : public QObject
{
  Q_OBJECT
  public:
    KBSWebToolBar(KMainWindow *parent, const char *name);
    
    virtual KToolBar *toolBar() const;
    
    virtual KBSBOINCMonitor *monitor() const;
    virtual void setMonitor(KBSBOINCMonitor *monitor);
  
  protected:
    virtual void clear();
  
  protected slots:
    virtual void addProjects(const QStringList &projects);
    virtual void removeProjects(const QStringList &projects);
    
    virtual void updateAccount(const QString &project);
  
  private:
    int generateID() const;
  
  protected:
    QMap<QString,int> m_projects;
    QIntDict<KBSWebMenu> m_menus;
  
  private:
    KToolBar *m_toolbar;
    KBSBOINCMonitor *m_monitor;
};

#endif
