#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsproxydialog.ui'
**
** Created: Mon Feb 6 18:28:49 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsproxydialog.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qcheckbox.h>
#include <qframe.h>
#include <klineedit.h>
#include <qgroupbox.h>
#include <kpassdlg.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSProxyDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
KBSProxyDialog::KBSProxyDialog( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "KBSProxyDialog" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMaximumSize( QSize( 32767, 32767 ) );
    KBSProxyDialogLayout = new QVBoxLayout( this, 11, 6, "KBSProxyDialogLayout"); 

    help = new QLabel( this, "help" );
    help->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    KBSProxyDialogLayout->addWidget( help );
    spacer_middle = new QSpacerItem( 16, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
    KBSProxyDialogLayout->addItem( spacer_middle );

    tab_proxy = new QTabWidget( this, "tab_proxy" );
    tab_proxy->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, tab_proxy->sizePolicy().hasHeightForWidth() ) );

    tab = new QWidget( tab_proxy, "tab" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tabLayout"); 

    socks = new QCheckBox( tab, "socks" );
    socks->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, socks->sizePolicy().hasHeightForWidth() ) );
    socks->setChecked( TRUE );
    tabLayout->addWidget( socks );

    frame_socks_server = new QFrame( tab, "frame_socks_server" );
    frame_socks_server->setFrameShape( QFrame::NoFrame );
    frame_socks_server->setFrameShadow( QFrame::Plain );
    frame_socks_serverLayout = new QHBoxLayout( frame_socks_server, 11, 6, "frame_socks_serverLayout"); 

    label_socks_server = new QLabel( frame_socks_server, "label_socks_server" );
    label_socks_server->setEnabled( TRUE );
    frame_socks_serverLayout->addWidget( label_socks_server );

    socks_server = new KLineEdit( frame_socks_server, "socks_server" );
    socks_server->setEnabled( TRUE );
    socks_server->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 3, 0, socks_server->sizePolicy().hasHeightForWidth() ) );
    frame_socks_serverLayout->addWidget( socks_server );

    label_socks_port = new QLabel( frame_socks_server, "label_socks_port" );
    label_socks_port->setEnabled( TRUE );
    frame_socks_serverLayout->addWidget( label_socks_port );

    socks_port = new KLineEdit( frame_socks_server, "socks_port" );
    socks_port->setEnabled( TRUE );
    socks_port->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 1, 0, socks_port->sizePolicy().hasHeightForWidth() ) );
    frame_socks_serverLayout->addWidget( socks_port );
    tabLayout->addWidget( frame_socks_server );

    socks_auth = new QGroupBox( tab, "socks_auth" );
    socks_auth->setEnabled( TRUE );
    socks_auth->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 1, socks_auth->sizePolicy().hasHeightForWidth() ) );
    socks_auth->setCheckable( TRUE );
    socks_auth->setColumnLayout(0, Qt::Vertical );
    socks_auth->layout()->setSpacing( 6 );
    socks_auth->layout()->setMargin( 11 );
    socks_authLayout = new QVBoxLayout( socks_auth->layout() );
    socks_authLayout->setAlignment( Qt::AlignTop );

    frame_socks_auth = new QFrame( socks_auth, "frame_socks_auth" );
    frame_socks_auth->setFrameShape( QFrame::NoFrame );
    frame_socks_auth->setFrameShadow( QFrame::Plain );
    frame_socks_authLayout = new QHBoxLayout( frame_socks_auth, 11, 6, "frame_socks_authLayout"); 

    label_socks_user = new QLabel( frame_socks_auth, "label_socks_user" );
    label_socks_user->setEnabled( TRUE );
    frame_socks_authLayout->addWidget( label_socks_user );

    socks_user = new KLineEdit( frame_socks_auth, "socks_user" );
    socks_user->setEnabled( TRUE );
    frame_socks_authLayout->addWidget( socks_user );

    label_socks_password = new QLabel( frame_socks_auth, "label_socks_password" );
    label_socks_password->setEnabled( TRUE );
    frame_socks_authLayout->addWidget( label_socks_password );

    socks_password = new KPasswordEdit( frame_socks_auth, "socks_password" );
    socks_password->setEnabled( TRUE );
    frame_socks_authLayout->addWidget( socks_password );
    socks_authLayout->addWidget( frame_socks_auth );
    tabLayout->addWidget( socks_auth );
    tab_proxy->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( tab_proxy, "tab_2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 11, 6, "tabLayout_2"); 

    http = new QCheckBox( tab_2, "http" );
    http->setChecked( TRUE );
    tabLayout_2->addWidget( http );

    frame_http_server = new QFrame( tab_2, "frame_http_server" );
    frame_http_server->setFrameShape( QFrame::NoFrame );
    frame_http_server->setFrameShadow( QFrame::Plain );
    frame_http_serverLayout = new QHBoxLayout( frame_http_server, 11, 6, "frame_http_serverLayout"); 

    label_http_server = new QLabel( frame_http_server, "label_http_server" );
    label_http_server->setEnabled( TRUE );
    frame_http_serverLayout->addWidget( label_http_server );

    http_server = new KLineEdit( frame_http_server, "http_server" );
    http_server->setEnabled( TRUE );
    http_server->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 3, 0, http_server->sizePolicy().hasHeightForWidth() ) );
    frame_http_serverLayout->addWidget( http_server );

    label_http_port = new QLabel( frame_http_server, "label_http_port" );
    label_http_port->setEnabled( TRUE );
    frame_http_serverLayout->addWidget( label_http_port );

    http_port = new KLineEdit( frame_http_server, "http_port" );
    http_port->setEnabled( TRUE );
    http_port->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 1, 0, http_port->sizePolicy().hasHeightForWidth() ) );
    http_port->setMaxLength( 5 );
    frame_http_serverLayout->addWidget( http_port );
    tabLayout_2->addWidget( frame_http_server );

    http_auth = new QGroupBox( tab_2, "http_auth" );
    http_auth->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 1, http_auth->sizePolicy().hasHeightForWidth() ) );
    http_auth->setCheckable( TRUE );
    http_auth->setColumnLayout(0, Qt::Vertical );
    http_auth->layout()->setSpacing( 6 );
    http_auth->layout()->setMargin( 11 );
    http_authLayout = new QVBoxLayout( http_auth->layout() );
    http_authLayout->setAlignment( Qt::AlignTop );

    frame_http_auth = new QFrame( http_auth, "frame_http_auth" );
    frame_http_auth->setFrameShape( QFrame::NoFrame );
    frame_http_auth->setFrameShadow( QFrame::Plain );
    frame_http_authLayout = new QHBoxLayout( frame_http_auth, 11, 6, "frame_http_authLayout"); 

    label_http_user = new QLabel( frame_http_auth, "label_http_user" );
    label_http_user->setEnabled( TRUE );
    frame_http_authLayout->addWidget( label_http_user );

    http_user = new KLineEdit( frame_http_auth, "http_user" );
    http_user->setEnabled( TRUE );
    frame_http_authLayout->addWidget( http_user );

    label_http_password = new QLabel( frame_http_auth, "label_http_password" );
    label_http_password->setEnabled( TRUE );
    frame_http_authLayout->addWidget( label_http_password );

    http_password = new KPasswordEdit( frame_http_auth, "http_password" );
    http_password->setEnabled( TRUE );
    frame_http_authLayout->addWidget( http_password );
    http_authLayout->addWidget( frame_http_auth );
    tabLayout_2->addWidget( http_auth );
    tab_proxy->insertTab( tab_2, QString::fromLatin1("") );
    KBSProxyDialogLayout->addWidget( tab_proxy );

    button_layout = new QHBoxLayout( 0, 0, 6, "button_layout"); 
    spacer_button = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    button_layout->addItem( spacer_button );

    button_ok = new QPushButton( this, "button_ok" );
    button_ok->setAutoDefault( TRUE );
    button_ok->setDefault( TRUE );
    button_layout->addWidget( button_ok );

    button_cancel = new QPushButton( this, "button_cancel" );
    button_cancel->setAutoDefault( TRUE );
    button_layout->addWidget( button_cancel );
    KBSProxyDialogLayout->addLayout( button_layout );
    languageChange();
    resize( QSize(403, 366).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( button_ok, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( button_cancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( socks, SIGNAL( toggled(bool) ), frame_socks_server, SLOT( setEnabled(bool) ) );
    connect( socks, SIGNAL( toggled(bool) ), socks_auth, SLOT( setEnabled(bool) ) );
    connect( http, SIGNAL( toggled(bool) ), frame_http_server, SLOT( setEnabled(bool) ) );
    connect( http, SIGNAL( toggled(bool) ), http_auth, SLOT( setEnabled(bool) ) );
    init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSProxyDialog::~KBSProxyDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSProxyDialog::languageChange()
{
    setCaption( tr2i18n( "Proxy Settings" ) );
    help->setText( tr2i18n( "Some organizations use an \"HTTP proxy\" or a \"SOCKS proxy\" (or both) for increased security. If you need to use a proxy, fill in the information below. If you need help, ask your System Administrator or Internet Service Provider." ) );
    socks->setText( tr2i18n( "&Connect via a SOCKS proxy server" ) );
    socks->setAccel( QKeySequence( tr2i18n( "Alt+C" ) ) );
    label_socks_server->setText( tr2i18n( "Server:" ) );
    label_socks_port->setText( tr2i18n( "Port:" ) );
    socks_auth->setTitle( tr2i18n( "Authentication" ) );
    label_socks_user->setText( tr2i18n( "User:" ) );
    label_socks_password->setText( tr2i18n( "Password:" ) );
    tab_proxy->changeTab( tab, tr2i18n( "&SOCKS Proxy" ) );
    http->setText( tr2i18n( "&Connect via an HTTP proxy server" ) );
    http->setAccel( QKeySequence( tr2i18n( "Alt+C" ) ) );
    label_http_server->setText( tr2i18n( "http://" ) );
    label_http_port->setText( tr2i18n( "Port:" ) );
    http_auth->setTitle( tr2i18n( "Authentication" ) );
    label_http_user->setText( tr2i18n( "User:" ) );
    label_http_password->setText( tr2i18n( "Password:" ) );
    tab_proxy->changeTab( tab_2, tr2i18n( "&HTTP Proxy" ) );
    button_ok->setText( tr2i18n( "&OK" ) );
    button_ok->setAccel( QKeySequence( QString::null ) );
    button_cancel->setText( tr2i18n( "&Cancel" ) );
    button_cancel->setAccel( QKeySequence( QString::null ) );
}

void KBSProxyDialog::init()
{
}

#include "kbsproxydialog.moc"
