/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KBSPANELVIEW_H
#define KBSPANELVIEW_H

#include <qptrdict.h>
#include <qptrlist.h>
#include <qsignalmapper.h>
#include <qstringlist.h>
#include <qtoolbutton.h>
#include <qvaluelist.h>
#include <qwidgetstack.h>

#include <kaction.h>
#include <kconfig.h>
#include <ktabwidget.h>
#include <kxmlguiclient.h> 

#include <kbstree.h>

class KBSPanel;
class KBSPanelNode;

class KBSPanelView : public QWidget
{
  Q_OBJECT
  public:
    KBSPanelView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);

    virtual KBSPanel *currentPanel() = 0;
    
    virtual bool isFrontView() const;

    virtual void readConfig(KConfig *config) = 0;
    virtual void writeConfig(KConfig *config) = 0;
  
  public slots:
    virtual void setFrontView(bool set);
    
    virtual void openPanel(KBSTreeNode *node);
    virtual void closePanel(KBSTreeNode *node);
  
  signals:
    void currentPanelChanged();
  
  protected:
    virtual void showPanel(KBSPanelNode *panel) = 0;
    virtual void hidePanel(KBSPanelNode *panel) = 0;
    
    virtual void showAllPanels() = 0;
    virtual void hideAllPanels() = 0;
  
  private:
    void addConnections(KBSTreeNode *node);
    void removeConnections(KBSTreeNode *node);
    
  private slots:
    void slotChildInserted(KBSTreeNode *child);
    void slotChildRemoved(KBSTreeNode *child);
    
  protected:
    KBSTreeNode *m_root;
    bool m_front;
};


class KBSSimplePanelView : public KBSPanelView, KXMLGUIClient
{
  Q_OBJECT
  public:
    KBSSimplePanelView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);
    
    virtual KBSPanel *currentPanel();
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  public slots:
    virtual void openPanel(KBSTreeNode *node);
    virtual void closePanel(KBSTreeNode *node);
  
  protected:
    virtual void showPanel(KBSPanelNode *panel);
    virtual void hidePanel(KBSPanelNode *panel);
  
    virtual void showAllPanels();
    virtual void hideAllPanels();
  
  protected:
    QWidget *m_view;
    KBSNamedPath m_opened;
    KBSPanel *m_visible;
};


class KBSTabbedPanelView : public KBSPanelView, KXMLGUIClient
{
  Q_OBJECT
  public:
    KBSTabbedPanelView(KBSTreeNode *root, QWidget *parent=0, const char *name=0);
    
    virtual KBSPanel *currentPanel();
    
    virtual void readConfig(KConfig *config);
    virtual void writeConfig(KConfig *config);
  
  public slots:
    virtual void openPanel(KBSTreeNode *node);
    virtual void closePanel(KBSTreeNode *node);
    
    virtual void closeThisPanel();
    virtual void closeAllPanels();
    virtual void closeOtherPanels();
    virtual void closeCurrentPanel();
  
  protected:
    virtual void showPanel(KBSPanelNode *panel);
    virtual void hidePanel(KBSPanelNode *panel);
  
    virtual void showAllPanels();
    virtual void hideAllPanels();
  
  private:
    unsigned orderedTabIndex(const KBSPanelNode *panel);
  
  private slots:
    void slotCloseRequest(QWidget *tab);
    void slotContextMenu(QWidget *tab, const QPoint &);
  
  protected:
    KTabWidget *m_view;
    QToolButton *m_close;
    QValueList<KBSNamedPath> m_opened;
    QPtrDict<KBSPanel> m_visible;
    
    KBSPanel *m_active;
    QSignalMapper *m_mapper;
    QPtrList<KAction> m_actions;
};

#endif
