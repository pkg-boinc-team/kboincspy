/****************************************************************************
** Form interface generated from reading ui file './kbslocationdialog.ui'
**
** Created: Mon Feb 6 18:28:47 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef KBSLOCATIONDIALOG_H
#define KBSLOCATIONDIALOG_H

#include <qvariant.h>
#include <qdialog.h>
#include <klocale.h>
#include <kurl.h>
#include <kbsboincmonitor.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class KURLRequester;
class KLineEdit;
class KComboBox;
class KPushButton;
class KBSLocation;

class KBSLocationDialog : public QDialog
{
    Q_OBJECT

public:
    KBSLocationDialog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~KBSLocationDialog();

    QLabel* label_url;
    KURLRequester* url;
    QLabel* label_host;
    KLineEdit* host;
    QLabel* label_port;
    KComboBox* port;
    KPushButton* ok;
    KPushButton* cancel;

    bool changed;

    virtual QString defaultHost();
    virtual KBSLocation location();

public slots:
    virtual void updateURL( const QString & string );
    virtual void updateHost( const QString & string );

protected:
    QVBoxLayout* KBSLocationDialogLayout;
    QSpacerItem* spacer;
    QHBoxLayout* layout_url;
    QHBoxLayout* layout_host;
    QHBoxLayout* layout_buttons;
    QSpacerItem* spacer_buttons;

protected slots:
    virtual void languageChange();

private:
    void init();

};

#endif // KBSLOCATIONDIALOG_H
