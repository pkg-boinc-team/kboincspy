#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file './kbsconfigpage.ui'
**
** Created: Mon Feb 6 18:28:47 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "kbsconfigpage.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qcheckbox.h>
#include <kurlrequester.h>
#include <klineedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a KBSConfigPage as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
KBSConfigPage::KBSConfigPage( QWidget* parent, const char* name, WFlags fl )
    : QWidget( parent, name, fl )
{
    if ( !name )
	setName( "KBSConfigPage" );
    KBSConfigPageLayout = new QVBoxLayout( this, 11, 6, "KBSConfigPageLayout"); 

    polling_layout = new QHBoxLayout( 0, 0, 6, "polling_layout"); 

    group_polling = new QGroupBox( this, "group_polling" );
    group_polling->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, group_polling->sizePolicy().hasHeightForWidth() ) );
    group_polling->setColumnLayout(0, Qt::Vertical );
    group_polling->layout()->setSpacing( 6 );
    group_polling->layout()->setMargin( 11 );
    group_pollingLayout = new QGridLayout( group_polling->layout() );
    group_pollingLayout->setAlignment( Qt::AlignTop );

    label_fam = new QLabel( group_polling, "label_fam" );
    label_fam->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_fam->sizePolicy().hasHeightForWidth() ) );

    group_pollingLayout->addWidget( label_fam, 0, 0 );

    label_rpc = new QLabel( group_polling, "label_rpc" );
    label_rpc->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, label_rpc->sizePolicy().hasHeightForWidth() ) );

    group_pollingLayout->addWidget( label_rpc, 1, 0 );

    kcfg_fam = new QSpinBox( group_polling, "kcfg_fam" );
    kcfg_fam->setButtonSymbols( QSpinBox::UpDownArrows );
    kcfg_fam->setMaxValue( 999 );
    kcfg_fam->setMinValue( 1 );
    kcfg_fam->setValue( 30 );

    group_pollingLayout->addWidget( kcfg_fam, 0, 1 );

    kcfg_rpc = new QSpinBox( group_polling, "kcfg_rpc" );
    kcfg_rpc->setButtonSymbols( QSpinBox::UpDownArrows );
    kcfg_rpc->setMaxValue( 9900 );
    kcfg_rpc->setMinValue( 100 );
    kcfg_rpc->setLineStep( 100 );
    kcfg_rpc->setValue( 2000 );

    group_pollingLayout->addWidget( kcfg_rpc, 1, 1 );
    polling_layout->addWidget( group_polling );

    group_tray = new QGroupBox( this, "group_tray" );
    group_tray->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, group_tray->sizePolicy().hasHeightForWidth() ) );
    group_tray->setColumnLayout(0, Qt::Vertical );
    group_tray->layout()->setSpacing( 6 );
    group_tray->layout()->setMargin( 11 );
    group_trayLayout = new QVBoxLayout( group_tray->layout() );
    group_trayLayout->setAlignment( Qt::AlignTop );

    kcfg_use_tray = new QCheckBox( group_tray, "kcfg_use_tray" );
    group_trayLayout->addWidget( kcfg_use_tray );

    kcfg_startup_tray = new QCheckBox( group_tray, "kcfg_startup_tray" );
    kcfg_startup_tray->setEnabled( FALSE );
    group_trayLayout->addWidget( kcfg_startup_tray );
    polling_layout->addWidget( group_tray );
    KBSConfigPageLayout->addLayout( polling_layout );

    log_layout = new QHBoxLayout( 0, 0, 6, "log_layout"); 

    log_group = new QGroupBox( this, "log_group" );
    log_group->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 1, 0, log_group->sizePolicy().hasHeightForWidth() ) );
    log_group->setColumnLayout(0, Qt::Vertical );
    log_group->layout()->setSpacing( 1 );
    log_group->layout()->setMargin( 11 );
    log_groupLayout = new QVBoxLayout( log_group->layout() );
    log_groupLayout->setAlignment( Qt::AlignTop );

    location_layout = new QHBoxLayout( 0, 0, 6, "location_layout"); 

    location_label = new QLabel( log_group, "location_label" );
    location_layout->addWidget( location_label );

    kcfg_location = new KURLRequester( log_group, "kcfg_location" );
    kcfg_location->setMode( 10 );
    location_layout->addWidget( kcfg_location );
    log_groupLayout->addLayout( location_layout );

    kcfg_write = new QCheckBox( log_group, "kcfg_write" );
    log_groupLayout->addWidget( kcfg_write );
    log_layout->addWidget( log_group );

    client_group = new QGroupBox( this, "client_group" );
    client_group->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, client_group->sizePolicy().hasHeightForWidth() ) );
    client_group->setColumnLayout(0, Qt::Vertical );
    client_group->layout()->setSpacing( 6 );
    client_group->layout()->setMargin( 11 );
    client_groupLayout = new QVBoxLayout( client_group->layout() );
    client_groupLayout->setAlignment( Qt::AlignTop );

    client_layout = new QHBoxLayout( 0, 0, 6, "client_layout"); 

    client_label = new QLabel( client_group, "client_label" );
    client_layout->addWidget( client_label );

    kcfg_client = new KLineEdit( client_group, "kcfg_client" );
    client_layout->addWidget( kcfg_client );
    client_groupLayout->addLayout( client_layout );

    client_options_layout = new QHBoxLayout( 0, 0, 6, "client_options_layout"); 

    kcfg_client_exec = new QCheckBox( client_group, "kcfg_client_exec" );
    kcfg_client_exec->setChecked( TRUE );
    client_options_layout->addWidget( kcfg_client_exec );

    kcfg_client_kill = new QCheckBox( client_group, "kcfg_client_kill" );
    kcfg_client_kill->setChecked( TRUE );
    client_options_layout->addWidget( kcfg_client_kill );
    client_groupLayout->addLayout( client_options_layout );
    log_layout->addWidget( client_group );
    KBSConfigPageLayout->addLayout( log_layout );
    spacer = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    KBSConfigPageLayout->addItem( spacer );
    languageChange();
    resize( QSize(452, 211).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( kcfg_use_tray, SIGNAL( toggled(bool) ), kcfg_startup_tray, SLOT( setEnabled(bool) ) );
    connect( kcfg_client_exec, SIGNAL( toggled(bool) ), kcfg_client_kill, SLOT( setEnabled(bool) ) );

    // tab order
    setTabOrder( kcfg_fam, kcfg_rpc );
    setTabOrder( kcfg_rpc, kcfg_use_tray );
    setTabOrder( kcfg_use_tray, kcfg_startup_tray );
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBSConfigPage::~KBSConfigPage()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void KBSConfigPage::languageChange()
{
    setCaption( tr2i18n( "General Preferences" ) );
    setIconText( tr2i18n( "General" ) );
    group_polling->setTitle( tr2i18n( "Polling" ) );
    label_fam->setText( tr2i18n( "File poll interval:" ) );
    label_rpc->setText( tr2i18n( "RPC check interval:" ) );
    kcfg_fam->setSuffix( tr2i18n( " seconds" ) );
    kcfg_rpc->setSuffix( tr2i18n( " msecs" ) );
    group_tray->setTitle( tr2i18n( "System Tray" ) );
    kcfg_use_tray->setText( tr2i18n( "&Enable sytem tray icon" ) );
    kcfg_use_tray->setAccel( QKeySequence( tr2i18n( "Alt+E" ) ) );
    kcfg_startup_tray->setText( tr2i18n( "&Start up in tray" ) );
    kcfg_startup_tray->setAccel( QKeySequence( tr2i18n( "Alt+S" ) ) );
    log_group->setTitle( tr2i18n( "Log" ) );
    location_label->setText( tr2i18n( "Location:" ) );
    kcfg_write->setText( tr2i18n( "&Write to log" ) );
    kcfg_write->setAccel( QKeySequence( tr2i18n( "Alt+W" ) ) );
    client_group->setTitle( tr2i18n( "Local Client" ) );
    client_label->setText( tr2i18n( "Client name:" ) );
    kcfg_client->setText( tr2i18n( "boinc" ) );
    kcfg_client_exec->setText( tr2i18n( "&Run on startup" ) );
    kcfg_client_exec->setAccel( QKeySequence( tr2i18n( "Alt+R" ) ) );
    kcfg_client_kill->setText( tr2i18n( "&Kill on exit" ) );
    kcfg_client_kill->setAccel( QKeySequence( tr2i18n( "Alt+K" ) ) );
}

#include "kbsconfigpage.moc"
