/***************************************************************************
 *   Copyright (C) 2004 by Roberto Virga                                   *
 *   rvirga@users.sourceforge.net                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlayout.h>

#include <kicontheme.h>

#include <kboincspy.h>
#include <kbsdocument.h>
#include <kbspanelnode.h>
#include <kbspanelview.h>
#include <kbstreeview.h>

#include "kbsview.h"

KBSView::KBSView(KBSTreeNode *root, QWidget *parent, const char *name)
       : QWidget(parent, name)
{
  (new QVBoxLayout(this))->setAutoAdd(true);
  
  m_split = new QSplitter(this);
  
  m_lhs = new QWidgetStack(m_split);
  m_lhs->addWidget(new KBSNestedTreeView(root, m_lhs), NestedView);
  m_lhs->addWidget(new KBSFlattenedTreeView(root, m_lhs), FlattenedView);
  m_lhs->raiseWidget(NestedView);
  
  m_rhs = new QWidgetStack(m_split);
  m_rhs->addWidget(new KBSSimplePanelView(root, m_rhs), SimpleView);
  m_rhs->addWidget(new KBSTabbedPanelView(root, m_rhs), TabbedView);
  panelView(SimpleView)->setFrontView(true);
  m_rhs->raiseWidget(SimpleView);
  
  for(unsigned treeView = NestedView; treeView <= FlattenedView; ++treeView)
    for(unsigned panelView = SimpleView; panelView <= TabbedView; ++panelView)
      connect(m_lhs->widget(treeView), SIGNAL(activated(KBSTreeNode *)),
              m_rhs->widget(panelView), SLOT(openPanel(KBSTreeNode *)));
}

KBSTreeView *KBSView::treeView(KBSView::TreeView view) const
{
  return static_cast<KBSTreeView*>(m_lhs->widget(view));
}

KBSTreeView *KBSView::currentTreeView() const
{
  return static_cast<KBSTreeView*>(m_lhs->visibleWidget());
}

void KBSView::setCurrentTreeView(KBSView::TreeView view)
{
  m_lhs->raiseWidget(view);
}

void KBSView::setCurrentTreeView(KBSTreeView *view)
{
  m_lhs->raiseWidget(view);
}

KBSPanelView *KBSView::panelView(KBSView::PanelView view) const
{
  return static_cast<KBSPanelView*>(m_rhs->widget(view));
}

KBSPanelView *KBSView::currentPanelView() const
{
  return static_cast<KBSPanelView*>(m_rhs->visibleWidget());
}

void KBSView::setCurrentPanelView(KBSView::PanelView view)
{
  setCurrentPanelView(panelView(view));
}

void KBSView::setCurrentPanelView(KBSPanelView *view)
{
  if(currentPanelView() == view) return;
  
  currentPanelView()->setFrontView(false);
  view->setFrontView(true);
  
  m_rhs->raiseWidget(view);
}

void KBSView::readConfig(KConfig *config)
{
  config->setGroup("KBSView");
  
  QValueList<int> sizes;
  sizes = config->readIntListEntry("Widget sizes");
  if(sizes.count() > 0) m_split->setSizes(sizes);
  else m_split->setSizes(sizes << 160 << 260);
  
  for(unsigned panelView = SimpleView; panelView <= TabbedView; ++panelView)
    static_cast<KBSPanelView*>(m_rhs->widget(panelView))->readConfig(config);
}

void KBSView::writeConfig(KConfig *config)
{
  config->setGroup("KBSView");
  
  config->writeEntry("Widget sizes", m_split->sizes());
  
  for(unsigned panelView = SimpleView; panelView <= TabbedView; ++panelView)
    this->panelView(PanelView(panelView))->writeConfig(config);
}

#include "kbsview.moc"
