/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

void KBSLocationDialog::init()
{
  changed = false;
  host->setText(defaultHost());
}

QString KBSLocationDialog::defaultHost()
{
  const KURL fileURL(url->url());
  return  (fileURL.isValid() && !fileURL.host().isEmpty()) ? fileURL.host() : "localhost";
}

void KBSLocationDialog::updateURL(const QString &string)
{
  const QString defaultHost = this->defaultHost();
  if(!changed && host->text() != defaultHost) host->setText(defaultHost);
  
  const KURL fileURL(string);
  ok->setEnabled(!fileURL.isEmpty() && fileURL.isValid() && fileURL.fileName() == "client_state.xml");
}

void KBSLocationDialog::updateHost(const QString &string)
{
  changed = string.stripWhiteSpace() != defaultHost();
}

KBSLocation KBSLocationDialog::location()
{
  KBSLocation out;
  
  KURL fileURL(url->url());
  out.url = KURL(fileURL, ".");
  out.url.adjustPath(+1);
  
  out.host = host->text().stripWhiteSpace();
  
  out.port = (port->currentItem() > 0) ? port->currentText().toUInt(0, 10) : 0;
  
  return out;
}



