<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.1.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!ENTITY kboincspy "<application>KBoincSpy</application>">
  <!ENTITY ksetispy "<application>KSetiSpy</application>">
  <!ENTITY kappname "&kboincspy;"><!-- Do *not* replace kappname-->
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE"><!-- change language only here -->
  
  
  <!-- Do not define any other entities; instead, use the entities
       from kde-genent.entities and $LANG/user.entities. -->
]>

<!-- The language must NOT be changed here. -->

<book lang="&language;">

<!-- This header contains all of the meta-information for the document such
as Authors, publish date, the abstract, and Keywords -->

<bookinfo>
<title>The &kappname; Handbook</title>

<authorgroup>
<author>
<firstname></firstname>
<othername></othername>
<surname>Roberto Virga</surname>
<affiliation>
<address><email>rvirga@users.sourceforge.net</email></address>
</affiliation>
</author>
</authorgroup>

<!-- TRANS:ROLES_OF_TRANSLATORS -->

<copyright>
<year>2005</year>
<holder>Roberto Virga</holder>
</copyright>
<!-- Translators: put here the copyright notice of the translation -->
<!-- Put here the FDL notice.  Read the explanation in fdl-notice.docbook
     and in the FDL itself on how to use it. -->
<legalnotice>&FDLNotice;</legalnotice>

<!-- Date and version information of the documentation
Don't forget to include this last date and this last revision number, we
need them for translation coordination !
Please respect the format of the date (YYYY-MM-DD) and of the version
(V.MM.LL), it could be used by automation scripts.
Do NOT change these in the translation. -->

<date>2005-03-28</date>
<releaseinfo>0.9.0</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para>
&kappname; is a KDE monitoring utility for the BOINC distributed client.
Designed to be the successor of &ksetispy;, it offers support for a virtually
unlimited number of BOINC projects through a plugin-based architecture.
</para>
</abstract>

<!-- This is a set of Keywords for indexing by search engines.
Please at least include KDE, the KDE package it is in, the name
 of your application, and a few relevant keywords. -->

<keywordset>
<keyword>KDE</keyword>
<keyword>KBoincSpy</keyword>
<keyword>monitoring</keyword>
<keyword>distributed computing</keyword>
</keywordset>

</bookinfo>

<chapter id="introduction">
<title>Introduction</title>
<sect1>
<title>Documentation</title>
<para>
The documentation for &kappname; isn't ready yet. For now, I recommend
you consult the <ulink url="http://ksetispy.sf.net/"> documentation for
&ksetispy;</ulink>, whose interface and options are, for the most part,
very similar.
</para>
<para>
In the following, I will briefly describe some features that are either
unique to &kappname; or present major changes with respect to &ksetispy;.
</para>

<sect2 id="calibration">
<title>Calibration</title>

<para>
&kappname; offers a self-learning calibration algorithm for the SETI@home
client, instead of a list of fixed calibration presets like &ksetispy; did.
This can be considered a major improvement, since now the calibration
values used are those which are a perfect fit for your computer, rather than
some preset that was computer on some other machine that may or may not
approximate yours. The downside of this change is that calibration will require
a short initial training period (no user interaction whatsoever is needed
for this training, however).
</para>
<para>
After monitoring the entire execution of a SETI@home workunit, &kappname;
will generate a set of calibration values for that execution. The
overall calibration values will be computed as the weighted average of
the sets of values obtained from each workunit, where each of these sets
has weight 1. The system will start with a set of null calibration values
(i.e. the reported and effective are the same) with weight 1.
</para>
<para>
You can use the Calibration panel to inspect the current calibration,
and optionally reset to its initial values.
</para>

</sect2>

<sect2 id="logs">
<title>Log Formats</title>
<para>
&kappname; adopts the BOINCLogX format for its logs. This format can be
divided in three parts. There's a project-independent log
(file <filename>boinc.csv</filename>), and two project-dependent ones, for
AstroPulse (file <filename>ap_boinc.csv</filename>) and for SETI@home
(file <filename>sah_boinc.csv</filename>). For SETI@home, &kappname;
offers additional formats from many popular SETI@home Classic add-ons.
</para>
<para>
These three parts can be set separately. The settings for the
project-independent part are in the General entry of the Preferences panel.
It's recommended that you set up the project-independent log, since it is used
in the Credits Calendar panel to display the credits you were awarded (or,
more appropriately, you should have been awarded, based on the amout of work
done) by the server.
</para>

</sect2>
</sect1>

</chapter>

<chapter id="credits">

<!-- Include credits for the programmers, documentation writers, and
contributors here. The license for your software should then be included below
the credits with a reference to the appropriate license file included in the KDE
distribution. -->

<title>Credits and License</title>

<para>
&kboincspy; 0.9
</para>
<para>
Program Copyright 2005 Roberto Virga <email>rvirga@users.sf.net</email>
</para>

<para>
Documentation Copyright 2004 Roberto Virga <email>rvirga@users.sf.net</email>
</para>

<!-- TRANS:CREDIT_FOR_TRANSLATORS -->

&underFDL;               <!-- FDL: do not remove -->

<!-- Determine which license your application is licensed under, 
     and delete all the remaining licenses below:

     (NOTE:  All documentation are licensed under the FDL, 
     regardless of what license the application uses) -->

&underGPL;        	 <!-- GPL License -->

</chapter>

</book>

<!--
Local Variables:
mode: sgml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab 
-->

